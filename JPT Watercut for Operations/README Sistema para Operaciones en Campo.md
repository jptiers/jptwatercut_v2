#INSTALACION DEL SISTEMA JPTWATERCUT EN CAMPO

El siguiente archvo explica de forma general como instalar el sistema JPT-WATERCUT en caso de no contar con la memoria Flasher de instalacion del mismo para la Beagle Bone.

Primero se debe tener en cuenta que el sistema depende de varias caracteristicas del software, en primeria instancia se abordan los requerimientos de sistema operativo:

* Un JPT-Hub el cual debe tener una Beagle Bone, con un sistema Debian Stretch intalado. 
* El sistema debian debe contar con las librerias de Qt5-Default, Qt5 Serial Port y  WmCtrl.
* El usuario por defecto, **debian**, debe registrarse dentro del sistema operativo al grupo **Root**.
* Eliminar la contrase?a de todos los usuarios o particularmente del usuario **debian** y del usuario **root**.

Para la instalacion del sistema ahora es necesario tener:

1.Una carpeta denominada **jpt** en la cual se encuentra el sistema **Jpt Watercut** y el sistema **Jpt Monitoring.** 	Dentro de la carpeta de operaciones del JPT-Watercut V2 existe un directorio denominado JPTWatercur for operations 	en este directorio se encuentra un archivo comprimido con todo lo necesario para instalar. 

2.Una memoria SD de nombre JPTSD.

---------------

Luego de tener los archivo y elementos necesarios se procede a realizar una copia de la carpeta **jpt** al directorio raiz donde esta instalado el **root** del sistema, la ruta por defecto tendria el nombre: 

**/jpt/**jptmonitoring and jptwatercut/

**NOTA:** A esta carpeta se le debe de conceder permisos de administrador. 

Dentro del directorio JPT, se encuentran dos directorios esl Jpt-Monitoring en el cual se encuentra el sistema de mismo nombre, es preciso hacer enfasis en que el sistema Jpt-Watercut depende plenamente del sistema de monitoreo para su funcionamiento, por este motivo primero se abordara de forma general como lograr que el sistema de monitoreo funcine. 

####Pasos para hacer funcionar JPT-Monitoring 

###Paso 1: Conocer el entorno: 

Dentro del directorio de JPT Monitoring se encuentran multiples elementos, entre ellos se resaltaran 4 : Un directorio de nombre **External**, un archivo ejecutable denominado **jptmonitoring**, un script en lenguaje Python denominado **jptmonitoring.py**, un script en lenguaje python denominado **rtcupdate,py**

En caso de que falte alguno de estos elementos se debe verificar el soporte del sistema o si se hizo un cambio en el mismo. 

####Paso 2: Intalacion.

Para el proceso de instalacion simplemente se debe de compilar los archivos jptmonitoring.py y rtcupdate.py , el resultado de la compilacion debe arrojar dos archivos de nombre omonimo los cuales seran de extension **.pyc**, es decir, archivo compilado de python. 

###Paso 3:Configurar la Base de datos. 
una vez con los archivos compilados se procede a ingresar en el directorio de Externel, dentro de este directoro existe una base de datos de nombre **jptdatabase.db**, se debe abrir la base de datos cualquier medio de visualizacion y configurar en la tabla **configuraciones** de la base de datos las caracteristicas de conexion de la red y tipos de datos de entrada. 


Esta base de datos configurada debe quedar localizada en la SD de nombre JPTSD anteriormente mencionada. 


###Paso 4: Sistema Funcional Listo. 
En este punto se puede correr el archivo ejecutable denominado **jptmonitoring** y el sistema comenzara a funcionar. 
por motivos de desarrollo es indispensable que el sistema funcione automaticamente cada que se inicia el sistema y se reinicie caa que el sistema se cierra. Para ello contamos un elemento denominado jptmonitoring.py el cual ya se menciono como un elemento necesario del sistema. 

####Paso 4.1: Auto inicio del sistema.
Para realizar el auto inicio del sistema se debe de crear un elemento de tipo autorun en linux, para ello debemos dirigirnos al directorio :

 /home/usuario_en nuestro_caso es_debian/.config/autostart/

Dentro de este directorio se crea un autostart el cual es un elemento de texto plano de la forma **nombre_comando.desktop**
A continuacion se da un ejemplo de un elemento de autostart: 

----------------------

[Desktop Entry]

Exec=/jpt/jptmonitoring/jptmonitoring.py

Icon=system-run

Terminal=false

TerminalOptions=

Type=Application

-------------------

**NOTA:**En caso de verificar si el sistema esta funcionano de manera correcta se puede cambiar la opcion **Terminal** del script a **True**, de esta manera se ense?ara una consola con cada uno de los procesos realizados por el sistema. 



















