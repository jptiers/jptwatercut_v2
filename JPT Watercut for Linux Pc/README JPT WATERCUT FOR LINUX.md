#JPT WATERCUT FOR LINUX 

El siguiente documento explica de forma simple como lograr que el Widget denominado JPT-Watercut funcione dentro de un computador o laptop con Linux instalado. Para obtener informacion mas completa sobre los procesos y funcionamiento del software se debe leer los comentarios dentro del codigo del sistema, los cuales explican las rutinas y elementos de cada funcion en el. 

En primera instancia es necesario tener en cuenta que el sistema presenta ciertas modificaciones respecto a la organizacion y arquitectura del sistema disenado para BeagleBoard y para Raspberry Pi 3.


Existe una ficha de despliegue la cual se encuentra dentro de la carpeta fichas del sistema y esta a su vez se encuentra dentro de la documentacion del software. 
La ficha en cuestion presenta los requerimientos basicos para la instalacion del sistema dentro de la Raspberry Pi 3.

para el funcionamiento correcto del sistema en la laptop se debe tener: 

**1. QT Creator superior a 5.9:**
	Las versiones anteriores a 5.9 no cuentan con las librerias de ModBus por tal razon, en caso de correr el sistema en una version anterior a la version 5.9 	se debera, eliminar o comentar todo lo referente a la funcion de Modbus en el sistema.

**2. QT 5 Tools:**
	Instalar las herramientas necesarias junto con las librerias mas usadas para un correcto manejo del sistema. 

**3. QT Serial Port:**
	La libreria de serial port de Qt no se instala por defecto con los aplicativos, ni con el instalador base de QT CREATOR. esta libreria incluye el Qt Serial port 	V5 y el Qt Serial Bus 5.
	Nota: En caso de encontrar el error de que no se encuentra Qt Serial Bus en el sistema, significa que esta libreria no esta instalada y dado que se 	intala por defecto con el serial port 5, esto significa que la version de Qt es inferiror a 5.9.


Luego de contar con las librerias de Qt Creator y sus aplicaivos es necesario tener en cuenta que el sistema Jpt Watercut depende de unos elementos externos del software especificos. Los elementos adicionales son :

		
**1. La base de datos:**
	Para el correcto funcionamiento del sistema es necesario tener una base de datos de nombre "watercut_jptdatabase.db", la cual debe estar ubicada en 	la  carpeta External del area de compilacion del sistema. Si el sistema esta funcionando en laptop, la carpeta debe estar dentro del directorio de compilacion o debug. Si esta funcionando en Beagle O Raspberry, el sistema debe contar un con una carpeta en el directorio raiz llamada **JPT** y dentro de ella la carpeta de **jptwatercut** y dentro de la misma la carpeta **External.**

**NOTA:** Para fines de desarrollo es pereciso tener en cuenta que la direccion de almacenamiento de la base de datos se encuentra en el archivo **jptdatabase.cpp**. la cual por defecto busca la direccion External; Esta direccion se pude cambiar segun los requeriemientos de implementacion o del sistema. 



**2. Archivos de configuracion:**
	El sistema depende de tres archivos de texto plano: El archivo de configuracin de la normalizacion, **delta_n.txt**; El archivo de la configuracion de la calibracion, **Calibration_values.text**; El archivo de configuracion de alto y bajo corte, **hlwc.txt**.

**NOTA:**Para fines de desarrollo es preciso tener en cuenta que la direccin de almacenamiento de los archivos se puede cambiar dentro del proceso de implementacion de cada uno de ellos. Para ello debemos cambiar dentro del archivo **jptwatercut.cpp** las direcciones que se encuentren en los procesos de **Update_calibration_values** , **Charge_valibration_values** ,**Update_normalzation**, **charge_normalization**, **update_hlwc**, `**charge_hlwc**.


----------------------

Sabiendo lo anterior ahora sera posible de compilar o correr de manera correcta el software del cual se ha dejado una copia dentro del repositorio de **BTBUCKET** , en la carpeta **JPT Watercut For Linux**

Los pasos para el despliegue como se dijo anteriormente se encuentran detallados  dentro de la ficha de despliegue, en la carpeta de docmentacion del software. 

Por otro lado se da una breve explicacion del proceso para hacer funcionar el software en tu equipo de escritorio.
El software el cual ya se encuentra compilado tiene por nomenclatura **build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug**, esta nomenclatura corresponde a un directorio dentro del JPT Watercut for Linux. 

Para lograr que el elemento funcione sin inconveniete se debe tener un sistema operativo Ubunto superior a 16.4 el cual debe contar con un usuario denominado **zahall** , este usuario a su vez debe contar con un directrio llamado **Documentos*, de tal manera que dentro de este directorio se situa el actual software quedando asi una ruta de la siguiente manera: 

**/home/zahall/Docuementos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/**

se recuerda que dentro de la carpeta principal se debe encontrar la carpeta con los elementos externos, esta carpeta se denormina **External**.

Teiendo esto se puede correr el programa abriendo una consola en el directorio de **Debug** y escribiendo:

**sudo ./jptwatercut**

**NOTA:**Se recuerda que el software Jpt Watercut depende de un sistema denominado Jpt Monitoring el cual dbe estar corriendo con anterioridad dentro del equipo. Existe un proceso similar al realizado para lograr correr el sistema Jpt Watercur para el sistema Jpt Monitring 


**NOTA:** Existe una carpeta llamada **build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Release** la cual contiene la version de Release del software, si se desea correr dicha version se debe de situar el directorio External dentro de la misma y correr el sistema. El sistema se corre con el mismo comando que para el **Debug**


















