#ifndef _SENTENCE__H
#define _SENTENCE__H
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
// [lv_ variables locales] [a_ atributos clase] [gv_ variables globales] [f_ variables de ciclos] [gui_ variables para interfaces]
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Explicacion cada sentencia del pre-procesador
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// [_HUB_ON             ]: Permite que se compile el programa para la BBB,
// [_FRAG_BITS_NAME_NODE]: Permite que la trama proveniente de los sensores se fragmente en 4{node|ID|value1|value2} y no en tres {node|value1|value2}
//
// [_COMPILE_GUI        ]: Permite que la aplicacion sea compilada con la interfaz grafica.
// [_COMPILE_MESSAGE    ]: Permite que se compilen los mensajes que
//
// [_CONNECT_DB         ]: Para obtener los parametros de la base de datos.
//
// [_TRAME_JP           ]: Para obtener los parametros provenientes de JPTMonitoring
// [_TRAME_CC           ]: Para obtener los parametros provenientes de phmonitor
//
// [_FLOW_              ]:
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

//#define _HUB_ON
#define _FLOW_
#define _COMPILE_GUI
#define _FRAG_BUTS_NAME_NODE

#define _COMPILE_MESSAGE
#define _CONNECT_DB
#define _TRAME_JP

#ifndef _TRAME_JP
    #define _TRAME_CC
#endif

#endif // _SENTENCE__H
