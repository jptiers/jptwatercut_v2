#ifndef JPTDATABASE_H
#define JPTDATABASE_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDebug>
#include <QObject>
#include <QStringList>

#include "_sentence_.h"

#define   name_fields    0
#define   type_fields    1

#define def_pos_table_params         0
#define def_num_colums_table_param  21

#define def_pos_sensor_temp     0
#define def_pos_apl_fx_temp     1
#define def_pos_fx_val_temp     2
#define def_pos_fx_val_flow     3
#define def_pos_lim_flow        4
#define def_pos_apl_ppm_1       5
#define def_pos_apl_ppm_6       6
#define def_pos_apl_fx_ppm      7
#define def_pos_fx_val_ppm      8
#define def_pos_lim_temp_res    9
#define def_pos_lim_temp_cap    10
#define def_pos_lim_ppm_res     11
#define def_pos_lim_ppm_cap     12
#define def_pos_apl_cor_temp    13
#define def_pos_apl_cor_ppm     14
#define def_pos_apl_cor_flow    15
#define def_pos_apl_equ_hc      16
#define def_pos_equ_hc          17
#define def_pos_lim_hc_con_sup  18
#define def_pos_lim_hc_con_inf  19
#define def_pos_lim_hc_por_inf  20

class jptdatabase : public QObject{
    Q_OBJECT

public:
    explicit jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent = 0);
    ~jptdatabase();

    QVector<QStringList> result;
    QSqlDatabase db;

    void create_new_table(const QString name_table);

#ifdef _FILE_
    void get_values(const QString name_table, int id, QString *value);
#else
    void get_values(int pos_name_table, QVector<QStringList> *vec_data);
#endif

    void update_register(QString name_table, QString query);
public slots:
    void insert_register(QString name_table, QString query);

private:
    QString name_database;
    void create_new_table(QString name_table, QString query);
};

#endif // JPTDATABASE_H
