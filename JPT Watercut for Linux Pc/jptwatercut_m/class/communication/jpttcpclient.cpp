#include "jpttcpclient.h"

bool gv_init_comunication = false;

//******************************************************************************************************************************
// Funcion principal                                                                                              [jpttcpclient]
//******************************************************************************************************************************
jpttcpclient::jpttcpclient(QObject *parent) : QObject(parent){
    a_client = new QTcpSocket(this);
    connect(a_client, SIGNAL(readyRead()), this, SLOT(query_from_server()));
    connect(a_client, SIGNAL(disconnected()), this, SLOT(restart_conection()));
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
}

//******************************************************************************************************************************
// Respuesta proveniente desde el servidor                                                   SIGNAL[QTcpSocket:readyRead - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
// TRAME CC: recibe los datos como los envia phmonitor
// TRAME JP: recibe los datos como los envia jptmonitor
//******************************************************************************************************************************
void jpttcpclient::query_from_server(){
    QString lv_answer;
    lv_answer.append(a_client->readAll());

#ifdef _TRAME_CC
    if(lv_answer.count("{") == 0){
        QStringList lv_message = lv_answer.split(" ");
        lv_message.removeAll("");
        while(lv_message.size() > 2){
            emit new_entry(lv_message.mid(0, 3));
            lv_message = lv_message.mid(3, -1);
        }
    }
#endif

#ifdef _TRAME_JP
    if(lv_answer == "Start")
        gv_init_comunication = true;

    if(gv_init_comunication)
        if(lv_answer.count("#") > 1){
            QStringList lv_message = lv_answer.split("-");
            lv_message.removeAll("##");
            while(lv_message.size() > 2){
               emit new_entry(lv_message.mid(0,3));
               lv_message = lv_message.mid(3,-1);
            }
        }
#endif
}
//******************************************************************************************************************************
// Intenta reiniciar la coneccion con el servidor                                           SIGNAL[QTcpSocket:disconnect - SLOT]
//******************************************************************************************************************************
void jpttcpclient::restart_conection(){
    gv_init_comunication = false;
    a_client->close();
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
    emit closed_conection();
}

//******************************************************************************************************************************
// Emite el mensaje hacie el servidor                                                    SIGNAL[jptwatercur:send_message - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
// Con la trama CC se emite al software de phmonitor, emite el inicio de transmision y los valores que se desean transmitir.
// Con la trama JP se emite al software jptmonitoring, en este caso se emite con un json que inicia la trasmision.
//******************************************************************************************************************************
void jpttcpclient::send_message(QString mode){

#ifdef _TRAME_CC
    QStringList lv_message_split;
    if(mode.contains("-"))
        lv_message_split = mode.split("-");
    else
        lv_message_split << "N";

    // Iniciando la comunicacion con el servidos JPTMonitoring
    if ((a_client->state() == QAbstractSocket::ConnectedState) && (lv_message_split[0] == "Start")){
        QJsonObject trame;
        trame["action"] = "register localhost:8080";
        trame["name"]   = lv_message_split[1];

        QJsonDocument Document(trame);
        QString strTrame(Document.toJson());

        a_client->write(strTrame.toLatin1().data(), strTrame.size());
    }else{
        // Enviando la trama: message<+>value1<+>value2
        a_client->write(mode.toLatin1().data());
    }
#endif

#ifdef _TRAME_JP
    if((a_client->state() == QAbstractSocket::ConnectedState) && mode == "Start"){
        QJsonObject lv_trame;
        lv_trame["name"]    = "WaterCut";
        lv_trame["mode"]    = mode;

        QJsonDocument lv_document(lv_trame);
        QString lv_str_trame(lv_document.toJson());

        a_client->write(lv_str_trame.toLatin1().data());

    }else
        restart_conection();
#endif
}
//******************************************************************************************************************************
// Emite el mensaje hacia el servidor                                                    SIGNAL[jptwatercut:send_message - SLOT]
//******************************************************************************************************************************
#ifdef _TRAME_JP
void jpttcpclient::send_message_data(QString value_1, QString value_2){

    if(a_client->state() == QAbstractSocket::ConnectedState){

        QJsonObject lv_trame;

        lv_trame["name"]    = "WaterCut";
        lv_trame["mode"]    = "Data";
        lv_trame["node"]    = "0008";

        lv_trame["value_1"] = value_1;
        lv_trame["value_2"] = value_2;

        QJsonDocument lv_document(lv_trame);
        QString lv_str_trame(lv_document.toJson());

        a_client->write(lv_str_trame.toLatin1().data());

     }else
        restart_conection();

}
#endif
//******************************************************************************************************************************
//
//******************************************************************************************************************************
