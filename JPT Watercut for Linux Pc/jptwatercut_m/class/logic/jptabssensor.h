#ifndef JPTABSSENSOR_H
#define JPTABSSENSOR_H

#include <QStringList>
#include "jptsensors.h"

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de calculo del sensor de temperatura
//-----------------------------------------------------------------------------------------------------------------------------
// [1]: JP-1000
// y = e1 + (e2 * exp(- raw / e3))
// [2]: PT-1000
// y = e1*raw + e2
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_value_temp_s1_e1  -52.0940
#define def_eq_value_temp_s1_e2  371.258
#define def_eq_value_temp_s1_e3  4293.72

#define def_eq_value_temp_s2_e1  0.2102824592
#define def_eq_value_temp_s2_e2 -1257.2342539

//-----------------------------------------------------------------------------------------------------------------------------
// Nombre de los sensores de Temperatura
//-----------------------------------------------------------------------------------------------------------------------------
#define def_sensor_temp_NT1000  0
#define def_sensor_temp_PT1000  1

//-----------------------------------------------------------------------------------------------------------------------------
// Nombre de los sensores de Presion
//-----------------------------------------------------------------------------------------------------------------------------
#define def_sensor_pres_JP1000  0
//-----------------------------------------------------------------------------------------------------------------------------

class jptabssensor : public jptsensors{

public:
    ~jptabssensor();
    jptabssensor();

    //******************************************************************************************************************************
    // Calculo de la fisica del sensor de presion
    //******************************************************************************************************************************
    void static calculate_physic_temp(int type_sen, int raw, double *temp){
        double lv_value(0);
        if(type_sen == def_sensor_temp_NT1000)
            lv_value = def_eq_value_temp_s1_e1  + (def_eq_value_temp_s1_e2*exp((-1*(double)raw)/def_eq_value_temp_s1_e3));
        else if(type_sen == def_sensor_temp_PT1000)
            lv_value = (def_eq_value_temp_s2_e1 * (double)raw) + def_eq_value_temp_s2_e2;
        *temp = lv_value;
    }

    //******************************************************************************************************************************
    // Calculo de la fisica del sensor de presion
    //******************************************************************************************************************************
    void static calculate_physic_pres(int type_sen, int raw, double *pres){
        double lv_value(0);
        if(type_sen == def_sensor_pres_JP1000)
            lv_value = (def_eq_value_temp_s2_e1 * (double)raw) + def_eq_value_temp_s2_e2;
        *pres = lv_value;
    }
};

#endif // JPTABSSENSOR_H

