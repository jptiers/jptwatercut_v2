#ifndef JPTWATERCUT_H
#define JPTWATERCUT_H


#include "_sentence_.h"

#include <QMainWindow>
#include <QLCDNumber>
#include <QDebug>

#include <QTimer>
#include <QObject>
#include <QDialog>
#include <QFileDialog>

#include "class/logic/jptressensor.h"
#include "class/logic/jptcapsensor.h"
#include "class/logic/jptabssensor.h"
#include "class/communication/jptdatabase.h"
#include "class/communication/jpttcpclient.h"

#include "class/logic/jptmodel.h"

#include <class/expression/expreval.h>
#include <cstdio>
#include <string>

using namespace ExprEval;
using namespace std;

const QString name_sensor_temp("TH-40104");

const QString gv_user_data_base("JPT");
const QString gv_name_data_base("jptwatercut.db");
const QString gv_pass_data_base("jptconsultingandservices"); // Leer la contrasena de la base de datos desde un archivo encryptado con GPG, su contrasena es la que se muestra aqui [leer en el bucle principal]


#define def_pos_num_node          0
#define def_pos_res_sensor        1
#define def_pos_cap_sensor        2

#define def_pos_tem_abs_sensor    0
#define def_pos_pre_abs_sensor    1

#define def_numer_nodes_cap_res   6
#define def_numer_nodes           7

#define def_temp_static           2
#define def_sali_static           2

#define def_porcentaje_varianza   10

#ifdef _COMPILE_GUI
namespace Ui {
class jptwatercut;
}

class jptwatercut : public QMainWindow {
        Q_OBJECT

    public:
        explicit jptwatercut(QWidget *parent = 0);
#else
    class jptwatercut : public QObject{
        Q_OBJECT

    public:
        explicit jptwatercut(QObject *parent = 0);

#endif

   ~jptwatercut();


public slots:
    void refactor_data();
    void data_connect();
    void duplicate_db();
    void node_helper_refactor(int node);
    void tam_matriz_node();
    void parse_node();
    //normalization
    void setup_normalization(QStringList values_entry);
    void capture_data_to_normalize();


    void charge_delta_normalization();
    void update_delta_normalization();
    void update_delta_average();
    /*
    void charge_delta_normalization_r();
    void update_delta_normalization_r();
    void update_delta_average_r();
    */


    void calcule_default_delta();
    void calcule_manual_delta();
    void calcule_auto_delta();

    void calcule_default_delta_r();
    void calcule_manual_delta_r();
    void calcule_auto_delta_r();

    //calibration
    void setup_capture_calib();
    void capture_data_calibration();
    void charge_calibration_values();
    void update_calibration_values();
    void avg_cali_oil();
    void avg_cali_air();
    void avg_cali_w();
    void avg_cali_fw();
    //process refactor
    void process_refactor();
    void persistan_refactor();

    //calcule curvas
    void curve_res();
    void curve_cap();
    void my_curves();
    //menu grafico activar correccciones
    void change_correct_temp_on_off(int selec);
    void change_correct_sali_on_off(int selec);

    //calculo de los modelos
    //para el calculo de los modelos primero realizamos el calculo por sensor.
    void wc_per_sensor_res();
    void wc_per_sensor_cap();
    //ahora realizamos el calculo de los modelos
    void calcule_models();
    void calcule_regimen_flow();
    //envio de datos al destino
    void send_to_refactor_db(int flow_regimen,double lam, double turb, double turb_laminar, double water,double oil, double gas);


    //high and low water cut
    //grafic functions
    void value_hwc_up_change(int data);
    void value_hwc_dw_change(int data);
    void value_hwc_up_2_change(int data);
    void value_hwc_dw_2_change(int data);

    void value_lwc_up_change(int data);
    void value_lwc_dw_change(int data);

    void percent_hwc_up_change(int data);
    void percent_hwc_dw_change(int data);
    void percent_hwc_up_2_change(int data);
    void percent_hwc_dw_2_change(int data);


    void percent_lwc_up_change(int data);
    void percent_lwc_dw_change(int data);

    //generador de curvas de corte alto y bajo
    void hlwc_curves();
    void hwc_curve_res();
    void hwc_curve_cap();
    void low_curve_cap_res();

    void update_hlwc_values();
    void charge_hl_wc_values();

    float high_turb_laminar(float _cuentas);
    float high_turb_laminar_res(float _cuentas);
    float low_turb_laminar(float _cuentas);
    //generador de curva del modelo de 3 fases
    void curve_three_phases();
    void calcule_three_phases();
    //generador del modelo de datos.
    float model_three_phases(float _cuentas);




    // void node_helper_refactor_prueba(int node);//create to analize the system.
    bool connect_db(QString MyDb);
    void create_pack_to_send();
    int get_id_from_time(QDateTime my_timestamp,int node);
    //int get_raw_1_from_id(int id,int node);
    // int get_raw_2_from_id(int id,int node);



    void process_new_entry_sensors(QStringList entry);
    void acumulator_entry_sensors(QStringList entry);
    void start_communication();
    void restart_communication();

    void change_ppm_mode(int selec);

    void change_temp_sensor(const int sensor);
    void change_temp_mode(int selec);

    void temp_fixed_change(double temp);
    void ppm_fixed_change(int ppm);

#ifdef _FLOW_
    void flow_fixed_change(double flow);
#endif
    void porc_fixed_change(int porc);

signals:
    void start_connection(QString);
#ifdef _TRAME_CC
    void send_message(QString);
#endif

#ifdef _TRAME_JP
    void send_message(QString, QString);
#endif
    void send_message_to_database(QString, QString);


private slots:
    void gen_graphic();

public slots:
    void save_data_base();

private:
    Ui::jptwatercut *ui;

    /*************************
     * Funciones del proceso.
    *************************/
    void charge_param_sensor_temp_and_press();
    void charge_param_sensor_resi_and_capac();

    void calcule_param_sensor_temp_and_pres(QStringList entry, int node);
    void calcule_param_sensor_resis(double valuesensor, int node);
    void calcule_param_sensor_capac(double valuesensor, int node);

    void calcule_physic_salinity(int *salinity, int num_node);
    void determine_real_salinity(int node_index);

    void compensate_temperature(double temperature);
    void compensate_salinity(double salinity);

    bool comprobar_nodes();

    /*****************************
     * Variables de comunicaciones
    ******************************/
    jptdatabase  *a_data_base;
    jpttcpclient *a_tcp_client;
    QTimer       *a_open_conection;

    /*************************
     * Variables del proceso.
    *************************/
    QVector<jptcapsensor *> a_cap_sensor;
    QVector<jptressensor *> a_res_sensor;
    QVector<jptabssensor *> a_abs_sensor;



    double  a_real_temperature = 0;
    double  a_real_pressure = 0;
    int     a_real_salinity = 0;

    int a_delta_cuentas_tem_res = 0;
    int a_delta_cuentas_tem_cap = 0;

    int a_delta_cuentas_sali_cap = 0;
    int a_delta_cuentas_sali_res = 0;

    int a_delta_cuentas_flow_cap = 0;
    int a_delta_cuentas_flow_res = 0;

    int a_real_porcentaje_variance = 0;

    jptmodel *new_model;

    void calcule_compensate_salinity();
#ifdef _FILE_
    void get_database_raw_inf(int id);
#endif
    void calcule_compensate_flow(int node);
    void high_cut_equa(double *high_cut);

    void configure_gui();

    };

#endif // JPTWATERCUT_H
