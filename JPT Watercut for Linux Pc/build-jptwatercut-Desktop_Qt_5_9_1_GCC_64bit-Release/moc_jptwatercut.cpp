/****************************************************************************
** Meta object code from reading C++ file 'jptwatercut.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../jptwatercut_m/jptwatercut.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'jptwatercut.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_jptwatercut_t {
    QByteArrayData data[64];
    char stringdata0[1023];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_jptwatercut_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_jptwatercut_t qt_meta_stringdata_jptwatercut = {
    {
QT_MOC_LITERAL(0, 0, 11), // "jptwatercut"
QT_MOC_LITERAL(1, 12, 16), // "start_connection"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 12), // "send_message"
QT_MOC_LITERAL(4, 43, 24), // "send_message_to_database"
QT_MOC_LITERAL(5, 68, 13), // "refactor_data"
QT_MOC_LITERAL(6, 82, 12), // "data_connect"
QT_MOC_LITERAL(7, 95, 12), // "duplicate_db"
QT_MOC_LITERAL(8, 108, 20), // "node_helper_refactor"
QT_MOC_LITERAL(9, 129, 4), // "node"
QT_MOC_LITERAL(10, 134, 15), // "tam_matriz_node"
QT_MOC_LITERAL(11, 150, 10), // "parse_node"
QT_MOC_LITERAL(12, 161, 19), // "setup_normalization"
QT_MOC_LITERAL(13, 181, 12), // "values_entry"
QT_MOC_LITERAL(14, 194, 25), // "capture_data_to_normalize"
QT_MOC_LITERAL(15, 220, 26), // "charge_delta_normalization"
QT_MOC_LITERAL(16, 247, 26), // "update_delta_normalization"
QT_MOC_LITERAL(17, 274, 20), // "update_delta_average"
QT_MOC_LITERAL(18, 295, 21), // "calcule_default_delta"
QT_MOC_LITERAL(19, 317, 20), // "calcule_manual_delta"
QT_MOC_LITERAL(20, 338, 18), // "calcule_auto_delta"
QT_MOC_LITERAL(21, 357, 19), // "setup_capture_calib"
QT_MOC_LITERAL(22, 377, 24), // "capture_data_calibration"
QT_MOC_LITERAL(23, 402, 25), // "charge_calibration_values"
QT_MOC_LITERAL(24, 428, 25), // "update_calibration_values"
QT_MOC_LITERAL(25, 454, 12), // "avg_cali_oil"
QT_MOC_LITERAL(26, 467, 12), // "avg_cali_air"
QT_MOC_LITERAL(27, 480, 10), // "avg_cali_w"
QT_MOC_LITERAL(28, 491, 11), // "avg_cali_fw"
QT_MOC_LITERAL(29, 503, 16), // "process_refactor"
QT_MOC_LITERAL(30, 520, 9), // "curve_res"
QT_MOC_LITERAL(31, 530, 9), // "curve_cap"
QT_MOC_LITERAL(32, 540, 9), // "my_curves"
QT_MOC_LITERAL(33, 550, 26), // "change_correct_temp_on_off"
QT_MOC_LITERAL(34, 577, 5), // "selec"
QT_MOC_LITERAL(35, 583, 26), // "change_correct_sali_on_off"
QT_MOC_LITERAL(36, 610, 17), // "wc_per_sensor_res"
QT_MOC_LITERAL(37, 628, 17), // "wc_per_sensor_cap"
QT_MOC_LITERAL(38, 646, 14), // "calcule_models"
QT_MOC_LITERAL(39, 661, 20), // "calcule_regimen_flow"
QT_MOC_LITERAL(40, 682, 10), // "connect_db"
QT_MOC_LITERAL(41, 693, 4), // "MyDb"
QT_MOC_LITERAL(42, 698, 19), // "create_pack_to_send"
QT_MOC_LITERAL(43, 718, 16), // "get_id_from_time"
QT_MOC_LITERAL(44, 735, 12), // "my_timestamp"
QT_MOC_LITERAL(45, 748, 25), // "process_new_entry_sensors"
QT_MOC_LITERAL(46, 774, 5), // "entry"
QT_MOC_LITERAL(47, 780, 24), // "acumulator_entry_sensors"
QT_MOC_LITERAL(48, 805, 19), // "start_communication"
QT_MOC_LITERAL(49, 825, 21), // "restart_communication"
QT_MOC_LITERAL(50, 847, 15), // "change_ppm_mode"
QT_MOC_LITERAL(51, 863, 18), // "change_temp_sensor"
QT_MOC_LITERAL(52, 882, 6), // "sensor"
QT_MOC_LITERAL(53, 889, 16), // "change_temp_mode"
QT_MOC_LITERAL(54, 906, 17), // "temp_fixed_change"
QT_MOC_LITERAL(55, 924, 4), // "temp"
QT_MOC_LITERAL(56, 929, 16), // "ppm_fixed_change"
QT_MOC_LITERAL(57, 946, 3), // "ppm"
QT_MOC_LITERAL(58, 950, 17), // "flow_fixed_change"
QT_MOC_LITERAL(59, 968, 4), // "flow"
QT_MOC_LITERAL(60, 973, 17), // "porc_fixed_change"
QT_MOC_LITERAL(61, 991, 4), // "porc"
QT_MOC_LITERAL(62, 996, 11), // "gen_graphic"
QT_MOC_LITERAL(63, 1008, 14) // "save_data_base"

    },
    "jptwatercut\0start_connection\0\0"
    "send_message\0send_message_to_database\0"
    "refactor_data\0data_connect\0duplicate_db\0"
    "node_helper_refactor\0node\0tam_matriz_node\0"
    "parse_node\0setup_normalization\0"
    "values_entry\0capture_data_to_normalize\0"
    "charge_delta_normalization\0"
    "update_delta_normalization\0"
    "update_delta_average\0calcule_default_delta\0"
    "calcule_manual_delta\0calcule_auto_delta\0"
    "setup_capture_calib\0capture_data_calibration\0"
    "charge_calibration_values\0"
    "update_calibration_values\0avg_cali_oil\0"
    "avg_cali_air\0avg_cali_w\0avg_cali_fw\0"
    "process_refactor\0curve_res\0curve_cap\0"
    "my_curves\0change_correct_temp_on_off\0"
    "selec\0change_correct_sali_on_off\0"
    "wc_per_sensor_res\0wc_per_sensor_cap\0"
    "calcule_models\0calcule_regimen_flow\0"
    "connect_db\0MyDb\0create_pack_to_send\0"
    "get_id_from_time\0my_timestamp\0"
    "process_new_entry_sensors\0entry\0"
    "acumulator_entry_sensors\0start_communication\0"
    "restart_communication\0change_ppm_mode\0"
    "change_temp_sensor\0sensor\0change_temp_mode\0"
    "temp_fixed_change\0temp\0ppm_fixed_change\0"
    "ppm\0flow_fixed_change\0flow\0porc_fixed_change\0"
    "porc\0gen_graphic\0save_data_base"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_jptwatercut[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      51,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  269,    2, 0x06 /* Public */,
       3,    2,  272,    2, 0x06 /* Public */,
       4,    2,  277,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  282,    2, 0x0a /* Public */,
       6,    0,  283,    2, 0x0a /* Public */,
       7,    0,  284,    2, 0x0a /* Public */,
       8,    1,  285,    2, 0x0a /* Public */,
      10,    0,  288,    2, 0x0a /* Public */,
      11,    0,  289,    2, 0x0a /* Public */,
      12,    1,  290,    2, 0x0a /* Public */,
      14,    0,  293,    2, 0x0a /* Public */,
      15,    0,  294,    2, 0x0a /* Public */,
      16,    0,  295,    2, 0x0a /* Public */,
      17,    0,  296,    2, 0x0a /* Public */,
      18,    0,  297,    2, 0x0a /* Public */,
      19,    0,  298,    2, 0x0a /* Public */,
      20,    0,  299,    2, 0x0a /* Public */,
      21,    0,  300,    2, 0x0a /* Public */,
      22,    0,  301,    2, 0x0a /* Public */,
      23,    0,  302,    2, 0x0a /* Public */,
      24,    0,  303,    2, 0x0a /* Public */,
      25,    0,  304,    2, 0x0a /* Public */,
      26,    0,  305,    2, 0x0a /* Public */,
      27,    0,  306,    2, 0x0a /* Public */,
      28,    0,  307,    2, 0x0a /* Public */,
      29,    0,  308,    2, 0x0a /* Public */,
      30,    0,  309,    2, 0x0a /* Public */,
      31,    0,  310,    2, 0x0a /* Public */,
      32,    0,  311,    2, 0x0a /* Public */,
      33,    1,  312,    2, 0x0a /* Public */,
      35,    1,  315,    2, 0x0a /* Public */,
      36,    0,  318,    2, 0x0a /* Public */,
      37,    0,  319,    2, 0x0a /* Public */,
      38,    0,  320,    2, 0x0a /* Public */,
      39,    0,  321,    2, 0x0a /* Public */,
      40,    1,  322,    2, 0x0a /* Public */,
      42,    0,  325,    2, 0x0a /* Public */,
      43,    2,  326,    2, 0x0a /* Public */,
      45,    1,  331,    2, 0x0a /* Public */,
      47,    1,  334,    2, 0x0a /* Public */,
      48,    0,  337,    2, 0x0a /* Public */,
      49,    0,  338,    2, 0x0a /* Public */,
      50,    1,  339,    2, 0x0a /* Public */,
      51,    1,  342,    2, 0x0a /* Public */,
      53,    1,  345,    2, 0x0a /* Public */,
      54,    1,  348,    2, 0x0a /* Public */,
      56,    1,  351,    2, 0x0a /* Public */,
      58,    1,  354,    2, 0x0a /* Public */,
      60,    1,  357,    2, 0x0a /* Public */,
      62,    0,  360,    2, 0x08 /* Private */,
      63,    0,  361,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QStringList,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QString,   41,
    QMetaType::Void,
    QMetaType::Int, QMetaType::QDateTime, QMetaType::Int,   44,    9,
    QMetaType::Void, QMetaType::QStringList,   46,
    QMetaType::Void, QMetaType::QStringList,   46,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void, QMetaType::Int,   52,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void, QMetaType::Double,   55,
    QMetaType::Void, QMetaType::Int,   57,
    QMetaType::Void, QMetaType::Double,   59,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void jptwatercut::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        jptwatercut *_t = static_cast<jptwatercut *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->start_connection((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->send_message((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->send_message_to_database((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->refactor_data(); break;
        case 4: _t->data_connect(); break;
        case 5: _t->duplicate_db(); break;
        case 6: _t->node_helper_refactor((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->tam_matriz_node(); break;
        case 8: _t->parse_node(); break;
        case 9: _t->setup_normalization((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 10: _t->capture_data_to_normalize(); break;
        case 11: _t->charge_delta_normalization(); break;
        case 12: _t->update_delta_normalization(); break;
        case 13: _t->update_delta_average(); break;
        case 14: _t->calcule_default_delta(); break;
        case 15: _t->calcule_manual_delta(); break;
        case 16: _t->calcule_auto_delta(); break;
        case 17: _t->setup_capture_calib(); break;
        case 18: _t->capture_data_calibration(); break;
        case 19: _t->charge_calibration_values(); break;
        case 20: _t->update_calibration_values(); break;
        case 21: _t->avg_cali_oil(); break;
        case 22: _t->avg_cali_air(); break;
        case 23: _t->avg_cali_w(); break;
        case 24: _t->avg_cali_fw(); break;
        case 25: _t->process_refactor(); break;
        case 26: _t->curve_res(); break;
        case 27: _t->curve_cap(); break;
        case 28: _t->my_curves(); break;
        case 29: _t->change_correct_temp_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->change_correct_sali_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 31: _t->wc_per_sensor_res(); break;
        case 32: _t->wc_per_sensor_cap(); break;
        case 33: _t->calcule_models(); break;
        case 34: _t->calcule_regimen_flow(); break;
        case 35: { bool _r = _t->connect_db((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 36: _t->create_pack_to_send(); break;
        case 37: { int _r = _t->get_id_from_time((*reinterpret_cast< QDateTime(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 38: _t->process_new_entry_sensors((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 39: _t->acumulator_entry_sensors((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 40: _t->start_communication(); break;
        case 41: _t->restart_communication(); break;
        case 42: _t->change_ppm_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 43: _t->change_temp_sensor((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 44: _t->change_temp_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 45: _t->temp_fixed_change((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 46: _t->ppm_fixed_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 47: _t->flow_fixed_change((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 48: _t->porc_fixed_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 49: _t->gen_graphic(); break;
        case 50: _t->save_data_base(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (jptwatercut::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::start_connection)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (jptwatercut::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::send_message)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (jptwatercut::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::send_message_to_database)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject jptwatercut::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_jptwatercut.data,
      qt_meta_data_jptwatercut,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *jptwatercut::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *jptwatercut::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_jptwatercut.stringdata0))
        return static_cast<void*>(const_cast< jptwatercut*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int jptwatercut::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 51)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 51;
    }
    return _id;
}

// SIGNAL 0
void jptwatercut::start_connection(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void jptwatercut::send_message(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void jptwatercut::send_message_to_database(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
