#ifndef JPTTCPCLIENT_H
#define JPTTCPCLIENT_H

#include <QObject>
#include <QtNetwork>
#include <QtNetwork/QNetworkInterface>
#include "_sentence_.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define def_ip_widgets_communication    "192.168.1.24"
#define def_port_widgets_communication  10000

class jpttcpclient : public QObject{
     Q_OBJECT

public:
    explicit jpttcpclient(QObject *parent = 0);
    void set_json_monitoring(QString json);
    QString get_json_monitoring();

public slots:
    void query_from_server();
    void restart_conection();

    void send_message(QString mode);
    void slot_siete_elementos(QString laminar, QString turbulento,
                              QString turbulento_lam, QString regimen_flujo, QString por_agua,
                              QString por_oil, QString por_gas, QString watercut);

#ifdef _TRAME_JP
    void send_message_data(QString value_1, QString value_2);
    void response_to_monitoring(QString response);
#endif

private:
     QTcpSocket *a_client;
     QString json_monitoring;

signals:
     void closed_conection();
     void new_entry(QStringList trame);
     void new_json_entry(QStringList raw_data,QStringList eng_data);

};

#endif // JPTTCPCLIENT_H
