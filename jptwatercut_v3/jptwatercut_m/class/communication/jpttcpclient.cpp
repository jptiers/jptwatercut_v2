#include "jpttcpclient.h"

bool gv_init_comunication = false;

//******************************************************************************************************************************
// Funcion principal                                                                                              [jpttcpclient]
//******************************************************************************************************************************
jpttcpclient::jpttcpclient(QObject *parent) : QObject(parent){
    a_client = new QTcpSocket(this);
    connect(a_client, SIGNAL(readyRead()), this, SLOT(query_from_server()));
    connect(a_client, SIGNAL(disconnected()), this, SLOT(restart_conection()));
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
}

//******************************************************************************************************************************
// Respuesta proveniente desde el servidor                                                   SIGNAL[QTcpSocket:readyRead - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
// TRAME CC: recibe los datos como los envia phmonitor
// TRAME JP: recibe los datos como los envia jptmonitor
//******************************************************************************************************************************
void jpttcpclient::query_from_server(){
    QString lv_answer;
    lv_answer.append(a_client->readAll());
/*
#ifdef _TRAME_CC
    if(lv_answer.count("{") == 0){
        QStringList lv_message = lv_answer.split(" ");
        lv_message.removeAll("");
        while(lv_message.size() > 2){
            emit new_entry(lv_message.mid(0, 3));
            lv_message = lv_message.mid(3, -1);
        }
    }
#endif
*/
//#ifdef _TRAME_JP
    if(lv_answer == "Start")
        gv_init_comunication = true;
    //en la trama vieja el sistema detectaba por nodo la cantidad de caracteres ## y validaba para enviarlos a su respectivo destino
    if(gv_init_comunication)
        if(lv_answer.count("#") > 1){
            QStringList lv_message = lv_answer.split("-");
            lv_message.removeAll("##");
            while(lv_message.size() > 2){
               emit new_entry(lv_message.mid(0,3));
               lv_message = lv_message.mid(3,-1);
            }
        }
//#endif
    //(Sentencia de visualizacion de trama.
    if(lv_answer!="Start")
    {
        //seteo el json que envia el monitoring }
        set_json_monitoring(lv_answer);

        //qDebug()<<"LLego Trama nueva en el sistema";
        qDebug()<<lv_answer;
        //si llega una trama nueva diferente de Start, es decir, de la trama de iniciacion del sistema entonces parceamos la data como json

        QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_answer.toUtf8());//es el documento entrante.
        QJsonObject   lv_trama_json   = lv_message_json.object();//se convierte en objeto.

        //parseamos device object.
        QJsonObject  lv_device=lv_trama_json.value("device").toObject();

        //parseamos respecto a sensores.
        QJsonObject lv_sensors=lv_device.value("sensors").toObject();

        QString temporal_raw[20];
        QString temporal_eng[20];

        QStringList parce_list_label=lv_sensors.keys();


        for(int i=0; i<parce_list_label.size();i++)
        {
            ////qDebug()<<parce_list_label[i];
            //obtenemos valor raw y de eng de cada dato
            QJsonObject sensor_final=lv_sensors.value(parce_list_label[i]).toObject();

            //obtenemos solo la cabecera
            QStringList cabecera=parce_list_label[i].split("-");

            if (cabecera[0] == "1"){
                //qDebug()<<"Valor 1<<"<<cabecera[0];
                temporal_raw[0]=(sensor_final.value("RAW").toString());
                temporal_eng[0]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "2"){
                //qDebug()<<"Valor 2<<"<<cabecera[0];
                temporal_raw[1]=(sensor_final.value("RAW").toString());
                temporal_eng[1]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "3"){
                //qDebug()<<"Valor 3<<"<<cabecera[0];
                temporal_raw[2]=(sensor_final.value("RAW").toString());
                temporal_eng[2]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "4"){
                //qDebug()<<"Valor 4<<"<<cabecera[0];
                temporal_raw[3]=(sensor_final.value("RAW").toString());
                temporal_eng[3]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "5"){
                //qDebug()<<"Valor 5<<"<<cabecera[0];
                temporal_raw[4]=(sensor_final.value("RAW").toString());
                temporal_eng[4]=(sensor_final.value("ENG").toString());


            }else if(cabecera[0] == "6"){
                //qDebug()<<"Valor 6<<"<<cabecera[0];
                temporal_raw[5]=(sensor_final.value("RAW").toString());
                temporal_eng[5]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "7"){
                //qDebug()<<"Valor 7<<"<<cabecera[0];
                temporal_raw[6]=(sensor_final.value("RAW").toString());
                temporal_eng[6]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "8"){
                //qDebug()<<"Valor 8<<"<<cabecera[0];
                temporal_raw[7]=(sensor_final.value("RAW").toString());
                temporal_eng[7]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "9"){
                //qDebug()<<"Valor 9<<"<<cabecera[0];
                temporal_raw[8]=(sensor_final.value("RAW").toString());
                temporal_eng[8]=(sensor_final.value("ENG").toString());

            }else if (cabecera[0] == "10"){
                //qDebug()<<"Valor 10<<"<<cabecera[0];
                temporal_raw[9]=(sensor_final.value("RAW").toString());
                temporal_eng[9]=(sensor_final.value("ENG").toString());
            }else if (cabecera[0] == "11"){
                //qDebug()<<"Valor 11<<"<<cabecera[0];
                temporal_raw[10]=(sensor_final.value("RAW").toString());
                temporal_eng[10]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "12"){
                //qDebug()<<"Valor 12<<"<<cabecera[0];
                temporal_raw[11]=(sensor_final.value("RAW").toString());
                temporal_eng[11]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "13"){
                //qDebug()<<"Valor 13<<"<<cabecera[0];
                temporal_raw[12]=(sensor_final.value("RAW").toString());
                temporal_eng[12]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "14"){
                temporal_raw[13]=(sensor_final.value("RAW").toString());
                temporal_eng[13]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "15"){
                temporal_raw[14]=(sensor_final.value("RAW").toString());
                temporal_eng[14]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "16"){
                temporal_raw[15]=(sensor_final.value("RAW").toString());
                temporal_eng[15]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "17"){
                temporal_raw[16]=(sensor_final.value("RAW").toString());
                temporal_eng[16]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "18"){
                temporal_raw[17]=(sensor_final.value("RAW").toString());
                temporal_eng[17]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "19"){
                temporal_raw[18]=(sensor_final.value("RAW").toString());
                temporal_eng[18]=(sensor_final.value("ENG").toString());

            }else if(cabecera[0] == "20"){
                temporal_raw[19]=(sensor_final.value("RAW").toString());
                temporal_eng[19]=(sensor_final.value("ENG").toString());

            }

        }

        qDebug()<<"%%"<<temporal_raw[10];
        QStringList raw_to_send;
        QStringList eng_to_send;

        for(int k=0;k<parce_list_label.size();k++)
        {
            raw_to_send.append(temporal_raw[k]);
            eng_to_send.append(temporal_eng[k]);

        }
        //ya organizados los datos procedemos a enviarlos a la clase maestra
        emit new_json_entry(raw_to_send,eng_to_send);

        //slot_siete_elementos("001", "002","003","004","005","006","007");


    }

}
//******************************************************************************************************************************
// Intenta reiniciar la coneccion con el servidor                                           SIGNAL[QTcpSocket:disconnect - SLOT]
//******************************************************************************************************************************
void jpttcpclient::restart_conection(){
    gv_init_comunication = false;
    a_client->close();
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
    emit closed_conection();
}

//******************************************************************************************************************************
// Emite el mensaje hacie el servidor                                                    SIGNAL[jptwatercur:send_message - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
// Con la trama CC se emite al software de phmonitor, emite el inicio de transmision y los valores que se desean transmitir.
// Con la trama JP se emite al software jptmonitoring, en este caso se emite con un json que inicia la trasmision.
//******************************************************************************************************************************
void jpttcpclient::send_message(QString mode){

/*
#ifdef _TRAME_CC
    QStringList lv_message_split;
    if(mode.contains("-"))
        lv_message_split = mode.split("-");
    else
        lv_message_split << "N";

    // Iniciando la comunicacion con el servidos JPTMonitoring
    if ((a_client->state() == QAbstractSocket::ConnectedState) && (lv_message_split[0] == "Start")){
        QJsonObject trame;
        trame["action"] = "register localhost:8080";
        trame["name"]   = lv_message_split[1];

        QJsonDocument Document(trame);
        QString strTrame(Document.toJson());

        a_client->write(strTrame.toLatin1().data(), strTrame.size());
    }else{
        // Enviando la trama: message<+>value1<+>value2
        a_client->write(mode.toLatin1().data());
    }
#endif
*/
#ifdef _TRAME_JP
    if((a_client->state() == QAbstractSocket::ConnectedState) && mode == "Start"){
        QJsonObject lv_trame;
        lv_trame["name"]    = "watercut";
        lv_trame["mode"]    = mode;

        QJsonDocument lv_document(lv_trame);
        QString lv_str_trame(lv_document.toJson());

        a_client->write(lv_str_trame.toLatin1().data());

    }else
        restart_conection();
#endif
}
//******************************************************************************************************************************
// Emite el mensaje hacia el servidor                                                    SIGNAL[jptwatercut:send_message - SLOT]
//******************************************************************************************************************************



//devuelve la informacion al monitoring con el json ya parseado.
//tareas:
//1)Se tiene una variable local, la cual recibe el json.
//>>guardar el json en una variable global de la clase. definir variable en el .h en la cual guardas tu json.---
//tarea 2
//hacer una slot que reciba 7elementos.
//los cuales son: laminar
//turbulento
//turbulento-lam
//regimen flijo
//% agua
//%oil
//%gas

//este slot, recibira estos datos del jptwatercutcomo señal y lo que hara es agregar estos elementos a los sensores.
//ejempplo
//sensor:{
//"1_resistividad-1":[...],...,"14-laminar":[//valor raw y eng tienen el mismo dato]
//}
void jpttcpclient::send_message_data(QString value_1, QString value_2){

    //validamos la salida del systema
    qDebug()<<"Valores de salida del sistema : "<<value_1<<"  !! "<<value_2;


    /*
    if(a_client->state() == QAbstractSocket::ConnectedState){

        QJsonObject lv_trame;

        lv_trame["name"]    = "WaterCut";
        lv_trame["mode"]    = "Data";
        lv_trame["node"]    = "0008";

        lv_trame["value_1"] = value_1;
        lv_trame["value_2"] = value_2;

        QJsonDocument lv_document(lv_trame);
        QString lv_str_trame(lv_document.toJson());

        a_client->write(lv_str_trame.toLatin1().data());

     }else
        restart_conection();
    */
}

void jpttcpclient::response_to_monitoring(QString response)
{
    if(a_client->state() == QAbstractSocket::ConnectedState){


        QString lv_str_trame(response);//casteo la respuesta

        a_client->write(lv_str_trame.toLatin1().data());

     }else
        restart_conection();

}


//Slot que recibira la data correspondiete para agregar los valors a las cabeceras correspondientes
void jpttcpclient::slot_siete_elementos(QString laminar, QString turbulento,
                                        QString turbulento_lam, QString regimen_flujo,
                                        QString por_agua, QString por_oil, QString por_gas, QString watercut){
    //es el documento entrante.
    QJsonDocument lv_message_json = QJsonDocument::fromJson(get_json_monitoring().toUtf8());
    //se convierte en objeto.
    QJsonObject   lv_trama_json = lv_message_json.object();
    //parseamos device object.
    QJsonObject  lv_device=lv_trama_json.value("device").toObject();
    //parseamos respecto a sensores.
    QJsonObject lv_sensors=lv_device.value("sensors").toObject();
    //objeto que agregara sensores
    QJsonObject add_sensors = lv_sensors.value("sensors").toObject();

    //cantidad de los sensores
    lv_device.remove("sensor_count");
    //cantidad de los sensores

    //laminar, turbulent, flow_regimen, turb_laminar, water_percent, oil_percent, gas_percent
    add_sensors.insert("RAW", laminar);
    add_sensors.insert("ENG", laminar);

    lv_sensors.insert("14-laminar", add_sensors);

    add_sensors.insert("RAW", turbulento);
    add_sensors.insert("ENG", turbulento);

    lv_sensors.insert("15-turbulent", add_sensors);

    add_sensors.insert("RAW", turbulento_lam);
    add_sensors.insert("ENG", turbulento_lam);

    lv_sensors.insert("16-turb_laminar", add_sensors);

    add_sensors.insert("RAW", regimen_flujo);
    add_sensors.insert("ENG", regimen_flujo);

    lv_sensors.insert("17-flow_regimen", add_sensors);

    add_sensors.insert("RAW", watercut);
    add_sensors.insert("ENG", watercut);

    lv_sensors.insert("18-watercut", add_sensors);

    add_sensors.insert("RAW", por_agua);
    add_sensors.insert("ENG", por_agua);

    lv_sensors.insert("19-water_percent", add_sensors);

    add_sensors.insert("RAW", por_oil);
    add_sensors.insert("ENG", por_oil);

    lv_sensors.insert("20-oil_percent", add_sensors);

    add_sensors.insert("RAW", por_gas);
    add_sensors.insert("ENG", por_gas);

    lv_sensors.insert("21-gas_percent", add_sensors);

    qDebug()<<"======================================";

    //lv_device.remove("sensors");
    lv_trama_json.remove("device");
    lv_device.remove("sensors");
    //
    lv_device.insert("sensor_count","21");
    lv_device.insert("sensors",lv_sensors);
    lv_trama_json.insert("device", lv_device);

    qDebug()<<lv_trama_json<<" JSON OKAAAA";

    QJsonDocument final_json_document(lv_trama_json);
    QString final_to_send(final_json_document.toJson(QJsonDocument::Compact));
    set_json_monitoring(final_to_send);

    qDebug()<<get_json_monitoring();

    response_to_monitoring(final_to_send);
}

//******************************************************************************************************************************
//SET AND GET JSON FROM JPTMONITORING
//******************************************************************************************************************************
void jpttcpclient::set_json_monitoring(QString json){
    json_monitoring = json;
}

QString jpttcpclient::get_json_monitoring(){
    return json_monitoring;
}
