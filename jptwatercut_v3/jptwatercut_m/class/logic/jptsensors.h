#ifndef JPTSENSORS_H
#define JPTSENSORS_H

#include "math.h"

class jptsensors{

public:
    jptsensors();
    double     raw;
    double     rawct;
    double     rawcs;
    double     rawcf;

    void compensate_temperature(int delta_cuentas);
    void compensate_salinity(int delta_cuentas);
    void compensate_flow(int delta_cuentas);
};

#endif // JPTSENSORS_H
