#ifndef MYTHREAD_H
#define MYTHREAD_H


#include <QMainWindow>
#include <QLCDNumber>
#include <QDebug>

#include <QTimer>
#include <QObject>
#include <QDialog>
#include <QFileDialog>
#include <QThread>



class MyThread : public QThread, public  QMainWindow
{

public:
    MyThread();
    void run();
};

#endif // MYTHREAD_H
