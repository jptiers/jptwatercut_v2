#-------------------------------------------------
#
# Project created by QtCreator
#
#-------------------------------------------------

QT       += core gui sql network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT +=sql

TARGET = jptwatercut
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#INCLUDEPATH += "/jpt/dep/include"
#LIBS += -L/jpt/dep/lib

SOURCES += main.cpp\
    jptwatercut.cpp \
    class/logic/jptabssensor.cpp \
    class/logic/jptcapsensor.cpp \
    class/communication/jptdatabase.cpp \
    class/logic/jptressensor.cpp \
    class/logic/jptsensors.cpp \
    class/communication/jpttcpclient.cpp \
    class/logic/jptmodel.cpp \
    class/expression/except.cpp \
    class/expression/expr.cpp \
    class/expression/func.cpp \
    class/expression/funclist.cpp \
    class/expression/node.cpp \
    class/expression/parser.cpp \
    class/expression/vallist.cpp \
    qcustomplot.cpp

HEADERS  += \
    _sentence_.h \
    jptwatercut.h \
    class/logic/jptabssensor.h \
    class/logic/jptcapsensor.h \
    class/communication/jptdatabase.h \
    class/logic/jptressensor.h \
    class/logic/jptsensors.h \
    class/communication/jpttcpclient.h \
    class/logic/jptmodel.h \
    class/expression/defs.h \
    class/expression/except.h \
    class/expression/expr.h \
    class/expression/expreval.h \
    class/expression/funclist.h \
    class/expression/node.h \
    class/expression/parser.h \
    class/expression/vallist.h \
    qcustomplot.h


FORMS    += \
    jptwatercut.ui

RESOURCES += \
    Image/image.qrc \
    Image/cap_image.qrc

DISTFILES += \
    Image/res_image.png

