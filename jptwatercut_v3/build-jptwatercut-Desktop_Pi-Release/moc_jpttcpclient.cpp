/****************************************************************************
** Meta object code from reading C++ file 'jpttcpclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../jptwatercut_m/class/communication/jpttcpclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'jpttcpclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_jpttcpclient_t {
    QByteArrayData data[26];
    char stringdata0[303];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_jpttcpclient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_jpttcpclient_t qt_meta_stringdata_jpttcpclient = {
    {
QT_MOC_LITERAL(0, 0, 12), // "jpttcpclient"
QT_MOC_LITERAL(1, 13, 16), // "closed_conection"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 9), // "new_entry"
QT_MOC_LITERAL(4, 41, 5), // "trame"
QT_MOC_LITERAL(5, 47, 14), // "new_json_entry"
QT_MOC_LITERAL(6, 62, 8), // "raw_data"
QT_MOC_LITERAL(7, 71, 8), // "eng_data"
QT_MOC_LITERAL(8, 80, 17), // "query_from_server"
QT_MOC_LITERAL(9, 98, 17), // "restart_conection"
QT_MOC_LITERAL(10, 116, 12), // "send_message"
QT_MOC_LITERAL(11, 129, 4), // "mode"
QT_MOC_LITERAL(12, 134, 20), // "slot_siete_elementos"
QT_MOC_LITERAL(13, 155, 7), // "laminar"
QT_MOC_LITERAL(14, 163, 10), // "turbulento"
QT_MOC_LITERAL(15, 174, 14), // "turbulento_lam"
QT_MOC_LITERAL(16, 189, 13), // "regimen_flujo"
QT_MOC_LITERAL(17, 203, 8), // "por_agua"
QT_MOC_LITERAL(18, 212, 7), // "por_oil"
QT_MOC_LITERAL(19, 220, 7), // "por_gas"
QT_MOC_LITERAL(20, 228, 8), // "watercut"
QT_MOC_LITERAL(21, 237, 17), // "send_message_data"
QT_MOC_LITERAL(22, 255, 7), // "value_1"
QT_MOC_LITERAL(23, 263, 7), // "value_2"
QT_MOC_LITERAL(24, 271, 22), // "response_to_monitoring"
QT_MOC_LITERAL(25, 294, 8) // "response"

    },
    "jpttcpclient\0closed_conection\0\0new_entry\0"
    "trame\0new_json_entry\0raw_data\0eng_data\0"
    "query_from_server\0restart_conection\0"
    "send_message\0mode\0slot_siete_elementos\0"
    "laminar\0turbulento\0turbulento_lam\0"
    "regimen_flujo\0por_agua\0por_oil\0por_gas\0"
    "watercut\0send_message_data\0value_1\0"
    "value_2\0response_to_monitoring\0response"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_jpttcpclient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    1,   60,    2, 0x06 /* Public */,
       5,    2,   63,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   68,    2, 0x0a /* Public */,
       9,    0,   69,    2, 0x0a /* Public */,
      10,    1,   70,    2, 0x0a /* Public */,
      12,    8,   73,    2, 0x0a /* Public */,
      21,    2,   90,    2, 0x0a /* Public */,
      24,    1,   95,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QStringList,    4,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QStringList,    6,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   13,   14,   15,   16,   17,   18,   19,   20,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   22,   23,
    QMetaType::Void, QMetaType::QString,   25,

       0        // eod
};

void jpttcpclient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        jpttcpclient *_t = static_cast<jpttcpclient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->closed_conection(); break;
        case 1: _t->new_entry((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 2: _t->new_json_entry((*reinterpret_cast< QStringList(*)>(_a[1])),(*reinterpret_cast< QStringList(*)>(_a[2]))); break;
        case 3: _t->query_from_server(); break;
        case 4: _t->restart_conection(); break;
        case 5: _t->send_message((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->slot_siete_elementos((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8]))); break;
        case 7: _t->send_message_data((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 8: _t->response_to_monitoring((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (jpttcpclient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jpttcpclient::closed_conection)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (jpttcpclient::*_t)(QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jpttcpclient::new_entry)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (jpttcpclient::*_t)(QStringList , QStringList );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jpttcpclient::new_json_entry)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject jpttcpclient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_jpttcpclient.data,
      qt_meta_data_jpttcpclient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *jpttcpclient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *jpttcpclient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_jpttcpclient.stringdata0))
        return static_cast<void*>(const_cast< jpttcpclient*>(this));
    return QObject::qt_metacast(_clname);
}

int jpttcpclient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void jpttcpclient::closed_conection()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void jpttcpclient::new_entry(QStringList _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void jpttcpclient::new_json_entry(QStringList _t1, QStringList _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
