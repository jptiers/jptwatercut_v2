﻿#JPT WATERCUT  Version 2.0




##INTRODUCCION 

El documento presenta en forma general la arquitectura y el funcionamiento del sistema o aplicativo **JPT-WATERCUT**.
El sistema **JPT-Watercut** esta diseñado bajo los estandares de desarrollo de Widgets, presentes en el entorno de desarrollo de **QT-Creator**  en su version de software libre.

EL sistema **JPT-Watercut**, esta diseñado con el fin de medir el **corte de agua** presente en un ducto de petroleo.(El corte de agua, es la cantidad de agua presente en el fluido dentro del tubo). 

El documento y sistema actual estan enfocados el software de medicion; El documento no aborda los estandares de desarrollo y funcionamiento del Hardware. Por conceptos generales se hara un bosquejo de la arquitectura del hardware de recepcion de datos. 

##ESTADO DEL ARTE DE LA VERSION 1


Antes de iniciar con el desarrollo de la version 2 del sistema JPT-Watercut, se contaba con una version anterior la cual usaba una arquitectura de datos TCP, para recepcionar parametros de entrada, realizar un analisis bajo un modelo matematico y entregar por medio de TCP una respuesta acorde a la entrada. 

De la primera version no se encontro documentacion a fondo del funcionamiento o desarrollo de la misma.  Existe dentro del repositorio de BItbucket de la empresa dos versiones **JPTWATERCUT** y **JPTWATERCUT_M** . Luego de analizar ambas versiones del software se percato que ambas versiones constan de la misma informacion.Para fines de desarrollo se puede tomar cualquiera de las dos. 

El sistema Watercut es un sistema de analitica de datos, el cual consume y escribe datos por medio del protocolo TCP; la lectura y escritura se realiza en el puerto 4000 por defecto.

Para que los datos lleguen hasta el sistema de analisis de corte de agua, se tiene un sistema de nombre **JPT-Monitoring** el cual se encarga de recepcionar los datos que llegan desde el hardware, enviarlos al JPT-Watercut para su analisis, recibir la respuesta de analisis del JPT-Watercut y almacenar los datos crudos de entrada y la respuesta en una base de datos; La base de datos se encuentra bajo la arquitectura de SQLITE. 

La arquitectura del sistema en general de la version uno se puede observar en la imagen **"arquitectura_basica_monitoring_and_watercut"** la cual se encuentra dentro de la carpeta imagenes de sistema. 

##ARQUITECTURA DEL SOFTWARE **JPT-WATERCUT**

La arquitectura del sistema en la nueva version del software, mantiene los estandares generales es decir:

El sistema depende aun de un elemento denominado JPT-Monitoring el cual tiene como funcion recepcionar la data proveniente desde los sensores en el dispositivo, para su manejo respectivo,
El dispositivo  en nuestro caso es una BeagleBoard en su version **Beagle Bone Black** , 

El elemento de recepcion de datos es un FTDI, el cual se encarga de transformar una señal serial a usb. Lo anterior significa que el sistema JPT-Monitoring esta leyendo de forma programada un puerto de entrada usb.Es preciso tener en cuenta este dato para temas de desarrollo. 

El sistema JPT-Monitoring continua comunicandose con el widget de manera rutinaria bajo el protocolo TCP; El puerto de lectura y escritura del sistema continua siendo el puerto 4000. 

basado en lo anterior se aborda plenamente el sistema **JPT-WATERCUT**

##Desarrollo en capas del sistema
A continuacion se presenta el esquema de la arquitectura en capas del sistema

Esl esquema se encuentra en la carpeta "Imagenes del sistema " con el nombre de **"MODELO EN CAPAS DE LA ARQUITECTURA DEL SOFTWARE JPT WATERCUT V2"**

las capas del sistema actual son : 

####**Capa de presentacion:** Esta capa contiene los diferentes procesos que permiten el desenvolvimiento de la interfaz grafica, user interface o **UI**, 
EL sistema JPT-Watercut, como se menciono anteriormente esta construido bajo los esquemas de desarrollo de widgets del Frameaork QT Creator. Se recuerda que QT es un framework de desarrollo grafico de bajo nivel. 
El sistema actual cuenta con un modelado grafico el cual se encuentra diseñado en QT Designer  denominado JPT watercut UI.Este modelo  depende la arquitecura de QT Ui para su correcto funcionamiento, la actualizacion del esquema grafico JPTWatercut Ui, se encuentra distrbuido mediante processos de controll en todo el desarrollo y dichos proceoss depende del funcionamiento de QT Connect. 

**QT Connect** 
Qt Connect es un modulo de manejo de informacion y procesos, un desapachador de señales o emisor de mensajes. En el esquema actual, el systema de QT connect esta configurado para desarrollar la funcion de despachador de señales y activacion de trigers de procesos, lo anterior permite el funcionamiento de cada capa del modelo en rutinas de recepcion y transmison de datos, 
se puede obtener mas informacion en : [Qt Connect](https://doc.qt.io/qt-5/signalsandslots.html) 

En resumen, las dieferentes funciones, botoness o cambios dentro de la parte grafica estan asociados directamente al modelo grafico,JPT-Watercut Ui, el cual funciona bajo Qt Ui, este sistema refleja los cambios en el controllador grafico del sistema, el cual se encarga de emitir una señal  relativa al proceso a desarrollar hasta el punto, funcion, espacio de memoria o proceso, que dependa de ella para realizar un cambio. De la misma manera cuando el proceso terimna enviara una señal de retorno hasta el controllador el cual dara la orden al QT ui para actualizar, cambiar o modificar el modelo, JPT Watercu ui. 

**NOTA:** para el caso de desarrollo la parte grafica se puede desahabilitar por medio de la funcion **#GUI_ON** de la libreria **Sentence.H**(para desabilitar comentar la cabecera #GUI_ON)

####**Capa de procesos :** Capa encargada del calculo de la respuesta de corte de agua

Esta capa recibe la informacion por medio de la lectura de señales provenientes de la capa de datos, estas señales seon manejadas mediante QT Connect , las señales con la infrmacion de entrada son tomadas por el proceso o clase  principal del JPTWatercut,(para fines de sarrollo este proceso se encuentra dentro del elemento **jptwatercut.cpp**)

**JPTWATERCUT:** 
Esta clase se encarga de leer la señal de entrada y vectorizar la informacion, luego la almacena en un puntero de memoria. para  realizar los diferenes procesos que de esta informacion dependan. .

NOTA: Para fines de nomenclatura y desarrollo, la informacin de entrada se denomina **informacion RAW** 

El orden de procesos realizados con la informacion son: 

	1.  Normalizacion.
	2.  Calibracion.
	3.  Calculo de los Modelos

		-  Calculo del Regimen de Flujo.
		-  Calculo del Corte de Agua.
		-  Envio o emision de la informacion.



-----------------------------

### **Normalizacion:**
La normalizacion es el proceso de encargado de tomar la informacion RAW del sistema y aplicar una funcion aditiva, a esta informacion, 

La informacion de entrada, es informacion que envia un sistema denominado **JPT-Monitoring**, esto se menciono con anterioridad, la informacion proveniente desde el sistema no tiene ningun tipo de proceso de correccin. Por diferentes estudios realizados a la primera version del JPT Watercut, se concluye que factores como la salinidad y compuestos dentro del agua hacen que las lecturas de los sensores  varien dependiendo la zona feografica donde se encuentren, por esta razon, se decide realizar un proceso el cual se denomina **Normalizacion de Agua**. 


El proceso de normalizacion de agua consiste en capturar la informacion del sistema, cuando el fluido que corre a traves de el es agua. Dicha informacion se varia aumentado o disminuyendo sus valores con el fin que cada sensor se encuentre alrededor de 5500cuentas,
 

**NOTA:** para obtener mas informacion acerca del proceso de normalizacion, la explicacin del mismo se encuentra dentro codigo del JPT Watercut.
Para el proceso de normalizacion se posee una interfaz grafica dentro del modelo Jpt Watercut UI, la cual permite la comunicacion con el proceso de normalizacion, esta comunicacion es bidireccional  y funciona por medio de QT Connect

El proceso de normalizacion esta diseñado para realizarce al momento de la instalacion del dispositivo, estos datos quedan almacenados dentro de un archivo .txt en el sistema. Para el proceso de almacenamiento se usa QT Connect para realizar un mensaje de cambio de informacion a los procesos de la capa de datos. 
**NOTA:**  para fines de desarrollo la carga y actualizacion de los datos se encuentra referenciada en el sistema JPT Watercut  como "Update_normalize " y "charge_normalize"

Cuando el software ya se encuentra con parametros de normalizacion, estos parametros se aplicaran a la informacion RAW del sistema por defecto. Existe dentro de la interfaz grafica un elemento denominado **Data Raw** en el cual se apecia la informacion de entrada y existe un elemento denominad **Data Raw +Normalize** en el cual se encuentra referenciada la informacion con los valores aditivos provenientes de la normalizacion. 

**NOTA** El sistema actual posee dos tipos de sensores capacitancia y resistividad, para cada uno de ellos debemos de realizar el proceso de normalizacion. 

-------------------------------
###**Calibracion:**

El sistema de calculo de corte de agua denominado JPT-Watercut, depende de una funcion de transferencia **"STF (specific transfer function )"**, la cual tiene como respuesta el corte de agua, es decir, la cantidad de agua en el fluido. 

Por medio de la primera version del software se percato que el sistema y la funcion de transferencia dependen de factores como lo son la salinidad del agua, y los compuestos o minerales dentro del fluido. Para la nueva version fue necesario definir unas nuevas funciones de transferencia, las cuales dependen o se asemejan a las funciones anteriores. A diferencia de la primera funcion donde se usaba siempre la misma funcion de transferencia, en la segunda version esta funcion es variable y depende de unos puntos de calibracion. 


Como se veio anteriormente el sistema posee un estandar o proceso para realizar una nonrmalizacion de los sensores cuando el fluido que pasa por el es agua. Nuestro primer punto de calibracion es el **punto de agua **,

**NOTA:** El punto de agua da el valor de normalizacion , es decir, si el sistema se normaliza a un valor diferente al a 5500 cuentas, el punto de calibracion de agua debe teoricamente ser similar a este valor, por conceptos tecnicos muchas veces el sistema puede normalizarce aun valor my cercano no exactamente el mismo para tener una aproximacion mayor en el calculo. 

Nuestros puntos de calibracion adicionales seran:

**1.  Calibracion en Aire:**

valor promedio de todos los sensores de la herramienta sin tener ningun fluido dentro de 	ella. 	Este valor 	oscila 16000 cuentas.

**2.  Calibracin de Petroleo o Gas:**

 valor promedio de los sensores de la herramienta al momento de tener crudo de 	petroleo dentro de ella. 

**3.  Calibracion de formaciones de agua:**
Las formaciones de agua, son fluidos los cuales poseen una cantidad significativa de minerales o compuestos 	adicionales en ella. Este tipo de agua se extrae directamente de la formacion geologica donde esta ubicado el pozo de petroleo.

**NOTA:** El sistema posee dos clases de sensores capacitancia y resistividad, son 6 sensores de cada uno. para cada uno de ellos se debe  aplicar la respectiva calibracion, 

A diferencia de la primera version del sistema la segunda version posee una calibracion adicional:

**1. Calibracion alto corte y bajo corte:** Esta calibracion generara una curva adicional la cual se usara cuando el regimen de flujo, es decir la velocidad del  tipo de fluido que se encuentra en el sistema. posee unas caracterisitcas especificas.

**2. Calibracaion del sistema de tres fases**: Esta calibracion permite calcular la variacion entre petroleo y gas en el sistema. El sistema y su funcionamiento depende de seis sensores capacitivos y  seis sensores resistivos, Estos sensores permiten diferenciar plenamente agua de otro tipo de fluido, para fines del sistema es necesario diferenciar Gas de CrudoPetroleo, pero actualmente los sensores miden valores muy similares para gas y para petroleo, por ello se define una funcion de transferencia que depende de un punto maximo de Gas y un punto minimo de Petroleo, lo que permite general un estandar de porcentaje de factibilidad de datos para definir si el tipo de fludo es Gas o Crudo de Petroleo. 

Para fines del desarrollo del software: La comunicacion entre el proceso calibracion con la parte grafica se encuentra definida bidireccionalmente por medio de QT Connect. De igual manera que el proceso de normalizacion el proceso de calibracion posee unos parametros por defecto y solo interactua con la parte grafica cuando se desea que estos parametros varien. 

La obtencion y cambio de los parametros de calibracion se realiza por medio del envio de una señal hacia un controlador en la capa de datos, la señal se envia al controlador or medio de Qt Connect, si la señal es de lectura el controlador retornara la informacion de calibracion del sistema. 

-------------------------------------------------

###**Calculo de los Modelos:**

Actualmente se menciono en el documento que el sistema de calculo de corte de agua en su primera version depende de una funcion de transferencia especifica, en cambio esta nueva version dependera de una funcion de transferencia variable segùn los parametros de calibracion. 


El calculo del modelo tomara los valores de calibracion, calibracion de alto corte y bajo corte y calibracion del sistema de tres fases, y generara una funcion de tranferencia especifica para cada uno de los modelos de calibracion. 

El sistema de calculo de modelos es el encargado de enviar  la informacion resultante a Qt Connect, el cual se encargara de despachar esta informacion al proceso de emision de datos, el cual retornara la informacion al sistema **JPT-Monitoring** y  al proceso de guardado de informacion en la base de tipo SQLITE. 

EL sistema de calculo del modelo depende de un controlador de informacion y de un elemento de calculo de regimen de flujo. 

Regimen de Flujo: 
Depende de valores especificos de cada sensor, los cuales pueden un porcentaje determinado de variacion o indice de error. 
Actualmente se manejan cuatro **Regimenes de Flujo**:

	1. RegimenTurbulento.
	2. Regimen Laminar.
	3. Regimen Turbulento Laminar.
	4. Regimen Emulsion

**NOTA**: Mas informacion sobre cada uno de los regimen se puede consultar en la ficha **Regimenes de Flujo** dentro de la carpeta **Fichas del sistema**.


Luego de que el sistema detecta el tipo de regimen de flujo, se encarga de almacenar este valor dentro de una variable del sistema, El proceso de Calculo de Modelo es el encargado de consultar este valor y posteriormente usar el controllador del proceso para enviar este valor hacia el Qt Connect, y que los mensajes se despachen  hacia su correspondiente destino.

En resumen: El sistema de calculo de modelo toma los valores los caules han pasado por un proceso de normalizacion previamente y le aplica a estos valores una funcion de transferencia. Esta funcion depende de los puntos de calibracion definidos en el sistema. Parar las caracteristicas del tipo de regimen existe un proceso de calulo de regimen de flujo, el cual analiza los valores  ya normalizados en el sistema y determina el regimen de flujo que se debe aplicar, dependiendo del tipo de regimen de flujo el sistema envia un corte de agua especifico. 

--------------------------------

###**Capa de Datos**: Encargada del manejo de las  diferenes secuencias  **CRUD** a la base de datos y a los archivos de texto planos. Asi mismo es el encargado de la recepcion y transmision de datos entre el JPTWATERCUT  y el JPTMONITORING

**NOTA:** **CRUD** es un acronimo de **"Crear Leer Actualizar y Borrar"**

Para la parte del almacenamiento  sistema Jpt Watercut, implementa una base de datos en **SQLITE**, la cual almacena parametros de configuracion interna y una copia de la informacion de entrada junto con la respuesta a esta informacion, es decir, el valor de cada sensor y su respectivo corte de agua. la informacion es almacenada por medio de un identificador y un tiempo de llegada. 

Aparte de la base de datos, el sistema cuenta con archivos texto plano que almacenan las variables de calibracion y normalizacion del sistema. 

Las conexiones a la base de datos Sqlite son manejadas por medio del  systema o librerias internas de Qt las cuales poseen un odb para la respectiva conexion y manejo de datos. 

**NOTA:**Al momento de desarrollo del software es preciso tener en cuenta que el sistema instala por defecto las librerias de Qt para MySql, se debe asegurar  las librerias de Sqlite estan instadas en el sistema de lo contrario siempre habra un error de directorio o archivo no encontrado. 

La capa de datos esta configurada para recibir señales por medio del sistema de Qt Connect,es decir, Qt Connect se encarga de realizar la persistencia de datos a un controlador especifico. Las señales que provienen desde Qt Connect se  transmiten a un contraolador diseñado para entender la informacion de entrada. En resumen el controlador recibe  el tipo de secuencia CRUD y los datos de la secuencia respectivamente. Esta secuencia se comunica al controlador interno de Qt  para realizar el respectivo proceso. 

Toda persistencia a la base de datos tiene una respuesta, ya sea de informacion de consulta de datos o de verificacin sobre la llegada de algun tipo de secuencia CRUD. Las respuestas de cada consulta son enviadas por medio de Qt Connect al sistema respectivo que hizo la solicitud. 

**NOTA:** Para fines de desarrollo es preciso decir aclarar que el sistema esta configurado para detener su funcionamiento si no se logra crear una conexion y persistencia adecuada con los elementos de la base de datos. 

Las conexiones a los archivos planos de texto son manejados por medio del sistema Qt Files , el cual permite manejar las funciones  CRUD dentro de diferentes tipos de archivos. EL manejo del texto plano, se diseño sin un protocolo de almacenamiento  de datos, como lo puede ser JSON o XML, simplemente se realiza una escritura linean, con un separador especifico entre elementos, donde cada elemento es un dato del sistema. Para un entendimiento mas simple se puede tomar como un tipo de texto separado por Tabulaciones. 

El manejo del controlador CRUD de los archivos de texto plano depende unicamente de las consultas o requerimientos que provienen del Qt Connect cuando una capa superior lo solicita. De igual manera que en la conexion de la base de datos, el sistema siempre espera una respuesta sobre la secuencia CRUD que se realiza, la respuesta puede ser algun tipo informacion o confirmacion. 

A diferencia del sistema de persistencia en la base de datos, el sistema de almacenamiento de archivos planos no depende del archivo para su funcionamiento, en caso de que exista carencia de estos elementos, el sistema tomara valores por defecto que se encuentran intrinsecos en el. 


**NOTA:** Para fines de desarollo, si fuese necesario duplicar las caracteristicas de configuracion de un sistema a otro, la forma mas factible es copiar la base de datos de configuracion y los archivos de texto plano de configuracion y reemplazarlo dentro del segundo sistema, de esta manera el segundo sistema tendra una configuracion identica a la del primero. 

--------------


**NOTA*:* Para mayor informacion dirigase a las fichas tecnicas del sistema, dentro de la carpeta **Fichas Tecnicas del Sustema**

































































