#include "jptwatercut.h"
#include "ui_jptwatercut.h"


#define interfaz        ui

#define def_fx_width    763
#define def_fx_heigth   704

#define def_idx_sali_6  6
#define def_idx_sali_1  1

#ifdef _FILE_
    int a_counter_id = 1220;
#endif

QVector<int> gv_cap_sensor_hcut;
QVector<int> gv_res_sensor_hcut;

double tur_med(0), lam_med(0);

double factor = 0.0;
const QString query = "(temp,sali,lam_cap,lam_res,med_lam,tur_cap,tur_res,med_tur)VALUES('";

QVector<QLCDNumber *> c_res_sensor_display;
QVector<QLCDNumber *> c_cap_sensor_display;

QVector<QStringList> gv_values_sensors_entry;       // Almacena los valores provenientes de JPTMonitoring
bool gv_init_conection_JPTMonitoring = false;       // Si han ingresado valores al widget de JPTMonitoring

int gv_salinity_res_1(0);                           // Salinidad por sensor resistivo 1 [0]
int gv_salinity_res_6(0);                           // Salinidad por sensor resistivo 6 [5]

QVector<QStringList >   gv_data_params;

#define def_interval_tcp_connect 4000
QString source_db_path;//guarda el path de la base de datos que manejaremos
QString pack_to_send;//se configuran los datos que se enviaran a la base de datos.
int f_pack;
int reg_pack;
int total_trans;

int id_node_1_last;
//puntero de guardado de la informacion de la matrix de datos
QDateTime my_vector_time_node_1[999999];
QDateTime my_vector_time_node_2[999999];
QDateTime my_vector_time_node_3[999999];
QDateTime my_vector_time_node_4[999999];
QDateTime my_vector_time_node_5[999999];
QDateTime my_vector_time_node_6[999999];
QDateTime my_vector_time_node_7[999999];
QDateTime my_vector_time_node_8[999999];
int my_matrix_node_1[999999][2];
int my_matrix_node_2[999999][2];
int my_matrix_node_3[999999][2];
int my_matrix_node_4[999999][2];
int my_matrix_node_5[999999][2];
int my_matrix_node_6[999999][2];
int my_matrix_node_7[999999][2];
int my_matrix_node_8[999999][2];

int lengh_node_1=0;
int lengh_node_2=0;
int lengh_node_3=0;
int lengh_node_4=0;
int lengh_node_5=0;
int lengh_node_6=0;
int lengh_node_7=0;
int lengh_node_8=0;

//validacin del sistema

//realizamos el captura de datos
//para ello se crean variables para guardar los datos de normalizacion
float res_data_raw[6];
float cap_data_raw[6];
//valor de cuentas a agregar
int delta_normalization[12];

//calibration process
int count_calib=0;//indice de dato entrante
int state_calib=0;//estado de la captura.
int calib_matrix[12][3];//es uan matriz de los 12 nodos y cada uno de estos tendra 3 valores de calibracion.
int calibration_values[8];

//proceso de refactorizacion de datos
int refactor=0;
int runner_factor=1;
//proceso del modelo.
double wc_sensor_res[6];
double wc_sensor_cap[6];
//valores de correccion ->1 significa false y 2 significa true.
int correcciones[4];
int hl_wc_values[12];





//************************************************************************************************************************************************************************************************************************************************************
// Constructor
//************************************************************************************************************************************************************************************************************************************************************
#ifdef _COMPILE_GUI
jptwatercut::jptwatercut(QWidget *parent) : QMainWindow(parent), interfaz(new Ui::jptwatercut){

    interfaz->setupUi(this);    
    setFixedSize(def_fx_width, def_fx_heigth);
    charge_delta_normalization();

   // ////qDebug() << "line inside>> "<<QString::number(delta_normalization[3]);
   //primero valido el checker del sistema
    //>>termina la validacion
    //conexion de señals secundarias de la parte grafica
    connect(ui->charge_data, SIGNAL(clicked()), this, SLOT(data_connect()));
    connect(ui->select_folder,SIGNAL(clicked()),this,SLOT(duplicate_db()));
    connect(ui->captura_data, SIGNAL(clicked()), this, SLOT(capture_data_to_normalize()));


    connect(ui->calcule_def_dlt, SIGNAL(clicked()), this, SLOT(calcule_default_delta()));
    connect(ui->calcule_man_dlt, SIGNAL(clicked()), this, SLOT(calcule_manual_delta()));
    connect(ui->calcule_auto_dlt, SIGNAL(clicked()), this, SLOT(calcule_auto_delta()));


    connect(ui->calcule_def_dlt_2, SIGNAL(clicked()), this, SLOT(calcule_default_delta_r()));
    connect(ui->calcule_man_dlt_2, SIGNAL(clicked()), this, SLOT(calcule_manual_delta_r()));
    connect(ui->calcule_auto_dlt_2, SIGNAL(clicked()), this, SLOT(calcule_auto_delta_r()));


    connect(ui->update_dlt_val, SIGNAL(clicked()), this, SLOT(update_delta_normalization()));
    connect(ui->update_dlt_average, SIGNAL(clicked()), this, SLOT(update_delta_average()));
    //calibraton process
    connect(ui->captura_data_2, SIGNAL(clicked()), this, SLOT(setup_capture_calib()));
    charge_calibration_values();//cargo los datos de calibracion desde el calib_values.txt en foder External.
    //cargo las curvas del sistema:
    ////qDebug("curve res ");
    curve_res();
    ////qDebug("curve cap ");
    curve_cap();
    charge_hl_wc_values();
    //qDebug("curve high and low ");
    hwc_curve_res();
    hwc_curve_cap();
    low_curve_cap_res();
    //qDebug("1");
    ui->refactor->setEnabled(false);
    ui->charge_data->setEnabled(true);
    ui->Group_set_parameters->setEnabled(false);


    ui->Group_set_parameters_2->setEnabled(false);
    ui->result_obj->setEnabled(false);

    connect(ui->avg_oil, SIGNAL(clicked()), this, SLOT(avg_cali_oil()));
    connect(ui->avg_air, SIGNAL(clicked()), this, SLOT(avg_cali_air()));
    connect(ui->avg_w, SIGNAL(clicked()), this, SLOT(avg_cali_w()));
    connect(ui->avg_fw, SIGNAL(clicked()), this, SLOT(avg_cali_fw()));
    //calculo de las curvas de valores:
    connect(ui->calcule_curves, SIGNAL(clicked()), this, SLOT(my_curves()));
    //parte grafica , activacion de correcciones
    connect(interfaz->RB_TP_CO,SIGNAL(stateChanged(int)),this,SLOT(change_correct_temp_on_off(int)));
    connect(interfaz->RB_ST_CO,SIGNAL(stateChanged(int)),this,SLOT(change_correct_sali_on_off(int)));


    //process refactor data.
    //ui->start_refactor->setEnabled(false);
    connect(ui->start_refactor, SIGNAL(clicked()), this, SLOT(process_refactor()));
    connect(ui->persistant, SIGNAL(clicked()), this, SLOT(persistan_refactor()));
    //coneccion de la parte grafica con la calibracion de alto y bajo corte

    connect(ui->value_hwc_up,SIGNAL(valueChanged(int)),this,SLOT(value_hwc_up_change(int)));
    connect(ui->value_hwc_dw,SIGNAL(valueChanged(int)),this,SLOT(value_hwc_dw_change(int)));
    connect(ui->value_hwc_up_2,SIGNAL(valueChanged(int)),this,SLOT(value_hwc_up_2_change(int)));
    connect(ui->value_hwc_dw_2,SIGNAL(valueChanged(int)),this,SLOT(value_hwc_dw_2_change(int)));

    connect(ui->value_lwc_up,SIGNAL(valueChanged(int)),this,SLOT(value_lwc_up_change(int)));
    connect(ui->value_lwc_dw,SIGNAL(valueChanged(int)),this,SLOT(value_lwc_dw_change(int)));

    connect(ui->percent_hwc_up,SIGNAL(valueChanged(int)),this,SLOT(percent_hwc_up_change(int)));
    connect(ui->percent_hwc_dw,SIGNAL(valueChanged(int)),this,SLOT(percent_hwc_dw_change(int)));
    connect(ui->percent_hwc_up_2,SIGNAL(valueChanged(int)),this,SLOT(percent_hwc_up_2_change(int)));
    connect(ui->percent_hwc_dw_2,SIGNAL(valueChanged(int)),this,SLOT(percent_hwc_dw_2_change(int)));

    connect(ui->percent_lwc_up,SIGNAL(valueChanged(int)),this,SLOT(percent_lwc_up_change(int)));
    connect(ui->percent_lwc_dw,SIGNAL(valueChanged(int)),this,SLOT(percent_lwc_dw_change(int)));




    //conecto el vector de las nueva curvas con el resusltado
    connect(ui->calcule_curves_hl,SIGNAL(clicked()),this,SLOT(hlwc_curves()));
    connect(ui->calcule_curves_TFM,SIGNAL(clicked()),this,SLOT(calcule_three_phases()));

    //qDebug("all my model .... ");

#else
jptwatercut::jptwatercut(QObject *parent) : QObject(parent){
#endif

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de la base de datos y carga de valores                                              [jptdatabase]
    //----------------------------------------------------------------------------------------------------------------
    a_data_base = new jptdatabase(gv_name_data_base, gv_pass_data_base, gv_user_data_base, this);
    connect(this, SIGNAL(send_message_to_database(QString,QString)), a_data_base, SLOT(insert_register(QString,QString)));

#ifndef _FILE_
    a_data_base->get_values(def_pos_table_params, &gv_data_params);
#endif

    //configure_gui();
    charge_correct_sali_on_off();
    charge_correct_temp_on_off();
    charge_correct_tempval_on_off();
    //----------------------------------------------------------------------------------------------------------------
    // Timer que permite abrir la comunicacion con jptmonitorig TCP                            [QTimer - jpttcpclient]
    //----------------------------------------------------------------------------------------------------------------
    a_open_conection = new QTimer();
    a_open_conection->setInterval(def_interval_tcp_connect);
    //a_open_conection->setInterval(500);
#ifndef _FILE_
    a_open_conection->setSingleShot(true);
#else
    connect(a_open_conection, SIGNAL(timeout()), this, SLOT(interval_data_data_base()));
#endif
    a_open_conection->start();
#ifndef _FILE_
    //----------------------------------------------------------------------------------------------------------------
    // Comunicacion TCP con JPTMonitoring                                                               [jpttcpclient]
    //----------------------------------------------------------------------------------------------------------------
    a_tcp_client = new jpttcpclient(this);
    connect(a_open_conection, SIGNAL(timeout()), this, SLOT(start_communication()));                         // Cuando la senal del timer arranca se inicia la conexion con el servidor.
    connect(this, SIGNAL(start_connection(QString)), a_tcp_client, SLOT(send_message(QString)));             // Se genera una senal de conexion con el servidor y se envia un mensaje al servidor de inicio de trama, para comenzar a recibir.

#ifdef _TRAME_CC
    connect(this, SIGNAL(send_message(QString)), a_tcp_client, SLOT(send_message(QString)));                 // Cuando se desea enviar una trama a al servidor.
#endif

#ifdef _TRAME_JP
    connect(this, SIGNAL(send_message(QString, QString)), a_tcp_client, SLOT(send_message_data(QString,QString)));
#endif
    //las siguientes lineas connectan el tcp cliente con el acumlador de datos que lee la informacion del sistema.
    connect(a_tcp_client, SIGNAL(closed_conection()), this, SLOT(restart_communication()));                     // Si se emite la senal de cierre de conexion, se envia al SLOT que va a cerrar esta conexion.
    connect(a_tcp_client, SIGNAL(new_entry(QStringList)), this, SLOT(acumulator_entry_sensors(QStringList)));   // cuando se tiene un ingreso de nueva trama del servidor, se comenzara a acumular.
#endif

    connect(ui->SINF, SIGNAL(clicked()), this, SLOT(save_data_base()));

    //----------------------------------------------------------------------------------------------------------------
    // Configuracion de las senales y slots para la interfaz grafica                                      [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    connect(interfaz->CB_TP_SL,  SIGNAL(currentIndexChanged(int)), this, SLOT(change_temp_sensor(int)));
    connect(interfaz->CK_TP_F_HD,SIGNAL(stateChanged(int)), this, SLOT(change_temp_mode(int)));             // modo de correccion de la temperatura, fija o sensor.
    connect(interfaz->SD_TP_F_CH,SIGNAL(valueChanged(double)),this,SLOT(temp_fixed_change(double)));        // Cuando cambia el valor de temperatura fija
    connect(interfaz->CK_ST_F_HD,SIGNAL(stateChanged(int)),this,SLOT(change_ppm_mode(int)));             // modo de correccion de PPM, Fijo o Sensores
    connect(interfaz->SI_ST_F_CH,SIGNAL(valueChanged(int)),this,SLOT(ppm_fixed_change(int)));               // Cuando cambia el valor de PPM fijo.
    connect(interfaz->BT_GE_GR, SIGNAL(clicked()), this, SLOT(gen_graphic()));
#ifdef _FLOW_
    connect(interfaz->SD_FL_F_CH,SIGNAL(valueChanged(double)),this, SLOT(flow_fixed_change(double)));
#endif
    connect(interfaz->SI_PO_CH, SIGNAL(valueChanged(int)), this, SLOT(porc_fixed_change(int)));
    //----------------------------------------------------------------------------------------------------------------
    // Cargar los parametros de los sensores [6]Temperatura y [7]Presion                                [jptabssensor]
    //----------------------------------------------------------------------------------------------------------------
    charge_param_sensor_temp_and_press();
    //----------------------------------------------------------------------------------------------------------------
    // Cargar los parametros de los sensores Resistivos y Capacitivos; n=0,...,5.          [jptressensor-jptcapsensor]
    //----------------------------------------------------------------------------------------------------------------
    charge_param_sensor_resi_and_capac();
    //----------------------------------------------------------------------------------------------------------------
    // Almacenar los display que contendran los valores ing. de cada sensor                               [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    c_res_sensor_display.append(interfaz->LD_RE_1);
    c_res_sensor_display.append(interfaz->LD_RE_2);
    c_res_sensor_display.append(interfaz->LD_RE_3);
    c_res_sensor_display.append(interfaz->LD_RE_4);
    c_res_sensor_display.append(interfaz->LD_RE_5);
    c_res_sensor_display.append(interfaz->LD_RE_6);

    c_cap_sensor_display.append(interfaz->LD_CA_1);
    c_cap_sensor_display.append(interfaz->LD_CA_2);
    c_cap_sensor_display.append(interfaz->LD_CA_3);
    c_cap_sensor_display.append(interfaz->LD_CA_4);
    c_cap_sensor_display.append(interfaz->LD_CA_5);
    c_cap_sensor_display.append(interfaz->LD_CA_6);


    c_res_sensor_display.append(interfaz->LD_RE_T1);
    c_res_sensor_display.append(interfaz->LD_RE_T2);
    c_res_sensor_display.append(interfaz->LD_RE_T3);
    c_res_sensor_display.append(interfaz->LD_RE_T4);
    c_res_sensor_display.append(interfaz->LD_RE_T5);
    c_res_sensor_display.append(interfaz->LD_RE_T6);

    c_res_sensor_display.append(interfaz->LD_RE_S1);
    c_res_sensor_display.append(interfaz->LD_RE_S2);
    c_res_sensor_display.append(interfaz->LD_RE_S3);
    c_res_sensor_display.append(interfaz->LD_RE_S4);
    c_res_sensor_display.append(interfaz->LD_RE_S5);
    c_res_sensor_display.append(interfaz->LD_RE_S6);



    c_cap_sensor_display.append(interfaz->LD_CA_T1); //+6
    c_cap_sensor_display.append(interfaz->LD_CA_T2);
    c_cap_sensor_display.append(interfaz->LD_CA_T3);
    c_cap_sensor_display.append(interfaz->LD_CA_T4);
    c_cap_sensor_display.append(interfaz->LD_CA_T5);
    c_cap_sensor_display.append(interfaz->LD_CA_T6);

    c_cap_sensor_display.append(interfaz->LD_CA_S1); //+12
    c_cap_sensor_display.append(interfaz->LD_CA_S2);
    c_cap_sensor_display.append(interfaz->LD_CA_S3);
    c_cap_sensor_display.append(interfaz->LD_CA_S4);
    c_cap_sensor_display.append(interfaz->LD_CA_S5);
    c_cap_sensor_display.append(interfaz->LD_CA_S6);

    //----------------------------------------------------------------------------------------------------------------
    // Inicializar con el sensor de temperatura NT-1000                                                   [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    change_temp_sensor(interfaz->CB_TP_SL->currentIndex());

    //----------------------------------------------------------------------------------------------------------------
    // Clase para obtener el modelo de mezclas                                                            [MainWindow]
    //----------------------------------------------------------------------------------------------------------------
    new_model = new jptmodel(this);

#ifdef _COMPILE_GUI
    a_real_porcentaje_variance = interfaz->SI_PO_CH->value();
#else
    a_real_porcentaje_variance = def_porcentaje_varianza;
#endif

    gen_graphic();



}
//******************************************************************************************************************************
//process refactor
//******************************************************************************************************************************
void jptwatercut::process_refactor()
{   
    /*
    QString _path;
    _path=ui->folder_txt->text()+"/"+ui->dest_db_label->text();
    //qDebug(_path.toLatin1());
    */


    QString _path;

    //_path="/home/zahall/Escritorio/prueba2/jptdatabase_refactor_viemar108:12:322019.db";
    //_path="/home/zahall/Escritorio/prueba4/jptdatabase_refactor_viemar109:24:582019.db";
    //_path="/home/zahall/Escritorio/final/jptdatabase_refactor_juemar715:18:572019.db";
    _path="/home/zahall/Escritorio/final_data2/jptdatabase_refactor_sábmar908:10:052019.db";

     connect_db(_path);
    QSqlQuery _query;
    QString _data="select * from nodex where id="+QString::number(runner_factor);//se crea la sentencia de llamado de la informacion
    _query.prepare(_data);
    _query.exec();
     //qDebug("hata aqui va bien. ");
    if(_query.next())//si existe el valor
    {
        //qDebug("sie entro en la sentencia ");
        //qDebug("///"+_query.value(1).toString().toLatin1());
        //qDebug("sie entro en la sentencia ");
         QStringList _entry1,_entry2,_entry3,_entry4,_entry5,_entry6,_entry7;

        _entry1<<"0001"<<_query.value(2).toString()<<_query.value(4).toString();
        _entry2<<"0002"<<_query.value(6).toString()<<_query.value(8).toString();
        _entry3<<"0003"<<_query.value(10).toString()<<_query.value(12).toString();
        _entry4<<"0004"<<_query.value(14).toString()<<_query.value(16).toString();
        _entry5<<"0005"<<_query.value(18).toString()<<_query.value(20).toString();
        _entry6<<"0006"<<_query.value(22).toString()<<_query.value(24).toString();
        _entry7<<"0007"<<_query.value(26).toString()<<_query.value(28).toString();




       for(int i=0;i<7;i++)
       {   //qDebug("entro al for.");
           if(i==0)
           {
               process_new_entry_sensors(_entry1);
           }
           else if(i==1)
           {
               process_new_entry_sensors(_entry2);
           }
           else if(i==2)
           {
               process_new_entry_sensors(_entry3);
           }
           else if(i==3)
           {
               process_new_entry_sensors(_entry4);
           }
           else if(i==4)
           {
               process_new_entry_sensors(_entry5);
           }
           else if(i==5)
           {
               process_new_entry_sensors(_entry6);
           }
           else if(i==6)
           {
               process_new_entry_sensors(_entry7);
           }


       }

       //luego de que recibi todos los datos comienzo  realizar el proceso de calibracion.

       //lo primero es compensar por salinidad y flujo.
       //calculo la saldad del sistema,
       //cuando termina de cargar todos los datos reaizacmos el proceso de calculo por salidad en el sistema.
       //para ello ya no se necesita el valor de nodo. Para ello tomamos simplemente siempre el valor de nodo 1
       determine_real_salinity(1);
       //si hemos terminado de verificar el proceso de salinidad procedemos a calcular el valor del Corte por Sensor.
       wc_per_sensor_res();//calcula el valor por sensor de la resitividad
       wc_per_sensor_cap();//calcula valores por sensor de la capacitancia
       //qDebug()<<"programade mierda si funcionaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
       calcule_regimen_flow();
       calcule_models();//calcula los valores de laminar y turbulento.



       if(state_calib==1)
       {
       capture_data_calibration();
       }

    }
      
    //cuando termina todo el proceso lo que hace es aumentar el elemento del runner en 1
    runner_factor++;
    
}

void jptwatercut::persistan_refactor()
{
    //se supone que la bdase de atos ya se encuentra abierta.
    runner_factor=ui->per_init->value();//inicio el runner en  0
    //ahora genero un ciclo para el muestreo menor a la cantidad de datos a analizar
    while(runner_factor<=ui->per_fin->value())
    {
        //aca hace el proceso de persistencia al process refactor.
        process_refactor();


    }

}

//******************************************************************************************************************************
// Configurar la interfaz con los valores de la base de datos
//******************************************************************************************************************************

void jptwatercut::configure_gui(){
    ui->CB_TP_SL->setCurrentIndex(gv_data_params[0][def_pos_sensor_temp].toInt());

    if(gv_data_params[0][def_pos_apl_fx_temp].toInt()){
        ui->CK_TP_F_HD->click();
        change_temp_mode(2);
    }else
        ui->CK_TP_F_HD->setChecked(false);

    ui->SD_TP_F_CH->setValue(gv_data_params[0][def_pos_fx_val_temp].toDouble());

    ui->SD_FL_F_CH->setValue(gv_data_params[0][def_pos_fx_val_flow].toDouble());
    ui->SI_PO_CH->setValue(gv_data_params[0][def_pos_lim_flow].toInt());

    ui->CK_ST_HD_1->setChecked(gv_data_params[0][def_pos_apl_ppm_1].toInt());
    ui->CK_ST_HD_6->setChecked(gv_data_params[0][def_pos_apl_ppm_6].toInt());
    ui->CK_ST_F_HD->setChecked(gv_data_params[0][def_pos_apl_fx_ppm].toInt());
    ui->SI_ST_F_CH->setValue(gv_data_params[0][def_pos_fx_val_ppm].toInt());

    ui->SI_RETP_L_CH->setValue(gv_data_params[0][def_pos_lim_temp_res].toInt());
    ui->SI_CATP_L_CH->setValue(gv_data_params[0][def_pos_lim_temp_cap].toInt());
    ui->SI_REST_L_CH->setValue(gv_data_params[0][def_pos_lim_ppm_res].toInt());
    ui->SI_CAST_L_CH->setValue(gv_data_params[0][def_pos_lim_ppm_cap].toInt());


    ui->RB_FL_CO->setChecked(gv_data_params[0][def_pos_apl_cor_flow].toInt());

    ui->RB_EQ_HC->setChecked(gv_data_params[0][def_pos_apl_equ_hc].toInt());
    ui->LE_HC->setText(gv_data_params[0][def_pos_equ_hc]);
    ui->SB_X2->setValue(gv_data_params[0][def_pos_lim_hc_con_sup].toInt());
    ui->SB_X1->setValue(gv_data_params[0][def_pos_lim_hc_con_inf].toInt());

    ui->SB_Y2->setValue(gv_data_params[0][def_pos_lim_hc_por_inf].toInt());

    charge_correct_sali_on_off();
    charge_correct_temp_on_off();
}
//******************************************************************************************************************************
// Guardar la informacion en la base de datos
//******************************************************************************************************************************
void jptwatercut::save_data_base(){
    a_data_base->update_register("params", "sensor_temp='"     + QString::number(ui->CB_TP_SL->currentIndex())
    +   "',apl_fx_temp='"   + QString::number(ui->CK_TP_F_HD->isChecked())
    +   "',fx_val_temp='"   + QString::number(ui->SD_TP_F_CH->value())
    +   "',fx_val_flow='"   + QString::number(ui->SD_FL_F_CH->value())
    +   "',lim_flow='"      + QString::number(ui->SI_PO_CH->value())
    +   "',apl_ppm_1='"     + QString::number(ui->CK_ST_HD_1->isChecked())
    +   "',apl_ppm_6='"     + QString::number(ui->CK_ST_HD_6->isChecked())
    +   "',apl_fx_ppm='"	+ QString::number(ui->CK_ST_F_HD->isChecked())
    +   "',fx_val_ppm='"	+ QString::number(ui->SI_ST_F_CH->value())
    +   "',lim_temp_res='"	+ QString::number(ui->SI_RETP_L_CH->value())
    +   "',lim_temp_cap='"	+ QString::number(ui->SI_CATP_L_CH->value())
    +   "',lim_ppm_res='"	+ QString::number(ui->SI_REST_L_CH->value())
    +   "',lim_ppm_cap='"	+ QString::number(ui->SI_CAST_L_CH->value())
    +   "',apl_cor_temp='"	+ QString::number(ui->RB_TP_CO->isChecked())
    +   "',apl_cor_ppm='"	+ QString::number(ui->RB_ST_CO->isChecked())
    +   "',apl_cor_flow='"	+ QString::number(ui->RB_FL_CO->isChecked())
    +   "',apl_equ_hc='"	+ QString::number(ui->RB_EQ_HC->isChecked())
    +   "',equ_hc='"        + ui->LE_HC->text()
    +   "',lim_hc_con_sup='"+ QString::number(ui->SB_X2->value())
    +   "',lim_hc_con_inf='"+ QString::number(ui->SB_X1->value())
    +   "',lim_hc_por_inf='"+ QString::number(ui->SB_Y2->value())
    +   "'");

}
//******************************************************************************************************************************
// destructor
//******************************************************************************************************************************
jptwatercut::~jptwatercut(){
    delete interfaz;
}
//******************************************************************************************************************************
// Verificar que la trama se encuentra correcta
//******************************************************************************************************************************
bool jptwatercut::comprobar_nodes(){
    bool state(false);
    for(int f_va = 1; f_va < 7; ++f_va){
        state = false;
        for(int f_vae = 1; f_vae < 7; ++f_vae)
        {
            if(gv_values_sensors_entry[f_va].contains("000" + QString::number(f_vae + 1))){
                state = true;
                break;
            }if(f_vae == 6 && !state)
                break;
        }
        if(!state)
            break;
    }
    return state;
}
//******************************************************************************************************************************
// Gestiona los valores provenientes de JPTMonitoring {node_x value_1 value_2}                   [1]SIGNAL [jpttcpclient - SLOT]
//******************************************************************************************************************************
void jptwatercut::acumulator_entry_sensors(QStringList entry){
    gv_values_sensors_entry.append(entry);

    //si el primer dato no se registra como un valor de nodo 1 en la variable de entrada
    //entonces el sistema lipia por complet toda la trama
    if(!gv_values_sensors_entry[0].contains("0001"))
            gv_values_sensors_entry.clear();

    //se debe de tener la variable de cantidad de nosos igual a la cantida de trama que  ingresaron al sistema.
    if(gv_values_sensors_entry.size() == def_numer_nodes){
        if(comprobar_nodes()){

            gv_values_sensors_entry.crend();
            for(int i = (def_numer_nodes - 1); i > -1; i--)
            {
                process_new_entry_sensors(gv_values_sensors_entry[i]);
            }//cuando el ciclo acaba the process new entre cargo todos los valores en el sistema.
            //ahora es cuando se valida la calibracion. gracias a la varible state y a la varible count.

            //lo primero es compensar por salinidad y flujo.
            //calculo la saldad del sistema,
            //cuando termina de cargar todos los datos reaizacmos el proceso de calculo por salidad en el sistema.
            //para ello ya no se necesita el valor de nodo. Para ello tomamos simplemente siempre el valor de nodo 1
            determine_real_salinity(1);
            //si hemos terminado de verificar el proceso de salinidad procedemos a calcular el valor del Corte por Sensor.
            wc_per_sensor_res();//calcula el valor por sensor de la resitividad
            wc_per_sensor_cap();//calcula valores por sensor de la capacitancia
            ////qDebug()<<"programade mierda si funcionaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            calcule_regimen_flow();
            calcule_models();//calcula los valores de laminar y turbulento.





            if(state_calib==1)
            {
            capture_data_calibration();
            }


        }else
            emit send_message(QString::number(lam_med), QString::number(tur_med));

        gv_values_sensors_entry.clear();
    }
}
//******************************************************************************************************************************
// Funcion que procesa la trama {node_x, value_1, value_2} (viene del nodo 7 al 1)      [2][acumulator_entry_sensors:MainWindow]
//******************************************************************************************************************************
void jptwatercut::process_new_entry_sensors(QStringList values_entry){

    setup_normalization(values_entry);
    //primero esta el proceso de normalizacion
    int  node_index = values_entry[def_pos_num_node].toInt();
    //ahora va el proceso de calibracion.

    if(node_index < def_numer_nodes){
        calcule_param_sensor_resis(values_entry[def_pos_res_sensor].toDouble(), node_index);

        calcule_param_sensor_capac(values_entry[def_pos_cap_sensor].toDouble(), node_index);
          //determine_real_salinity(node_index);//calculo que deende del index solamente.

    }
    else if(node_index == def_numer_nodes)
    {
        calcule_param_sensor_temp_and_pres(values_entry, node_index);
    }


}
//******************************************************************************************************************************
// Setup_normalization
//******************************************************************************************************************************
void jptwatercut::setup_normalization(QStringList values_entry)
{
    int  node_index = values_entry[def_pos_num_node].toInt();
    int res_val=values_entry[def_pos_res_sensor].toInt();
    int cap_val=values_entry[def_pos_cap_sensor].toInt();
    //qDebug(">>>>>>valor del nodo entrando>>> "+QString::number(node_index).toLatin1());
    //qDebug("valor de resitencia>>> "+QString::number(res_val).toLatin1());
    //qDebug("valor de capacitancia>>> "+QString::number(cap_val).toLatin1());

    if(node_index==1)
    {
        ui->ld_my_r1->display(res_val);
        ui->ld_my_c1->display(cap_val);
    }
    else if (node_index==2) {
        ui->ld_my_r2->display(res_val);
        ui->ld_my_c2->display(cap_val);
    }
    else if (node_index==3) {
        ui->ld_my_r3->display(res_val);
        ui->ld_my_c3->display(cap_val);
    }
    else if (node_index==4) {
        ui->ld_my_r4->display(res_val);
        ui->ld_my_c4->display(cap_val);
    }
    else if (node_index==5) {
        ui->ld_my_r5->display(res_val);
        ui->ld_my_c5->display(cap_val);
    }
    else if (node_index==6) {
        ui->ld_my_r6->display(res_val);
        ui->ld_my_c6->display(cap_val);
    }
    else if (node_index==7){
        //es ese cao primero se calcula los parametros de temperatura y presion.
        calcule_param_sensor_temp_and_pres(values_entry,7);

    }
    else
    {
        //en el caso de que el valor no sea ningun nodo entre el 1 y 6 ,simplemente no hace nada

    }
    //qDebug("salio ");


}
//******************************************************************************************************************************
// capture_data_to_normalize() captura la data que se muestra en el area de normalizacion para ser procesada
//******************************************************************************************************************************
void jptwatercut::capture_data_to_normalize()
{
        QString temporal;
        res_data_raw[0]=float(ui->ld_my_r1->value());
        cap_data_raw[0]=float(ui->ld_my_c1->value());
        temporal=QString::number(res_data_raw[0]);
        ui->cap_r_1->setText(temporal);
        temporal=QString::number(cap_data_raw[0]);
        ui->cap_c_1->setText(temporal);

        res_data_raw[1]=float(ui->ld_my_r2->value());
        cap_data_raw[1]=float(ui->ld_my_c2->value());
        temporal=QString::number(res_data_raw[1]);
        ui->cap_r_2->setText(temporal);
        temporal=QString::number(cap_data_raw[1]);
        ui->cap_c_2->setText(temporal);


        res_data_raw[2]=float(ui->ld_my_r3->value());
        cap_data_raw[2]=float(ui->ld_my_c3->value());
        temporal=QString::number(res_data_raw[2]);
        ui->cap_r_3->setText(temporal);
        temporal=QString::number(cap_data_raw[2]);
        ui->cap_c_3->setText(temporal);


        res_data_raw[3]=float(ui->ld_my_r4->value());
        cap_data_raw[3]=float(ui->ld_my_c4->value());
        temporal=QString::number(res_data_raw[3]);
        ui->cap_r_4->setText(temporal);
        temporal=QString::number(cap_data_raw[3]);
        ui->cap_c_4->setText(temporal);


        res_data_raw[4]=float(ui->ld_my_r5->value());
        cap_data_raw[4]=float(ui->ld_my_c5->value());
        temporal=QString::number(res_data_raw[4]);
        ui->cap_r_5->setText(temporal);
        temporal=QString::number(cap_data_raw[4]);
        ui->cap_c_5->setText(temporal);


        res_data_raw[5]=float(ui->ld_my_r6->value());
        cap_data_raw[5]=float(ui->ld_my_c6->value());
        temporal=QString::number(res_data_raw[5]);
        ui->cap_r_6->setText(temporal);
        temporal=QString::number(cap_data_raw[5]);
        ui->cap_c_6->setText(temporal);

        ui->Group_set_parameters->setEnabled(true);//inicia el menu grafico de la nrmalizacion de capacitancia.
        ui->Group_set_parameters_2->setEnabled(true);//inicia el menu grafico de normalizacion de resitividad

        ui->update_dlt_val->setEnabled(false);
        ui->update_dlt_average->setEnabled(false);

        ui->update_dlt_val_2->setEnabled(false);
        ui->update_dlt_average_2->setEnabled(false);
}
//******************************************************************************************************************************
// charge delta normalization: cargo los valores de normalizacion del sistema en caso ed que no se haya
// realizado el proceso de normalizacion este valor estara en 0.
//******************************************************************************************************************************
void jptwatercut::charge_delta_normalization()
{
    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/delta_n.txt";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/External/delta_n.txt";
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/delta_n.txt";
        //qDebug("se creo el path ");

#endif

           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;


           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<12){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");

                   int my_delta=line.toInt();
                   delta_normalization[record]=my_delta;
                   //qDebug() << "line>> "<<QString::number(delta_normalization[record]);
                   record++;
               }
               record=0;
           }
           file.close();



}
//******************************************************************************************************************************
// Update_delta_normalization: funcion que actualiza la variable delta de normalizacion
//******************************************************************************************************************************
void jptwatercut::update_delta_normalization()
{
        // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/delta_n.txt";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/External/delta_n.txt";
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/delta_n.txt";
        ////qDebug("se creo el path ");

#endif
        //cargamos el delta de normalizacion dentro del sistema
        QString _temporal;
        _temporal=ui->delta_r_1->text();
        delta_normalization[0]=_temporal.toInt();

        _temporal=ui->delta_r_1->text();
        delta_normalization[1]=_temporal.toInt();

        _temporal=ui->delta_r_3->text();
        delta_normalization[2]=_temporal.toInt();

        _temporal=ui->delta_r_4->text();
        delta_normalization[3]=_temporal.toInt();

        _temporal=ui->delta_r_5->text();
        delta_normalization[4]=_temporal.toInt();

        _temporal=ui->delta_r_6->text();
        delta_normalization[5]=_temporal.toInt();

        _temporal=ui->delta_c_1->text();
        delta_normalization[6]=_temporal.toInt();

        _temporal=ui->delta_c_2->text();
        delta_normalization[7]=_temporal.toInt();

        _temporal=ui->delta_c_3->text();
        delta_normalization[8]=_temporal.toInt();

        _temporal=ui->delta_c_4->text();
        delta_normalization[9]=_temporal.toInt();

        _temporal=ui->delta_c_5->text();
        delta_normalization[10]=_temporal.toInt();

        _temporal=ui->delta_c_6->text();
        delta_normalization[11]=_temporal.toInt();










        //procedemos a cagar en el .txt la informacion
        QFile file(path_source);
        if(!file.exists()){
            //qDebug() << "NO existe el archivo "<<path_source;
        }else{
            //qDebug() << path_source <<" encontrado...";
        }
        QString line;

        if (file.open(QIODevice::ReadWrite | QIODevice::Text)){
            QTextStream stream(&file);
            QString _data="";
            for (int record=0;record<12;record++) {
               _data=_data+QString::number(delta_normalization[record])+"\n";

            }
             stream << _data <<endl;
             //qDebug()<<_data;


        }
        file.close();
        ui->update_labal->setText("¡SUCCESS!");
}
//******************************************************************************************************************************
// Update_delta_normalization: funcion que actualiza la variable delta de normalizacion
//******************************************************************************************************************************
void jptwatercut::update_delta_average()
{
        // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/delta_n.txt";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/External/delta_n.txt";
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/delta_n.txt";
        ////qDebug("se creo el path ");

#endif
        //cargamos el delta de normalizacion dentro del sistema
        QString _temporal;
         _temporal=ui->delta_average->text();
        delta_normalization[0]=_temporal.toInt();
        delta_normalization[1]=_temporal.toInt();
        delta_normalization[2]=_temporal.toInt();
        delta_normalization[3]=_temporal.toInt();
        delta_normalization[4]=_temporal.toInt();
        delta_normalization[5]=_temporal.toInt();
        delta_normalization[6]=_temporal.toInt();
        delta_normalization[7]=_temporal.toInt();
        delta_normalization[8]=_temporal.toInt();
        delta_normalization[9]=_temporal.toInt();
        delta_normalization[10]=_temporal.toInt();
        delta_normalization[11]=_temporal.toInt();

        //procedemos a cagar en el .txt la informacion
        QFile file(path_source);
        if(!file.exists()){
            //qDebug() << "NO existe el archivo "<<path_source;
        }else{
            //qDebug() << path_source <<" encontrado...";
        }
        QString line;

        if (file.open(QIODevice::ReadWrite | QIODevice::Text)){
            QTextStream stream(&file);
            QString _data="";
            for (int record=0;record<12;record++) {
               _data=_data+QString::number(delta_normalization[record])+"\n";

            }
             stream << _data <<endl;
             //qDebug()<<_data;


        }
        file.close();
        ui->update_labal->setText("¡SUCCESS!");


}
//******************************************************************************************************************************
// calcule_default_delta(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_default_delta()
{
    //secalcula el valor de defecto de delta, primero obtengo el valo en la parte grafica
    QString value_grafic=ui->default_var->text();
    //convierto el valor a entero
    int my_int=value_grafic.toInt();
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;

    temporal=my_int-cap_data_raw[0];
    ui->delta_c_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[1];
    ui->delta_c_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[2];
    ui->delta_c_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[3];
    ui->delta_c_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[4];
    ui->delta_c_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[5];
    ui->delta_c_6->setText(QString::number(temporal));
    average=average+temporal;
    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average->setText(QString::number(average));
    ui->update_dlt_val->setEnabled(true);
    ui->update_dlt_average->setEnabled(true);


}
//******************************************************************************************************************************
// calcule manual delta(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_manual_delta()
{
    //secalcula el valor manual ingresado de delta, primero obtengo el valor en la parte grafica
    QString value_grafic=ui->manual_var->text();
    //convierto el valor a entero
    int my_int=value_grafic.toInt();
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;

    temporal=my_int-cap_data_raw[0];
    ui->delta_c_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[1];
    ui->delta_c_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[2];
    ui->delta_c_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[3];
    ui->delta_c_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[4];
    ui->delta_c_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-cap_data_raw[5];
    ui->delta_c_6->setText(QString::number(temporal));
    average=average+temporal;
    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average->setText(QString::number(average));

    ui->update_dlt_val->setEnabled(true);
    ui->update_dlt_average->setEnabled(true);


}
//******************************************************************************************************************************
// calcule auto delta(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_auto_delta()
{
    //se calculal delta automaticamente, prmero obtengo el valor promedio de todos los sensores.
    float average_sensor=0;
    for(int j=0;j<6;j++)
    {
        average_sensor=average_sensor+cap_data_raw[j];
    }
    //como es promerio dividimos entre la cantidad de sensores.
    int average_int=int(average_sensor/6);
    //convierto el valor a entero
    //int average_int=int(average_sensor);

    //ahora se setea en la parte grafica el valor del promedio de los sensores.
    ui->atomatic_var->setText(QString::number(average_int));
    //esto codigo es rehuso de las otras varibles por ello convierto la variable average en my_int.
    int my_int=average_int;
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;


    temporal=my_int-int(cap_data_raw[0]);
    ui->delta_c_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(cap_data_raw[1]);
    ui->delta_c_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(cap_data_raw[2]);
    ui->delta_c_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(cap_data_raw[3]);
    ui->delta_c_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(cap_data_raw[4]);
    ui->delta_c_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(cap_data_raw[5]);
    ui->delta_c_6->setText(QString::number(temporal));
    average=average+temporal;
    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average->setText(QString::number(average));

    ui->update_dlt_val->setEnabled(true);
    ui->update_dlt_average->setEnabled(true);

}


//******************************************************************************************************************************
// calcule_default_delta_r(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_default_delta_r()
{
    //secalcula el valor de defecto de delta, primero obtengo el valo en la parte grafica
    QString value_grafic=ui->default_var_2->text();
    //convierto el valor a entero
    int my_int=value_grafic.toInt();
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;

    temporal=my_int-res_data_raw[0];
    ui->delta_r_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[1];
    ui->delta_r_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[2];
    ui->delta_r_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[3];
    ui->delta_r_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[4];
    ui->delta_r_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[5];
    ui->delta_r_6->setText(QString::number(temporal));
    average=average+temporal;



    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average_2->setText(QString::number(average));
    ui->update_dlt_val_2->setEnabled(true);
    ui->update_dlt_average_2->setEnabled(true);


}
//******************************************************************************************************************************
// calcule_manual delta  r(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_manual_delta_r()
{
    //secalcula el valor manual ingresado de delta, primero obtengo el valor en la parte grafica
    QString value_grafic=ui->manual_var_2->text();
    //convierto el valor a entero
    int my_int=value_grafic.toInt();
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;
    temporal=my_int-res_data_raw[0];
    ui->delta_r_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[1];
    ui->delta_r_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[2];
    ui->delta_r_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[3];
    ui->delta_r_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[4];
    ui->delta_r_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-res_data_raw[5];
    ui->delta_r_6->setText(QString::number(temporal));
    average=average+temporal;

    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average_2->setText(QString::number(average));

    ui->update_dlt_val_2->setEnabled(true);
    ui->update_dlt_average_2->setEnabled(true);


}

//******************************************************************************************************************************
// calcule_default_delta(): funcion que calcula el delta normal.
//******************************************************************************************************************************
void jptwatercut::calcule_auto_delta_r()
{
    //se calculal delta automaticamente, prmero obtengo el valor promedio de todos los sensores.
    float average_sensor=0;
    for(int j=0;j<6;j++)
    {
        average_sensor=average_sensor+res_data_raw[j];

    }

    //como es promerio dividimos entre la cantidad de sensores.
    average_sensor=average_sensor/6;
    //convierto el valor a entero
    int average_int=int(average_sensor);
    //ahora se setea en la parte grafica el valor del promedio de los sensores.
    ui->atomatic_var_2->setText(QString::number(average_int));
    //esto codigo es rehuso de las otras varibles por ello convierto la variable average en my_int.
    int my_int=average_int;
    //realizo la diferencia entre cada valor obtenido
    //y los valores de restividad y capacitancia capturados
    //este valor lo guardo de forma temporal en las variables de la parte grafica.
    int temporal;
    int average=0;
    temporal=my_int-int(res_data_raw[0]);
    ui->delta_r_1->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(res_data_raw[1]);
    ui->delta_r_2->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(res_data_raw[2]);
    ui->delta_r_3->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(res_data_raw[3]);
    ui->delta_r_4->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(res_data_raw[4]);
    ui->delta_r_5->setText(QString::number(temporal));
    average=average+temporal;
    temporal=my_int-int(res_data_raw[5]);
    ui->delta_r_6->setText(QString::number(temporal));
    average=average+temporal;



    //calculamos ahora el valor del averae el cual es simpemente la suma de todos los elementos divididos entre 12.
    average=average/6;
    //el valor de average se carga en la grafica
    ui->delta_average_2->setText(QString::number(average));

    ui->update_dlt_val_2->setEnabled(true);
    ui->update_dlt_average_2->setEnabled(true);

}


//******************************************************************************************************************************
//setup capture calib: carga los valores iniciales de la calibracion de datos y enciende la carible state_calib
//para seguir verificando la calibracin cada que lleguen o bien el sistema lea datos.
//******************************************************************************************************************************
void jptwatercut::setup_capture_calib(){
    //si es la segunda vez que se realiza la calibracion entonces se deshabilita el resultado .
    ui->result_obj->setEnabled(false);
    //y la matrix se setea vacia con todo cero.
    for(int j=0;j<12;j++)
    {//este elemento recorre cada una de la filas de la matrix
        for (int k=0;k<3;k++)
        {
           calib_matrix[j][k]=0;

        }

    }

    //cuando se presiona el boton,lo primero sera obtener el valor de los elementos que ya estn cargados en sistema
    //para ello tomamos los datos de los display calib_raw_R/C_# y lo pasamos a los elementos de la matrix calib_matrix[x][0]
    int _temporal;
    _temporal=int(ui->calib_raw_r_1->value());
    calib_matrix[0][0]=_temporal;
    _temporal=int(ui->calib_raw_r_2->value());
    calib_matrix[1][0]=_temporal;
    _temporal=int(ui->calib_raw_r_3->value());
    calib_matrix[2][0]=_temporal;
    _temporal=int(ui->calib_raw_r_4->value());
    calib_matrix[3][0]=_temporal;
    _temporal=int(ui->calib_raw_r_5->value());
    calib_matrix[4][0]=_temporal;
    _temporal=int(ui->calib_raw_r_6->value());
    calib_matrix[5][0]=_temporal;
    _temporal=int(ui->calib_raw_c_1->value());
    calib_matrix[6][0]=_temporal;
    _temporal=int(ui->calib_raw_c_2->value());
    calib_matrix[7][0]=_temporal;
    _temporal=int(ui->calib_raw_c_3->value());
    calib_matrix[8][0]=_temporal;
    _temporal=int(ui->calib_raw_c_4->value());
    calib_matrix[9][0]=_temporal;
    _temporal=int(ui->calib_raw_c_5->value());
    calib_matrix[10][0]=_temporal;
    _temporal=int(ui->calib_raw_c_6->value());
    calib_matrix[11][0]=_temporal;
    //qDebug(QString::number(calib_matrix[1][0]).toLatin1());
    //qDebug(QString::number(calib_matrix[2][0]).toLatin1());
    //qDebug(QString::number(calib_matrix[3][0]).toLatin1());
    //qDebug(QString::number(calib_matrix[4][0]).toLatin1());
    //recorro toda la matrix en busca de un valor nulo
    int _null=0;
    for(int l=0;l<12;l++)
    {//este elemento recorre cada una de la filas de la matrix
            //qDebug(QString::number(calib_matrix[l][0]).toLatin1());
            //qDebug(QString::number(_null).toLatin1());
            //si el valor es cero, '0', entonces debe de cargar la calibracion en como errada.
            if(calib_matrix[l][0]==0)
            {
                _null=_null+1;
            }

    }
    if (_null==0)
    {
        //ahora se deben imprimir los datos como 16000/ Data2 /Data3 por ejemplo en sistema sobre la variable calib_cap_R/C_#
        QString _tempo_str;
        _tempo_str=""+QString::number(calib_matrix[0][0])+"/ Data2 /Data3";
        ui->calib_cap_r_1->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[1][0])+"/ Data2 /Data3";
        ui->calib_cap_r_2->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[2][0])+"/ Data2 /Data3";
        ui->calib_cap_r_3->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[3][0])+"/ Data2 /Data3";
        ui->calib_cap_r_4->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[4][0])+"/ Data2 /Data3";
        ui->calib_cap_r_5->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[5][0])+"/ Data2 /Data3";
        ui->calib_cap_r_6->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[6][0])+"/ Data2 /Data3";
        ui->calib_cap_c_1->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[7][0])+"/ Data2 /Data3";
        ui->calib_cap_c_2->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[8][0])+"/ Data2 /Data3";
        ui->calib_cap_c_3->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[9][0])+"/ Data2 /Data3";
        ui->calib_cap_c_4->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[10][0])+"/ Data2 /Data3";
        ui->calib_cap_c_5->setText(_tempo_str);

        _tempo_str=""+QString::number(calib_matrix[11][0])+"/ Data2 /Data3";
        ui->calib_cap_c_6->setText(_tempo_str);

        //por ultimo aumentosmo o bien seteamos el valor de la funcion state en 0, count en 1 y la connecion del boton en false.
        state_calib=1; //encendido el capture
        count_calib=1; //el contador se setea en 1, para que al terminar la siguiente recepcion en el procesoo de acumulacion
                       //de datos, este cargue los nuevos a la parte grafica del calibrador, esto lo hace llamando a la funcion
                       //capture_data_calibration.
        ui->captura_data_2->setText("Waiting Second Value...[The process can take a few minuts]");//cambio el mensaje del boton de captura a esperando el segundo valor.
        ui->captura_data_2->setEnabled(false);//Deshabilito el boton de seteo de datos de calibracion.

    }
    else//en caso contrario se debera ver un mensaje de que no es posbible realizar calibracion.
    {
        ui->calib_message->setText(">> Error: Capture Data-> Exists null values on data.");
    }

    _null=0;

}
//******************************************************************************************************************************
//capture data calibration:
//count calib>>realiza el coneo del dato entrando, state calib>> dice si el proceso debe leer (0) o no (1)
//******************************************************************************************************************************
void jptwatercut::capture_data_calibration()
{  //se recuerda que al moento de presionar el boton se carga el prmer dato que se encuentre en sistema.
   //los datos dos y tres son lo que se cargaran a continuacion.
    //primero se valida si el sistema esta en modo de captura de datos para eso la variable state_calib debe estar en 1.
    if(state_calib==1)
    {
        if (count_calib==2)
        {
            //cuando se cargan nuevos valores se al sistema provenientes del jpt monitoring en sistema,
            //tambien se deben de cargar estos en la matrix[][] que guarda los datos de calibracion

            int _temporal;
            _temporal=int(ui->calib_raw_r_1->value());
            calib_matrix[0][2]=_temporal;
            _temporal=int(ui->calib_raw_r_2->value());
            calib_matrix[1][2]=_temporal;
            _temporal=int(ui->calib_raw_r_3->value());
            calib_matrix[2][2]=_temporal;
            _temporal=int(ui->calib_raw_r_4->value());
            calib_matrix[3][2]=_temporal;
            _temporal=int(ui->calib_raw_r_5->value());
            calib_matrix[4][2]=_temporal;
            _temporal=int(ui->calib_raw_r_6->value());
            calib_matrix[5][2]=_temporal;
            _temporal=int(ui->calib_raw_c_1->value());
            calib_matrix[6][2]=_temporal;
            _temporal=int(ui->calib_raw_c_2->value());
            calib_matrix[7][2]=_temporal;
            _temporal=int(ui->calib_raw_c_3->value());
            calib_matrix[8][2]=_temporal;
            _temporal=int(ui->calib_raw_c_4->value());
            calib_matrix[9][2]=_temporal;
            _temporal=int(ui->calib_raw_c_5->value());
            calib_matrix[10][2]=_temporal;
            _temporal=int(ui->calib_raw_c_6->value());
            calib_matrix[11][2]=_temporal;

            //ahora se deben imprimir los datos como 16000/ 16000 /16000 por ejemplo en sistema sobre la variable calib_cap_R/C_#
            QString _tempo_str;
            _tempo_str=""+QString::number(calib_matrix[0][0])+"/"+QString::number(calib_matrix[0][1]) +"/"+QString::number(calib_matrix[0][2]);
            ui->calib_cap_r_1->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[1][0])+"/"+QString::number(calib_matrix[1][1]) +"/"+QString::number(calib_matrix[1][2]);
            ui->calib_cap_r_2->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[2][0])+"/"+QString::number(calib_matrix[2][1])+"/"+QString::number(calib_matrix[2][2]);
            ui->calib_cap_r_3->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[3][0])+"/"+QString::number(calib_matrix[3][1])+"/"+QString::number(calib_matrix[3][2]);
            ui->calib_cap_r_4->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[4][0])+"/"+QString::number(calib_matrix[4][1]) +"/"+QString::number(calib_matrix[4][2]);
            ui->calib_cap_r_5->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[5][0])+"/"+QString::number(calib_matrix[5][1]) +"/"+QString::number(calib_matrix[5][2]);
            ui->calib_cap_r_6->setText(_tempo_str);


            _tempo_str=""+QString::number(calib_matrix[6][0])+"/"+QString::number(calib_matrix[6][1]) +"/"+QString::number(calib_matrix[6][2]);
            ui->calib_cap_c_1->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[7][0])+"/"+QString::number(calib_matrix[7][1]) +"/"+QString::number(calib_matrix[7][2]);
            ui->calib_cap_c_2->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[8][0])+"/"+QString::number(calib_matrix[8][1]) +"/"+QString::number(calib_matrix[8][2]);
            ui->calib_cap_c_3->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[9][0])+"/"+QString::number(calib_matrix[9][1]) +"/"+QString::number(calib_matrix[9][2]);
            ui->calib_cap_c_4->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[10][0])+"/"+QString::number(calib_matrix[10][1]) +"/"+QString::number(calib_matrix[10][2]);
            ui->calib_cap_c_5->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[11][0])+"/"+QString::number(calib_matrix[11][1])+"/"+QString::number(calib_matrix[11][2]);
            ui->calib_cap_c_6->setText(_tempo_str);

            //cuando termina de cargar el tercer dado el sistema de captura se setea nuevamente en 0,0.
            state_calib=0;
            count_calib=0;
            //y preparamos el sistema para cargar de nuevo los datos
            ui->captura_data_2->setText("Capture Data");
            ui->captura_data_2->setEnabled(true);
            //no se debe realizr de uevo a conexion pues solamente se deshabilito temporalmente el botton.
            //se habilita entonces por ultimo el menu grafico de los resultados
            ui->result_obj->setEnabled(true);//este elemento se deshabilita cuando se presiona la funcion de setup calib
            //ahora que el result esta eneble cargo los valores por defecto en cada uno de los elementos.


        }//count=2
        else if (count_calib==1)
        {
            //cuando se cargan nuevos valores se al sistema provenientes del jpt monitoring en sistema,
            //tambien se deben de cargar estos en la matrix[][] que guarda los datos de calibracion

            int _temporal;
            _temporal=int(ui->calib_raw_r_1->value());
            calib_matrix[0][1]=_temporal;
            _temporal=int(ui->calib_raw_r_2->value());
            calib_matrix[1][1]=_temporal;
            _temporal=int(ui->calib_raw_r_3->value());
            calib_matrix[2][1]=_temporal;
            _temporal=int(ui->calib_raw_r_4->value());
            calib_matrix[3][1]=_temporal;
            _temporal=int(ui->calib_raw_r_5->value());
            calib_matrix[4][1]=_temporal;
            _temporal=int(ui->calib_raw_r_6->value());
            calib_matrix[5][1]=_temporal;
            _temporal=int(ui->calib_raw_c_1->value());
            calib_matrix[6][1]=_temporal;
            _temporal=int(ui->calib_raw_c_2->value());
            calib_matrix[7][1]=_temporal;
            _temporal=int(ui->calib_raw_c_3->value());
            calib_matrix[8][1]=_temporal;
            _temporal=int(ui->calib_raw_c_4->value());
            calib_matrix[9][1]=_temporal;
            _temporal=int(ui->calib_raw_c_5->value());
            calib_matrix[10][1]=_temporal;
            _temporal=int(ui->calib_raw_c_6->value());
            calib_matrix[11][1]=_temporal;
            //ahora se deben imprimir los datos como 16000/ 16000 /Data3 por ejemplo en sistema sobre la variable calib_cap_R/C_#
            QString _tempo_str;
            _tempo_str=""+QString::number(calib_matrix[0][0])+"/"+QString::number(calib_matrix[0][1]) +"/Data3";
            ui->calib_cap_r_1->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[1][0])+"/"+QString::number(calib_matrix[1][1]) +"/Data3";
            ui->calib_cap_r_2->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[2][0])+"/"+QString::number(calib_matrix[2][1]) +"/Data3";
            ui->calib_cap_r_3->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[3][0])+"/"+QString::number(calib_matrix[3][1]) +"/Data3";
            ui->calib_cap_r_4->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[4][0])+"/"+QString::number(calib_matrix[4][1]) +"/Data3";
            ui->calib_cap_r_5->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[5][0])+"/"+QString::number(calib_matrix[5][1]) +"/Data3";
            ui->calib_cap_r_6->setText(_tempo_str);


            _tempo_str=""+QString::number(calib_matrix[6][0])+"/"+QString::number(calib_matrix[6][1]) +"/Data3";
            ui->calib_cap_c_1->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[7][0])+"/"+QString::number(calib_matrix[7][1]) +"/Data3";
            ui->calib_cap_c_2->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[8][0])+"/"+QString::number(calib_matrix[8][1]) +"/Data3";
            ui->calib_cap_c_3->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[9][0])+"/"+QString::number(calib_matrix[9][1]) +"/Data3";
            ui->calib_cap_c_4->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[10][0])+"/"+QString::number(calib_matrix[10][1]) +"/Data3";
            ui->calib_cap_c_5->setText(_tempo_str);

            _tempo_str=""+QString::number(calib_matrix[11][0])+"/"+QString::number(calib_matrix[11][1]) +"/Data3";
            ui->calib_cap_c_6->setText(_tempo_str);
            count_calib=2; //tomara el dato dos del vector
            //cambio el mensaje del boton de captura a esperando el segundo valor.
            ui->captura_data_2->setText("Waiting third Value...[The process can take a few minuts]");


        }//count=1

    }//state_calib==1 && res_o_cap==0


}

//******************************************************************************************************************************
//calcule agv promedio
//******************************************************************************************************************************
void jptwatercut::avg_cali_oil()
{
    //the first is calculate de temporal R and C calib values
    int _res=0;
    int _cap=0;
    for(int j=0;j<6;j++)
    {
        for(int k=0;k<3;k++)
        {
        _res=_res+calib_matrix[j][k];
        _cap=_cap+calib_matrix[j+6][k];
        }

    }
    //ahora sacamos el promedio de los valores .
    _res=_res/18;
    _cap=_cap/18;
    //qDebug("valor medio deresitencia<<"+QString::number(_res).toLatin1()+">>valor medio de capcitancia>>"+QString::number(_res).toLatin1());
    //res_avg_oil
    ui->res_avg_oil->setValue(_res);
    ui->cap_avg_oil->setValue(_cap);

}

void jptwatercut::avg_cali_air()
{
    //the first is calculate de temporal R and C calib values
    int _res=0;
    int _cap=0;
    for(int j=0;j<6;j++)
    {
        for(int k=0;k<3;k++)
        {
        _res=_res+calib_matrix[j][k];
        _cap=_cap+calib_matrix[j+6][k];
        }

    }
    //ahora sacamos el promedio de los valores .
    _res=_res/18;
    _cap=_cap/18;
    //qDebug("valor medio deresitencia<<"+QString::number(_res).toLatin1()+">>valor medio de capcitancia>>"+QString::number(_res).toLatin1());
    //res_avg_oil
    ui->res_avg_air->setValue(_res);
    ui->cap_avg_air->setValue(_cap);

}

void jptwatercut::avg_cali_w()
{
    //the first is calculate de temporal R and C calib values
    int _res=0;
    int _cap=0;
    for(int j=0;j<6;j++)
    {
        for(int k=0;k<3;k++)
        {
        _res=_res+calib_matrix[j][k];
        _cap=_cap+calib_matrix[j+6][k];
        }

    }
    //ahora sacamos el promedio de los valores .
    _res=_res/18;
    _cap=_cap/18;
    //qDebug("valor medio deresitencia<<"+QString::number(_res).toLatin1()+">>valor medio de capcitancia>>"+QString::number(_res).toLatin1());
    //res_avg_oil
    ui->res_avg_water->setValue(_res);
    ui->cap_avg_water->setValue(_cap);


}

void jptwatercut::avg_cali_fw()
{
    //the first is calculate de temporal R and C calib values
    int _res=0;
    int _cap=0;
    for(int j=0;j<6;j++)
    {
        for(int k=0;k<3;k++)
        {
        _res=_res+calib_matrix[j][k];
        _cap=_cap+calib_matrix[j+6][k];
        }

    }
    //ahora sacamos el promedio de los valores .
    _res=_res/18;
    _cap=_cap/18;
    //qDebug("valor medio deresitencia<<"+QString::number(_res).toLatin1()+">>valor medio de capcitancia>>"+QString::number(_res).toLatin1());
    //res_avg_oil
    ui->res_avg_f_water->setValue(_res);
    ui->cap_avg_f_water->setValue(_cap);

}

//******************************************************************************************************************************
//funcion que invoca ambas curvas al msmo tiempo.
//******************************************************************************************************************************
void jptwatercut::my_curves()
{
    curve_cap();
    curve_res();
    update_calibration_values();
    //luego de que el proceso se realiza de calibracion tambien se guardan los valores o datos de las curvas.

    int _c=ui->customplot_c->legend->itemCount();
    ui->customplot_c->legend->setVisible(true);
    for(int i=3;i<_c;i++){
         ui->customplot_c->legend->remove(ui->customplot_c->legend->item(i));
       }
    ui->customplot_c->replot();


    int _r=ui->customplot_r->legend->itemCount();
    ui->customplot_r->legend->setVisible(true);
    for(int i=3;i<_c;i++){
         ui->customplot_r->legend->remove(ui->customplot_r->legend->item(i));
       }
    ui->customplot_r->replot();



}
void jptwatercut::hlwc_curves()
{
   hwc_curve_res();
   hwc_curve_cap();
   low_curve_cap_res();
   update_hlwc_values();

}

void jptwatercut::update_hlwc_values()
{
    // se carga el path de almacenamiento de os valores de normalizacion
#ifdef _HUB_ON
    QString path_source = "/jpt/jptwatercut/External/hlwc.txt";
#else
    QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/hlwc.txt";

    ////qDebug("se creo el path ");

#endif

    //cargo los vlores de calibracion en la variable de calibracion
    //estos son valores de resitividad
    hl_wc_values[0]=ui->percent_hwc_up->value();
    hl_wc_values[1]=ui->value_hwc_up->value();

    hl_wc_values[2]=ui->percent_hwc_dw->value();
    hl_wc_values[3]=ui->value_hwc_dw->value();

    hl_wc_values[4]=ui->percent_lwc_up->value();
    hl_wc_values[5]=ui->value_lwc_up->value();

    hl_wc_values[6]=ui->percent_lwc_dw->value();
    hl_wc_values[7]=ui->value_lwc_dw->value();

    hl_wc_values[8]=ui->percent_hwc_up_2->value();
    hl_wc_values[9]=ui->value_hwc_up_2->value();

    hl_wc_values[10]=ui->percent_hwc_dw_2->value();
    hl_wc_values[11]=ui->value_hwc_dw_2->value();




    //procedemos a cagar en el .txt la informacion
    QFile file(path_source);
    if(!file.exists()){
        //qDebug() << "NO existe el archivo "<<path_source;
    }else{
        //qDebug() << path_source <<" encontrado...";
    }
    QString line;

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QString _data="";
        for (int record=0;record<12;record++)
        {
           _data=_data+QString::number(hl_wc_values[record])+"\n";

        }
         stream << _data <<endl;
         //qDebug()<<_data;


    }
    file.close();
    //ui->update_labal->setText("¡SUCCESS!");// si se desea se puede poner un mensaje de update de la calibracion.



}



//******************************************************************************************************************************
//calcule_curve_res
//******************************************************************************************************************************
void jptwatercut::curve_res()
{ //lo primero es calcular la pendiente entre RCater y RCoil
   int RCWater;
   RCWater=ui->res_avg_water->value();
   int RCOil;
   RCOil=ui->res_avg_oil->value();
   //la pendiente es igual a M=(-100)/(Rco-Cw)
   float m;
   m=(-100)/static_cast<float>((RCOil-RCWater));
   //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
   //Yo=(-m*RCwater)+100
   float Yo;
   Yo=static_cast<float>((-m*RCWater))+100.0;

   //qDebug()<<"Yo>>"<<Yo;
   //qDebug()<<"m>>"<<m;
   //*********************************************************************************************************
   //Aqui inicia el proceso de ploting de la grafica de resistividad.
   //*********************************************************************************************************
   // generate some data:
   QVector<double> x(170), y(170); // initialize with entries 0..100
   for (int i=0; i<170; i++)
   { ////qDebug()<<"ciclo"<<i;
     x[i] = i*100;
     y[i] = static_cast<double>((m*x[i])+Yo);
     if(y[i]>=100)
     {
         y[i]=100;
     }
     else if(y[i]<=0)
     {
         y[i]=0;
     }

   }
   ////qDebug("salio del ciclo for. ");

   // create graph and assign data to it:
   ui->customplot_r->addGraph();
   ui->customplot_r->graph(0)->setData(x,y);
   // give the axes some labels:
   ui->customplot_r->xAxis->setLabel("Resistivity [Rx]");
   ui->customplot_r->yAxis->setLabel("Water [ % ] ");
   // set axes ranges, so we see all data:
   ui->customplot_r->xAxis->setRange(-10, 17000);
   ui->customplot_r->yAxis->setRange(-1,Yo+10);
   ui->customplot_r->replot();




}

void jptwatercut::hwc_curve_res()
{
    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_hwc_up_2->value();
    int cuentas_bajas=ui->value_hwc_dw_2->value();
    int percet_alto=ui->percent_hwc_up_2->value();
    int percet_bajo=ui->percent_hwc_dw_2->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    ////qDebug()<<m;
    ////qDebug()<<Yo;

   //*********************************************************************************************************
   //Aqui inicia el proceso de ploting de la grafica de resistividad.
   //*********************************************************************************************************
   // generate some data:
   //obtengo cantidad de datos del vector
   int _tam=cuentas_bajas-cuentas_altas;
   _tam=static_cast<int>(_tam/100);
   ////qDebug()<<_tam;
   QVector<double> x(_tam), y(_tam); // initialize with entries 0..100


   for (int i=0; i<_tam; i++)
   { ////qDebug()<<"ciclo"<<i;
     x[i] = cuentas_altas+(i*100);
     y[i] = static_cast<double>((m*x[i])+Yo);

   }
   ////qDebug("salio del ciclo for. ")


   // create graph and assign data to it:
   ui->customplot_r->addGraph();
   ui->customplot_r->graph(1)->setPen(QPen(Qt::red));
   ui->customplot_r->graph(1)->setBrush(QBrush(QColor(0,0,250,20)));
   ui->customplot_r->graph(1)->setData(x,y);

   ui->customplot_r->graph(1)->setName("Hight WC");
   ui->customplot_r->replot();


}



//******************************************************************************************************************************
//calcule_curve_cap
//******************************************************************************************************************************
void jptwatercut::curve_cap()
{

    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //para el caso de la grafica el valor de evaluacion es X[i]=i*100 con i variando 170 veces. esto se hace con el fin de
    //resivir 170  valores diferentes en Y[i]

    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater
    int CWater;
    CWater=ui->cap_avg_water->value();//obtengo el valor de calibracion ara agua
    int CF_Water;
    CF_Water=ui->cap_avg_f_water->value();//obtengo el valor de calibracion para formaciones de agua.
    //con estos valores debo de general mi curva dinamica.
    /*NOTA: Para general la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jtmodel en CW y con eso obtenems el valor de x.
     *
     */
    double percent_p2=static_cast<double>(new_model->cap_mixture_model_040_070(CWater));
    ////qDebug()<<"%>>"<<percent_p2;
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    //*********************************************************************************************************
    //Aqui inicia el proceso de ploting de la grafica de resistividad.
    //*********************************************************************************************************
    // generate some data:
    QVector<double> x(170), y(170); // initialize with entries 0..100

    for (int i=0; i<170; i++)
    { ////qDebug()<<"ciclo"<<i;
      x[i] = i*100;
      y[i] =((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-x[i])+percent_p2;
      if(x[i]<=CF_Water)
      {
          //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
          y[i]=100;
      }
      else
      {
          if(x[i]<=CWater)
          {
              // nohaga nada es el valor actual de Y[i] con la curva dinamica
          }
          else{
              if(x[i]<=7501)
              {
                 y[i]=new_model->cap_mixture_model_040_070(x[i]);


              }
              else
              {
                  if(x[i]<=13801)
                  {
                     y[i]=new_model->cap_mixture_model_030_040(x[i]);


                  }
                  else
                  {
                      if(x[i]<=15001)
                      {
                         y[i]=new_model->cap_mixture_model_000_030(x[i]);


                      }
                      else
                      {
                          y[i]=0;
                      }
                  }

              }

          }
      }

    }
    ////qDebug("salio del ciclo for. ");

    // create graph and assign data to it:
    ui->customplot_c->addGraph();
    ui->customplot_c->graph(0)->setData(x,y);
    // give the axes some labels:
    ui->customplot_c->xAxis->setLabel("Resistivity [Rx]");
    ui->customplot_c->yAxis->setLabel("Water [ % ] ");
    // set axes ranges, so we see all data:
    ui->customplot_c->xAxis->setRange(-10, 17000);
    ui->customplot_c->yAxis->setRange(-1,130);
    ui->customplot_c->replot();






}
void jptwatercut::hwc_curve_cap()
{
    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_hwc_up->value();
    int cuentas_bajas=ui->value_hwc_dw->value();
    int percet_alto=ui->percent_hwc_up->value();
    int percet_bajo=ui->percent_hwc_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    ////qDebug()<<m;
    ////qDebug()<<Yo;

   //*********************************************************************************************************
   //Aqui inicia el proceso de ploting de la grafica de resistividad.
   //*********************************************************************************************************
   // generate some data:
   //obtengo cantidad de datos del vector
   int _tam=cuentas_bajas-cuentas_altas;
   _tam=static_cast<int>(_tam/100);
   ////qDebug()<<_tam;
   QVector<double> x(_tam), y(_tam); // initialize with entries 0..100


   for (int i=0; i<_tam; i++)
   { ////qDebug()<<"ciclo"<<i;
     x[i] = cuentas_altas+(i*100);
     y[i] = static_cast<double>((m*x[i])+Yo);

   }
   ////qDebug("salio del ciclo for. ")



   // create graph and assign data to it:
   ui->customplot_c->addGraph();
   ui->customplot_c->graph(1)->setPen(QPen(Qt::red));
    ui->customplot_c->graph(1)->setBrush(QBrush(QColor(0,0,250,20)));
   ui->customplot_c->graph(1)->setData(x,y);

   ui->customplot_c->graph(1)->setName("Hight WC");
   ui->customplot_c->replot();

}

void jptwatercut::low_curve_cap_res()
{
    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_lwc_up->value();
    int cuentas_bajas=ui->value_lwc_dw->value();
    int percet_alto=ui->percent_lwc_up->value();
    int percet_bajo=ui->percent_lwc_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    //qDebug()<<m<<"penediente";
    //qDebug()<<Yo<<"desplazamiento";

   //*********************************************************************************************************
   //Aqui inicia el proceso de ploting de la grafica de resistividad.
   //*********************************************************************************************************
   // generate some data:
   //obtengo cantidad de datos del vector
   int _tam=cuentas_bajas-cuentas_altas;
   _tam=static_cast<int>(_tam/100);
   //qDebug()<<_tam;
   QVector<double> x(_tam), y(_tam); // initialize with entries 0..100


   for (int i=0; i<_tam; i++)
   { ////qDebug()<<"ciclo"<<i;
     x[i] = cuentas_altas+(i*100);
     y[i] = static_cast<double>((m*x[i])+Yo);

   }
   ////qDebug("salio del ciclo for. ")



   // create graph and assign data to it:
   ui->customplot_c->addGraph();
   ui->customplot_c->graph(2)->setPen(QPen(Qt::green));

   ui->customplot_c->graph(2)->setBrush(QBrush(QColor(0,0,250,20)));
   ui->customplot_c->graph(2)->setData(x,y);

   ui->customplot_c->graph(2)->setName("Low WC");
   ui->customplot_c->replot();



   // create graph and assign data to it:
   ui->customplot_r->addGraph();
   ui->customplot_r->graph(2)->setPen(QPen(Qt::green));
   ui->customplot_r->graph(2)->setBrush(QBrush(QColor(0,0,250,20)));

   ui->customplot_r->graph(2)->setData(x,y);
   ui->customplot_r->graph(2)->setName("Low WC");
   ui->customplot_r->replot();


   ui->customplot_c->graph(0)->setName("Capacitance STF");
   int _c=ui->customplot_c->legend->itemCount();
   ui->customplot_c->legend->setVisible(true);
   for(int i=3;i<_c;i++){
        ui->customplot_c->legend->remove(ui->customplot_c->legend->item(i));
      }
   ui->customplot_c->replot();

   ui->customplot_r->graph(0)->setName("Resistivity STF");
   int _r=ui->customplot_r->legend->itemCount();
   ui->customplot_r->legend->setVisible(true);
   for(int i=3;i<_c;i++){
        ui->customplot_r->legend->remove(ui->customplot_r->legend->item(i));
      }
   ui->customplot_r->replot();




}
//******************************************************************************************************************************
//calcule_three_phases_curve
//******************************************************************************************************************************
void jptwatercut::calcule_three_phases()
{
    curve_three_phases();

}

void jptwatercut::curve_three_phases()
{
    // primero obtengo los valores de los datos
    int corte_cien=ui->value_t_phases_up->value();
    int corte_cero=ui->value_t_phases_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(-100)/static_cast<float>(corte_cero-corte_cien);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*corte_cien)+100);
    //qDebug()<<m<<"penediente three phases";
    //qDebug()<<Yo<<"desplazamiento three phases";

   //*********************************************************************************************************
   //Aqui inicia el proceso de ploting de la grafica de resistividad.
   //*********************************************************************************************************
   // generate some data:
   //obtengo cantidad de datos del vector
   int _tam=corte_cero-corte_cien;
   _tam=static_cast<int>(_tam/100);
   //qDebug()<<_tam;
   QVector<double> x(_tam), y(_tam); // initialize with entries every 100


   for (int i=0; i<_tam; i++)
   { ////qDebug()<<"ciclo"<<i;
     x[i] = corte_cien+(i*100);
     y[i] = static_cast<double>((m*x[i])+Yo);

   }
   ////qDebug("salio del ciclo for ")
   //qDebug()<<y[5];
   // create graph and assign data to it:
   ui->customplot_TFM->addGraph();
   ui->customplot_TFM->graph(0)->setPen(QPen(Qt::blue));

   ui->customplot_TFM->graph(0)->setBrush(QBrush(QColor(0,0,250,20)));
   ui->customplot_TFM->graph(0)->setData(x,y);

   ui->customplot_TFM->graph(0)->setName("Three Phases Model");
   ui->customplot_TFM->xAxis->setLabel("Capacitance [Cx]");
   ui->customplot_TFM->yAxis->setLabel("Oil Percent");
   // set axes ranges, so we see all data:
   ui->customplot_TFM->xAxis->setRange(12000, 17000);
   ui->customplot_TFM->yAxis->setRange(-1,140);

   ui->customplot_TFM->replot();
   //qDebug()<<"creo la grafica";

}

float jptwatercut::model_three_phases(float _cuentas)
{
    //si el sistema detecta que las cuentas estn en el valor entonces regresa un numero de porcentaje

    // primero obtengo los valores de los datos
    int corte_cien=ui->value_t_phases_up->value();
    int corte_cero=ui->value_t_phases_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(-100)/static_cast<float>(corte_cero-corte_cien);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*corte_cien)+100);
    //qDebug()<<m<<"penediente three phases";
    //qDebug()<<Yo<<"desplazamiento three phases";
    float function=static_cast<float>((m*_cuentas)+Yo);

    return function;

}
//******************************************************************************************************************************
//turb_laminar models.
//******************************************************************************************************************************
float jptwatercut::high_turb_laminar_res(float _cuentas)
{
    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_hwc_up_2->value();
    int cuentas_bajas=ui->value_hwc_dw_2->value();
    int percet_alto=ui->percent_hwc_up_2->value();
    int percet_bajo=ui->percent_hwc_dw_2->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    ////qDebug()<<m;
    ////qDebug()<<Yo;
    float function=static_cast<float>((m*_cuentas)+Yo);
    return function;
}

float jptwatercut::high_turb_laminar(float _cuentas)
{
    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_hwc_up->value();
    int cuentas_bajas=ui->value_hwc_dw->value();
    int percet_alto=ui->percent_hwc_up->value();
    int percet_bajo=ui->percent_hwc_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    ////qDebug()<<m;
    ////qDebug()<<Yo;
    float function=static_cast<float>((m*_cuentas)+Yo);
    return function;
}


float jptwatercut::low_turb_laminar(float _cuentas)
{

    // primero obtengo los valores de los datos  de mi pendiente
    int cuentas_altas=ui->value_lwc_up->value();
    int cuentas_bajas=ui->value_lwc_dw->value();
    int percet_alto=ui->percent_lwc_up->value();
    int percet_bajo=ui->percent_lwc_dw->value();

    //ahora calculo la pendiente con la que trabajare.
    float m;

    m=static_cast<float>(percet_bajo-percet_alto)/static_cast<float>(cuentas_bajas-cuentas_altas);
    //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
    float Yo;
    Yo=static_cast<float>((-1*m*cuentas_altas)+percet_alto);
    float function=static_cast<float>((m*_cuentas)+Yo);
    return function;
}


//******************************************************************************************************************************
//Wc per Sensor res:: funcion que retorna el valor segun la curva dada con los parametros de medicion indicados
//******************************************************************************************************************************
void jptwatercut::wc_per_sensor_res()
{
    //lo primero es calcular el modelo con la pendiente entre RCater y RCoil
   int RCWater;
   RCWater=ui->res_avg_water->value();
   int RCOil;
   RCOil=ui->res_avg_oil->value();
   //la pendiente es igual a M=(-100)/(Rco-Cw)
   float m;
   m=(-100)/static_cast<float>((RCOil-RCWater));
   //calculamos el valor de desplazamiento Yo, en la recta =F(0)=mX+yo
   //Yo=(-m*RCwater)+100
   float Yo;
   Yo=static_cast<float>((-m*RCWater))+100.0;
   //qDebug()<<"Yo--->"<<Yo;
   //qDebug()<<"M--->"<<m;
   //el anterior proceso me da de resultado  la pendiente m y el desplazamieto Yo.
   // de la formula F(x)=mX+Yo

   //ahora paso a realizar el proceso para cada dato de la resitencia.

    //defino ciclo que recorrera cada uno de los nodos del sensor.
    for(int _node=0 ;_node<6;_node++)
    {
        //primero se define mis cuentas a trabajar para cada sensor.
        //mis cuentas totales son las cuentas raw con las correcciones de salinidad y temperatura.
        int total_cuentas_res=static_cast<int>(a_res_sensor[_node]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
        //ahora defino el modelo

        double wc_per_sensor = static_cast<double>((m*total_cuentas_res)+Yo);
        if(wc_per_sensor>=100)
        {
            wc_per_sensor=100;
        }
        else if(wc_per_sensor<=0)
        {
            wc_per_sensor=0;
        }
        //en este punto nuestra cuentas ya retornan el valor del corte para resistividad.
        // //qDebug()<<"valor entrante ****";
        ////qDebug()<<a_res_sensor[_node]->raw;
        ////qDebug()<<a_delta_cuentas_tem_res;
        ////qDebug()<<a_delta_cuentas_sali_res;
        ////qDebug()<<total_cuentas_res;

        ////qDebug()<<"valor saliente +"<<wc_per_sensor;
        //se guarda los del WC per sensor de la resistividad en un vector y se muestran en la parte grafica.
        if(_node==0)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_1->display(wc_per_sensor);
        }
        if(_node==1)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_2->display(wc_per_sensor);

        }
        if(_node==2)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_3->display(wc_per_sensor);
        }
        if(_node==3)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_4->display(wc_per_sensor);
        }
        if(_node==4)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_5->display(wc_per_sensor);
        }
        if(_node==5)
        {

            //guaramos el valor en nuestra funcion
            wc_sensor_res[_node]=wc_per_sensor;
            //enseñamos la funcion en la parte grafica.
            ui->wc_res_6->display(wc_per_sensor);
        }





    }



}

//******************************************************************************************************************************
//Wc per Sensor res:: funcion que retorna el valor segun la curva dada con los parametros de medicion indicados
//******************************************************************************************************************************
void jptwatercut::wc_per_sensor_cap()
{
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.


    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater
    int CWater;
    CWater=ui->cap_avg_water->value();//obtengo el valor de calibracion para agua
    int CF_Water;
    CF_Water=ui->cap_avg_f_water->value();//obtengo el valor de calibracion para formaciones de agua.

    //con estos valores debo de general mi curva dinamica.
    /*NOTA: Para general la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
     * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
     * para ello se evalue la funcion cap_mixture_40_70 del jtmodel en CW y con eso obtenems el valor de x.
     *
     */
    double percent_p2=static_cast<double>(new_model->cap_mixture_model_040_070(CWater));
    ////qDebug()<<"%>>"<<percent_p2;
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    //*********************************************************************************************************
    //Aqui inicia el proceso calculo de los datos.
    //*********************************************************************************************************

    for (int _node=0; _node<6; _node++)
    {
      ////qDebug()<<"ciclo"<<i;
      int _cuentas_cap=a_cap_sensor[_node]->raw;
      double wc_per_sensor =((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-_cuentas_cap)+percent_p2;

      if(_cuentas_cap<=CF_Water)
      {
          //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
          wc_per_sensor=100;
      }
      else
      {
          if(_cuentas_cap<=CWater)
          {
              // nohaga nada es el valor actual de wc_per_sensor con la curva dinamica
          }
          else{
              if(_cuentas_cap<=7501)
              {
                wc_per_sensor =new_model->cap_mixture_model_040_070(_cuentas_cap);


              }
              else
              {
                  if(_cuentas_cap<=13801)
                  {
                     wc_per_sensor=new_model->cap_mixture_model_030_040(_cuentas_cap);


                  }
                  else
                  {
                      if(_cuentas_cap<=15001)
                      {
                         wc_per_sensor=new_model->cap_mixture_model_000_030(_cuentas_cap);


                      }
                      else
                      {
                          wc_per_sensor=0;
                      }
                  }

              }

          }
      }


      //En este punto comienzo a asignar los valores a la parte grafica
      if(_node==0)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_1->display(wc_per_sensor);
      }
      if(_node==1)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_2->display(wc_per_sensor);

      }
      if(_node==2)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_3->display(wc_per_sensor);
      }
      if(_node==3)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_4->display(wc_per_sensor);
      }
      if(_node==4)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_5->display(wc_per_sensor);
      }
      if(_node==5)
      {

          //guaramos el valor en nuestra funcion
          wc_sensor_cap[_node]=wc_per_sensor;
          //enseñamos la funcion en la parte grafica.
          ui->wc_cap_6->display(wc_per_sensor);
      }

    }
}

//******************************************************************************************************************************
//Wc per Sensor res:: funcion que retorna el valor segun la curva dada con los parametros de medicion indicados
//******************************************************************************************************************************
void jptwatercut::calcule_models()
{
    //para esta nueva funcion se evalua todo por el regimen que desempeña
    //Primero se define los valores donde se almacenaran estos datos
    double res_lam,cap_lam,res_turb,cap_turb;
    //calculo el corte laminar para la resistividad.
    res_lam=new_model->res_laminar(wc_sensor_res);
    ui->LD_LM_RE->display(res_lam);//imprimo en pantalla el corte,

    //calculo el corte laminar para la capacitancia.
    cap_lam=new_model->cap_laminar(wc_sensor_cap);
    ui->LD_LM_CA->display(cap_lam);//imprimo en pantalla el corte,

    //calculo el corte turbulento para la resistividad.
    res_turb=new_model->res_turbulent(wc_sensor_res);
    ui->LD_TU_RE->display(res_turb);//imrpimo el corte en pantalla.

    //calculo el corte turbulento para la resistividad.
    cap_turb=new_model->cap_turbulent(wc_sensor_cap);
    ui->LD_TU_CA->display(cap_turb);//imrpimo el corte en pantalla.

    //ahora calculo el corte laminar y turbulento comola media entre resistivo y capacitivo

    double final_laminar=(cap_lam+res_lam)/2;

    double final_turb=(cap_turb+res_turb)/2;

    //ahora aplico una funcion de calculo del regimen turb_lam.
    //lo primero es evalua sensor por sensor en la funcion que le corresponde
   double turb_lam_res=0;
   double turb_lam_cap=0;
   double turb_lam_counter_cap=0;
   double turb_lam_counter_res=0;
   for(int j=0;j<6;j++)
   {
       //obtengo los datos a evaluar
       float cuentas_res=static_cast<float>(a_res_sensor[j]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
       float cuentas_cap=static_cast<float>(a_cap_sensor[j]->raw + a_delta_cuentas_tem_cap + a_delta_cuentas_sali_cap);
       //verifico que esl dato se encuentre en el valor deseado
       if(ui->value_hwc_up_2->value() < cuentas_res && ui->value_hwc_dw_2->value() > cuentas_res)
       {
           //en esta caso esta en el corte alto.
           //entonces aplico el corte de esa curva.
           turb_lam_res=turb_lam_res+static_cast<double>(high_turb_laminar_res(cuentas_res));
           //qDebug()<<static_cast<double>(high_turb_laminar_res(cuentas_res));
           //qDebug("DAto entro al modelo high  , resistencia ");




       }
       else if (ui->value_lwc_up->value() < cuentas_res && ui->value_lwc_dw->value() > cuentas_res)
       {
           turb_lam_res=turb_lam_res+static_cast<double>(low_turb_laminar(cuentas_res));
           turb_lam_counter_res++;
           //qDebug("DAto entro al modelo low  , resistencia ");
       }
       else
       {
           //En este caso se aplica el modelo normal
           turb_lam_res=turb_lam_res+wc_sensor_res[j];
           //qDebug("DAto entro al modelo normal  , resistencia ");
       }


        //qDebug()<<cuentas_cap;
       //ahora realizamos el proceso para el calculo de la capacitancia.
       if(ui->value_hwc_up->value() < cuentas_cap && ui->value_hwc_dw->value() > cuentas_cap)
       {
           //en esta caso esta en el corte alto.
           //entonces aplico el corte de esa curva.
           turb_lam_cap=turb_lam_cap+static_cast<double>(high_turb_laminar(cuentas_cap));
           //qDebug("DAto entro en cuentas altas capacitancia. ");


       }
       else if (ui->value_lwc_up->value() < cuentas_cap && ui->value_lwc_dw->value() > cuentas_cap)
       {
           turb_lam_cap=turb_lam_cap+static_cast<double>(low_turb_laminar(cuentas_cap));
           //qDebug("DAto entro en cuentas bajas de capacitancia.");
           turb_lam_counter_cap++;
       }
       else
       {
           //En este caso se aplica el modelo normal
           turb_lam_cap=turb_lam_cap+wc_sensor_cap[j];
           //qDebug("DAto entro al modelo normal , capacitnacia.  ");
       }

   }
   int divid=6-turb_lam_counter_res;
   //qDebug()<<turb_lam_counter_res<<"cuentas de resistividad";
   turb_lam_res=turb_lam_res/divid;
   ui->LD_TL_RES->display(turb_lam_res);

   divid=6-turb_lam_counter_cap;
   turb_lam_cap=turb_lam_cap/divid;
   ui->LD_TL_CAP->display(turb_lam_cap);

   double final_turb_laminar=(turb_lam_res+turb_lam_cap)/2;
   ui->LD_TL_FINAL->display(final_turb_laminar);
   // turb_lam_cap=turb_lam_cap*6/5;
   //ui->LD_RE_16->display(turb_lam_cap);

    double hwc=0;
    ui->LD_RE_LAM->display(final_laminar);

    ui->LD_RE_TUR->display(final_turb);
    QString r_flow=ui->REGIME_LABEL->text();
    //seteo variables de envio de datos.
    double dato_1;
    double dato_2;
    int type_flow=0;
    if(r_flow=="LAMINAR")
    {

        ui->LD_SEND_TYPE->display("11");
        dato_1=11;
        ui->LD_SEND_VALUE->display(final_laminar);
        dato_2=final_laminar;
        type_flow=11;
    }
    else if(r_flow=="TURBULENT")
    {
        ui->LD_SEND_TYPE->display("12");
        dato_1=12;
        ui->LD_SEND_VALUE->display(final_turb);
        dato_2=final_turb;
        type_flow=12;
    }
    else if(r_flow=="TURB-LAM")
    {
        ui->LD_SEND_TYPE->display("13");
        dato_1=13;
        ui->LD_SEND_VALUE->display(final_turb_laminar);
        dato_2=final_turb_laminar;
        type_flow=13;
    }
    else
    {//este es el caso de que sea otro flujo

        ui->LD_RE_LAM->display(14);
        dato_1=14;
        ui->LD_RE_TUR->display(final_turb_laminar);
        dato_2=final_turb_laminar;
        type_flow=14;

    }

    //En este punto inicia el proceso de analisis para el estado Water-Oil-Gas

    //primero se diferencia los elementos de water del elemento de gas o superior.
    //todo elemento es gas por encima de 15900.
    //tambien sondeo la cantidad de gas y de emulsion que existe y la guardo en un parametro que sera temp_emulsion y temp_gas
    double temp_emulsion=0;
    double temp_gas_area=0;
    int fluid_type[6];
    float temp_gas=0;
    float temp_watercut=0;
    int count_gas=0;
    int count_watercut=0;

    const double area_sen[6] = {0.17,0.15,0.15,0.17,0.15,0.21};
    for (int j=0;j<6;j++) {
        float my_data=static_cast<float>(a_cap_sensor[j]->raw + a_delta_cuentas_tem_cap);
        if(my_data>=ui->value_t_phases_up->value())//si esto sucede es gas o aire.
        {
            fluid_type[j]=1;
            temp_gas_area=temp_gas_area+area_sen[j];//voy sumando las areas

            //si el dato es de tipo gas entonces debemos de calcular el porcentaje de gas que abarca.
            temp_gas=temp_gas+static_cast<float>(model_three_phases(my_data)*static_cast<float>(area_sen[j]));
            //qDebug()<<my_data;

            //qDebug()<<model_three_phases(my_data)<<"porcentaje area del modelo gas//oil ";
            //qDebug()<<temp_gas;
            count_gas++;
        }
        else
        {
            fluid_type[j]=0;
            temp_emulsion=temp_emulsion+area_sen[j];
            temp_watercut=temp_watercut+static_cast<float>(wc_sensor_cap[j]);
            count_watercut++;

        }
     //para criterios dela ponderacion
    }

    float area_agua=0;
    if(count_watercut==0 )
    {
        temp_watercut=0;
        area_agua=0;
        if(type_flow==13 || type_flow==14)
        {
            type_flow=12;
        }
        final_turb_laminar=0;

    }
    else
    {
        temp_watercut=temp_watercut/count_watercut;//saco el promedio de turbulento

        //ahora proceo el area por cantidad de sensor.
        area_agua=(static_cast<float>(temp_watercut)*static_cast<float>(temp_emulsion));

    }


   //Al terminar el ciclo he analisado todo los tipos de datos y tengo el porcentaje de gas y de emulsion.
   //qDebug()<<"Emulsion // Gas >>";
   //qDebug()<<temp_emulsion<<"cantidad de area de emulsion";
   //qDebug()<<count_watercut<<"ccantidad de sensores en emulsion";
   //qDebug()<<temp_watercut<<"corte de agua ";
   //qDebug()<<area_agua<<"area del agua";
   //qDebug()<<static_cast<float>((temp_emulsion*100)-area_agua)<<"oil en el corte de agua.";
   //qDebug()<<"**********************+";

   float area_oil=temp_gas;
   float area_gas=static_cast<float>(temp_gas_area*100)-area_oil;
   //qDebug()<<temp_gas_area*100;
   //qDebug()<<area_oil<<"area del petroleo";
   //qDebug()<<area_gas<<"area del gas";
   float area_total_oil=area_oil+static_cast<float>((temp_emulsion*100)-static_cast<float>(area_agua));
   if(count_watercut==6)
   {
       area_total_oil=static_cast<float>(100-area_agua);
       area_gas=0;
   }

   //qDebug()<<area_total_oil;

   //en este punto evaluo el corte del sistema.

   ui->LD_RE_21->display(static_cast<double>(area_agua));//agua
   ui->LD_RE_22->display(static_cast<double>(area_total_oil));//oil
   ui->LD_RE_23->display(static_cast<double>(area_gas));//gas


   //aca seteo los datos a enviar.
   QString data_send;
   if(type_flow==11)
   {
       data_send.setNum(final_laminar);
   }
   else if(type_flow==12)
   {
          data_send.setNum(final_turb);
   }
   else
   {
       data_send.setNum(final_turb_laminar);

   }
   //qDebug()<<"entro a los datos ";
   emit send_message( QString::number(type_flow), data_send);
   //qDebug()<<"Envio el dato a emit_message";

   send_to_refactor_db_persistan(type_flow,final_laminar,final_turb,final_turb_laminar,static_cast<double>(area_agua),static_cast<double>(area_total_oil),static_cast<double>(area_gas));
   //qDebug()<<"error al momebto del sql";
   //send_to_refactor_db(dato_1,dato_2);//envio los datos al refactor.

    //en este punto genero el envio de datos al sistema de salvado que se almacena en el external.


    /*
    //El calculo de los modelos es necesario que los datos por sensor ya se hayan realizado
    //usaremos estos datos y las funciones del jptmodel->res_laminar, cap_laminar,res_turbulent y cap turbulent
    //estan funciones toman los vectores de datos por sensor y regresan los valores laminar y turbulento para res o cap.



    calcule_regimen_flow();
    //*************************************************************************************
    //En este punto tengo los valores de corte generados y cargados. pero debo calcular los valores.
    //*************************************************************************************



    //ahora imprim los valores en pantalla.
    ui->LD_RE_LAM->display(final_laminar);
    ui->LD_RE_TUR->display(final_turb);
    //ahora envio el mensaje de regreso el JPT-Monitoring.

    emit send_message(QString::number(final_laminar), QString::number(final_turb));

    //ademas de ello se envia la informacion a la base de datos que se esta manejando.
    send_to_refactor_db(final_laminar,final_turb);

    */


}

//******************************************************************************************************************************
//send to recfactor db: envia la informacon de correjida de los nodos a donde corresponde.
//******************************************************************************************************************************
void jptwatercut::send_to_refactor_db(int flow_regimen,double lam, double turb, double turb_laminar, double water,double oil, double gas)
{
    //QString _path;
    //_path=ui->folder_txt->text()+"/"+ui->dest_db_label->text();
    ////qDebug(_path.toLatin1());


    //QString _path;
    //_path="/home/zahall/Escritorio/final/jptdatabase_refactor_juemar715:18:572019.db";

    // se supone ya esta conectado a la base de datos. dado que viene desde el process refactor,
    //pero como la conexion se cirra cada que se guarda en la base alerna del external, entonces se conecta de nuevo.

    //connect_db(_path);
    QSqlQuery _query;

    QString _data;//almacena la sentencia de sonsulta

    QString _lam;//seteo el dato de laminar
    _lam.setNum(lam);
    QString _turb;//seteo el dato de turbulento.
    _turb.setNum(turb);

    QString _flow_regimen;//seteo el dato de regmen de flujo
    _flow_regimen.setNum(flow_regimen);
    QString _turb_laminar;//seteo el dato de turbulento laminar
    _turb_laminar.setNum(turb_laminar);
    QString _water;//seteo el dato de area de agua
    _water.setNum(water);
    QString _oil;//seteo el dato de area de petroleo
    _oil.setNum(oil);
    QString _gas;//seteo el dato de area de gas
    _gas.setNum(gas);

    // _data="update nodex set n8_physic_1="+QString::(lam)+ " , n8_physic_2=" +QString::number(turb)+"  where id="+QString::number(runner_factor);
    //la siguiente sentencia crea el sistema de envio de datos. de laminar y turbulento para el id que estamos manejando.
     _data="update nodex set n8_physic_1="+ _lam  +" , n8_physic_2="+_turb+" , flow_regimen="+_flow_regimen+" , turb_laminar="+_turb_laminar+" , water_percent="+ _water+" , oil_percent="+_oil+" , gas_percent="+_gas+"   where id="+QString::number(runner_factor);

    _query.prepare(_data);
    _query.exec();
     ////qDebug("hata aqui va bien. ");
    if(_query.next())//si existe el valor
    {
        //si la query de update se realizo de forma correcta no pasa nada.
        //qDebug()<<"Dato de laminar y turbulento cargados de forma correcta en la base de datos.";

    }
    else
    {
        //qDebug()<<"error al cargar los valores lamimanr y turbulento en la base de datos. ";
    }


}

void jptwatercut::send_to_refactor_db_persistan(int flow_regimen,double lam, double turb, double turb_laminar, double water,double oil, double gas)
{


    //QString _path;
    //_path=ui->folder_txt->text()+"/"+ui->dest_db_label->text();
    ////qDebug(_path.toLatin1());


    //_path="/media/debian/JPTSD/watercut_jptdatabase.db";

    // se supone ya esta conectado a la base de datos. dado que viene desde el process refactor,
    //pero como la conexion se cirra cada que se guarda en la base alerna del external, entonces se conecta de nuevo.

    //connect_db(_path);


    //qDebug()<<"Entro al send to refactor";
    QSqlQuery _query;


    QString _data="";//almacena la sentencia de consulta

    QString _lam;//seteo el dato de laminar
    _lam.setNum(lam);
    //qDebug()<<_lam;

    QString _turb;//seteo el dato de turbulento.
    _turb.setNum(turb);
    //qDebug()<<_turb;


    QString _flow_regimen;//seteo el dato de regmen de flujo
    _flow_regimen.setNum(flow_regimen);
    //qDebug()<<_flow_regimen;


    QString _turb_laminar;//seteo el dato de turbulento laminar
    _turb_laminar.setNum(turb_laminar);
    //qDebug()<<_turb_laminar;

    QString _water;//seteo el dato de area de agua
    _water.setNum(water);
    //qDebug()<<_water;

    QString _oil;//seteo el dato de area de petroleo
    _oil.setNum(oil);
    //qDebug()<<_oil;

    QString _gas;//seteo el dato de area de gas
    _gas.setNum(gas);
    //qDebug()<<_gas;


    //inicio el sistema e tiempo para ingresar el nuevo dato.
    QDateTime temp_date=QDateTime::currentDateTime();
    //qDebug()<<temp_date;
    QString my_time=temp_date.toString("dd/MM/yyyy hh:mm:ss");
    //qDebug()<<my_time;

    _data="insert into Nodex (datex, n1_raw_1,n1_raw_2 ,n2_raw_1 ,n2_raw_2 ,n3_raw_1 ,n3_raw_2 ,n4_raw_1 ,n4_raw_2 ,n5_raw_1 ,n5_raw_2 ,n6_raw_1 ,n6_raw_2 , laminar ,turbulent, flow_regimen, turb_laminar ,water_percent , oil_percent, gas_percent)values";
    //qDebug()<<_data;


    _data=_data+"('"+my_time+"',";//ingreso el valor de datex
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[0]->raw).toLatin1()+",";//ingreso valor de nodo 1 raw 1
    _data=_data+""+QString::number(a_cap_sensor[0]->raw).toLatin1()+",";//ingreso valor de nodo 1 raw 2
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[1]->raw).toLatin1()+",";//ingreso valor de nodo 2 raw 1
    _data=_data+""+QString::number(a_cap_sensor[1]->raw).toLatin1()+",";//ingreso valor de nodo 2 raw 2
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[2]->raw).toLatin1()+",";//ingreso valor de nodo 3 raw 1
    _data=_data+""+QString::number(a_cap_sensor[2]->raw).toLatin1()+",";//ingreso valor de nodo 3 raw 2
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[3]->raw).toLatin1()+",";//ingreso valor de nodo 4 raw 1
    _data=_data+""+QString::number(a_cap_sensor[3]->raw).toLatin1()+",";//ingreso valor de nodo 4 raw 2
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[4]->raw).toLatin1()+",";//ingreso valor de nodo 5 raw 1
    _data=_data+""+QString::number(a_cap_sensor[4]->raw).toLatin1()+",";//ingreso valor de nodo 5 raw 2
     //qDebug()<<_data;

    _data=_data+""+QString::number(a_res_sensor[5]->raw).toLatin1()+",";//ingreso valor de nodo 6 raw 1
    _data=_data+""+QString::number(a_cap_sensor[5]->raw).toLatin1()+",";//ingreso valor de nodo 6 raw 2
     //qDebug()<<_data;

    _data=_data+""+_lam+","+_turb+","+_flow_regimen+","+_turb_laminar+","+_water+","+_oil+","+_gas+")";

    //qDebug()<<_data;

    _query.prepare(_data);
    //qDebug()<<"///////////////////////////////////////////////////////////////////////////7";
    _query.exec();
    //qDebug("hata aqui va bien. ");
    if(_query.next())//si existe el valor
    {
        //si la query de update se realizo de forma correcta no pasa nada.
        //qDebug()<<"Datos de corte y regimen cargados.";

    }
    else
    {
        //qDebug()<<"error al cargar los valores lamimanr y turbulento en la base de datos. ";
    }


}

//******************************************************************************************************************************
//calcule regimen flujo
//******************************************************************************************************************************
void jptwatercut::calcule_regimen_flow()
{


   //qDebug()<<"**************************************regimen de flujo ";
  //primero creo variables para evaluarel sitema
  int _resistividad=0;
  int _capacitancia=0;


  //para el regimen de flujo se toma solo los valores de la media del sistema.
  double _media_res=0;
  double _media_cap=0;
  for (int i=0;i<6;i++)
  {
      _media_res =_media_res+static_cast<double>(a_res_sensor[i]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
      _media_cap =_media_cap+static_cast<double>(a_cap_sensor[i]->raw + a_delta_cuentas_tem_cap + a_delta_cuentas_sali_cap);



  }

  _media_res=_media_res/6;
  _media_cap=_media_cap/6;
  //qDebug()<<"Media del regimen>>>>"<<_media_res<<"***"<<_media_cap;

  //con los valores de media procedemos a evaluar cada valor en un lapso de intervalo del 10% o del 30% vaiale en el sistema.
  //para llos calculamo los valoes a evaluar.
  //obtengo primero el valor del procentaje de variacion.

  float spin=ui->spin_regime->value();
  float spin_max=1+ static_cast<float>(spin/100);
  float spin_min=1- static_cast<float>(spin/100);
  //qDebug()<<"Spin";
  //qDebug()<<spin_max;
  //qDebug()<<spin_min;

  double _max_res=_media_res*static_cast<double>(spin_max);//calculo el valor minimo
  double _min_res=_media_res*static_cast<double>(spin_min);//calculo el valor maximo

  double _max_cap=_media_cap*static_cast<double>(spin_max);//calculo el valor minimo
  double _min_cap=_media_cap*static_cast<double>(spin_min);//calculo el valor maximo


  int cuenta_ciclos_res=0;
  int cuenta_ciclos_cap=0;


  //qDebug()<<_max_res<<"***"<<_max_cap<<"//max///"<<_min_res<<"**"<<_min_cap<<"//min//";
  //ya tengo mis valores medio de cada sensor y us variaciones respecto al spin escogido en la parte grafica.

  for (int j=0;j<6;j++)
  {    //evaluamos los valores resistivos
      int dato_sensor_res=static_cast<double>(a_res_sensor[j]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
      if(dato_sensor_res<_max_res && dato_sensor_res>_min_res )
      {
         // si el cilo se cumple entonces aumento mi contador de ciclos.
          cuenta_ciclos_res++;
      }
      //evaluamos los valores capacitivos
      int dato_sensor_cap=static_cast<double>(a_cap_sensor[j]->raw + a_delta_cuentas_tem_cap + a_delta_cuentas_sali_cap);
      if(dato_sensor_cap<_max_cap && dato_sensor_cap>_min_cap )
      {
         // si el cilo se cumple entonces aumento mi contador de ciclos.
          cuenta_ciclos_cap++;
      }


  }
  //qDebug()<<cuenta_ciclos_res<<"***"<<cuenta_ciclos_cap<<"///************************aca esta los ciclos del sistema para el regimende flujo. ";

  // si mi cuenta ciclos es 5 o supoerior  entonces el flujo es turbulento
  if(cuenta_ciclos_res>=5)
  {
      //turbulento
      //ui->REGIME_LABEL->setText("TURBULENT");
      _resistividad=1;


  }
  else
  {
      //laminar O NO SE SABE QUE MAS
     //obtengo los datos del sensor 1 y 4 de resistividad y de capacitancia
     int sensor_1=static_cast<double>(a_res_sensor[0]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
     int sensor_4=static_cast<double>(a_res_sensor[3]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);

     //primero validamos todo lo de laminar
     if( sensor_1<5000 && sensor_4>10000)
     {
         //en este caso es laminar
         //ui->REGIME_LABEL->setText("LAMINAR");
         _resistividad=2;
     }
     else if(sensor_4<5000 && sensor_1>10000)
     {
         //en este caso es laminar
         //ui->REGIME_LABEL->setText("LAMINAR");
         _resistividad=2;
     }
     else if( sensor_1<7600 && sensor_4>12000)
     {
         //en este caso es laminar
         //qDebug("el flujo es turb_lam");
         //ui->REGIME_LABEL->setText("TURB-LAM");
         _resistividad=3;
     }
     else
     {//ni puta idea que regimen es
         //qDebug()<<"este seria nuestro 4 regimen, aun desconocido. ";
         //qDebug()<<sensor_1;
         //qDebug()<<sensor_4;
         //ui->REGIME_LABEL->setText("OTROS");
         _resistividad=4;
     }






  }


  if(cuenta_ciclos_cap>=5)
  {
      //turbulento
      //ui->REGIME_LABEL->setText("TURBULENT");
      _capacitancia=1;


  }
  else
  {
      //laminar O NO SE SABE QUE MAS
     //obtengo los datos del sensor 1 y 4 de resistividad y de capacitancia

     int sensor_1=static_cast<double>(a_cap_sensor[0]->raw + a_delta_cuentas_tem_cap + a_delta_cuentas_sali_cap);
     int sensor_4=static_cast<double>(a_cap_sensor[3]->raw + a_delta_cuentas_tem_cap + a_delta_cuentas_sali_cap);

     //primero validamos todo lo de laminar
     if( sensor_1<5000 && sensor_4>10000)
     {
         //en este caso es laminar
         //ui->REGIME_LABEL->setText("LAMINAR");
         _capacitancia=2;
     }
     else if(sensor_4<5000 && sensor_1>10000)
     {
         //en este caso es laminar
         //ui->REGIME_LABEL->setText("LAMINAR");
         _capacitancia=2;
     }
     else if( sensor_1<7600 && sensor_4>12000)
     {
         //en este caso es laminar
         //qDebug("el flujo es turb_lam");
         //ui->REGIME_LABEL->setText("TURB-LAM");
         _capacitancia=3;
     }
     else
     {//ni puta idea que regimen es
         //qDebug()<<"este seria nuestro 4 regimen, aun desconocido. ";
         //qDebug()<<sensor_1;
         //qDebug()<<sensor_4;
         _capacitancia=4;
         //ui->REGIME_LABEL->setText("OTROS");
     }



  }
    //ahora valido los valores del sistema para cada regimen
  if(_resistividad==1 && _capacitancia==1)
  {
    //el regimen es turbulento puro
    ui->REGIME_LABEL->setText("TURBULENT");

  }
  else if(_resistividad==2 && _capacitancia==2)
  {
    //el regimen es laminar puro
    ui->REGIME_LABEL->setText("LAMINAR");

  }
  else if(_resistividad==1 && _capacitancia==2)
  {
    //turbulento por resistividad y laminar por capacitancia.
    ui->REGIME_LABEL->setText("TURB-LAM");

  }
  else if(_resistividad==2 && _capacitancia==1)
  {
    //laminar por descarte de la capacitancia.
    ui->REGIME_LABEL->setText("LAMINAR");

  }
  else if(_resistividad==3)
  {
    //siempre resistividad 3 sera turbulento laminar.
    ui->REGIME_LABEL->setText("TURB-LAM");

  }
  else
  {
      int sensor_1=static_cast<double>(a_res_sensor[0]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
      int sensor_4=static_cast<double>(a_res_sensor[3]->raw + a_delta_cuentas_tem_res + a_delta_cuentas_sali_res);
      if(cuenta_ciclos_res==6)
      {//el sistema es urbulento
        ui->REGIME_LABEL->setText("TURBULENT");
      }
      //primero validamos todo lo de laminar
      else if( sensor_1<7500 && sensor_4>11000)
      {
          //en este caso es laminar
          ui->REGIME_LABEL->setText("TURB-LAM");

      }
      else
      {
        ui->REGIME_LABEL->setText("OTROS");
      }



  }




}

//******************************************************************************************************************************
//charge calibration values: carga al momento de que el sistema arranca los valores de calibracion iniciales.
//******************************************************************************************************************************
void jptwatercut::charge_calibration_values()
{
    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/calibration_values.txt";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/External/calibration_values.txt";
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/calibration_values.txt";
        //qDebug("se creo el path ");

#endif

           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;

           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<8){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");

                   int my_calib_Value=line.toInt();

                   calibration_values[record]=my_calib_Value;
                   //qDebug() << "line>> "<<QString::number(calibration_values[record]);
                   record++;
               }
               record=0;
           }
           file.close();
           //ahora que el sistema ya se enuentra cargado con los datos los guardams en la parte grafica para que se visualicen
           ui->res_avg_oil->setValue(calibration_values[0]);
           ui->res_avg_air->setValue(calibration_values[1]);
           ui->res_avg_water->setValue(calibration_values[2]);
           ui->res_avg_f_water->setValue(calibration_values[3]);

           ui->cap_avg_oil->setValue(calibration_values[4]);
           ui->cap_avg_air->setValue(calibration_values[5]);
           ui->cap_avg_water->setValue(calibration_values[6]);
           ui->cap_avg_f_water->setValue(calibration_values[7]);



}

void jptwatercut::charge_hl_wc_values()
{
    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/hlwc.txt";
#else
        QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/hlwc.txt";
        //qDebug("se creo el path ");

#endif

           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;

           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<12){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");

                   int my_calib_Value=line.toInt();

                   hl_wc_values[record]=my_calib_Value;
                   //qDebug() << "line>> "<<QString::number(hl_wc_values[record]);
                   record++;
               }
               record=0;
           }
           file.close();
          //ahora que el sistema ya se enuentra cargado con los datos los guardams en la parte grafica para que se visualicen
          ui->percent_hwc_up->setValue( hl_wc_values[0]);
          ui->value_hwc_up->setValue( hl_wc_values[1]);

          ui->percent_hwc_dw->setValue( hl_wc_values[2]);
          ui->value_hwc_dw->setValue( hl_wc_values[3]);

          ui->percent_lwc_up->setValue( hl_wc_values[4]);
          ui->value_lwc_up->setValue( hl_wc_values[5]);

          ui->percent_lwc_dw->setValue( hl_wc_values[6]);
          ui->value_lwc_dw->setValue( hl_wc_values[7]);

          ui->percent_hwc_up_2->setValue( hl_wc_values[8]);
          ui->value_hwc_up_2->setValue( hl_wc_values[9]);

          ui->percent_hwc_dw_2->setValue( hl_wc_values[10]);
          ui->value_hwc_dw_2->setValue( hl_wc_values[11]);

}

//******************************************************************************************************************************
//update calibration values: permite actualizar los valores de calibracion dentro del sistema.
//******************************************************************************************************************************
void jptwatercut::update_calibration_values()
{
        // se carga el path de almacenamiento de os valores de normalizacion
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/calibration_values.txt";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/calibration_values.txt";
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/calibration_values.txt";
        ////qDebug("se creo el path ");

#endif

        //cargo los vlores de calibracion en la variable de calibracion
        //estos son valores de resitividad
        calibration_values[0]=ui->res_avg_oil->value();
        calibration_values[1]=ui->res_avg_air->value();
        calibration_values[2]=ui->res_avg_water->value();
        calibration_values[3]=ui->res_avg_f_water->value();
        //estons son de capacitancia
        calibration_values[4]=ui->cap_avg_oil->value();
        calibration_values[5]=ui->cap_avg_air->value();
        calibration_values[6]=ui->cap_avg_water->value();
        calibration_values[7]=ui->cap_avg_f_water->value();



        //procedemos a cagar en el .txt la informacion
        QFile file(path_source);
        if(!file.exists()){
            //qDebug() << "NO existe el archivo "<<path_source;
        }else{
            //qDebug() << path_source <<" encontrado...";
        }
        QString line;

        if (file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            QTextStream stream(&file);
            QString _data="";
            for (int record=0;record<8;record++)
            {
               _data=_data+QString::number(calibration_values[record])+"\n";

            }
             stream << _data <<endl;
             //qDebug()<<_data;


        }
        file.close();
        //ui->update_labal->setText("¡SUCCESS!");// si se desea se puede poner un mensaje de update de la calibracion.

}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


//******************************************************************************************************************************
// Calculando el valor de la salinidad para los sensores resistivos [1] y/o [6]           [process new entry sensors:MainWindow]
//******************************************************************************************************************************
void jptwatercut::determine_real_salinity(int node_index)
{// redefine la funcion validando 4 tipos de datos.
 // opcion 1>>check_sen_1 y check_sen_6
 // opcion 2>>solamente check 1
 // opcion 3>>solamente check 6
 // opcion 4>>esta checkeado el valor de funcion manual, es decir, el ppm es variable.
 //No es necesario realizar el ciclo cada que al sensor  1 o 6 se lea. para ello se hara una sola vez cuando en el
 //Process new entry entre terminen de entrar todos los datos.
    if(interfaz->CK_ST_F_HD->isChecked())//primero verifico si el ppm variable esta fijo.
    {
        //en este caso la salinidad esta dada por el elemento de la pantalla del sistema.

    }
    else//si entra al else significa que esta checked el nodo 1 o el 6 o ambos.
    {
        if(interfaz->CK_ST_HD_1->isChecked()  && interfaz->CK_ST_HD_6->isChecked())// estan checkeados ambos.
        {   //calculo la salinidad segun nodo 1.
            gv_salinity_res_1=jptressensor::calcule_salinity_temp(a_res_sensor[0]->raw);
            if(gv_salinity_res_1 < 0)
            {gv_salinity_res_1 = 0;
            }
            //calculo la salidad seugn nodo 6
            gv_salinity_res_6=jptressensor::calcule_salinity_temp(a_res_sensor[5]->raw);
            if(gv_salinity_res_6 < 0)
            {gv_salinity_res_6 = 0;
            }
            a_real_salinity = (gv_salinity_res_1 + gv_salinity_res_6)/2;


        }
        else if (interfaz->CK_ST_HD_1->isChecked()  && !interfaz->CK_ST_HD_6->isChecked())//valido que el 1 este on y el 6 off
        {
            //calculo la salinidad segun nodo 1.
            gv_salinity_res_1=jptressensor::calcule_salinity_temp(a_res_sensor[0]->raw);
            if(gv_salinity_res_1 < 0)
            {gv_salinity_res_1 = 0;
            }
            gv_salinity_res_6=0;
            a_real_salinity = (gv_salinity_res_1);
        }
        else if (!interfaz->CK_ST_HD_1->isChecked()  && interfaz->CK_ST_HD_6->isChecked())//valido que el 1 este off y el 6 on
        {
            //calculo la salinidad segun nodo 1.
            gv_salinity_res_6=jptressensor::calcule_salinity_temp(a_res_sensor[5]->raw);
            if(gv_salinity_res_6 < 0)
            {gv_salinity_res_6 = 0;
            }
            gv_salinity_res_1=0;
            a_real_salinity = (gv_salinity_res_6);
        }
        else
        {
            a_real_salinity=0;
        }


    }


    //qDebug()<<"*****************************salinidad********************************************";
    //qDebug()<<gv_salinity_res_1;
    //qDebug()<<gv_salinity_res_6;
    //qDebug()<<a_real_salinity;
    //seteo la salindad en el sistema.
    interfaz->LD_ST->display(a_real_salinity);

    //luego de calcular la salinidad real, se debe de calcular la compensacion por salinidad.
    //ahora realizamos los calculos de compensacion por salinidad
    calcule_compensate_salinity();


/*
    if(node_index == def_idx_sali_1)
    {    //qDebug()<<"analizando nodo 1";
        //evalua si el nodo es el numero 1.
        if(!interfaz->CK_ST_F_HD->isChecked())
        {
            if(interfaz->CK_ST_HD_1->isChecked())
            {
                //jptressensor::calcule_physic_salinity(&gv_salinity_res_1, a_res_sensor[node_index - 1]->raw);
                gv_salinity_res_1=jptressensor::calcule_salinity_temp(a_res_sensor[node_index-1]->raw);
                if(gv_salinity_res_1 < 0)
                    gv_salinity_res_1 = 0;
                if(!interfaz->CK_ST_HD_6->isChecked())
                {
                    a_real_salinity = gv_salinity_res_1;
                    //qDebug()<<a_real_salinity<<">>valor de salinidad Nodo 1, sin  6";
                    //qDebug()<<"Valor RAW>>"<<a_res_sensor[node_index-1]->raw;
                   // compensate_salinity(a_real_salinity);
                }else{
                    a_real_salinity = (gv_salinity_res_1 + gv_salinity_res_6)/2;
                    //qDebug()<<"salinidad 1>>"<<gv_salinity_res_1<<"////salindad 6>>"<<gv_salinity_res_6;
                    //qDebug()<<a_real_salinity<<">>valor de salinidad Nodo 1, con  6";
                    //compensate_salinity(a_real_salinity);
                }
            }
            interfaz->LD_ST->display(a_real_salinity);
        }

        //calcule_compensate_salinity();

    }else if(node_index == def_idx_sali_6)
    {
        if(!interfaz->CK_ST_F_HD->isChecked())
        {
            if(interfaz->CK_ST_HD_6->isChecked())
            {
                jptressensor::calcule_physic_salinity(&gv_salinity_res_6, a_res_sensor[node_index - 1]->raw);
                //qDebug()<<"en el nodo 6. salinidad 6"<<gv_salinity_res_6;
                if(gv_salinity_res_6 < 0)
                    gv_salinity_res_6 = 0;
            }
            if(!interfaz->CK_ST_HD_1->isChecked()){
                a_real_salinity = gv_salinity_res_6;
               // compensate_salinity(a_real_salinity);
            }
        }
    }

*/

}
//******************************************************************************************************************************
// Calcula la compensacion por salinidad                                                    [determine real salinity:MainWindow]
//******************************************************************************************************************************
void jptwatercut::calcule_compensate_salinity()
{

    for(int node = 0; node < def_numer_nodes_cap_res; ++node)
    {
        if(interfaz->RB_ST_CO->isChecked())
        {
            if(a_res_sensor[node]->raw < interfaz->SI_REST_L_CH->value())
            {
                a_delta_cuentas_sali_res =jptressensor::calcule_delta_sali(a_real_salinity);
            }



            if(a_cap_sensor[node]->raw < interfaz->SI_CAST_L_CH->value())
            {
              a_delta_cuentas_sali_cap =jptressensor::calcule_delta_sali(a_real_salinity);
            }

            c_res_sensor_display[node + 12]->display(a_res_sensor[node]->raw+a_delta_cuentas_sali_res);
            c_cap_sensor_display[node + 12]->display(a_cap_sensor[node]->raw+a_delta_cuentas_sali_cap);
        }
        else
        {
            c_res_sensor_display[node + 12]->display(0);
            c_cap_sensor_display[node + 12]->display(0);
        }


        //if(interfaz->RB_FL_CO->isChecked())
           // calcule_compensate_flow(node);
    }

    /*




    QVector<int> lv_cap_sensor_ncut;
    QVector<int> lv_res_sensor_ncut;

    lv_cap_sensor_ncut.append(a_cap_sensor[0]->raw); lv_res_sensor_ncut.append(a_res_sensor[0]->raw);
    lv_cap_sensor_ncut.append(a_cap_sensor[1]->raw); lv_res_sensor_ncut.append(a_res_sensor[1]->raw);
    lv_cap_sensor_ncut.append(a_cap_sensor[2]->raw); lv_res_sensor_ncut.append(a_res_sensor[2]->raw);
    lv_cap_sensor_ncut.append(a_cap_sensor[3]->raw); lv_res_sensor_ncut.append(a_res_sensor[3]->raw);
    lv_cap_sensor_ncut.append(a_cap_sensor[4]->raw); lv_res_sensor_ncut.append(a_res_sensor[4]->raw);
    lv_cap_sensor_ncut.append(a_cap_sensor[5]->raw); lv_res_sensor_ncut.append(a_res_sensor[5]->raw);

    double lam_cap(0), lam_res(0), tur_cap(0), tur_res(0), var_res(0), var_cap(0), hcut_tur_res(0), hcut_tur_cap(0), hcut_tur_med(0);

    QVector<double> cut_cap, cut_res;
    new_model->aplicate_all_model(lv_res_sensor_ncut, lv_cap_sensor_ncut, &lam_cap, &lam_res, &tur_cap, &tur_res, &var_res, &var_cap, a_real_porcentaje_variance, &cut_cap, &cut_res);

    lam_med = (lam_cap + lam_res)/2;
    tur_med = (tur_cap + tur_res)/2;

    if(var_res < 1000 && var_cap < 1000)
        tur_med = (lam_res + tur_res)/2;

    emit send_message(QString::number(lam_med), QString::number(tur_med));

    new_model->aplicate_high_model(gv_res_sensor_hcut, gv_cap_sensor_hcut, &hcut_tur_cap, &hcut_tur_res, factor, interfaz->SB_X1->value());

    double hcut_tur_cap_mod = hcut_tur_cap;

    if(interfaz->RB_EQ_HC->isChecked())
        high_cut_equa(&hcut_tur_cap_mod);
    hcut_tur_med = (hcut_tur_cap + hcut_tur_res)/2;

    gv_res_sensor_hcut.clear();
    gv_cap_sensor_hcut.clear();

    interfaz->LD_LM_CA->display(lam_cap);
    interfaz->LD_LM_RE->display(lam_res);

    interfaz->LD_TU_CA->display(tur_cap);
    interfaz->LD_TU_RE->display(tur_res);

    interfaz->LD_LM_ME->display(lam_med);
    interfaz->LD_TU_ME->display(tur_med);

    interfaz->LD_VA_CA->display(var_cap);
    interfaz->LD_VA_RE->display(var_res);

    interfaz->PB_CA_VW->setValue((int) tur_med);
    interfaz->PB_RE_VW->setValue((int) lam_med);

    interfaz->LD_HC_CA->display(hcut_tur_cap);
    interfaz->LD_HC_CA_CO->display(hcut_tur_cap_mod);
    interfaz->LD_HC_RE->display(hcut_tur_res);
    interfaz->LD_HC_ME->display(hcut_tur_res);



    */

}

#ifdef _FLOW_
//******************************************************************************************************************************
// Calcula la compensacion por flujo                                                        [determinerealsalinity:MainWindow]
//******************************************************************************************************************************
void jptwatercut::calcule_compensate_flow(int node){
    a_res_sensor[node]->compensate_flow(a_delta_cuentas_flow_res);
    a_cap_sensor[node]->compensate_flow(a_delta_cuentas_flow_cap);
}
#endif

//******************************************************************************************************************************
// Asigna el valor crudo y compensa las cuentas por temperatura : resistivo             [process_new_entry_sensors():MainWindow]
//******************************************************************************************************************************
void jptwatercut::calcule_param_sensor_resis(double valuesensor, int node){
    node = node - 1;

    a_res_sensor[node]->raw = valuesensor+delta_normalization[node];
    //paso el dato el menu de calibracion
    //creo variable temporal donde almacenar el dato RAw para poder pasarlo al menu de calibracion
    int _temporal;
    _temporal=int(a_res_sensor[node]->raw);
    //valido el nodo para pasar el dato respectivo
    if(node==0)
    {
    ui->calib_raw_r_1->display(_temporal);
    }
    if(node==1)
    {
    ui->calib_raw_r_2->display(_temporal);
    }
    else if(node==2)
    {
    ui->calib_raw_r_3->display(_temporal);
    }
    else if(node==3)
    {
    ui->calib_raw_r_4->display(_temporal);
    }
    else if(node==4)
    {
    ui->calib_raw_r_5->display(_temporal);
    }
    else if(node==5)
    {
    ui->calib_raw_r_6->display(_temporal);
    }
    //en este punto va el validador de llos casos del state_calib=1



    //gv_res_sensor_hcut.append(valuesensor);
    //esta linea muestra el valor RAW en pantalla.
    //qDebug()<<"prueba de funcionamiento de compensate_temperatura";
    c_res_sensor_display[node]->display(a_res_sensor[node]->raw);

    if(ui->RB_TP_CO->isChecked()){
        ////qDebug()<<"entro al checker";
        if(valuesensor < ui->SI_RETP_L_CH->value())
        {  // //qDebug()<<"entro compensate";

            a_delta_cuentas_tem_res=jptressensor::calcule_cuentas_temp(int(ui->LD_TP->value()));
            ////qDebug()<<a_delta_cuentas_tem_res;
            //a_res_sensor[node]->compensate_temperature(a_delta_cuentas_tem_res);


           // //qDebug()<<"compenso";
        }
        c_res_sensor_display[node + 6]->display((a_res_sensor[node]->raw)+a_delta_cuentas_tem_res);

    }
    else{
        a_delta_cuentas_tem_res=0;
        c_res_sensor_display[node + 6]->display(0);
    }






}

//******************************************************************************************************************************
// Asigna el valor crudo y compensa las cuentas por temperatura : capacitivos           [process new entry sensors():MainWindow]
//******************************************************************************************************************************
void jptwatercut::calcule_param_sensor_capac(double valuesensor, int node){
    node = node - 1;
    a_cap_sensor[node]->raw = valuesensor+delta_normalization[node + 6];

    //paso el dato el menu de calibracion
    //creo variable temporal donde almacenar el dato RAw para poder pasarlo al menu de calibracion
    int _temporal;
    _temporal=int(a_cap_sensor[node]->raw);
    //valido el nodo para pasar el dato respectivo
    if(node==0)
    {
    ui->calib_raw_c_1->display(_temporal);
    }
    if(node==1)
    {
    ui->calib_raw_c_2->display(_temporal);
    }
    else if(node==2)
    {
    ui->calib_raw_c_3->display(_temporal);
    }
    else if(node==3)
    {
    ui->calib_raw_c_4->display(_temporal);
    }
    else if(node==4)
    {
    ui->calib_raw_c_5->display(_temporal);
    }
    else if(node==5)
    {
    ui->calib_raw_c_6->display(_temporal);
    }





    gv_cap_sensor_hcut.append(valuesensor);

    c_cap_sensor_display[node]->display(a_cap_sensor[node]->raw);

    if(interfaz->RB_TP_CO->isChecked()){
        if(valuesensor < interfaz->SI_CATP_L_CH->value())
        {   a_delta_cuentas_tem_cap=jptcapsensor::calcule_cuentas_temp(int(ui->LD_TP->value()));
            //a_cap_sensor[node]->compensate_temperature(a_delta_cuentas_tem_cap);
            //qDebug()<<"delta c cap"<<a_delta_cuentas_tem_cap;
        }
        c_cap_sensor_display[node + 6]->display(a_cap_sensor[node]->raw+a_delta_cuentas_tem_cap);
    }
    else
    {   a_delta_cuentas_tem_cap=0;
        c_cap_sensor_display[node + 6]->display(0);
    }



}
//******************************************************************************************************************************
// Asigna el valor crudo y calcula con la funcion : temperatura - presion               [process new entry sensors():MainWindow]
//******************************************************************************************************************************
void jptwatercut::calcule_param_sensor_temp_and_pres(QStringList entry, int node){

    int node_t = node - 7;
    int node_p = node - 6;


    //gv_init_conection_JPTMonitoring = true;

    if(!interfaz->CK_TP_F_HD->isChecked()){
        a_real_temperature  = entry[def_pos_res_sensor].toDouble();
        a_real_pressure     = entry[def_pos_cap_sensor].toDouble();
    }

   // compensate_temperature(a_real_salinity);//?? no entiendo porque ponene esto en el codigo. . . si el parametrocambia abjo est linea no hra nada


    a_abs_sensor[node_t]->raw = entry[def_pos_res_sensor].toInt();       // Dato crudo de la temperatura
    a_abs_sensor[node_p]->raw = entry[def_pos_cap_sensor].toInt();       // Dato crudo de la presion

    if(!interfaz->CK_TP_F_HD->isChecked()){
        jptabssensor::calculate_physic_temp(interfaz->CB_TP_SL->currentIndex(), a_abs_sensor[def_pos_tem_abs_sensor]->raw, &a_real_temperature);
        interfaz->CB_TP_SL->setEnabled(true);
    }

    //compensate_temperature(a_real_temperature);
    jptabssensor::calculate_physic_pres(0, a_abs_sensor[def_pos_pre_abs_sensor]->raw, &a_real_pressure);



    ui->temperature_normal->setValue(a_real_temperature);
    interfaz->LD_TP->   display(a_real_temperature);
    interfaz->LD_PS->   display(a_real_pressure);


}
//******************************************************************************************************************************
// Funcion que calcula los delta cuentas para compensacion por temperatura                [jptcapsensor,jptressensor:MainWindow]
//******************************************************************************************************************************
void jptwatercut::compensate_temperature(double temperature){
    jptcapsensor::calcule_delta_cuentas_temp(temperature, &a_delta_cuentas_tem_cap);
    jptressensor::calcule_delta_cuentas_temp(temperature, &a_delta_cuentas_tem_res);
}
//******************************************************************************************************************************
// Funcion que calcula los delta cuentas para compensacion por Salinidad     (********)   [jptcapsensor,jptressensor:MainWindow]
//******************************************************************************************************************************
void jptwatercut::compensate_salinity(double salinity){
    jptcapsensor::calcule_delta_cuentas_sali(salinity, &a_delta_cuentas_sali_cap);
    jptressensor::calcule_delta_cuentas_sali(salinity, &a_delta_cuentas_sali_res);
}
//******************************************************************************************************************************
// Funcion que selecciona el tipo de sensor de temperatura a utilizar                                   SIGNAL [CB_TP_SL - SLOT]
//******************************************************************************************************************************
void jptwatercut::change_temp_sensor(const int sensor){
    if(gv_init_conection_JPTMonitoring)
        jptabssensor::calculate_physic_temp(sensor, a_abs_sensor[def_pos_tem_abs_sensor]->raw, &a_real_temperature);

    interfaz->LD_TP->display(QString::number(a_real_temperature, 'f', 2));
}
//******************************************************************************************************************************
// Funcion que selecciona entre salinidad estatica o calculada con res[1] y/o res[6]                  [SIGNAL:CK_ST_F_HD - SLOT]
//******************************************************************************************************************************
void jptwatercut::change_ppm_mode(int selec){
    if(selec == def_sali_static){
        interfaz->SI_ST_F_CH->setEnabled(true);
        interfaz->CK_ST_HD_1->setEnabled(false);
        interfaz->CK_ST_HD_6->setEnabled(false);

        a_real_salinity = interfaz->SI_ST_F_CH->value();
        interfaz->LD_ST->display(a_real_salinity);

    }else{
        interfaz->SI_ST_F_CH->setEnabled(false);
        if(gv_init_conection_JPTMonitoring){
            determine_real_salinity(def_idx_sali_6);
            determine_real_salinity(def_idx_sali_1);
        }else
            a_real_salinity = 0;

        interfaz->CK_ST_HD_1->setEnabled(true);
        interfaz->CK_ST_HD_6->setEnabled(true);
        interfaz->LD_ST->display(a_real_salinity);
    }
}


void jptwatercut::change_correct_temp_on_off(int selec)
{   if(selec==2)
    {
     ui->tab_correct_temp->setEnabled(true);
      update_correct_temp_on_off(1);
    }
    else
    {
       ui->tab_correct_temp->setEnabled(false);
       //Ahora situamos los valores de delta temperatura de resistencia y capacitancia  en cero.
       a_delta_cuentas_tem_cap=0;
       a_delta_cuentas_tem_res=0;
        update_correct_temp_on_off(0);

    }

}
void jptwatercut::update_correct_temp_on_off(int _status)
{
    // se carga el path de almacenamiento de os valores de normalizacion
#ifdef _HUB_ON
    QString path_source = "/jpt/jptwatercut/External/temp.txt";
#else
    //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/calibration_values.txt";
    QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/temp.txt";

    ////qDebug("se creo el path ");

#endif
    int my_estatus=_status;


    //procedemos a cagar en el .txt la informacion
    QFile file(path_source);
    if(!file.exists()){
        //qDebug() << "NO existe el archivo "<<path_source;
    }else{
        //qDebug() << path_source <<" encontrado...";
    }
    QString line;

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QString _data="";
        for (int record=0;record<1;record++)
        {
           _data=_data+QString::number(my_estatus)+"\n";

        }
         stream << _data <<endl;
         //qDebug()<<_data;


    }
    file.close();




}
void jptwatercut::charge_correct_temp_on_off()
{
    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/temp.txt";
#else

        QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/temp.txt";
        ////qDebug("se creo el path ");

#endif
           int my_estatus_temp=0;
           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;

           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<1){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");

                   int my_calib_Value=line.toInt();
                   my_estatus_temp=my_calib_Value;


                   //qDebug() << "line>> "<<my_estatus_temp;
                   record++;
               }
               record=0;
           }
           file.close();
          //valido el checker.
          if(my_estatus_temp==1)
          {
              ui->RB_TP_CO->setChecked(true);
              ui->tab_correct_temp->setEnabled(true);

          }
          else
          {
              ui->RB_TP_CO->setChecked(false);
              ui->tab_correct_temp->setEnabled(false);
          }


}


void jptwatercut::update_correct_tempval_on_off(int _status)
{
    // se carga el path de almacenamiento de os valores de normalizacion
#ifdef _HUB_ON
    QString path_source = "/jpt/jptwatercut/External/tempval.txt";
#else
    //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/calibration_values.txt";
    QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/tempval.txt";

    ////qDebug("se creo el path ");

#endif
    double my_estatus[2];
    my_estatus[0]=_status;
    my_estatus[1]=ui->SD_TP_F_CH->value();



    //procedemos a cagar en el .txt la informacion
    QFile file(path_source);
    if(!file.exists()){
        //qDebug() << "NO existe el archivo "<<path_source;
    }else{
        //qDebug() << path_source <<" encontrado...";
    }
    QString line;

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QString _data="";
        for (int record=0;record<2;record++)
        {
           _data=_data+QString::number(my_estatus[record])+"\n";

        }
         stream << _data <<endl;
         //qDebug()<<_data;


    }
    file.close();




}
void jptwatercut::charge_correct_tempval_on_off()
{
    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/tempval.txt";
#else

        QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/tempval.txt";
        ////qDebug("se creo el path ");

#endif
           int my_estatus_temp=0;
           double value_data=0;
           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;

           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<2){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");
                   if(record==0){
                       int my_calib_Value=line.toInt();
                       my_estatus_temp=my_calib_Value;
                   }
                   if(record==1){
                       value_data=line.toDouble();
                   }



                   //qDebug() << "line>> "<<my_estatus_temp;
                   record++;
               }
               record=0;
           }
           file.close();
          //valido el checker.
          if(my_estatus_temp==1)
          {

              ui->CK_TP_F_HD->setChecked(true);
              ui->SD_TP_F_CH->setValue(value_data);

          }
          else
          {
              ui->CK_TP_F_HD->setChecked(false);
              ui->SD_TP_F_CH->setValue(value_data);
          }







}


void jptwatercut::change_correct_sali_on_off(int selec)
{   if(selec==2)
    {
     ui->tab_correct_sali->setEnabled(true);
     update_correct_sali_on_off(1);
    }
    else
    {
       ui->tab_correct_sali->setEnabled(false);
       //Ahora situamos los valores de delta temperatura de resistencia y capacitancia  en cero.
       a_delta_cuentas_sali_cap=0;
       a_delta_cuentas_sali_res=0;
       update_correct_sali_on_off(0);

    }

}
void jptwatercut::update_correct_sali_on_off(int _status)
{

    // se carga el path de almacenamiento de os valores de normalizacion
#ifdef _HUB_ON
    QString path_source = "/jpt/jptwatercut/External/sali.txt";
#else

    QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/sali.txt";

    ////qDebug("se creo el path ");

#endif
    int my_estatus=_status;

    //procedemos a cagar en el .txt la informacion
    QFile file(path_source);
    if(!file.exists()){
        //qDebug() << "NO existe el archivo "<<path_source;
    }else{
        //qDebug() << path_source <<" encontrado...";
    }
    QString line;

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QString _data="";
        for (int record=0;record<1;record++)
        {
           _data=_data+QString::number(my_estatus)+"\n";

        }
         stream << _data <<endl;
         //qDebug()<<_data;


    }
    file.close();





}
void jptwatercut::charge_correct_sali_on_off()
{

    // se carga el path de almacenamiento del delta de normalizacion.
#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/sali.txt";
#else

        QString path_source ="/home/zahall/Documentos/build-jptwatercut-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/sali.txt";
        ////qDebug("se creo el path ");

#endif
           int my_estatus_temp=0;
           QFile file(path_source);
           if(!file.exists()){
               //qDebug() << "NO existe el archivo "<<path_source;
           }else{
               //qDebug() << path_source <<" encontrado...";
           }
           QString line;

           if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
               QTextStream stream(&file);
               int record=0;
               while (!stream.atEnd() && record<1){
                   line = stream.readLine();
                   //ui->textEdit->setText(ui->textEdit->toPlainText()+line+"\n");

                   int my_calib_Value=line.toInt();
                   my_estatus_temp=my_calib_Value;


                   //qDebug() << "line>> "<<my_estatus_temp;
                   record++;
               }
               record=0;
           }
           file.close();
          //valido el checker.
          if(my_estatus_temp==1)
          {
              ui->RB_ST_CO->setChecked(true);
              ui->tab_correct_sali->setEnabled(true);

          }
          else
          {
              ui->RB_ST_CO->setChecked(false);
              ui->tab_correct_sali->setEnabled(false);
          }


}



void jptwatercut::value_hwc_up_change(int entry_data)
{
    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_hwc_dw->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_hwc_up->setValue(dw_val-1000);

    }

}
void jptwatercut::value_hwc_dw_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_hwc_up->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_hwc_dw->setValue(dw_val+1000);

    }

}


void jptwatercut::value_hwc_up_2_change(int entry_data)
{
    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_hwc_dw_2->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_hwc_up_2->setValue(dw_val-1000);

    }

}
void jptwatercut::value_hwc_dw_2_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_hwc_up_2->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_hwc_dw_2->setValue(dw_val+1000);

    }

}



void jptwatercut::value_lwc_up_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_lwc_dw->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_lwc_dw->setValue(dw_val-1000);

    }

}
void jptwatercut::value_lwc_dw_change(int entry_data)
{
    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->value_lwc_up->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->value_lwc_dw->setValue(dw_val+1000);

    }

}

void jptwatercut::percent_hwc_up_change(int entry_data)
{
    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_hwc_dw->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_hwc_up->setValue(dw_val+5);

    }

}
void jptwatercut::percent_hwc_dw_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_hwc_up->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_hwc_dw->setValue(dw_val-5);

    }

}

void jptwatercut::percent_hwc_up_2_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_hwc_dw_2->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_hwc_up_2->setValue(dw_val+5);

    }

}
void jptwatercut::percent_hwc_dw_2_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_hwc_up_2->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_hwc_dw_2->setValue(dw_val-5);

    }

}


void jptwatercut::percent_lwc_up_change(int entry_data)
{
    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_lwc_dw->value();//obtengo el valor bajo
    if(entry_data>dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_lwc_up->setValue(dw_val+5);

    }

}
void jptwatercut::percent_lwc_dw_change(int entry_data)
{

    //comparo este valor con el valor de corte bajo si no lo dejo en el maximo
    int dw_val=ui->percent_lwc_up->value();//obtengo el valor bajo
    if(entry_data<dw_val)
    {
        //si esto sucede no hago nada, dado que es correcto
    }
    else
    {//si el valor del up es inferior al del down entonces seteo el valor en 1000 cuentas arriba del otro valor.
        ui->percent_lwc_dw->setValue(dw_val-5);

    }

}


//******************************************************************************************************************************
// Funcion que selecciona entre temperatura estatica o calculada con temp[7]                          [SIGNAL:CK_FP_F_HD - SLOT]
//******************************************************************************************************************************
void jptwatercut::change_temp_mode(int selec){
    if(selec == def_temp_static){
        interfaz->SD_TP_F_CH->setEnabled(true);
        interfaz->CB_TP_SL->setEnabled(false);
        a_real_temperature = interfaz->SD_TP_F_CH->value();
        interfaz->LD_TP->display(QString::number(a_real_temperature, 'f', 2));
        //aca meto mi update de temperatura.
        update_correct_tempval_on_off(1);
    }else{
        interfaz->SD_TP_F_CH->setEnabled(false);
        interfaz->CB_TP_SL->setEnabled(true);
        update_correct_tempval_on_off(0);
        change_temp_sensor(interfaz->CB_TP_SL->currentIndex());

    }
}
//******************************************************************************************************************************
// Funcion que varia el valor de temperatura, segun lo seleccionado en el SpinBox para valor fijo     [SIGNAL:SI_TP_F_CH - SLOT]
//******************************************************************************************************************************
void jptwatercut::temp_fixed_change(double temp){
    a_real_temperature = temp;
    compensate_temperature(a_real_temperature);
    interfaz->LD_TP->display(QString::number(a_real_temperature, 'f', 2));
}
//******************************************************************************************************************************
// Funcion que varia el valor de porcentaje de varianza                                                 [SIGNAL:SI_PO_CH - SLOT]
//******************************************************************************************************************************
void jptwatercut::porc_fixed_change(int porc){
    a_real_porcentaje_variance = porc;
}
//******************************************************************************************************************************
// Funcion que varia el valor de Salinidad, segun lo seleccionado en el SpinBox para valor fijo       [SIGNAL:SI_ST_F_CH - SLOT]
//******************************************************************************************************************************
void jptwatercut::ppm_fixed_change(int ppm){
    a_real_salinity = ppm;
    compensate_salinity(a_real_salinity);
    interfaz->LD_ST->display(a_real_salinity);
}
#ifdef _FLOW_
//******************************************************************************************************************************
// Funcion que varia el valor de Flujo, segun lo seleccionado en el SpinBox para valor fijo           [SIGNAL:SD_FL_F_CH - SLOT]
//******************************************************************************************************************************
void jptwatercut::flow_fixed_change(double flow){
    jptcapsensor::calcule_delta_cuentas_flow(flow, &a_delta_cuentas_flow_cap);
    jptcapsensor::calcule_delta_cuentas_flow(flow, &a_delta_cuentas_flow_res);
}
#endif

//******************************************************************************************************************************
// Funcion que inicia la conexion con JPTMonitoring                                         [SIGNAL - jpttcpclient:send_message]
//******************************************************************************************************************************
void jptwatercut::start_communication(){
#ifdef _TRAME_CC
    emit start_connection("Start-WaterCut");
#endif
#ifdef _TRAME_JP
    emit start_connection("Start");
#endif
}
//******************************************************************************************************************************
// Si la conexion no se inicia o se bloquea se reinica el timer para intentar conectar de nuevo     [SIGNAL:jpttcpclient - SLOT]
//******************************************************************************************************************************
void jptwatercut::restart_communication(){
    a_open_conection->start();
}
//******************************************************************************************************************************
// Creando la instancia del sensor de temperatura[6] y de presion[7]                                   [jptabssensor:MainWindow]
//******************************************************************************************************************************
void jptwatercut::charge_param_sensor_temp_and_press(){
    a_abs_sensor.append(new jptabssensor());
    a_abs_sensor.append(new jptabssensor());
}
//******************************************************************************************************************************
// Creando las instancias de los sensores resistivos y capacitivos                        [jptressensor,jptcapsensor:MainWindow]
//******************************************************************************************************************************
void jptwatercut::charge_param_sensor_resi_and_capac(){
    for(int sensor =   0; sensor < def_numer_nodes_cap_res; sensor++){
        a_cap_sensor.append(new jptcapsensor());
        a_res_sensor.append(new jptressensor());
    }
}
//******************************************************************************************************************************
// Generando grafica de alto corte                                                                   [3_Tab_High_Cut:MainWindow]
//******************************************************************************************************************************
void jptwatercut::gen_graphic(){

    int x_1 = interfaz->SB_X1->value();
    int x_2 = interfaz->SB_X2->value();

    int y_1 = interfaz->SB_Y1->value();
    int y_2 = interfaz->SB_Y2->value();

    factor = (double)(y_2-y_1)/(x_2-x_1);
}
//******************************************************************************************************************************
// Correccion para alto corte                                                                                     [:MainWindow]
//******************************************************************************************************************************
void jptwatercut::high_cut_equa(double *high_cut){

    QString equation = interfaz->LE_HC->text();

    try{
        equation.replace("w", QString::number(*high_cut));
        ValueList       lv_vlist;
        FunctionList    lv_flist;
        lv_vlist.AddDefaultValues();
        lv_flist.AddDefaultFunctions();

        Expression lv_e;
        lv_e.SetValueList(&lv_vlist);
        lv_e.SetFunctionList(&lv_flist);
        lv_e.Parse(equation.toStdString());

        *high_cut = lv_e.Evaluate();

        interfaz->LE_HC->setStyleSheet("background-color:rgb(255,255,255)");
    }catch(...){
        interfaz->LE_HC->setStyleSheet("background-color:rgb(200,100,100)");
    }
}


//******************************************************************************************************************************
//Refactor process                                                                                  [:MainWindow]
//******************************************************************************************************************************
void jptwatercut::refactor_data()
{
    //qDebug("Beging REfactor data");
    //iniciamos la variable para escritura en el sistema
    QString data_message=ui->connect_txt->text();
    ui->charge_data->setEnabled(false);
    ui->refactor->setEnabled(false);
    /*
    //se inicia la base de datos del source
    QSqlDatabase Source_db=QSqlDatabase::database("SourceDb");
    Source_db.open();
    */
    //se inicia un elemento para hacer la persistencia en la base de datos
    QSqlQuery refactor_query;
    refactor_query.prepare( "select count(*) from Characteristics");
    //refactor_query.prepare( "select count(*) from Node_0001");//prueba select

    refactor_query.exec();

    //esta variable "number_nodes" tiene el numero de nodos a hacer refactor
    int number_nodes;

    //Nota: es necesario poner el query.next() o el valor no se cargara y se podra ver.
    if(refactor_query.next())
    {
        number_nodes=refactor_query.value(0).toInt();
        if(number_nodes>7)
         {
            //mensaje de despliegue de la cantidad de nodos.
            data_message=data_message+"\n";
            data_message=data_message+"Preparating System \n";
            data_message=data_message+"Number of nodes to refactor: ..."+QString::number(number_nodes)+"\n";
            ui->refactor->setEnabled(false);
            ui->connect_txt->setText(data_message);
            //qDebug("Begin node helper refactor");
            node_helper_refactor(1);
            tam_matriz_node();

            QString dest_path;
            dest_path=""+ui->folder_txt->text();
            dest_path=dest_path+"/"+ui->dest_db_label->text();

            bool database_refactor;
            database_refactor=connect_db(dest_path);



            if(!database_refactor)
            {
                //se despliega error de envio de paquete
                //qDebug("error enviando paquete");

            }
            else
            {
                //qDebug("Begin Create pack to send");
                parse_node();
                //qDebug("Finish to parse nodes.");

            }




         }
        else {//number_nodes<=0
            data_message=data_message+"\n";
            data_message=data_message+"Database: Source Db>>Error>> the number of nodes is not enough ";
            data_message=data_message+"\n";
            data_message=data_message+". . .Please Verify the selected Database";
            ui->refactor->setEnabled(false);
            ui->charge_data->setEnabled(true);
            ui->connect_txt->setText(data_message);

        }
    }//refactor_query.next
    else
    {
       data_message=data_message+"\n";
       data_message=data_message+"Database:Source Db>>Error>>System cannot persist the Database";
       data_message=data_message+refactor_query.exec();
       ui->connect_txt->setText(data_message);

    }


    //ui->connect_txt->setText(data_message);

}
//******************************************************************************************************************************
//node_helper_refactor : se usa para calcular la cantidad de paquetes que seenviaran
//******************************************************************************************************************************
void jptwatercut::node_helper_refactor(int id_node)
{
    //iniciamos la variable para escritura en el sistema
    QString data_message=ui->connect_txt->text();

    //aun tenemos la persistencia creada en la base de datos inicial. . .
    //realizamos la primera consulta.
    QSqlQuery node_1_count;
    node_1_count.prepare("select count(*) from Node_0001");
    node_1_count.exec();
    //lo usare para validar la cantidad de datos en un paquete.
    int c_number_data=0;
    //recorrera los datos totales existentes en cada nodo, basado en el Nodo_0001
    int c_node_1;
    int c_data_discart=0;
    if(node_1_count.next())
    {   //obtenemos el valor de datos en el nodo 1.
        c_node_1=node_1_count.value(0).toInt();

        int packages=0;//paquetes totales
        if(c_node_1==0)
        {//si es igual a cero no existen datos que parcear o existe un error en la secuencia de conexion SQLITE.
           data_message=data_message+"\n"+"Error connectiong to the Node_0001";
           data_message=data_message+"\n"+"Error: Data in Nodes is not enought.";
        }
        else
        {//c_node_1==0
            lengh_node_1=c_node_1;

             //si es el ciclo es significa que existe data para parsear
             //obtenemos la cantidad de paquetes completos
             int full_pack=c_node_1/1000;
             //obtenemos si hay paquetes incompletos
             int regular_pack=c_node_1%1000;
             data_message=data_message+"\n"+"Data packes to refactor: "+QString::number(full_pack);
             f_pack=full_pack;
             data_message=data_message+"\n"+"Data packes incomplete: "+QString::number(regular_pack);
             reg_pack=regular_pack;
             //cantidad de transacciones totales segun paquetes.
             if(regular_pack==0)
             {   packages=full_pack;
             }
             else
             {   packages=full_pack+1;
             }
             data_message=data_message+"\n"+"Total Transactions="+QString::number(packages);
             ui->connect_txt->setText(data_message);




        }//c_node_1==0



    }//node_1_c.next
    else
    {//node_1_c.next
         data_message=data_message+"\n"+"Error: error on next sequence.";
         ui->connect_txt->setText(data_message);
    }//node_1_c.next



    //ui->connect_txt->setText(data_message);
}
//******************************************************************************************************************************
//Create_pack_to send: organiza los paquetes segun la organizacion que dio el helper refactor.
//******************************************************************************************************************************
void jptwatercut::create_pack_to_send()
{
    //primero iniciamos el data messages:
    QString data_message=ui->connect_txt->text();
    //Se crea una matriz de tamaño n, donde n es la cantidad de datos que posee el nodo 1 y que sabemos gracias apasos anteiores
    //en las columanas de la matriz se guardarn 2 datos el RAw 1 y el Raw 2

    //int my_matrix_node_1[lengh_node_1][2];

    //QDateTime my_vector_time_node_1[lengh_node_1];
    //creamos nuestra primera consulta query la cual sera del nodo 1,para ello creamos un elemento y de ahi en adelante.
    QSqlQuery node_1_rows;
    node_1_rows.prepare("select * from node_0001 ");

    node_1_rows.exec();


    //inicio el elemento que recorrera la matriz
    int record_node_1=0;
    //qDebug("cargamos los valores del nodo 1 en una matrix de elementos ");
    while(node_1_rows.next())
    {//recorre todos los valores del nodo 1 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_1[record_node_1][0]=node_1_rows.value(1).toInt();
            my_matrix_node_1[record_node_1][1]=node_1_rows.value(3).toInt();            
            my_vector_time_node_1[record_node_1]=node_1_rows.value(5).toDateTime();

            //data_message=data_message+">>"+QString::number(my_matrix_node_1[record_node_1][0])+"::"+QString::number(my_matrix_node_1[record_node_1][1])+"::"+my_vector_time_node_1[record_node_1].toString("yy/MM/dd hh:mm:ss")+"\n";
            //ui->connect_txt->setText(data_message);
            record_node_1=record_node_1+1;

    }//while(node_1_rows.next())


    //qDebug("verificamos la cantidad de paquetes a crear");
    if(f_pack==0){
        //no crea ningun paquete dado que el f_pack debe de er mayor a cero .
    }
    else
    {

        //qDebug("comanzamos a crear paquetes");
        //data_message="aca ando";
        //ui->connect_txt->setText(data_message);
        //creo un ciclo para los paquetes completos a analizar.
        record_node_1=0;
        int pack =1;
        int index_trama=0;
        for (pack=1;pack<(f_pack+1);pack++)
        {
            //qDebug("index pack "+QString::number(pack).toLatin1());
            //NOTA:
            //En las siguientes lineas de codigo se realiza el proceso de creacion de cada paquete,
            //partiendo de la informacon obtenida por las diferentes consultas.
            //
            //creo variable de indexacion de la trama

            //creo una matriz de elementos con n filas= 1000 tamaño de las tramas en el paquete
            //y columnas 14( id trama, 2 datos por nodos y son 7 nodos,el timestamp)
            int trama[1001][14];
            QDateTime time_trama[1001];

            //variables temporales de datos
            int raw1_n_2;
            int raw2_n_2;
            int raw1_n_3;
            int raw2_n_3;
            int raw1_n_4;
            int raw2_n_4;
            int raw1_n_5;
            int raw2_n_5;
            int raw1_n_6;
            int raw2_n_6;
            int raw1_n_7;
            int raw2_n_7;
            //data_message=data_message+"\n >>creando variables <<"+QString::number(record_node_1);
            //ui->connect_txt->setText(data_message);



            for (int j=0;j<1000;j++)
            { //este ciclo for arma tramas dentro del paquete de 1000 elementos.
                //qDebug("trama>>"+QString::number(j).toLatin1());


                //data_message=data_message+QString::number(record_node_1)+">> \n";
                //ui->connect_txt->setText(data_message);

                //creamos el time stamp inicial y final
                QDateTime time_init=my_vector_time_node_1[record_node_1].addSecs(-5);
                QDateTime time_final=my_vector_time_node_1[record_node_1].addSecs(5);
                QString t_init=time_init.toString("yy/MM/dd hh:mm:ss");
                QString t_final=time_final.toString("yy/MM/dd hh:mm:ss");

                QSqlQuery node_2;
                node_2.prepare("select * from Node_0002 where date between '20"+t_init+"' and '20"+t_final+"'");
                node_2.exec();
                if(node_2.next())
                {//si existe respuesta guarda el valor de los datos del nodo 2

                    raw1_n_2=node_2.value(1).toInt();
                    raw2_n_2=node_2.value(3).toInt();
                    //qDebug("cargando valores _N2");


                    //ahora trata de realizar el mismo proceso para el 3
                    QSqlQuery node_3;
                    node_3.prepare("select * from Node_0003 where date between '20"+t_init+"' and '20"+t_final+"'");
                    node_3.exec();
                    if(node_3.next())
                    {//si existe respuesta guarda el valor de los datos del nodo 3

                        raw1_n_3=node_3.value(1).toInt();
                        raw2_n_3=node_3.value(3).toInt();
                        //qDebug("cargando valores _N3");
                        //ahora trata de realizar el mismo proceso para el 4
                        QSqlQuery node_4;
                        node_4.prepare("select * from Node_0004 where date between '20"+t_init+"' and '20"+t_final+"'");
                        node_4.exec();
                        if(node_4.next())
                        {//si existe respuesta guarda el valor de los datos del nodo 4
                            raw1_n_4=node_4.value(1).toInt();
                            raw2_n_4=node_4.value(3).toInt();
                            //qDebug("cargando valores _N4");
                            //ahora trata de realizar el mismo proceso para el 5
                            QSqlQuery node_5;
                            node_5.prepare("select * from Node_0005 where date between '20"+t_init+"' and '20"+t_final+"'");
                            node_5.exec();
                            if(node_5.next())
                            {//si existe respuesta guarda el valor de los datos del nodo 5
                                raw1_n_5=node_5.value(1).toInt();
                                raw2_n_5=node_5.value(3).toInt();
                                //qDebug("cargando valores _N5");
                                //ahora trata de realizar el mismo proceso para el 6
                                QSqlQuery node_6;
                                node_6.prepare("select * from Node_0006 where date between '20"+t_init+"' and '20"+t_final+"'");
                                node_6.exec();
                                if(node_6.next())
                                {//si existe respuesta guarda el valor de los datos del nodo 6
                                    raw1_n_6=node_6.value(1).toInt();
                                    raw2_n_6=node_6.value(3).toInt();
                                    //qDebug("cargando valores _N6");
                                    //ahora trata de realizar el mismo proceso para el 7 el cual es el ultimo nodo
                                    QSqlQuery node_7;
                                    node_7.prepare("select * from Node_0007 where date between '20"+t_init+"' and '20"+t_final+"'");
                                    node_7.exec();
                                    if(node_7.next())
                                    {//si existe respuesta guarda el valor de los datos del nodo 7
                                        raw1_n_7=node_7.value(1).toInt();
                                        raw2_n_7=node_7.value(3).toInt();
                                        //qDebug("cargando valores _N7");
                                        //si el sistema llega ha este punto entonces guarda la trama en el vector
                                        /*
                                        trama[index_trama][0]=my_matrix_node_1[record_node_1][0];
                                        trama[index_trama][1]=my_matrix_node_1[record_node_1][1];
                                        trama[index_trama][2]=raw1_n_2;
                                        trama[index_trama][3]=raw2_n_2;
                                        trama[index_trama][4]=raw1_n_3;
                                        trama[index_trama][5]=raw2_n_3;
                                        trama[index_trama][6]=raw1_n_4;
                                        trama[index_trama][7]=raw2_n_4;
                                        trama[index_trama][8]=raw1_n_5;
                                        trama[index_trama][9]=raw2_n_5;
                                        trama[index_trama][10]=raw1_n_6;
                                        trama[index_trama][11]=raw2_n_6;
                                        trama[index_trama][12]=raw1_n_7;
                                        trama[index_trama][13]=raw2_n_7;
                                        */



                                        //time_trama[index_trama]=my_vector_time_node_1[record_node_1];

                                        //si la informacion se guarda de forma correcta entonce se realiza un proceso de cirrimiento de la trama
                                        index_trama++;
                                        //qDebug("Generando Trama>>>>>>>>>>>>>>>>>>>>>"+QString::number(index_trama).toLatin1());
                                        //qDebug("Timestamp>>>>>>>>>>>"+my_vector_time_node_1[record_node_1].toString("yy/MM/dd hh:mm:ss").toLatin1());








                                    }//node 7






                                }//node 6

                            }//node 5

                        }//node 4

                    }//node 3

                }//node 2
                else
                {
                   data_message="no tiene valor nexth nodo 2";
                }



            //
            record_node_1++;
            }//recorro la matriz del nodo 1
            data_message=data_message+"\n"+"rwas 1 node 2"+QString::number(raw1_n_2);




        }//for pack...
        data_message=data_message+">>"+QString::number(pack);

    }

    data_message=data_message+">>"+QString::number(f_pack)+">>"+QString::number(reg_pack);

    ui->connect_txt->setText(data_message);

}
//******************************************************************************************************************************
//get_id_from_time:regresa el valor del Id encontrado para un nodo segun un valor de Time Stamp
//******************************************************************************************************************************
int jptwatercut::get_id_from_time(QDateTime my_timestamp,int node)
{
    int id_data=0;
    return id_data;
}
//******************************************************************************************************************************
//funcion que encuentra el tamaño de la matriz de datos y carga los valores a cada matriz de datos.
//******************************************************************************************************************************
void jptwatercut::tam_matriz_node()
{   //esta funcion encontrara la cantidad de datos que se tiene por cada nodo y los guardaremos en el sistema
    //>>>>>>>>FUNCION NODO_0001
    QSqlQuery query_node;
    query_node.prepare("select * from node_0001 ");
    query_node.exec();
    int record_node=0;
    //qDebug("cargamos los valores del nodo 1 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 1 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_1[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_1[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_1[record_node]=query_node.value(5).toDateTime();

            record_node=record_node+1;

    }//while(node_1_rows.next())

    lengh_node_1=record_node;


    //>>>>>>>>FUNCION NODO_0002
    query_node.prepare("select * from node_0002 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 2 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 2 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_2[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_2[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_2[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_2_rows.next())

    lengh_node_2=record_node;


    //>>>>>>>>FUNCION NODO_0003
    query_node.prepare("select * from node_0003 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 3 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 3 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_3[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_3[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_3[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_2_rows.next())

    lengh_node_3=record_node;

    //>>>>>>>>FUNCION NODO_0004
    query_node.prepare("select * from node_0004 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 4 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 4 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_4[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_4[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_4[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_4_rows.next())

    lengh_node_4=record_node;

    //>>>>>>>>FUNCION NODO_0005
    query_node.prepare("select * from node_0005 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 5 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 5 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_5[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_5[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_5[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_5_rows.next())

    lengh_node_5=record_node;


    //>>>>>>>>FUNCION NODO_0006
    query_node.prepare("select * from node_0006 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 6 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 6 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_6[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_6[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_6[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_6_rows.next())

    lengh_node_6=record_node;


    //>>>>>>>>FUNCION NODO_0007
    query_node.prepare("select * from node_0007 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 7 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 7 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_7[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_7[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_7[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_2_rows.next())

    lengh_node_7=record_node;


    //>>>>>>>>FUNCION NODO_0008
    query_node.prepare("select * from node_0008 ");
    query_node.exec();
    record_node=0;
    //qDebug("cargamos los valores del nodo 8 en una matrix de elementos ");
    while(query_node.next())
    {//recorre todos los valores del nodo 8 los cuales nos dan la cantidad de paquetes posibles.

            my_matrix_node_8[record_node][0]=query_node.value(1).toInt();
            my_matrix_node_8[record_node][1]=query_node.value(3).toInt();
            my_vector_time_node_8[record_node]=query_node.value(5).toDateTime();


            record_node=record_node+1;

    }//while(node_2_rows.next())

    lengh_node_8=record_node;


    //qDebug("los valores del tamaño de nodos son:");
    //qDebug("nodo 1>>"+QString::number(lengh_node_1).toLatin1());
    //qDebug("nodo 2>>"+QString::number(lengh_node_2).toLatin1());
    //qDebug("nodo 3>>"+QString::number(lengh_node_3).toLatin1());
    //qDebug("nodo 4>>"+QString::number(lengh_node_4).toLatin1());
    //qDebug("nodo 5>>"+QString::number(lengh_node_5).toLatin1());
    //qDebug("nodo 6>>"+QString::number(lengh_node_6).toLatin1());
    //qDebug("nodo 7>>"+QString::number(lengh_node_7).toLatin1());
    //qDebug("nodo 8>>"+QString::number(lengh_node_8).toLatin1());




}
//******************************************************************************************************************************
//funcion que encuentra el tamaño de la matriz de datos y carga los valores a cada matriz de datos.
//******************************************************************************************************************************
void jptwatercut::parse_node()
{
    //qDebug("Enter to parser node");


    //qDebug("verificamos la cantidad de paquetes a crear");
    if(f_pack==0 &&  reg_pack==0){
        //no crea ningun paquete dado que el f_pack debe de ser mayor a cero .
    }
    else
    {

        //qDebug("comanzamos a crear paquetes");
        //creo un ciclo para los paquetes completos a analizar.
        int record_node=0;
        int pack =1;
        int runer_2=0;
        int runer_3=0;
        int runer_4=0;
        int runer_5=0;
        int runer_6=0;
        int runer_7=0;
        int runer_8=0;
        //inicio una variable de paquete que se cargara con la informacion de todas las tramas.

        QString pack_to_send="";
        QString _trama_temporal="";

        //int id_trama=0;
        for (pack=1;pack<(f_pack+2);pack++)
        {

            pack_to_send="insert into Nodex (datex, n1_raw_1,n1_raw_2 ,n2_raw_1 ,n2_raw_2 ,n3_raw_1 ,n3_raw_2 ,n4_raw_1 ,n4_raw_2 ,n5_raw_1 ,n5_raw_2 ,n6_raw_1 ,n6_raw_2 ,n7_raw_1 ,n7_raw_2, n8_raw_1 ,n8_raw_2 )values";
            //qDebug("<<<<<<<<<<<<<<<<<<<<<<<<INDEX PACK <<<< "+QString::number(pack).toLatin1());
            //qDebug("<<<<<<<<<<<<<<<<<<<<<<<<INDEX PACK <<<< "+QString::number(pack).toLatin1());
            //NOTA:
            //En las siguientes lineas de codigo se realiza el proceso de creacion de cada paquete,
            //partiendo de la informacon obtenida por las diferentes consultas.
            //
            //creo variable de indexacion de la trama

            //creo una matriz de elementos con n filas= 1000 tamaño de las tramas en el paquete
            //y columnas 14( id trama, 2 datos por nodos y son 7 nodos,el timestamp)
            QDateTime time_trama[lengh_node_1+1];

            //variables temporales de datos
            int raw1_n_1;
            int raw2_n_1;
            int raw1_n_2;
            int raw2_n_2;
            int raw1_n_3;
            int raw2_n_3;
            int raw1_n_4;
            int raw2_n_4;
            int raw1_n_5;
            int raw2_n_5;
            int raw1_n_6;
            int raw2_n_6;
            int raw1_n_7;
            int raw2_n_7;
            int raw1_n_8;
            int raw2_n_8;


            if(pack==(f_pack+1) || f_pack==0)
            {
                //qDebug("entro a relizar el parseo de los 330 datos ***");
                //en este punto realiza la funcion para los paquetes faltantes.
                for (int j=0;j<reg_pack;j++)
                {

                    //CREO VARIABLES TEMPORALES DE RECORRIDO
                    int run_2_temp=runer_2;
                    int run_3_temp=runer_3;
                    int run_4_temp=runer_4;
                    int run_5_temp=runer_5;
                    int run_6_temp=runer_6;
                    int run_7_temp=runer_7;
                    int run_8_temp=runer_8;
                    //este ciclo for arma tramas dentro del paquete de  elemento, la cntiad de paqueres es la vriable guardada en reg_pack
                    //qDebug("Pack"+QString::number(pack).toLatin1()+">>>trama>>"+QString::number(j).toLatin1());
                    int temporal_trama=1;
                    //creamos el time stamp inicial y final
                    QDateTime time_init=my_vector_time_node_1[record_node].addSecs(-7);
                    QDateTime time_final=my_vector_time_node_1[record_node].addSecs(7);
                    raw1_n_1=my_matrix_node_1[record_node][0];
                    raw2_n_1=my_matrix_node_1[record_node][1];

                    //se cargan los valores del nodo 2
                    int bandera=0;
                    //int runer_2=0;
                    while ( runer_2!=lengh_node_2 && bandera==0 )
                    {
                        if(my_vector_time_node_2[runer_2]>time_init && my_vector_time_node_2[runer_2]<time_final)
                        {
                            raw1_n_2=my_matrix_node_2[runer_2][0];
                            raw2_n_2=my_matrix_node_2[runer_2][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_2++;
                        }

                    }//While node 2

                    //se cargan los valores del nodo 3
                    bandera=0;
                    //int runer_3=0;
                    while (  runer_3!=lengh_node_3 && bandera==0 )
                    {
                        if(my_vector_time_node_3[runer_3]>time_init && my_vector_time_node_3[runer_3]<time_final)
                        {
                            raw1_n_3=my_matrix_node_3[runer_3][0];
                            raw2_n_3=my_matrix_node_3[runer_3][1];
                            bandera++;
                             temporal_trama++;


                        }
                        else {
                            runer_3++;
                        }

                    }//While node 3

                    //se cargan los valores del nodo 4
                    bandera=0;
                    //int runer_4=0;
                    while (  runer_4!=lengh_node_4 && bandera==0 )
                    {
                        if(my_vector_time_node_4[runer_4]>time_init && my_vector_time_node_4[runer_4]<time_final)
                        {
                            raw1_n_4=my_matrix_node_4[runer_4][0];
                            raw2_n_4=my_matrix_node_4[runer_4][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_4++;
                        }

                    }//While node 4

                    //se cargan los valores del nodo 5
                    bandera=0;
                    //int runer_5=0;
                    while (  runer_5!=lengh_node_5 && bandera==0 )
                    {
                        if(my_vector_time_node_5[runer_5]>time_init && my_vector_time_node_5[runer_5]<time_final)
                        {
                            raw1_n_5=my_matrix_node_5[runer_5][0];
                            raw2_n_5=my_matrix_node_5[runer_5][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_5++;
                        }

                    }//While node 5

                    //se cargan los valores del nodo 6
                    bandera=0;
                    //int runer_6=0;
                    while (  runer_6!=lengh_node_6 && bandera==0 )
                    {
                        if(my_vector_time_node_6[runer_6]>time_init && my_vector_time_node_6[runer_6]<time_final)
                        {
                            raw1_n_6=my_matrix_node_6[runer_6][0];
                            raw2_n_6=my_matrix_node_6[runer_6][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_6++;
                        }

                    }//While node 6

                    //se cargan los valores del nodo 7
                    bandera=0;
                    //int runer_7=0;
                    while (  runer_7!=lengh_node_7 && bandera==0 )
                    {
                        if(my_vector_time_node_7[runer_7]>time_init && my_vector_time_node_7[runer_7]<time_final)
                        {
                            raw1_n_7=my_matrix_node_7[runer_7][0];
                            raw2_n_7=my_matrix_node_7[runer_7][1];
                            bandera++;
                            temporal_trama++;


                        }
                        else {
                            runer_7++;
                        }

                    }//While node 7

                    //se cargan los valores del nodo 8
                    bandera=0;
                    //int runer_8=0;
                    while (  runer_8!=lengh_node_8 && bandera==0 )
                    {
                        if(my_vector_time_node_8[runer_8]>time_init && my_vector_time_node_8[runer_8]<time_final)
                        {
                            raw1_n_8=my_matrix_node_8[runer_8][0];
                            raw2_n_8=my_matrix_node_8[runer_8][1];
                            bandera++;
                            temporal_trama++;


                        }
                        else {
                            runer_8++;
                        }

                    }//While node 8




                    if(temporal_trama==8)
                    {
                        _trama_temporal="";

                        if(j==0)//no cargo el valor de la coma al final
                        {

                        }
                        else {
                            _trama_temporal=_trama_temporal+",";

                        }
                        //existen los datos
                        ////qDebug("TimeStamp"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss").toLatin1()+"RW1_N1>"+QString::number(raw1_n_1).toLatin1()+">>RW1_N2>"+QString::number(raw1_n_2).toLatin1());
                        ////qDebug("TimeStamp"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss").toLatin1()+"RW2_N7>"+QString::number(raw2_n_7).toLatin1()+">>RW2_N7>"+QString::number(raw1_n_7).toLatin1());
                        //en este punto genero la trama a enviar
                        _trama_temporal=_trama_temporal+"('"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss")+"',";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_1).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_1).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_2).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_2).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_3).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_3).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_4).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_4).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_5).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_5).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_6).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_6).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_7).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_7).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_8).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_8).toLatin1()+")";
                        ////qDebug(_trama_temporal.toLatin1());
                        _trama_temporal=_trama_temporal.remove("tam_ma");
                        pack_to_send=pack_to_send+_trama_temporal;


                    }
                    else
                    {
                        //no se hace nada pero inicia el runer en el ultimo valor que termino
                        runer_2=run_2_temp;
                        runer_3=run_3_temp;
                        runer_4=run_4_temp;
                        runer_5=run_5_temp;
                        runer_6=run_6_temp;
                        runer_7=run_7_temp;
                        runer_8=run_8_temp;


                    }
                    raw1_n_1=0;
                    raw2_n_1=0;
                    raw1_n_2=0;
                    raw2_n_2=0;
                    raw1_n_3=0;
                    raw2_n_3=0;
                    raw1_n_4=0;
                    raw2_n_4=0;
                    raw1_n_5=0;
                    raw2_n_5=0;
                    raw1_n_6=0;
                    raw2_n_6=0;
                    raw1_n_7=0;
                    raw2_n_7=0;
                    raw1_n_8=0;
                    raw2_n_8=0;




                record_node++;
                }//recorro la matriz del nodo 1y creo paquetes.

            }
            else
            {
                //en este punto reaiza las funciones para los paquetes completos
                for (int j=0;j<1000;j++)
                {

                    //CREO VARIABLES TEMPORALES DE RECORRIDO
                    int run_2_temp=runer_2;
                    int run_3_temp=runer_3;
                    int run_4_temp=runer_4;
                    int run_5_temp=runer_5;
                    int run_6_temp=runer_6;
                    int run_7_temp=runer_7;
                    int run_8_temp=runer_8;
                    //este ciclo for arma tramas dentro del paquete de 1000 elementos.
                    //qDebug("Pack"+QString::number(pack).toLatin1()+">>>trama>>"+QString::number(j).toLatin1());
                    int temporal_trama=1;
                    //creamos el time stamp inicial y final
                    QDateTime time_init=my_vector_time_node_1[record_node].addSecs(-7);
                    QDateTime time_final=my_vector_time_node_1[record_node].addSecs(7);
                    raw1_n_1=my_matrix_node_1[record_node][0];
                    raw2_n_1=my_matrix_node_1[record_node][1];

                    //se cargan los valores del nodo 2
                    int bandera=0;
                    //int runer_2=0;
                    while (  runer_2!=lengh_node_2 && bandera==0 )
                    {
                        if(my_vector_time_node_2[runer_2]>time_init && my_vector_time_node_2[runer_2]<time_final)
                        {
                            raw1_n_2=my_matrix_node_2[runer_2][0];
                            raw2_n_2=my_matrix_node_2[runer_2][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_2++;
                        }

                    }//While node 2

                    //se cargan los valores del nodo 3
                    bandera=0;
                    //int runer_3=0;
                    while (  runer_3!=lengh_node_3 && bandera==0 )
                    {
                        if(my_vector_time_node_3[runer_3]>time_init && my_vector_time_node_3[runer_3]<time_final)
                        {
                            raw1_n_3=my_matrix_node_3[runer_3][0];
                            raw2_n_3=my_matrix_node_3[runer_3][1];
                            bandera++;
                             temporal_trama++;


                        }
                        else {
                            runer_3++;
                        }

                    }//While node 3

                    //se cargan los valores del nodo 4
                    bandera=0;
                    //int runer_4=0;
                    while (  runer_4!=lengh_node_4 && bandera==0 )
                    {
                        if(my_vector_time_node_4[runer_4]>time_init && my_vector_time_node_4[runer_4]<time_final)
                        {
                            raw1_n_4=my_matrix_node_4[runer_4][0];
                            raw2_n_4=my_matrix_node_4[runer_4][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_4++;
                        }

                    }//While node 4

                    //se cargan los valores del nodo 5
                    bandera=0;
                    //int runer_5=0;
                    while (  runer_5!=lengh_node_5 && bandera==0 )
                    {
                        if(my_vector_time_node_5[runer_5]>time_init && my_vector_time_node_5[runer_5]<time_final)
                        {
                            raw1_n_5=my_matrix_node_5[runer_5][0];
                            raw2_n_5=my_matrix_node_5[runer_5][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_5++;
                        }

                    }//While node 5

                    //se cargan los valores del nodo 6
                    bandera=0;
                    //int runer_6=0;
                    while (  runer_6!=lengh_node_6 && bandera==0 )
                    {
                        if(my_vector_time_node_6[runer_6]>time_init && my_vector_time_node_6[runer_6]<time_final)
                        {
                            raw1_n_6=my_matrix_node_6[runer_6][0];
                            raw2_n_6=my_matrix_node_6[runer_6][1];
                            bandera++;
                            temporal_trama++;

                        }
                        else {
                            runer_6++;
                        }

                    }//While node 6

                    //se cargan los valores del nodo 7
                    bandera=0;
                    //int runer_7=0;
                    while (  runer_7!=lengh_node_7 && bandera==0 )
                    {
                        if(my_vector_time_node_7[runer_7]>time_init && my_vector_time_node_7[runer_7]<time_final)
                        {
                            raw1_n_7=my_matrix_node_7[runer_7][0];
                            raw2_n_7=my_matrix_node_7[runer_7][1];
                            bandera++;
                            temporal_trama++;


                        }
                        else {
                            runer_7++;
                        }

                    }//While node 7

                    //se cargan los valores del nodo 8
                    bandera=0;
                    //int runer_8=0;
                    while (  runer_8!=lengh_node_8 && bandera==0 )
                    {
                        if(my_vector_time_node_8[runer_8]>time_init && my_vector_time_node_8[runer_8]<time_final)
                        {
                            raw1_n_8=my_matrix_node_8[runer_8][0];
                            raw2_n_8=my_matrix_node_8[runer_8][1];
                            bandera++;
                            temporal_trama++;


                        }
                        else {
                            runer_8++;
                        }

                    }//While node 8





                    if(temporal_trama==8)
                    {
                        _trama_temporal="";
                        if(j==0)//no cargo el valor de la coma al final
                        {

                        }
                        else {
                            _trama_temporal=_trama_temporal+",";

                        }
                        //existen los datos
                        ////qDebug("TimeStamp"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss").toLatin1()+"RW1_N1>"+QString::number(raw1_n_1).toLatin1()+">>RW1_N2>"+QString::number(raw1_n_2).toLatin1());
                        ////qDebug("TimeStamp"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss").toLatin1()+"RW2_N7>"+QString::number(raw2_n_7).toLatin1()+">>RW2_N7>"+QString::number(raw1_n_7).toLatin1());
                        //en este punto genero la trama a enviar
                        _trama_temporal=_trama_temporal+"('"+my_vector_time_node_1[record_node].toString("yy/MM/dd hh:mm:ss")+"',";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_1).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_1).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_2).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_2).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_3).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_3).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_4).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_4).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_5).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_5).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_6).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_6).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_7).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_7).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw1_n_8).toLatin1()+",";
                        _trama_temporal=_trama_temporal+QString::number(raw2_n_8).toLatin1()+")";
                        ////qDebug(_trama_temporal.toLatin1());
                        _trama_temporal=_trama_temporal.remove("tam_ma");
                        pack_to_send=pack_to_send+_trama_temporal;







                    }
                    else
                    {
                        //no se hace nada pero inicia el runer en el ultimo valor que termino
                        runer_2=run_2_temp;
                        runer_3=run_3_temp;
                        runer_4=run_4_temp;
                        runer_5=run_5_temp;
                        runer_6=run_6_temp;
                        runer_7=run_7_temp;
                        runer_8=run_8_temp;

                    }
                    raw1_n_1=0;
                    raw2_n_1=0;
                    raw1_n_2=0;
                    raw2_n_2=0;
                    raw1_n_3=0;
                    raw2_n_3=0;
                    raw1_n_4=0;
                    raw2_n_4=0;
                    raw1_n_5=0;
                    raw2_n_5=0;
                    raw1_n_6=0;
                    raw2_n_6=0;
                    raw1_n_7=0;
                    raw2_n_7=0;
                    raw1_n_8=0;
                    raw2_n_8=0;




                record_node++;
                }//recorro la matriz del nodo 1y creo paquetes.


            }







            //EN ESTE PUNTO CUANDO TERMINE EL CICLO DE CADA 1000 SE DEBE ENVIAR EL PAQUETE DE DATOS
            //qDebug(pack_to_send.toLatin1());
            QSqlQuery send_query;
            pack_to_send=pack_to_send.replace("values,","values");
            send_query.prepare(pack_to_send);
            if(send_query.exec())
            {
               //qDebug("se envio data exitosamente");
            }
            else{
               //qDebug("Error enviando la data");

            }



        }//for pack...
        //en este punto cargo el valor de datos para el refactor.
        ui->start_refactor->setEnabled(true);
    }

}
//******************************************************************************************************************************
//Funcion que crea base de datos en blanco donde hacer el refactoring.
//******************************************************************************************************************************
void jptwatercut::duplicate_db()
{
    QString data_message="";

#ifdef _HUB_ON
        QString path_source = "/jpt/jptwatercut/External/jptdatabase_refactor.db";
#else
        //QString path_source= "/home/pi/Desktop/Compilar/.jptinstaller/External/jptdatabase_refactor.db;
        QString path_source ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/jptdatabase_refactor.db";

#endif
    //creamos el directorio en el cual se va a guardar la data.
        QString dir_refactor="";
    try {
        dir_refactor= QFileDialog::getExistingDirectory(this,tr("Select Directory"),"/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        data_message=dir_refactor;

    } catch (...) {

    }

    //si se obtuvo el path y la copia de la base de datos a analizar entonces se habilita
    if(data_message=="")
    {
        data_message="Error Selecting the Folder";
        ui->connect_txt->setText(data_message);
    }
    else{
      //si el folder se cargo de manera exitosa entones copiamos la base de datos para realiar refactor.
        QDir my_dir_path(dir_refactor);

        if(my_dir_path.exists())
        {
            //tomamos la hora actual del sistema
            QDateTime temp_date=QDateTime::currentDateTime();
            QString date_str=temp_date.toString();
            date_str.remove("/");
            date_str.remove(".");
            date_str.remove(" ");
            QString label_charge="jptdatabase_refactor_"+date_str+".db";


            ui->dest_db_label->setText(label_charge);


            dir_refactor=dir_refactor+"/"+label_charge;
            if(!QFile::copy(path_source, dir_refactor))
            {

                ui->folder_txt->setText(data_message);
                data_message=">>..Unable to create refactor database on Destination Folder ";
                ui->connect_txt->setText(data_message);

            }
            else{

                ui->charge_data->setEnabled(true);

                ui->folder_txt->setText(data_message);
                data_message=ui->connect_txt->text();
                data_message=+"\n";
                data_message=data_message+"Creating database where refactor data"+"\n";
                ui->connect_txt->setText(data_message);
                ui->select_folder->setEnabled(false);
            }


        }

    }







}
//******************************************************************************************************************************
//connectar la base de datos al systema
//******************************************************************************************************************************
bool jptwatercut::connect_db(QString MyDbPath)
{

    QSqlDatabase my_db;
    my_db.close();
    my_db=QSqlDatabase::addDatabase("QSQLITE");
    my_db.setDatabaseName(MyDbPath);
    bool db_state;
    if(!my_db.open())
    {
        db_state=false;
    }
    else
    {
        db_state=true;
    }
    return db_state;

}
//******************************************************************************************************************************
//connectar la base para refactor.
//******************************************************************************************************************************

void jptwatercut::data_connect()
{
     QString data_message;
     data_message=">>--";



     //crea ventana emergente para obtener la base de datos
     QString data_path;
     data_path=QFileDialog::getOpenFileName(this,"Select Database:","/home/","database( *jptdatabase.db  database.sqlite)");

     QStringList extension=data_path.split(".");


     if (extension.last()=="db")
     {




         bool database_refactor;
         database_refactor=connect_db(data_path);



         if(!database_refactor)
         {
             //se despliega error de carga de la base de datos

             data_message="Error : Charging the database";

         }
         else
         {
             //se despliega mensaje sobre la base cargada
             data_message="Trying to Connect Databases \n Database :Source Connection OK";
             source_db_path=data_path;


             ui->refactor->setEnabled(true);
             connect(ui->refactor,SIGNAL(clicked()),this,SLOT(refactor_data()));

         }




     }
     else
     {
        //se despliega error de tipo de archivo
         data_message="The File Type is Wrong ";
     }

    // */




     ui->connect_txt->setText(data_message);


}

//************************************************************************************************************************************************************************************************************************************************************
