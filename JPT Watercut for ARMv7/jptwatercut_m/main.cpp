#include "_sentence_.h"

#ifdef _COMPILE_GUI
    #include <QApplication>
#else
    #include <QCoreApplication>
    #include <QDebug>
#endif

#include "jptwatercut.h"

int main(int argc, char *argv[]){

#ifdef _COMPILE_GUI
    QApplication a(argc, argv);
    jptwatercut w;
    w.show();
#else
    QCoreApplication a(argc, argv);
    jptwatercut w;
#endif
    return a.exec();
}
