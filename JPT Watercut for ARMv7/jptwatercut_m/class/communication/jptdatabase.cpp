﻿#include "jptdatabase.h"

const QString           gv_list_name_tables[1] = {"params"};
//****************************************************************************
// Constructor
//****************************************************************************
jptdatabase::jptdatabase(QString name_data_base, QString password, QString user_name, QObject *parent) : QObject(parent){

    db = QSqlDatabase::addDatabase("QSQLITE");

#ifdef _HUB_ON
        //QString path = "/jpt/jptwatercut/External/" + name_data_base;
        QString path = "/media/debian/JPTSD/" + name_data_base;

#else

        //QString path = "/home/pi/Desktop/Compilar/.jptinstaller/External/" + name_data_base;
          QString path ="/home/zahall/Documentos/build-jptmonitoring-Desktop_Qt_5_9_1_GCC_64bit-Debug/External/" + name_data_base;

#endif

    db.setPassword(password);
    db.setUserName(user_name);
    db.setDatabaseName(path);
    db.open();

}
//****************************************************************************
// Destructor
//****************************************************************************
jptdatabase::~jptdatabase(){
    db.close();
}

#ifdef _FILE_
//*********************************************************************************************************************************************************
// Obtener los registros necesitados
//*********************************************************************************************************************************************************
void jptdatabase::get_values(const QString name_table, int id, QString *value){
    QString lv_query;
    lv_query.append("SELECT raw FROM " + name_table + " WHERE id=" + QString::number(id));
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec()){
        while(lv_get_query.next())
                *value = lv_get_query.value(0).toString();
    }
}
#else
//*********************************************************************************************************************************************************
// Obtener los registros necesitados
//*********************************************************************************************************************************************************
void jptdatabase::get_values(int pos_name_table, QVector<QStringList > *vec_data){
    vec_data->clear();

    QString lv_name_table(gv_list_name_tables[pos_name_table]);
    int lv_num_item_table(0);

    if(pos_name_table == def_pos_table_params)
        lv_num_item_table  = def_num_colums_table_param;


    QString lv_query;
    lv_query.append("SELECT * FROM " + lv_name_table);
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    if(lv_get_query.exec())
        while(lv_get_query.next()){
            QStringList lv_row_data;
            for(int f_colum = 0; f_colum < lv_num_item_table; ++f_colum)
                lv_row_data << lv_get_query.value(f_colum).toString();
            vec_data->append(lv_row_data);
        }

}
#endif
//*********************************************************************************************************************************************************
// Crea la nueva tabla
//*********************************************************************************************************************************************************
void jptdatabase::create_new_table(QString name_table, QString query){
    QString new_query;
    new_query.append("CREATE TABLE IF NOT EXISTS " + name_table + query);
    QSqlQuery create;
    create.prepare(new_query);
    create.exec();
}
//*********************************************************************************************************************************************************
// Insertar nuevas tablas
//*********************************************************************************************************************************************************
void jptdatabase::create_new_table(const QString name_table){

    QString id_query("(id INTEGER PRIMARY KEY AUTOINCREMENT, ");
    QString query;

#ifdef _FLOW_
    query = id_query + "temp INTEGER, sali INTEGER, flow INTEGER, raw INTEGER, raw_t INTEGER, raw_s INTEGER, raw_f INTEGER, lam_cap INTEGER, lam_res INTEGER, med_lam INTEGER, tur_cap INTEGER, tur_res INTEGER, med_tur INTEGER);";
#else
    query = id_query + "temp VARCHAR(30), sali VARCHAR(30), lam_cap VARCHAR(30), lam_res VARCHAR(30), med_lam VARCHAR(30), tur_cap VARCHAR(30), tur_res VARCHAR(30), med_tur VARCHAR(30));";
#endif
    create_new_table(name_table, query);
}
//*********************************************************************************************************************************************************
// Insertar Datos en las tablas
//*********************************************************************************************************************************************************
void jptdatabase::insert_register(QString name_table, QString query){
    QString new_query;
    new_query.append("INSERT INTO " + name_table + query);
    QSqlQuery create;
    create.prepare(new_query);

    create.exec();
}


//*********************************************************************************************************************************************************
// Actualizar los registros de la base de datos
//*********************************************************************************************************************************************************
void jptdatabase::update_register(QString name_table, QString query){
    QString lv_query;
    lv_query.append("UPDATE " + name_table + " SET " + query + ";");
    QSqlQuery lv_get_query;
    lv_get_query.setForwardOnly(true);
    lv_get_query.prepare(lv_query);

    lv_get_query.exec();

}
