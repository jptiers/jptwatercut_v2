#ifndef JPTTCPCLIENT_H
#define JPTTCPCLIENT_H

#include <QObject>
#include <QtNetwork>
#include <QtNetwork/QNetworkInterface>
#include "_sentence_.h"

#define def_ip_widgets_communication    "127.0.0.1"
#define def_port_widgets_communication  10000

class jpttcpclient : public QObject{
     Q_OBJECT

public:
    explicit jpttcpclient(QObject *parent = 0);

public slots:
    void query_from_server();
    void restart_conection();

    void send_message(QString mode);

#ifdef _TRAME_JP
    void send_message_data(QString value_1, QString value_2);
#endif

private:
     QTcpSocket *a_client;

signals:
     void closed_conection();
     void new_entry(QStringList trame);
};

#endif // JPTTCPCLIENT_H
