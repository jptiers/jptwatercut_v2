/****************************************************************************
** Meta object code from reading C++ file 'jptwatercut.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../jptwatercut_m/jptwatercut.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'jptwatercut.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_jptwatercut_t {
    QByteArrayData data[108];
    char stringdata0[1849];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_jptwatercut_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_jptwatercut_t qt_meta_stringdata_jptwatercut = {
    {
QT_MOC_LITERAL(0, 0, 11), // "jptwatercut"
QT_MOC_LITERAL(1, 12, 16), // "start_connection"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 12), // "send_message"
QT_MOC_LITERAL(4, 43, 24), // "send_message_to_database"
QT_MOC_LITERAL(5, 68, 13), // "refactor_data"
QT_MOC_LITERAL(6, 82, 12), // "data_connect"
QT_MOC_LITERAL(7, 95, 12), // "duplicate_db"
QT_MOC_LITERAL(8, 108, 20), // "node_helper_refactor"
QT_MOC_LITERAL(9, 129, 4), // "node"
QT_MOC_LITERAL(10, 134, 15), // "tam_matriz_node"
QT_MOC_LITERAL(11, 150, 10), // "parse_node"
QT_MOC_LITERAL(12, 161, 19), // "setup_normalization"
QT_MOC_LITERAL(13, 181, 12), // "values_entry"
QT_MOC_LITERAL(14, 194, 25), // "capture_data_to_normalize"
QT_MOC_LITERAL(15, 220, 26), // "charge_delta_normalization"
QT_MOC_LITERAL(16, 247, 26), // "update_delta_normalization"
QT_MOC_LITERAL(17, 274, 20), // "update_delta_average"
QT_MOC_LITERAL(18, 295, 21), // "calcule_default_delta"
QT_MOC_LITERAL(19, 317, 20), // "calcule_manual_delta"
QT_MOC_LITERAL(20, 338, 18), // "calcule_auto_delta"
QT_MOC_LITERAL(21, 357, 23), // "calcule_default_delta_r"
QT_MOC_LITERAL(22, 381, 22), // "calcule_manual_delta_r"
QT_MOC_LITERAL(23, 404, 20), // "calcule_auto_delta_r"
QT_MOC_LITERAL(24, 425, 19), // "setup_capture_calib"
QT_MOC_LITERAL(25, 445, 24), // "capture_data_calibration"
QT_MOC_LITERAL(26, 470, 25), // "charge_calibration_values"
QT_MOC_LITERAL(27, 496, 25), // "update_calibration_values"
QT_MOC_LITERAL(28, 522, 12), // "avg_cali_oil"
QT_MOC_LITERAL(29, 535, 12), // "avg_cali_air"
QT_MOC_LITERAL(30, 548, 10), // "avg_cali_w"
QT_MOC_LITERAL(31, 559, 11), // "avg_cali_fw"
QT_MOC_LITERAL(32, 571, 16), // "process_refactor"
QT_MOC_LITERAL(33, 588, 18), // "persistan_refactor"
QT_MOC_LITERAL(34, 607, 9), // "curve_res"
QT_MOC_LITERAL(35, 617, 9), // "curve_cap"
QT_MOC_LITERAL(36, 627, 9), // "my_curves"
QT_MOC_LITERAL(37, 637, 26), // "change_correct_temp_on_off"
QT_MOC_LITERAL(38, 664, 5), // "selec"
QT_MOC_LITERAL(39, 670, 26), // "update_correct_temp_on_off"
QT_MOC_LITERAL(40, 697, 7), // "_status"
QT_MOC_LITERAL(41, 705, 26), // "charge_correct_temp_on_off"
QT_MOC_LITERAL(42, 732, 29), // "update_correct_tempval_on_off"
QT_MOC_LITERAL(43, 762, 29), // "charge_correct_tempval_on_off"
QT_MOC_LITERAL(44, 792, 26), // "change_correct_sali_on_off"
QT_MOC_LITERAL(45, 819, 26), // "update_correct_sali_on_off"
QT_MOC_LITERAL(46, 846, 26), // "charge_correct_sali_on_off"
QT_MOC_LITERAL(47, 873, 17), // "wc_per_sensor_res"
QT_MOC_LITERAL(48, 891, 17), // "wc_per_sensor_cap"
QT_MOC_LITERAL(49, 909, 14), // "calcule_models"
QT_MOC_LITERAL(50, 924, 20), // "calcule_regimen_flow"
QT_MOC_LITERAL(51, 945, 19), // "send_to_refactor_db"
QT_MOC_LITERAL(52, 965, 12), // "flow_regimen"
QT_MOC_LITERAL(53, 978, 3), // "lam"
QT_MOC_LITERAL(54, 982, 4), // "turb"
QT_MOC_LITERAL(55, 987, 12), // "turb_laminar"
QT_MOC_LITERAL(56, 1000, 5), // "water"
QT_MOC_LITERAL(57, 1006, 3), // "oil"
QT_MOC_LITERAL(58, 1010, 3), // "gas"
QT_MOC_LITERAL(59, 1014, 29), // "send_to_refactor_db_persistan"
QT_MOC_LITERAL(60, 1044, 19), // "value_hwc_up_change"
QT_MOC_LITERAL(61, 1064, 4), // "data"
QT_MOC_LITERAL(62, 1069, 19), // "value_hwc_dw_change"
QT_MOC_LITERAL(63, 1089, 21), // "value_hwc_up_2_change"
QT_MOC_LITERAL(64, 1111, 21), // "value_hwc_dw_2_change"
QT_MOC_LITERAL(65, 1133, 19), // "value_lwc_up_change"
QT_MOC_LITERAL(66, 1153, 19), // "value_lwc_dw_change"
QT_MOC_LITERAL(67, 1173, 21), // "percent_hwc_up_change"
QT_MOC_LITERAL(68, 1195, 21), // "percent_hwc_dw_change"
QT_MOC_LITERAL(69, 1217, 23), // "percent_hwc_up_2_change"
QT_MOC_LITERAL(70, 1241, 23), // "percent_hwc_dw_2_change"
QT_MOC_LITERAL(71, 1265, 21), // "percent_lwc_up_change"
QT_MOC_LITERAL(72, 1287, 21), // "percent_lwc_dw_change"
QT_MOC_LITERAL(73, 1309, 11), // "hlwc_curves"
QT_MOC_LITERAL(74, 1321, 13), // "hwc_curve_res"
QT_MOC_LITERAL(75, 1335, 13), // "hwc_curve_cap"
QT_MOC_LITERAL(76, 1349, 17), // "low_curve_cap_res"
QT_MOC_LITERAL(77, 1367, 18), // "update_hlwc_values"
QT_MOC_LITERAL(78, 1386, 19), // "charge_hl_wc_values"
QT_MOC_LITERAL(79, 1406, 17), // "high_turb_laminar"
QT_MOC_LITERAL(80, 1424, 8), // "_cuentas"
QT_MOC_LITERAL(81, 1433, 21), // "high_turb_laminar_res"
QT_MOC_LITERAL(82, 1455, 16), // "low_turb_laminar"
QT_MOC_LITERAL(83, 1472, 18), // "curve_three_phases"
QT_MOC_LITERAL(84, 1491, 20), // "calcule_three_phases"
QT_MOC_LITERAL(85, 1512, 18), // "model_three_phases"
QT_MOC_LITERAL(86, 1531, 10), // "connect_db"
QT_MOC_LITERAL(87, 1542, 4), // "MyDb"
QT_MOC_LITERAL(88, 1547, 19), // "create_pack_to_send"
QT_MOC_LITERAL(89, 1567, 16), // "get_id_from_time"
QT_MOC_LITERAL(90, 1584, 12), // "my_timestamp"
QT_MOC_LITERAL(91, 1597, 25), // "process_new_entry_sensors"
QT_MOC_LITERAL(92, 1623, 5), // "entry"
QT_MOC_LITERAL(93, 1629, 24), // "acumulator_entry_sensors"
QT_MOC_LITERAL(94, 1654, 19), // "start_communication"
QT_MOC_LITERAL(95, 1674, 21), // "restart_communication"
QT_MOC_LITERAL(96, 1696, 15), // "change_ppm_mode"
QT_MOC_LITERAL(97, 1712, 18), // "change_temp_sensor"
QT_MOC_LITERAL(98, 1731, 6), // "sensor"
QT_MOC_LITERAL(99, 1738, 16), // "change_temp_mode"
QT_MOC_LITERAL(100, 1755, 17), // "temp_fixed_change"
QT_MOC_LITERAL(101, 1773, 4), // "temp"
QT_MOC_LITERAL(102, 1778, 16), // "ppm_fixed_change"
QT_MOC_LITERAL(103, 1795, 3), // "ppm"
QT_MOC_LITERAL(104, 1799, 17), // "porc_fixed_change"
QT_MOC_LITERAL(105, 1817, 4), // "porc"
QT_MOC_LITERAL(106, 1822, 11), // "gen_graphic"
QT_MOC_LITERAL(107, 1834, 14) // "save_data_base"

    },
    "jptwatercut\0start_connection\0\0"
    "send_message\0send_message_to_database\0"
    "refactor_data\0data_connect\0duplicate_db\0"
    "node_helper_refactor\0node\0tam_matriz_node\0"
    "parse_node\0setup_normalization\0"
    "values_entry\0capture_data_to_normalize\0"
    "charge_delta_normalization\0"
    "update_delta_normalization\0"
    "update_delta_average\0calcule_default_delta\0"
    "calcule_manual_delta\0calcule_auto_delta\0"
    "calcule_default_delta_r\0calcule_manual_delta_r\0"
    "calcule_auto_delta_r\0setup_capture_calib\0"
    "capture_data_calibration\0"
    "charge_calibration_values\0"
    "update_calibration_values\0avg_cali_oil\0"
    "avg_cali_air\0avg_cali_w\0avg_cali_fw\0"
    "process_refactor\0persistan_refactor\0"
    "curve_res\0curve_cap\0my_curves\0"
    "change_correct_temp_on_off\0selec\0"
    "update_correct_temp_on_off\0_status\0"
    "charge_correct_temp_on_off\0"
    "update_correct_tempval_on_off\0"
    "charge_correct_tempval_on_off\0"
    "change_correct_sali_on_off\0"
    "update_correct_sali_on_off\0"
    "charge_correct_sali_on_off\0wc_per_sensor_res\0"
    "wc_per_sensor_cap\0calcule_models\0"
    "calcule_regimen_flow\0send_to_refactor_db\0"
    "flow_regimen\0lam\0turb\0turb_laminar\0"
    "water\0oil\0gas\0send_to_refactor_db_persistan\0"
    "value_hwc_up_change\0data\0value_hwc_dw_change\0"
    "value_hwc_up_2_change\0value_hwc_dw_2_change\0"
    "value_lwc_up_change\0value_lwc_dw_change\0"
    "percent_hwc_up_change\0percent_hwc_dw_change\0"
    "percent_hwc_up_2_change\0percent_hwc_dw_2_change\0"
    "percent_lwc_up_change\0percent_lwc_dw_change\0"
    "hlwc_curves\0hwc_curve_res\0hwc_curve_cap\0"
    "low_curve_cap_res\0update_hlwc_values\0"
    "charge_hl_wc_values\0high_turb_laminar\0"
    "_cuentas\0high_turb_laminar_res\0"
    "low_turb_laminar\0curve_three_phases\0"
    "calcule_three_phases\0model_three_phases\0"
    "connect_db\0MyDb\0create_pack_to_send\0"
    "get_id_from_time\0my_timestamp\0"
    "process_new_entry_sensors\0entry\0"
    "acumulator_entry_sensors\0start_communication\0"
    "restart_communication\0change_ppm_mode\0"
    "change_temp_sensor\0sensor\0change_temp_mode\0"
    "temp_fixed_change\0temp\0ppm_fixed_change\0"
    "ppm\0porc_fixed_change\0porc\0gen_graphic\0"
    "save_data_base"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_jptwatercut[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      86,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  444,    2, 0x06 /* Public */,
       3,    2,  447,    2, 0x06 /* Public */,
       4,    2,  452,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  457,    2, 0x0a /* Public */,
       6,    0,  458,    2, 0x0a /* Public */,
       7,    0,  459,    2, 0x0a /* Public */,
       8,    1,  460,    2, 0x0a /* Public */,
      10,    0,  463,    2, 0x0a /* Public */,
      11,    0,  464,    2, 0x0a /* Public */,
      12,    1,  465,    2, 0x0a /* Public */,
      14,    0,  468,    2, 0x0a /* Public */,
      15,    0,  469,    2, 0x0a /* Public */,
      16,    0,  470,    2, 0x0a /* Public */,
      17,    0,  471,    2, 0x0a /* Public */,
      18,    0,  472,    2, 0x0a /* Public */,
      19,    0,  473,    2, 0x0a /* Public */,
      20,    0,  474,    2, 0x0a /* Public */,
      21,    0,  475,    2, 0x0a /* Public */,
      22,    0,  476,    2, 0x0a /* Public */,
      23,    0,  477,    2, 0x0a /* Public */,
      24,    0,  478,    2, 0x0a /* Public */,
      25,    0,  479,    2, 0x0a /* Public */,
      26,    0,  480,    2, 0x0a /* Public */,
      27,    0,  481,    2, 0x0a /* Public */,
      28,    0,  482,    2, 0x0a /* Public */,
      29,    0,  483,    2, 0x0a /* Public */,
      30,    0,  484,    2, 0x0a /* Public */,
      31,    0,  485,    2, 0x0a /* Public */,
      32,    0,  486,    2, 0x0a /* Public */,
      33,    0,  487,    2, 0x0a /* Public */,
      34,    0,  488,    2, 0x0a /* Public */,
      35,    0,  489,    2, 0x0a /* Public */,
      36,    0,  490,    2, 0x0a /* Public */,
      37,    1,  491,    2, 0x0a /* Public */,
      39,    1,  494,    2, 0x0a /* Public */,
      41,    0,  497,    2, 0x0a /* Public */,
      42,    1,  498,    2, 0x0a /* Public */,
      43,    0,  501,    2, 0x0a /* Public */,
      44,    1,  502,    2, 0x0a /* Public */,
      45,    1,  505,    2, 0x0a /* Public */,
      46,    0,  508,    2, 0x0a /* Public */,
      47,    0,  509,    2, 0x0a /* Public */,
      48,    0,  510,    2, 0x0a /* Public */,
      49,    0,  511,    2, 0x0a /* Public */,
      50,    0,  512,    2, 0x0a /* Public */,
      51,    7,  513,    2, 0x0a /* Public */,
      59,    7,  528,    2, 0x0a /* Public */,
      60,    1,  543,    2, 0x0a /* Public */,
      62,    1,  546,    2, 0x0a /* Public */,
      63,    1,  549,    2, 0x0a /* Public */,
      64,    1,  552,    2, 0x0a /* Public */,
      65,    1,  555,    2, 0x0a /* Public */,
      66,    1,  558,    2, 0x0a /* Public */,
      67,    1,  561,    2, 0x0a /* Public */,
      68,    1,  564,    2, 0x0a /* Public */,
      69,    1,  567,    2, 0x0a /* Public */,
      70,    1,  570,    2, 0x0a /* Public */,
      71,    1,  573,    2, 0x0a /* Public */,
      72,    1,  576,    2, 0x0a /* Public */,
      73,    0,  579,    2, 0x0a /* Public */,
      74,    0,  580,    2, 0x0a /* Public */,
      75,    0,  581,    2, 0x0a /* Public */,
      76,    0,  582,    2, 0x0a /* Public */,
      77,    0,  583,    2, 0x0a /* Public */,
      78,    0,  584,    2, 0x0a /* Public */,
      79,    1,  585,    2, 0x0a /* Public */,
      81,    1,  588,    2, 0x0a /* Public */,
      82,    1,  591,    2, 0x0a /* Public */,
      83,    0,  594,    2, 0x0a /* Public */,
      84,    0,  595,    2, 0x0a /* Public */,
      85,    1,  596,    2, 0x0a /* Public */,
      86,    1,  599,    2, 0x0a /* Public */,
      88,    0,  602,    2, 0x0a /* Public */,
      89,    2,  603,    2, 0x0a /* Public */,
      91,    1,  608,    2, 0x0a /* Public */,
      93,    1,  611,    2, 0x0a /* Public */,
      94,    0,  614,    2, 0x0a /* Public */,
      95,    0,  615,    2, 0x0a /* Public */,
      96,    1,  616,    2, 0x0a /* Public */,
      97,    1,  619,    2, 0x0a /* Public */,
      99,    1,  622,    2, 0x0a /* Public */,
     100,    1,  625,    2, 0x0a /* Public */,
     102,    1,  628,    2, 0x0a /* Public */,
     104,    1,  631,    2, 0x0a /* Public */,
     106,    0,  634,    2, 0x08 /* Private */,
     107,    0,  635,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QStringList,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   40,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   40,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double,   52,   53,   54,   55,   56,   57,   58,
    QMetaType::Void, QMetaType::Int, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double,   52,   53,   54,   55,   56,   57,   58,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Float, QMetaType::Float,   80,
    QMetaType::Float, QMetaType::Float,   80,
    QMetaType::Float, QMetaType::Float,   80,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Float, QMetaType::Float,   80,
    QMetaType::Bool, QMetaType::QString,   87,
    QMetaType::Void,
    QMetaType::Int, QMetaType::QDateTime, QMetaType::Int,   90,    9,
    QMetaType::Void, QMetaType::QStringList,   92,
    QMetaType::Void, QMetaType::QStringList,   92,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   98,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Double,  101,
    QMetaType::Void, QMetaType::Int,  103,
    QMetaType::Void, QMetaType::Int,  105,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void jptwatercut::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        jptwatercut *_t = static_cast<jptwatercut *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->start_connection((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->send_message((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->send_message_to_database((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->refactor_data(); break;
        case 4: _t->data_connect(); break;
        case 5: _t->duplicate_db(); break;
        case 6: _t->node_helper_refactor((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->tam_matriz_node(); break;
        case 8: _t->parse_node(); break;
        case 9: _t->setup_normalization((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 10: _t->capture_data_to_normalize(); break;
        case 11: _t->charge_delta_normalization(); break;
        case 12: _t->update_delta_normalization(); break;
        case 13: _t->update_delta_average(); break;
        case 14: _t->calcule_default_delta(); break;
        case 15: _t->calcule_manual_delta(); break;
        case 16: _t->calcule_auto_delta(); break;
        case 17: _t->calcule_default_delta_r(); break;
        case 18: _t->calcule_manual_delta_r(); break;
        case 19: _t->calcule_auto_delta_r(); break;
        case 20: _t->setup_capture_calib(); break;
        case 21: _t->capture_data_calibration(); break;
        case 22: _t->charge_calibration_values(); break;
        case 23: _t->update_calibration_values(); break;
        case 24: _t->avg_cali_oil(); break;
        case 25: _t->avg_cali_air(); break;
        case 26: _t->avg_cali_w(); break;
        case 27: _t->avg_cali_fw(); break;
        case 28: _t->process_refactor(); break;
        case 29: _t->persistan_refactor(); break;
        case 30: _t->curve_res(); break;
        case 31: _t->curve_cap(); break;
        case 32: _t->my_curves(); break;
        case 33: _t->change_correct_temp_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->update_correct_temp_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->charge_correct_temp_on_off(); break;
        case 36: _t->update_correct_tempval_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->charge_correct_tempval_on_off(); break;
        case 38: _t->change_correct_sali_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 39: _t->update_correct_sali_on_off((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 40: _t->charge_correct_sali_on_off(); break;
        case 41: _t->wc_per_sensor_res(); break;
        case 42: _t->wc_per_sensor_cap(); break;
        case 43: _t->calcule_models(); break;
        case 44: _t->calcule_regimen_flow(); break;
        case 45: _t->send_to_refactor_db((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6])),(*reinterpret_cast< double(*)>(_a[7]))); break;
        case 46: _t->send_to_refactor_db_persistan((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6])),(*reinterpret_cast< double(*)>(_a[7]))); break;
        case 47: _t->value_hwc_up_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 48: _t->value_hwc_dw_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 49: _t->value_hwc_up_2_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 50: _t->value_hwc_dw_2_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 51: _t->value_lwc_up_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 52: _t->value_lwc_dw_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 53: _t->percent_hwc_up_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 54: _t->percent_hwc_dw_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 55: _t->percent_hwc_up_2_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 56: _t->percent_hwc_dw_2_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 57: _t->percent_lwc_up_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 58: _t->percent_lwc_dw_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 59: _t->hlwc_curves(); break;
        case 60: _t->hwc_curve_res(); break;
        case 61: _t->hwc_curve_cap(); break;
        case 62: _t->low_curve_cap_res(); break;
        case 63: _t->update_hlwc_values(); break;
        case 64: _t->charge_hl_wc_values(); break;
        case 65: { float _r = _t->high_turb_laminar((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 66: { float _r = _t->high_turb_laminar_res((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 67: { float _r = _t->low_turb_laminar((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 68: _t->curve_three_phases(); break;
        case 69: _t->calcule_three_phases(); break;
        case 70: { float _r = _t->model_three_phases((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 71: { bool _r = _t->connect_db((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 72: _t->create_pack_to_send(); break;
        case 73: { int _r = _t->get_id_from_time((*reinterpret_cast< QDateTime(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 74: _t->process_new_entry_sensors((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 75: _t->acumulator_entry_sensors((*reinterpret_cast< QStringList(*)>(_a[1]))); break;
        case 76: _t->start_communication(); break;
        case 77: _t->restart_communication(); break;
        case 78: _t->change_ppm_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 79: _t->change_temp_sensor((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 80: _t->change_temp_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 81: _t->temp_fixed_change((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 82: _t->ppm_fixed_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 83: _t->porc_fixed_change((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 84: _t->gen_graphic(); break;
        case 85: _t->save_data_base(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (jptwatercut::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::start_connection)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (jptwatercut::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::send_message)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (jptwatercut::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&jptwatercut::send_message_to_database)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject jptwatercut::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_jptwatercut.data,
      qt_meta_data_jptwatercut,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *jptwatercut::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *jptwatercut::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_jptwatercut.stringdata0))
        return static_cast<void*>(const_cast< jptwatercut*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int jptwatercut::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 86)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 86;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 86)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 86;
    }
    return _id;
}

// SIGNAL 0
void jptwatercut::start_connection(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void jptwatercut::send_message(QString _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void jptwatercut::send_message_to_database(QString _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
