/********************************************************************************
** Form generated from reading UI file 'jptwatercut.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JPTWATERCUT_H
#define UI_JPTWATERCUT_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_jptwatercut
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tab_5;
    QTabWidget *tabWidget_2;
    QWidget *tab_6;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_16;
    QGridLayout *gridLayout_29;
    QLCDNumber *ld_my_r3;
    QLabel *label_112;
    QLCDNumber *ld_my_c3;
    QLCDNumber *ld_my_c1;
    QLabel *label_113;
    QLabel *label_114;
    QLabel *label_115;
    QLabel *label_116;
    QFrame *line_35;
    QLCDNumber *ld_my_r1;
    QLCDNumber *ld_my_c2;
    QFrame *line_36;
    QLCDNumber *ld_my_r2;
    QLabel *label_117;
    QGridLayout *gridLayout_26;
    QLabel *label_122;
    QLCDNumber *ld_my_r6;
    QLabel *label_123;
    QLabel *label_118;
    QFrame *line_37;
    QFrame *line_38;
    QLCDNumber *ld_my_c6;
    QLabel *label_120;
    QLabel *label_121;
    QLCDNumber *ld_my_c4;
    QLCDNumber *ld_my_r5;
    QLCDNumber *ld_my_c5;
    QLabel *label_119;
    QLCDNumber *ld_my_r4;
    QLabel *label_99;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_24;
    QGridLayout *gridLayout_33;
    QLabel *cap_r_4;
    QLabel *label_153;
    QLabel *cap_c_6;
    QLabel *cap_r_6;
    QLabel *label_159;
    QLabel *cap_c_4;
    QLabel *label_154;
    QLabel *cap_r_5;
    QLabel *cap_c_5;
    QFrame *line_45;
    QFrame *line_46;
    QLabel *label_157;
    QLabel *label_149;
    QLabel *label_150;
    QGridLayout *gridLayout_31;
    QLabel *cap_r_1;
    QLabel *label_135;
    QLabel *label_134;
    QLabel *cap_r_2;
    QFrame *line_41;
    QLabel *cap_c_2;
    QLabel *label_132;
    QLabel *label_130;
    QFrame *line_42;
    QLabel *cap_c_3;
    QLabel *cap_r_3;
    QLabel *label_131;
    QLabel *cap_c_1;
    QLabel *label_133;
    QPushButton *captura_data;
    QFrame *line_28;
    QDateEdit *dateEdit;
    QLabel *label_92;
    QLabel *label_93;
    QSpinBox *spinBox;
    QSpinBox *temperature_normal;
    QSpinBox *spinBox_3;
    QLabel *label_87;
    QLabel *label_94;
    QTabWidget *tabWidget_3;
    QWidget *tab_14;
    QGroupBox *Group_set_parameters_2;
    QPushButton *calcule_def_dlt_2;
    QLabel *label_178;
    QLabel *default_var_2;
    QLabel *label_179;
    QLabel *label_180;
    QLabel *label_181;
    QPushButton *calcule_man_dlt_2;
    QFrame *line_44;
    QLabel *label_182;
    QPushButton *calcule_auto_dlt_2;
    QPushButton *update_dlt_val_2;
    QLabel *label_183;
    QLabel *atomatic_var_2;
    QFrame *line_57;
    QPushButton *update_dlt_average_2;
    QSpinBox *manual_var_2;
    QLabel *update_labal_2;
    QWidget *tab_15;
    QGroupBox *Group_set_parameters;
    QPushButton *calcule_def_dlt;
    QLabel *label_136;
    QLabel *default_var;
    QLabel *label_138;
    QLabel *label_139;
    QLabel *label_140;
    QPushButton *calcule_man_dlt;
    QFrame *line_43;
    QLabel *label_141;
    QPushButton *calcule_auto_dlt;
    QPushButton *update_dlt_val;
    QLabel *label_160;
    QLabel *atomatic_var;
    QFrame *line_27;
    QPushButton *update_dlt_average;
    QSpinBox *manual_var;
    QLabel *update_labal;
    QWidget *gridLayoutWidget_6;
    QGridLayout *gridLayout_27;
    QGridLayout *gridLayout_34;
    QLabel *delta_c_6;
    QLabel *delta_c_4;
    QLabel *label_162;
    QFrame *line_47;
    QLabel *label_158;
    QLabel *label_156;
    QLabel *delta_r_4;
    QLabel *label_152;
    QFrame *line_48;
    QLabel *label_155;
    QLabel *delta_c_5;
    QLabel *delta_r_5;
    QLabel *label_151;
    QLabel *delta_r_6;
    QGridLayout *gridLayout_32;
    QFrame *line_49;
    QLabel *label_146;
    QLabel *delta_r_2;
    QLabel *label_143;
    QLabel *delta_c_3;
    QLabel *label_144;
    QLabel *delta_c_1;
    QLabel *delta_r_1;
    QFrame *line_50;
    QLabel *label_142;
    QLabel *label_145;
    QLabel *delta_r_3;
    QLabel *delta_c_2;
    QLabel *label_147;
    QLabel *delta_average;
    QLabel *label_81;
    QLabel *label_89;
    QLabel *label_91;
    QLabel *label_77;
    QLabel *label_184;
    QLabel *label_185;
    QLabel *delta_average_2;
    QLabel *label_186;
    QWidget *tab_8;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_18;
    QGridLayout *gridLayout_30;
    QLCDNumber *calib_raw_r_3;
    QLabel *label_124;
    QLCDNumber *calib_raw_c_3;
    QLCDNumber *calib_raw_c_1;
    QLabel *label_125;
    QLabel *label_126;
    QLabel *label_127;
    QLabel *label_128;
    QFrame *line_39;
    QLCDNumber *calib_raw_r_1;
    QLCDNumber *calib_raw_c_2;
    QFrame *line_40;
    QLCDNumber *calib_raw_r_2;
    QLabel *label_129;
    QLabel *label_100;
    QGridLayout *gridLayout_28;
    QLabel *label_137;
    QLCDNumber *calib_raw_r_6;
    QLabel *label_148;
    QLabel *label_161;
    QFrame *line_51;
    QFrame *line_52;
    QLCDNumber *calib_raw_c_6;
    QLabel *label_163;
    QLabel *label_164;
    QLCDNumber *calib_raw_c_4;
    QLCDNumber *calib_raw_r_5;
    QLCDNumber *calib_raw_c_5;
    QLabel *label_165;
    QLCDNumber *calib_raw_r_4;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_25;
    QPushButton *captura_data_2;
    QGridLayout *gridLayout_36;
    QLabel *calib_cap_r_1;
    QLabel *label_172;
    QLabel *label_173;
    QLabel *calib_cap_r_2;
    QFrame *line_55;
    QLabel *calib_cap_c_2;
    QLabel *label_174;
    QLabel *label_175;
    QFrame *line_56;
    QLabel *calib_cap_c_3;
    QLabel *calib_cap_r_3;
    QLabel *label_176;
    QLabel *calib_cap_c_1;
    QLabel *label_177;
    QGridLayout *gridLayout_35;
    QLabel *calib_cap_r_4;
    QLabel *label_166;
    QLabel *label_167;
    QLabel *calib_cap_r_5;
    QFrame *line_53;
    QLabel *calib_cap_c_5;
    QLabel *label_168;
    QLabel *label_169;
    QFrame *line_54;
    QLabel *calib_cap_c_6;
    QLabel *calib_cap_r_6;
    QLabel *label_170;
    QLabel *calib_cap_c_4;
    QLabel *label_171;
    QGroupBox *groupBox;
    QDoubleSpinBox *doubleSpinBox;
    QDoubleSpinBox *doubleSpinBox_3;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QSpinBox *spinBox_4;
    QFrame *line_30;
    QFrame *line_29;
    QLabel *calib_message;
    QTabWidget *tabWidget_4;
    QWidget *tab_16;
    QGroupBox *result_obj;
    QLabel *label_95;
    QLabel *label_96;
    QLabel *label_97;
    QLabel *label_101;
    QFrame *line_31;
    QLabel *label_98;
    QLabel *label_102;
    QPushButton *avg_oil;
    QPushButton *avg_air;
    QPushButton *avg_w;
    QPushButton *avg_fw;
    QSpinBox *res_avg_oil;
    QSpinBox *res_avg_f_water;
    QSpinBox *res_avg_water;
    QSpinBox *cap_avg_oil;
    QSpinBox *res_avg_air;
    QSpinBox *cap_avg_air;
    QSpinBox *cap_avg_water;
    QSpinBox *cap_avg_f_water;
    QLabel *label_216;
    QLabel *label_217;
    QLabel *label_218;
    QLabel *label_219;
    QLabel *label_220;
    QLabel *label_221;
    QLabel *label_222;
    QFrame *line_32;
    QLabel *label_223;
    QLabel *label_224;
    QLabel *label_240;
    QLabel *label_241;
    QLabel *label_242;
    QLabel *label_243;
    QPushButton *calcule_curves;
    QLabel *label_109;
    QWidget *tab_17;
    QWidget *widget;
    QLabel *label_108;
    QSpinBox *value_hwc_up;
    QSpinBox *value_hwc_dw;
    QSpinBox *percent_hwc_up;
    QLabel *label_187;
    QLabel *label_193;
    QSpinBox *percent_hwc_dw;
    QFrame *line_58;
    QWidget *widget_2;
    QLabel *label_192;
    QSpinBox *value_lwc_up;
    QSpinBox *value_lwc_dw;
    QSpinBox *percent_lwc_up;
    QLabel *label_189;
    QLabel *label_206;
    QSpinBox *percent_lwc_dw;
    QFrame *line_59;
    QPushButton *calcule_curves_hl;
    QLabel *label_207;
    QLabel *label_208;
    QLabel *label_209;
    QLabel *label_210;
    QLabel *label_211;
    QLabel *label_212;
    QLabel *label_213;
    QLabel *label_214;
    QLabel *label_215;
    QLabel *label_226;
    QWidget *widget_5;
    QLabel *label_228;
    QSpinBox *value_hwc_up_2;
    QSpinBox *value_hwc_dw_2;
    QSpinBox *percent_hwc_up_2;
    QLabel *label_230;
    QLabel *label_271;
    QSpinBox *percent_hwc_dw_2;
    QFrame *line_62;
    QWidget *tab_18;
    QWidget *widget_3;
    QLabel *label_200;
    QSpinBox *value_t_phases_up;
    QFrame *line_60;
    QLabel *label_227;
    QWidget *widget_4;
    QSpinBox *value_t_phases_dw;
    QFrame *line_61;
    QLabel *label_229;
    QPushButton *calcule_curves_TFM;
    QWidget *tab_7;
    QTabWidget *capacitance_curves;
    QWidget *tab_10;
    QCustomPlot *customplot_r;
    QLabel *label_111;
    QWidget *tab_11;
    QCustomPlot *customplot_c;
    QLabel *label_110;
    QWidget *tab_19;
    QCustomPlot *customplot_TFM;
    QLabel *label_225;
    QWidget *tab_9;
    QGroupBox *groupBox_2;
    QCheckBox *checkBox_4;
    QCheckBox *RB_ST_CO;
    QCheckBox *checkBox_5;
    QCheckBox *RB_TP_CO;
    QGroupBox *groupBox_3;
    QLabel *label_194;
    QLCDNumber *LD_RE_LAM;
    QLabel *label_195;
    QLCDNumber *LD_RE_TUR;
    QLabel *label_196;
    QLCDNumber *LD_TL_FINAL;
    QLabel *label_197;
    QLCDNumber *LD_RE_16;
    QLabel *label_198;
    QLCDNumber *LD_RE_17;
    QLabel *label_199;
    QLCDNumber *LD_RE_18;
    QLCDNumber *LD_RE_20;
    QLabel *label_201;
    QLabel *label_202;
    QLabel *label_203;
    QLabel *label_204;
    QLabel *label_205;
    QLCDNumber *LD_RE_21;
    QLCDNumber *LD_RE_22;
    QLCDNumber *LD_RE_23;
    QFrame *line_63;
    QFrame *line_64;
    QFrame *line_65;
    QFrame *line_66;
    QLabel *label_190;
    QLCDNumber *LD_TL_RES;
    QLabel *label_191;
    QLCDNumber *LD_TL_CAP;
    QTabWidget *functionescorrect;
    QWidget *tab_correct_temp;
    QWidget *gridLayoutWidget_10;
    QGridLayout *gridLayout_23;
    QGridLayout *gridLayout_45;
    QLabel *label_259;
    QLCDNumber *LD_RE_T6;
    QLabel *label_260;
    QLabel *label_261;
    QFrame *line_79;
    QFrame *line_80;
    QLCDNumber *LD_CA_T6;
    QLabel *label_262;
    QLabel *label_263;
    QLCDNumber *LD_CA_T4;
    QLCDNumber *LD_RE_T5;
    QLCDNumber *LD_CA_T5;
    QLabel *label_264;
    QLCDNumber *LD_RE_T4;
    QGridLayout *gridLayout_46;
    QLCDNumber *LD_RE_T3;
    QLabel *label_265;
    QLCDNumber *LD_CA_T3;
    QLCDNumber *LD_CA_T1;
    QLabel *label_266;
    QLabel *label_267;
    QLabel *label_268;
    QLabel *label_269;
    QFrame *line_81;
    QLCDNumber *LD_RE_T1;
    QLCDNumber *LD_CA_T2;
    QFrame *line_82;
    QLCDNumber *LD_RE_T2;
    QLabel *label_270;
    QLCDNumber *LD_TP;
    QDoubleSpinBox *SD_TP_F_CH;
    QCheckBox *CK_TP_F_HD;
    QLabel *label_104;
    QLabel *label_105;
    QWidget *tab_correct_sali;
    QWidget *gridLayoutWidget_9;
    QGridLayout *gridLayout_22;
    QGridLayout *gridLayout_43;
    QLabel *label_247;
    QLCDNumber *LD_RE_S6;
    QLabel *label_248;
    QLabel *label_249;
    QFrame *line_75;
    QFrame *line_76;
    QLCDNumber *LD_CA_S6;
    QLabel *label_250;
    QLabel *label_251;
    QLCDNumber *LD_CA_S4;
    QLCDNumber *LD_RE_S5;
    QLCDNumber *LD_CA_S5;
    QLabel *label_252;
    QLCDNumber *LD_RE_S4;
    QGridLayout *gridLayout_44;
    QLCDNumber *LD_RE_S3;
    QLabel *label_253;
    QLCDNumber *LD_CA_S3;
    QLCDNumber *LD_CA_S1;
    QLabel *label_254;
    QLabel *label_255;
    QLabel *label_256;
    QLabel *label_257;
    QFrame *line_77;
    QLCDNumber *LD_RE_S1;
    QLCDNumber *LD_CA_S2;
    QFrame *line_78;
    QLCDNumber *LD_RE_S2;
    QLabel *label_258;
    QFrame *line_33;
    QSpinBox *SI_ST_F_CH;
    QCheckBox *CK_ST_F_HD;
    QCheckBox *CK_ST_HD_1;
    QCheckBox *CK_ST_HD_6;
    QFrame *line_34;
    QWidget *tab_13;
    QGroupBox *groupBox_4;
    QSpinBox *spin_regime;
    QLabel *REGIME_LABEL;
    QLabel *label_106;
    QLabel *label_107;
    QWidget *tab_12;
    QWidget *gridLayoutWidget_8;
    QGridLayout *gridLayout_21;
    QGridLayout *gridLayout_42;
    QLabel *label_237;
    QLCDNumber *wc_res_6;
    QLabel *label_238;
    QLabel *label_239;
    QFrame *line_73;
    QFrame *line_74;
    QLCDNumber *wc_cap_6;
    QLabel *label_244;
    QLabel *label_245;
    QLCDNumber *wc_cap_4;
    QLCDNumber *wc_res_5;
    QLCDNumber *wc_cap_5;
    QLabel *label_246;
    QLCDNumber *wc_res_4;
    QGridLayout *gridLayout_41;
    QLCDNumber *wc_res_3;
    QLabel *label_231;
    QLCDNumber *wc_cap_3;
    QLCDNumber *wc_cap_1;
    QLabel *label_232;
    QLabel *label_233;
    QLabel *label_234;
    QLabel *label_235;
    QFrame *line_71;
    QLCDNumber *wc_res_1;
    QLCDNumber *wc_cap_2;
    QFrame *line_72;
    QLCDNumber *wc_res_2;
    QLabel *label_236;
    QLabel *label_75;
    QLabel *label_72;
    QLabel *label_74;
    QLabel *label_71;
    QLCDNumber *LD_LM_CA;
    QLCDNumber *LD_LM_RE;
    QLCDNumber *LD_TU_CA;
    QLCDNumber *LD_TU_RE;
    QLabel *label_188;
    QLCDNumber *LD_SEND_TYPE;
    QLCDNumber *LD_SEND_VALUE;
    QWidget *tab_3;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_11;
    QLabel *label_29;
    QCheckBox *CK_TP_F_HDx;
    QLabel *label_45;
    QLabel *label_30;
    QComboBox *CB_TP_SL;
    QDoubleSpinBox *SD_TP_F_CHx;
    QFrame *line_5;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout_2;
    QLabel *label_14;
    QLabel *label;
    QDoubleSpinBox *SD_FL_F_CH;
    QLabel *label_35;
    QFrame *line_6;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout_17;
    QLabel *label_82;
    QLabel *label_85;
    QLabel *label_86;
    QSpinBox *SI_PO_CH;
    QFrame *line;
    QGridLayout *gridLayout_14;
    QLabel *label_15;
    QCheckBox *CK_ST_F_HDx;
    QSpinBox *SI_ST_F_CHx;
    QCheckBox *CK_ST_HD_1x;
    QCheckBox *CK_ST_HD_6x;
    QFrame *line_2;
    QGridLayout *gridLayout_7;
    QProgressBar *PB_RE_VW;
    QLabel *label_27;
    QProgressBar *PB_CA_VW;
    QLabel *label_25;
    QLabel *label_23;
    QFrame *line_3;
    QGridLayout *gridLayout_10;
    QLabel *label_34;
    QSpinBox *SI_REST_L_CH;
    QSpinBox *SI_CATP_L_CH;
    QLabel *label_26;
    QLabel *label_28;
    QSpinBox *SI_RETP_L_CH;
    QLabel *label_31;
    QSpinBox *SI_CAST_L_CH;
    QLabel *label_24;
    QFrame *line_4;
    QGridLayout *gridLayout_8;
    QLabel *label_17;
    QFrame *line_11;
    QLabel *label_18;
    QLabel *label_37;
    QLCDNumber *LD_TPx;
    QLCDNumber *LD_ST;
    QLCDNumber *LD_PS;
    QLabel *label_36;
    QLabel *label_42;
    QLabel *label_16;
    QRadioButton *RB_TP_COx;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_19;
    QRadioButton *RB_ST_COx;
    QRadioButton *RB_FL_CO;
    QPushButton *SINF;
    QWidget *tab;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout_12;
    QLCDNumber *LD_TU_CAX;
    QLCDNumber *LD_LM_REX;
    QLCDNumber *L_LM_CA;
    QLCDNumber *LD_TU_REX;
    QFrame *line_24;
    QFrame *line_26;
    QLabel *label_83;
    QLCDNumber *LD_TU_ME;
    QLabel *label_84;
    QLabel *label_88;
    QLCDNumber *LD_VA_RE;
    QLCDNumber *LD_LM_ME;
    QLabel *label_90;
    QLCDNumber *LD_VA_CA;
    QFrame *line_22;
    QLabel *label_80;
    QGridLayout *gridLayout;
    QLCDNumber *LD_RE_3;
    QLabel *label_10;
    QLCDNumber *LD_CA_3;
    QLCDNumber *LD_CA_1;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_7;
    QLabel *label_11;
    QFrame *line_7;
    QLCDNumber *LD_RE_1;
    QLCDNumber *LD_CA_2;
    QFrame *line_8;
    QLCDNumber *LD_RE_2;
    QLabel *label_6;
    QGridLayout *gridLayout_3;
    QLabel *label_8;
    QLabel *label_5;
    QLCDNumber *LD_CA_6;
    QFrame *line_10;
    QLCDNumber *LD_RE_5;
    QLabel *label_9;
    QLCDNumber *LD_CA_5;
    QLCDNumber *LD_RE_6;
    QLabel *label_13;
    QLabel *label_4;
    QLabel *label_12;
    QLCDNumber *LD_CA_4;
    QLCDNumber *LD_RE_4;
    QFrame *line_9;
    QFrame *line_13;
    QLabel *label_79;
    QGridLayout *gridLayout_4;
    QLabel *label_47;
    QLabel *label_48;
    QFrame *line_14;
    QLCDNumber *LD_RE_T1x;
    QLCDNumber *LD_CA_T1x;
    QLabel *label_49;
    QLabel *label_51;
    QLCDNumber *LD_CA_T2x;
    QFrame *line_15;
    QLCDNumber *LD_RE_T2x;
    QLabel *label_52;
    QLCDNumber *LD_RE_T3x;
    QLabel *label_53;
    QLCDNumber *LD_CA_T3x;
    QGridLayout *gridLayout_5;
    QLCDNumber *LD_CA_T4x;
    QFrame *line_17;
    QLCDNumber *LD_CA_T5x;
    QLabel *label_57;
    QLabel *label_55;
    QLCDNumber *LD_CA_T6x;
    QLabel *label_58;
    QLCDNumber *LD_RE_T6x;
    QLabel *label_61;
    QLCDNumber *LD_RE_T5x;
    QLabel *label_46;
    QLCDNumber *LD_RE_T4x;
    QLabel *label_50;
    QFrame *line_16;
    QFrame *line_23;
    QLabel *label_78;
    QGridLayout *gridLayout_9;
    QLabel *label_64;
    QLabel *label_65;
    QFrame *line_20;
    QLCDNumber *LD_RE_S1x;
    QLCDNumber *LD_CA_S1x;
    QLabel *label_66;
    QLabel *label_67;
    QLCDNumber *LD_CA_S2x;
    QFrame *line_21;
    QLCDNumber *LD_RE_S2x;
    QLabel *label_68;
    QLCDNumber *LD_RE_S3x;
    QLabel *label_69;
    QLCDNumber *LD_CA_S3x;
    QGridLayout *gridLayout_6;
    QFrame *line_18;
    QLabel *label_62;
    QLCDNumber *LD_RE_S4x;
    QLabel *label_60;
    QFrame *line_19;
    QLabel *label_54;
    QLabel *label_56;
    QLCDNumber *LD_RE_S6x;
    QLCDNumber *LD_CA_S4x;
    QLabel *label_63;
    QLCDNumber *LD_RE_S5x;
    QLCDNumber *LD_CA_S5x;
    QLCDNumber *LD_CA_S6x;
    QLabel *label_59;
    QWidget *tab_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_13;
    QLabel *label_21;
    QLabel *label_20;
    QLabel *label_33;
    QSpinBox *SB_Y2;
    QSpinBox *SB_X1;
    QSpinBox *SB_Y1;
    QSpinBox *SB_X2;
    QLabel *label_32;
    QPushButton *BT_GE_GR;
    QLabel *label_40;
    QRadioButton *RB_EQ_HC;
    QFrame *line_12;
    QLineEdit *LE_HC;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_15;
    QLabel *label_22;
    QLabel *label_39;
    QLCDNumber *LD_HC_CA;
    QLCDNumber *LD_HC_ME;
    QLabel *label_38;
    QLCDNumber *LD_HC_RE;
    QLCDNumber *LD_HC_CA_CO;
    QWidget *tab_4;
    QLabel *label_41;
    QPushButton *charge_data;
    QFrame *line_25;
    QLabel *label_43;
    QPushButton *refactor;
    QLabel *label_44;
    QLabel *connect_txt;
    QLabel *label_70;
    QPushButton *select_folder;
    QLabel *folder_txt;
    QLabel *dest_db_label;
    QLabel *label_73;
    QLabel *label_76;
    QPushButton *start_refactor;
    QLabel *label_103;
    QCheckBox *check_refactor;
    QPushButton *persistant;
    QSpinBox *per_init;
    QSpinBox *per_fin;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *jptwatercut)
    {
        if (jptwatercut->objectName().isEmpty())
            jptwatercut->setObjectName(QStringLiteral("jptwatercut"));
        jptwatercut->resize(762, 704);
        QIcon icon;
        icon.addFile(QStringLiteral(":/water_cut.png"), QSize(), QIcon::Normal, QIcon::Off);
        jptwatercut->setWindowIcon(icon);
        centralWidget = new QWidget(jptwatercut);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setEnabled(true);
        tabWidget->setGeometry(QRect(10, 10, 741, 671));
        QFont font;
        font.setPointSize(12);
        tabWidget->setFont(font);
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        tabWidget_2 = new QTabWidget(tab_5);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setGeometry(QRect(6, 9, 721, 621));
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        gridLayoutWidget_3 = new QWidget(tab_6);
        gridLayoutWidget_3->setObjectName(QStringLiteral("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(-1, -1, 471, 141));
        gridLayout_16 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        gridLayout_16->setContentsMargins(0, 0, 0, 0);
        gridLayout_29 = new QGridLayout();
        gridLayout_29->setSpacing(6);
        gridLayout_29->setObjectName(QStringLiteral("gridLayout_29"));
        ld_my_r3 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r3->setObjectName(QStringLiteral("ld_my_r3"));
        ld_my_r3->setSegmentStyle(QLCDNumber::Filled);
        ld_my_r3->setProperty("intValue", QVariant(0));

        gridLayout_29->addWidget(ld_my_r3, 1, 7, 1, 1);

        label_112 = new QLabel(gridLayoutWidget_3);
        label_112->setObjectName(QStringLiteral("label_112"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_112->setFont(font1);

        gridLayout_29->addWidget(label_112, 1, 6, 1, 1);

        ld_my_c3 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c3->setObjectName(QStringLiteral("ld_my_c3"));

        gridLayout_29->addWidget(ld_my_c3, 2, 7, 1, 1);

        ld_my_c1 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c1->setObjectName(QStringLiteral("ld_my_c1"));

        gridLayout_29->addWidget(ld_my_c1, 2, 1, 1, 1);

        label_113 = new QLabel(gridLayoutWidget_3);
        label_113->setObjectName(QStringLiteral("label_113"));
        label_113->setFont(font1);

        gridLayout_29->addWidget(label_113, 2, 0, 1, 1);

        label_114 = new QLabel(gridLayoutWidget_3);
        label_114->setObjectName(QStringLiteral("label_114"));
        label_114->setFont(font1);

        gridLayout_29->addWidget(label_114, 1, 0, 1, 1);

        label_115 = new QLabel(gridLayoutWidget_3);
        label_115->setObjectName(QStringLiteral("label_115"));
        label_115->setFont(font1);

        gridLayout_29->addWidget(label_115, 2, 3, 1, 1);

        label_116 = new QLabel(gridLayoutWidget_3);
        label_116->setObjectName(QStringLiteral("label_116"));
        label_116->setFont(font1);

        gridLayout_29->addWidget(label_116, 2, 6, 1, 1);

        line_35 = new QFrame(gridLayoutWidget_3);
        line_35->setObjectName(QStringLiteral("line_35"));
        line_35->setFrameShape(QFrame::VLine);
        line_35->setFrameShadow(QFrame::Sunken);

        gridLayout_29->addWidget(line_35, 1, 2, 2, 1);

        ld_my_r1 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r1->setObjectName(QStringLiteral("ld_my_r1"));

        gridLayout_29->addWidget(ld_my_r1, 1, 1, 1, 1);

        ld_my_c2 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c2->setObjectName(QStringLiteral("ld_my_c2"));

        gridLayout_29->addWidget(ld_my_c2, 2, 4, 1, 1);

        line_36 = new QFrame(gridLayoutWidget_3);
        line_36->setObjectName(QStringLiteral("line_36"));
        line_36->setFrameShape(QFrame::VLine);
        line_36->setFrameShadow(QFrame::Sunken);

        gridLayout_29->addWidget(line_36, 1, 5, 2, 1);

        ld_my_r2 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r2->setObjectName(QStringLiteral("ld_my_r2"));
        QFont font2;
        font2.setFamily(QStringLiteral("Roboto"));
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setWeight(50);
        ld_my_r2->setFont(font2);

        gridLayout_29->addWidget(ld_my_r2, 1, 4, 1, 1);

        label_117 = new QLabel(gridLayoutWidget_3);
        label_117->setObjectName(QStringLiteral("label_117"));
        label_117->setFont(font1);

        gridLayout_29->addWidget(label_117, 1, 3, 1, 1);


        gridLayout_16->addLayout(gridLayout_29, 1, 0, 1, 1);

        gridLayout_26 = new QGridLayout();
        gridLayout_26->setSpacing(6);
        gridLayout_26->setObjectName(QStringLiteral("gridLayout_26"));
        label_122 = new QLabel(gridLayoutWidget_3);
        label_122->setObjectName(QStringLiteral("label_122"));
        label_122->setFont(font1);

        gridLayout_26->addWidget(label_122, 2, 6, 1, 1);

        ld_my_r6 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r6->setObjectName(QStringLiteral("ld_my_r6"));
        ld_my_r6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_26->addWidget(ld_my_r6, 1, 7, 1, 1);

        label_123 = new QLabel(gridLayoutWidget_3);
        label_123->setObjectName(QStringLiteral("label_123"));
        label_123->setFont(font1);

        gridLayout_26->addWidget(label_123, 1, 3, 1, 1);

        label_118 = new QLabel(gridLayoutWidget_3);
        label_118->setObjectName(QStringLiteral("label_118"));
        label_118->setFont(font1);

        gridLayout_26->addWidget(label_118, 1, 6, 1, 1);

        line_37 = new QFrame(gridLayoutWidget_3);
        line_37->setObjectName(QStringLiteral("line_37"));
        line_37->setFrameShape(QFrame::VLine);
        line_37->setFrameShadow(QFrame::Sunken);

        gridLayout_26->addWidget(line_37, 1, 2, 2, 1);

        line_38 = new QFrame(gridLayoutWidget_3);
        line_38->setObjectName(QStringLiteral("line_38"));
        line_38->setFrameShape(QFrame::VLine);
        line_38->setFrameShadow(QFrame::Sunken);

        gridLayout_26->addWidget(line_38, 1, 5, 2, 1);

        ld_my_c6 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c6->setObjectName(QStringLiteral("ld_my_c6"));

        gridLayout_26->addWidget(ld_my_c6, 2, 7, 1, 1);

        label_120 = new QLabel(gridLayoutWidget_3);
        label_120->setObjectName(QStringLiteral("label_120"));
        label_120->setFont(font1);

        gridLayout_26->addWidget(label_120, 1, 0, 1, 1);

        label_121 = new QLabel(gridLayoutWidget_3);
        label_121->setObjectName(QStringLiteral("label_121"));
        label_121->setFont(font1);

        gridLayout_26->addWidget(label_121, 2, 3, 1, 1);

        ld_my_c4 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c4->setObjectName(QStringLiteral("ld_my_c4"));

        gridLayout_26->addWidget(ld_my_c4, 2, 1, 1, 1);

        ld_my_r5 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r5->setObjectName(QStringLiteral("ld_my_r5"));
        ld_my_r5->setFont(font2);

        gridLayout_26->addWidget(ld_my_r5, 1, 4, 1, 1);

        ld_my_c5 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_c5->setObjectName(QStringLiteral("ld_my_c5"));

        gridLayout_26->addWidget(ld_my_c5, 2, 4, 1, 1);

        label_119 = new QLabel(gridLayoutWidget_3);
        label_119->setObjectName(QStringLiteral("label_119"));
        label_119->setFont(font1);

        gridLayout_26->addWidget(label_119, 2, 0, 1, 1);

        ld_my_r4 = new QLCDNumber(gridLayoutWidget_3);
        ld_my_r4->setObjectName(QStringLiteral("ld_my_r4"));

        gridLayout_26->addWidget(ld_my_r4, 1, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_26, 2, 0, 1, 1);

        label_99 = new QLabel(gridLayoutWidget_3);
        label_99->setObjectName(QStringLiteral("label_99"));
        label_99->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(label_99, 0, 0, 1, 1);

        gridLayoutWidget_4 = new QWidget(tab_6);
        gridLayoutWidget_4->setObjectName(QStringLiteral("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(0, 140, 711, 133));
        gridLayout_24 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_24->setSpacing(6);
        gridLayout_24->setContentsMargins(11, 11, 11, 11);
        gridLayout_24->setObjectName(QStringLiteral("gridLayout_24"));
        gridLayout_24->setContentsMargins(0, 0, 0, 0);
        gridLayout_33 = new QGridLayout();
        gridLayout_33->setSpacing(6);
        gridLayout_33->setObjectName(QStringLiteral("gridLayout_33"));
        cap_r_4 = new QLabel(gridLayoutWidget_4);
        cap_r_4->setObjectName(QStringLiteral("cap_r_4"));
        cap_r_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_r_4, 1, 1, 1, 1);

        label_153 = new QLabel(gridLayoutWidget_4);
        label_153->setObjectName(QStringLiteral("label_153"));
        label_153->setFont(font1);

        gridLayout_33->addWidget(label_153, 1, 0, 1, 1);

        cap_c_6 = new QLabel(gridLayoutWidget_4);
        cap_c_6->setObjectName(QStringLiteral("cap_c_6"));
        cap_c_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_c_6, 2, 7, 1, 1);

        cap_r_6 = new QLabel(gridLayoutWidget_4);
        cap_r_6->setObjectName(QStringLiteral("cap_r_6"));
        cap_r_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_r_6, 1, 7, 1, 1);

        label_159 = new QLabel(gridLayoutWidget_4);
        label_159->setObjectName(QStringLiteral("label_159"));
        label_159->setFont(font1);

        gridLayout_33->addWidget(label_159, 2, 3, 1, 1);

        cap_c_4 = new QLabel(gridLayoutWidget_4);
        cap_c_4->setObjectName(QStringLiteral("cap_c_4"));
        cap_c_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_c_4, 2, 1, 1, 1);

        label_154 = new QLabel(gridLayoutWidget_4);
        label_154->setObjectName(QStringLiteral("label_154"));
        label_154->setFont(font1);

        gridLayout_33->addWidget(label_154, 1, 6, 1, 1);

        cap_r_5 = new QLabel(gridLayoutWidget_4);
        cap_r_5->setObjectName(QStringLiteral("cap_r_5"));
        cap_r_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_r_5, 1, 4, 1, 1);

        cap_c_5 = new QLabel(gridLayoutWidget_4);
        cap_c_5->setObjectName(QStringLiteral("cap_c_5"));
        cap_c_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_33->addWidget(cap_c_5, 2, 4, 1, 1);

        line_45 = new QFrame(gridLayoutWidget_4);
        line_45->setObjectName(QStringLiteral("line_45"));
        line_45->setFrameShape(QFrame::VLine);
        line_45->setFrameShadow(QFrame::Sunken);

        gridLayout_33->addWidget(line_45, 1, 2, 2, 1);

        line_46 = new QFrame(gridLayoutWidget_4);
        line_46->setObjectName(QStringLiteral("line_46"));
        line_46->setFrameShape(QFrame::VLine);
        line_46->setFrameShadow(QFrame::Sunken);

        gridLayout_33->addWidget(line_46, 1, 5, 2, 1);

        label_157 = new QLabel(gridLayoutWidget_4);
        label_157->setObjectName(QStringLiteral("label_157"));
        label_157->setFont(font1);

        gridLayout_33->addWidget(label_157, 2, 0, 1, 1);

        label_149 = new QLabel(gridLayoutWidget_4);
        label_149->setObjectName(QStringLiteral("label_149"));
        label_149->setFont(font1);

        gridLayout_33->addWidget(label_149, 1, 3, 1, 1);

        label_150 = new QLabel(gridLayoutWidget_4);
        label_150->setObjectName(QStringLiteral("label_150"));
        label_150->setFont(font1);

        gridLayout_33->addWidget(label_150, 2, 6, 1, 1);


        gridLayout_24->addLayout(gridLayout_33, 2, 0, 1, 1);

        gridLayout_31 = new QGridLayout();
        gridLayout_31->setSpacing(6);
        gridLayout_31->setObjectName(QStringLiteral("gridLayout_31"));
        cap_r_1 = new QLabel(gridLayoutWidget_4);
        cap_r_1->setObjectName(QStringLiteral("cap_r_1"));
        cap_r_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_r_1, 1, 1, 1, 1);

        label_135 = new QLabel(gridLayoutWidget_4);
        label_135->setObjectName(QStringLiteral("label_135"));
        label_135->setFont(font1);

        gridLayout_31->addWidget(label_135, 1, 3, 1, 1);

        label_134 = new QLabel(gridLayoutWidget_4);
        label_134->setObjectName(QStringLiteral("label_134"));
        label_134->setFont(font1);

        gridLayout_31->addWidget(label_134, 2, 6, 1, 1);

        cap_r_2 = new QLabel(gridLayoutWidget_4);
        cap_r_2->setObjectName(QStringLiteral("cap_r_2"));
        cap_r_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_r_2, 1, 4, 1, 1);

        line_41 = new QFrame(gridLayoutWidget_4);
        line_41->setObjectName(QStringLiteral("line_41"));
        line_41->setFrameShape(QFrame::VLine);
        line_41->setFrameShadow(QFrame::Sunken);

        gridLayout_31->addWidget(line_41, 1, 2, 2, 1);

        cap_c_2 = new QLabel(gridLayoutWidget_4);
        cap_c_2->setObjectName(QStringLiteral("cap_c_2"));
        cap_c_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_c_2, 2, 4, 1, 1);

        label_132 = new QLabel(gridLayoutWidget_4);
        label_132->setObjectName(QStringLiteral("label_132"));
        label_132->setFont(font1);

        gridLayout_31->addWidget(label_132, 1, 0, 1, 1);

        label_130 = new QLabel(gridLayoutWidget_4);
        label_130->setObjectName(QStringLiteral("label_130"));
        label_130->setFont(font1);

        gridLayout_31->addWidget(label_130, 1, 6, 1, 1);

        line_42 = new QFrame(gridLayoutWidget_4);
        line_42->setObjectName(QStringLiteral("line_42"));
        line_42->setFrameShape(QFrame::VLine);
        line_42->setFrameShadow(QFrame::Sunken);

        gridLayout_31->addWidget(line_42, 1, 5, 2, 1);

        cap_c_3 = new QLabel(gridLayoutWidget_4);
        cap_c_3->setObjectName(QStringLiteral("cap_c_3"));
        cap_c_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_c_3, 2, 7, 1, 1);

        cap_r_3 = new QLabel(gridLayoutWidget_4);
        cap_r_3->setObjectName(QStringLiteral("cap_r_3"));
        cap_r_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_r_3, 1, 7, 1, 1);

        label_131 = new QLabel(gridLayoutWidget_4);
        label_131->setObjectName(QStringLiteral("label_131"));
        label_131->setFont(font1);

        gridLayout_31->addWidget(label_131, 2, 0, 1, 1);

        cap_c_1 = new QLabel(gridLayoutWidget_4);
        cap_c_1->setObjectName(QStringLiteral("cap_c_1"));
        cap_c_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_31->addWidget(cap_c_1, 2, 1, 1, 1);

        label_133 = new QLabel(gridLayoutWidget_4);
        label_133->setObjectName(QStringLiteral("label_133"));
        label_133->setFont(font1);

        gridLayout_31->addWidget(label_133, 2, 3, 1, 1);


        gridLayout_24->addLayout(gridLayout_31, 1, 0, 1, 1);

        captura_data = new QPushButton(gridLayoutWidget_4);
        captura_data->setObjectName(QStringLiteral("captura_data"));

        gridLayout_24->addWidget(captura_data, 0, 0, 1, 1);

        line_28 = new QFrame(tab_6);
        line_28->setObjectName(QStringLiteral("line_28"));
        line_28->setGeometry(QRect(470, 10, 20, 121));
        line_28->setFrameShape(QFrame::VLine);
        line_28->setFrameShadow(QFrame::Sunken);
        dateEdit = new QDateEdit(tab_6);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));
        dateEdit->setGeometry(QRect(620, 20, 91, 26));
        dateEdit->setLayoutDirection(Qt::RightToLeft);
        dateEdit->setMaximumDate(QDate(9999, 12, 29));
        dateEdit->setCalendarPopup(true);
        dateEdit->setDate(QDate(2019, 1, 1));
        label_92 = new QLabel(tab_6);
        label_92->setObjectName(QStringLiteral("label_92"));
        label_92->setGeometry(QRect(490, 20, 71, 20));
        label_93 = new QLabel(tab_6);
        label_93->setObjectName(QStringLiteral("label_93"));
        label_93->setGeometry(QRect(490, 50, 111, 20));
        spinBox = new QSpinBox(tab_6);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(620, 50, 91, 26));
        spinBox->setLayoutDirection(Qt::RightToLeft);
        spinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBox->setMinimum(0);
        spinBox->setMaximum(150);
        spinBox->setValue(0);
        temperature_normal = new QSpinBox(tab_6);
        temperature_normal->setObjectName(QStringLiteral("temperature_normal"));
        temperature_normal->setGeometry(QRect(620, 80, 91, 26));
        temperature_normal->setLayoutDirection(Qt::RightToLeft);
        temperature_normal->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        temperature_normal->setMinimum(-30);
        temperature_normal->setValue(25);
        spinBox_3 = new QSpinBox(tab_6);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setGeometry(QRect(620, 110, 91, 26));
        spinBox_3->setLayoutDirection(Qt::RightToLeft);
        spinBox_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_87 = new QLabel(tab_6);
        label_87->setObjectName(QStringLiteral("label_87"));
        label_87->setGeometry(QRect(490, 80, 131, 17));
        label_94 = new QLabel(tab_6);
        label_94->setObjectName(QStringLiteral("label_94"));
        label_94->setGeometry(QRect(490, 110, 67, 17));
        tabWidget_3 = new QTabWidget(tab_6);
        tabWidget_3->setObjectName(QStringLiteral("tabWidget_3"));
        tabWidget_3->setGeometry(QRect(6, 279, 701, 171));
        tab_14 = new QWidget();
        tab_14->setObjectName(QStringLiteral("tab_14"));
        Group_set_parameters_2 = new QGroupBox(tab_14);
        Group_set_parameters_2->setObjectName(QStringLiteral("Group_set_parameters_2"));
        Group_set_parameters_2->setGeometry(QRect(10, 0, 681, 131));
        Group_set_parameters_2->setAlignment(Qt::AlignCenter);
        calcule_def_dlt_2 = new QPushButton(Group_set_parameters_2);
        calcule_def_dlt_2->setObjectName(QStringLiteral("calcule_def_dlt_2"));
        calcule_def_dlt_2->setGeometry(QRect(250, 30, 131, 25));
        label_178 = new QLabel(Group_set_parameters_2);
        label_178->setObjectName(QStringLiteral("label_178"));
        label_178->setGeometry(QRect(20, 30, 111, 17));
        default_var_2 = new QLabel(Group_set_parameters_2);
        default_var_2->setObjectName(QStringLiteral("default_var_2"));
        default_var_2->setGeometry(QRect(150, 30, 91, 20));
        default_var_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_179 = new QLabel(Group_set_parameters_2);
        label_179->setObjectName(QStringLiteral("label_179"));
        label_179->setGeometry(QRect(130, 30, 31, 17));
        label_180 = new QLabel(Group_set_parameters_2);
        label_180->setObjectName(QStringLiteral("label_180"));
        label_180->setGeometry(QRect(20, 70, 111, 17));
        label_181 = new QLabel(Group_set_parameters_2);
        label_181->setObjectName(QStringLiteral("label_181"));
        label_181->setGeometry(QRect(130, 70, 31, 17));
        calcule_man_dlt_2 = new QPushButton(Group_set_parameters_2);
        calcule_man_dlt_2->setObjectName(QStringLiteral("calcule_man_dlt_2"));
        calcule_man_dlt_2->setGeometry(QRect(250, 70, 131, 25));
        line_44 = new QFrame(Group_set_parameters_2);
        line_44->setObjectName(QStringLiteral("line_44"));
        line_44->setGeometry(QRect(20, 50, 361, 20));
        line_44->setFrameShape(QFrame::HLine);
        line_44->setFrameShadow(QFrame::Sunken);
        label_182 = new QLabel(Group_set_parameters_2);
        label_182->setObjectName(QStringLiteral("label_182"));
        label_182->setGeometry(QRect(20, 100, 101, 20));
        calcule_auto_dlt_2 = new QPushButton(Group_set_parameters_2);
        calcule_auto_dlt_2->setObjectName(QStringLiteral("calcule_auto_dlt_2"));
        calcule_auto_dlt_2->setGeometry(QRect(250, 100, 131, 25));
        update_dlt_val_2 = new QPushButton(Group_set_parameters_2);
        update_dlt_val_2->setObjectName(QStringLiteral("update_dlt_val_2"));
        update_dlt_val_2->setGeometry(QRect(460, 30, 201, 25));
        label_183 = new QLabel(Group_set_parameters_2);
        label_183->setObjectName(QStringLiteral("label_183"));
        label_183->setGeometry(QRect(130, 100, 31, 17));
        atomatic_var_2 = new QLabel(Group_set_parameters_2);
        atomatic_var_2->setObjectName(QStringLiteral("atomatic_var_2"));
        atomatic_var_2->setGeometry(QRect(160, 100, 81, 20));
        atomatic_var_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        line_57 = new QFrame(Group_set_parameters_2);
        line_57->setObjectName(QStringLiteral("line_57"));
        line_57->setGeometry(QRect(390, 30, 20, 91));
        line_57->setFrameShape(QFrame::VLine);
        line_57->setFrameShadow(QFrame::Sunken);
        update_dlt_average_2 = new QPushButton(Group_set_parameters_2);
        update_dlt_average_2->setObjectName(QStringLiteral("update_dlt_average_2"));
        update_dlt_average_2->setGeometry(QRect(460, 60, 201, 25));
        manual_var_2 = new QSpinBox(Group_set_parameters_2);
        manual_var_2->setObjectName(QStringLiteral("manual_var_2"));
        manual_var_2->setGeometry(QRect(160, 70, 81, 26));
        manual_var_2->setLayoutDirection(Qt::RightToLeft);
        manual_var_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        manual_var_2->setMaximum(20000);
        manual_var_2->setValue(16000);
        update_labal_2 = new QLabel(Group_set_parameters_2);
        update_labal_2->setObjectName(QStringLiteral("update_labal_2"));
        update_labal_2->setGeometry(QRect(460, 90, 201, 31));
        update_labal_2->setAlignment(Qt::AlignCenter);
        tabWidget_3->addTab(tab_14, QString());
        tab_15 = new QWidget();
        tab_15->setObjectName(QStringLiteral("tab_15"));
        Group_set_parameters = new QGroupBox(tab_15);
        Group_set_parameters->setObjectName(QStringLiteral("Group_set_parameters"));
        Group_set_parameters->setGeometry(QRect(10, 0, 681, 131));
        Group_set_parameters->setAlignment(Qt::AlignCenter);
        calcule_def_dlt = new QPushButton(Group_set_parameters);
        calcule_def_dlt->setObjectName(QStringLiteral("calcule_def_dlt"));
        calcule_def_dlt->setGeometry(QRect(250, 30, 131, 25));
        label_136 = new QLabel(Group_set_parameters);
        label_136->setObjectName(QStringLiteral("label_136"));
        label_136->setGeometry(QRect(20, 30, 111, 17));
        default_var = new QLabel(Group_set_parameters);
        default_var->setObjectName(QStringLiteral("default_var"));
        default_var->setGeometry(QRect(150, 30, 91, 20));
        default_var->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_138 = new QLabel(Group_set_parameters);
        label_138->setObjectName(QStringLiteral("label_138"));
        label_138->setGeometry(QRect(130, 30, 31, 17));
        label_139 = new QLabel(Group_set_parameters);
        label_139->setObjectName(QStringLiteral("label_139"));
        label_139->setGeometry(QRect(20, 70, 111, 17));
        label_140 = new QLabel(Group_set_parameters);
        label_140->setObjectName(QStringLiteral("label_140"));
        label_140->setGeometry(QRect(130, 70, 31, 17));
        calcule_man_dlt = new QPushButton(Group_set_parameters);
        calcule_man_dlt->setObjectName(QStringLiteral("calcule_man_dlt"));
        calcule_man_dlt->setGeometry(QRect(250, 70, 131, 25));
        line_43 = new QFrame(Group_set_parameters);
        line_43->setObjectName(QStringLiteral("line_43"));
        line_43->setGeometry(QRect(20, 50, 361, 20));
        line_43->setFrameShape(QFrame::HLine);
        line_43->setFrameShadow(QFrame::Sunken);
        label_141 = new QLabel(Group_set_parameters);
        label_141->setObjectName(QStringLiteral("label_141"));
        label_141->setGeometry(QRect(20, 100, 101, 20));
        calcule_auto_dlt = new QPushButton(Group_set_parameters);
        calcule_auto_dlt->setObjectName(QStringLiteral("calcule_auto_dlt"));
        calcule_auto_dlt->setGeometry(QRect(250, 100, 131, 25));
        update_dlt_val = new QPushButton(Group_set_parameters);
        update_dlt_val->setObjectName(QStringLiteral("update_dlt_val"));
        update_dlt_val->setGeometry(QRect(460, 30, 201, 25));
        label_160 = new QLabel(Group_set_parameters);
        label_160->setObjectName(QStringLiteral("label_160"));
        label_160->setGeometry(QRect(130, 100, 31, 17));
        atomatic_var = new QLabel(Group_set_parameters);
        atomatic_var->setObjectName(QStringLiteral("atomatic_var"));
        atomatic_var->setGeometry(QRect(160, 100, 81, 20));
        atomatic_var->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        line_27 = new QFrame(Group_set_parameters);
        line_27->setObjectName(QStringLiteral("line_27"));
        line_27->setGeometry(QRect(390, 30, 20, 91));
        line_27->setFrameShape(QFrame::VLine);
        line_27->setFrameShadow(QFrame::Sunken);
        update_dlt_average = new QPushButton(Group_set_parameters);
        update_dlt_average->setObjectName(QStringLiteral("update_dlt_average"));
        update_dlt_average->setGeometry(QRect(460, 60, 201, 25));
        manual_var = new QSpinBox(Group_set_parameters);
        manual_var->setObjectName(QStringLiteral("manual_var"));
        manual_var->setGeometry(QRect(160, 70, 81, 26));
        manual_var->setLayoutDirection(Qt::RightToLeft);
        manual_var->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        manual_var->setMaximum(20000);
        manual_var->setValue(16000);
        update_labal = new QLabel(Group_set_parameters);
        update_labal->setObjectName(QStringLiteral("update_labal"));
        update_labal->setGeometry(QRect(460, 90, 201, 31));
        update_labal->setAlignment(Qt::AlignCenter);
        tabWidget_3->addTab(tab_15, QString());
        gridLayoutWidget_6 = new QWidget(tab_6);
        gridLayoutWidget_6->setObjectName(QStringLiteral("gridLayoutWidget_6"));
        gridLayoutWidget_6->setGeometry(QRect(10, 476, 491, 111));
        gridLayout_27 = new QGridLayout(gridLayoutWidget_6);
        gridLayout_27->setSpacing(6);
        gridLayout_27->setContentsMargins(11, 11, 11, 11);
        gridLayout_27->setObjectName(QStringLiteral("gridLayout_27"));
        gridLayout_27->setContentsMargins(0, 0, 0, 0);
        gridLayout_34 = new QGridLayout();
        gridLayout_34->setSpacing(6);
        gridLayout_34->setObjectName(QStringLiteral("gridLayout_34"));
        delta_c_6 = new QLabel(gridLayoutWidget_6);
        delta_c_6->setObjectName(QStringLiteral("delta_c_6"));
        delta_c_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_c_6, 2, 7, 1, 1);

        delta_c_4 = new QLabel(gridLayoutWidget_6);
        delta_c_4->setObjectName(QStringLiteral("delta_c_4"));
        delta_c_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_c_4, 2, 1, 1, 1);

        label_162 = new QLabel(gridLayoutWidget_6);
        label_162->setObjectName(QStringLiteral("label_162"));
        label_162->setFont(font1);

        gridLayout_34->addWidget(label_162, 2, 3, 1, 1);

        line_47 = new QFrame(gridLayoutWidget_6);
        line_47->setObjectName(QStringLiteral("line_47"));
        line_47->setFrameShape(QFrame::VLine);
        line_47->setFrameShadow(QFrame::Sunken);

        gridLayout_34->addWidget(line_47, 1, 2, 2, 1);

        label_158 = new QLabel(gridLayoutWidget_6);
        label_158->setObjectName(QStringLiteral("label_158"));
        label_158->setFont(font1);

        gridLayout_34->addWidget(label_158, 2, 0, 1, 1);

        label_156 = new QLabel(gridLayoutWidget_6);
        label_156->setObjectName(QStringLiteral("label_156"));
        label_156->setFont(font1);

        gridLayout_34->addWidget(label_156, 1, 6, 1, 1);

        delta_r_4 = new QLabel(gridLayoutWidget_6);
        delta_r_4->setObjectName(QStringLiteral("delta_r_4"));
        delta_r_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_r_4, 1, 1, 1, 1);

        label_152 = new QLabel(gridLayoutWidget_6);
        label_152->setObjectName(QStringLiteral("label_152"));
        label_152->setFont(font1);

        gridLayout_34->addWidget(label_152, 2, 6, 1, 1);

        line_48 = new QFrame(gridLayoutWidget_6);
        line_48->setObjectName(QStringLiteral("line_48"));
        line_48->setFrameShape(QFrame::VLine);
        line_48->setFrameShadow(QFrame::Sunken);

        gridLayout_34->addWidget(line_48, 1, 5, 2, 1);

        label_155 = new QLabel(gridLayoutWidget_6);
        label_155->setObjectName(QStringLiteral("label_155"));
        label_155->setFont(font1);

        gridLayout_34->addWidget(label_155, 1, 0, 1, 1);

        delta_c_5 = new QLabel(gridLayoutWidget_6);
        delta_c_5->setObjectName(QStringLiteral("delta_c_5"));
        delta_c_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_c_5, 2, 4, 1, 1);

        delta_r_5 = new QLabel(gridLayoutWidget_6);
        delta_r_5->setObjectName(QStringLiteral("delta_r_5"));
        delta_r_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_r_5, 1, 4, 1, 1);

        label_151 = new QLabel(gridLayoutWidget_6);
        label_151->setObjectName(QStringLiteral("label_151"));
        label_151->setFont(font1);

        gridLayout_34->addWidget(label_151, 1, 3, 1, 1);

        delta_r_6 = new QLabel(gridLayoutWidget_6);
        delta_r_6->setObjectName(QStringLiteral("delta_r_6"));
        delta_r_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_34->addWidget(delta_r_6, 1, 7, 1, 1);


        gridLayout_27->addLayout(gridLayout_34, 1, 0, 1, 1);

        gridLayout_32 = new QGridLayout();
        gridLayout_32->setSpacing(6);
        gridLayout_32->setObjectName(QStringLiteral("gridLayout_32"));
        line_49 = new QFrame(gridLayoutWidget_6);
        line_49->setObjectName(QStringLiteral("line_49"));
        line_49->setFrameShape(QFrame::VLine);
        line_49->setFrameShadow(QFrame::Sunken);

        gridLayout_32->addWidget(line_49, 1, 2, 2, 1);

        label_146 = new QLabel(gridLayoutWidget_6);
        label_146->setObjectName(QStringLiteral("label_146"));
        label_146->setFont(font1);

        gridLayout_32->addWidget(label_146, 2, 0, 1, 1);

        delta_r_2 = new QLabel(gridLayoutWidget_6);
        delta_r_2->setObjectName(QStringLiteral("delta_r_2"));
        delta_r_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_r_2, 1, 4, 1, 1);

        label_143 = new QLabel(gridLayoutWidget_6);
        label_143->setObjectName(QStringLiteral("label_143"));
        label_143->setFont(font1);

        gridLayout_32->addWidget(label_143, 2, 6, 1, 1);

        delta_c_3 = new QLabel(gridLayoutWidget_6);
        delta_c_3->setObjectName(QStringLiteral("delta_c_3"));
        delta_c_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_c_3, 2, 7, 1, 1);

        label_144 = new QLabel(gridLayoutWidget_6);
        label_144->setObjectName(QStringLiteral("label_144"));
        label_144->setFont(font1);

        gridLayout_32->addWidget(label_144, 1, 0, 1, 1);

        delta_c_1 = new QLabel(gridLayoutWidget_6);
        delta_c_1->setObjectName(QStringLiteral("delta_c_1"));
        delta_c_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_c_1, 2, 1, 1, 1);

        delta_r_1 = new QLabel(gridLayoutWidget_6);
        delta_r_1->setObjectName(QStringLiteral("delta_r_1"));
        delta_r_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_r_1, 1, 1, 1, 1);

        line_50 = new QFrame(gridLayoutWidget_6);
        line_50->setObjectName(QStringLiteral("line_50"));
        line_50->setFrameShape(QFrame::VLine);
        line_50->setFrameShadow(QFrame::Sunken);

        gridLayout_32->addWidget(line_50, 1, 5, 2, 1);

        label_142 = new QLabel(gridLayoutWidget_6);
        label_142->setObjectName(QStringLiteral("label_142"));
        label_142->setFont(font1);

        gridLayout_32->addWidget(label_142, 1, 3, 1, 1);

        label_145 = new QLabel(gridLayoutWidget_6);
        label_145->setObjectName(QStringLiteral("label_145"));
        label_145->setFont(font1);

        gridLayout_32->addWidget(label_145, 1, 6, 1, 1);

        delta_r_3 = new QLabel(gridLayoutWidget_6);
        delta_r_3->setObjectName(QStringLiteral("delta_r_3"));
        delta_r_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_r_3, 1, 7, 1, 1);

        delta_c_2 = new QLabel(gridLayoutWidget_6);
        delta_c_2->setObjectName(QStringLiteral("delta_c_2"));
        delta_c_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(delta_c_2, 2, 4, 1, 1);

        label_147 = new QLabel(gridLayoutWidget_6);
        label_147->setObjectName(QStringLiteral("label_147"));
        label_147->setFont(font1);

        gridLayout_32->addWidget(label_147, 2, 3, 1, 1);


        gridLayout_27->addLayout(gridLayout_32, 0, 0, 1, 1);

        delta_average = new QLabel(tab_6);
        delta_average->setObjectName(QStringLiteral("delta_average"));
        delta_average->setGeometry(QRect(560, 490, 91, 20));
        delta_average->setAlignment(Qt::AlignCenter);
        label_81 = new QLabel(tab_6);
        label_81->setObjectName(QStringLiteral("label_81"));
        label_81->setGeometry(QRect(530, 460, 151, 20));
        label_81->setAlignment(Qt::AlignCenter);
        label_89 = new QLabel(tab_6);
        label_89->setObjectName(QStringLiteral("label_89"));
        label_89->setGeometry(QRect(530, 490, 31, 21));
        QFont font3;
        font3.setFamily(QStringLiteral("Ubuntu Condensed"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        label_89->setFont(font3);
        label_89->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_91 = new QLabel(tab_6);
        label_91->setObjectName(QStringLiteral("label_91"));
        label_91->setGeometry(QRect(650, 490, 31, 21));
        label_91->setFont(font3);
        label_77 = new QLabel(tab_6);
        label_77->setObjectName(QStringLiteral("label_77"));
        label_77->setGeometry(QRect(10, 450, 491, 20));
        label_77->setAlignment(Qt::AlignCenter);
        label_184 = new QLabel(tab_6);
        label_184->setObjectName(QStringLiteral("label_184"));
        label_184->setGeometry(QRect(530, 550, 31, 21));
        label_184->setFont(font3);
        label_184->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_185 = new QLabel(tab_6);
        label_185->setObjectName(QStringLiteral("label_185"));
        label_185->setGeometry(QRect(530, 520, 151, 20));
        label_185->setAlignment(Qt::AlignCenter);
        delta_average_2 = new QLabel(tab_6);
        delta_average_2->setObjectName(QStringLiteral("delta_average_2"));
        delta_average_2->setGeometry(QRect(560, 550, 91, 20));
        delta_average_2->setAlignment(Qt::AlignCenter);
        label_186 = new QLabel(tab_6);
        label_186->setObjectName(QStringLiteral("label_186"));
        label_186->setGeometry(QRect(650, 550, 31, 21));
        label_186->setFont(font3);
        tabWidget_2->addTab(tab_6, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QStringLiteral("tab_8"));
        gridLayoutWidget_5 = new QWidget(tab_8);
        gridLayoutWidget_5->setObjectName(QStringLiteral("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(10, 0, 381, 141));
        gridLayout_18 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_18->setSpacing(6);
        gridLayout_18->setContentsMargins(11, 11, 11, 11);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        gridLayout_18->setContentsMargins(0, 0, 0, 0);
        gridLayout_30 = new QGridLayout();
        gridLayout_30->setSpacing(6);
        gridLayout_30->setObjectName(QStringLiteral("gridLayout_30"));
        calib_raw_r_3 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_3->setObjectName(QStringLiteral("calib_raw_r_3"));
        calib_raw_r_3->setSegmentStyle(QLCDNumber::Filled);
        calib_raw_r_3->setProperty("intValue", QVariant(0));

        gridLayout_30->addWidget(calib_raw_r_3, 1, 7, 1, 1);

        label_124 = new QLabel(gridLayoutWidget_5);
        label_124->setObjectName(QStringLiteral("label_124"));
        label_124->setFont(font1);

        gridLayout_30->addWidget(label_124, 1, 6, 1, 1);

        calib_raw_c_3 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_3->setObjectName(QStringLiteral("calib_raw_c_3"));

        gridLayout_30->addWidget(calib_raw_c_3, 2, 7, 1, 1);

        calib_raw_c_1 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_1->setObjectName(QStringLiteral("calib_raw_c_1"));

        gridLayout_30->addWidget(calib_raw_c_1, 2, 1, 1, 1);

        label_125 = new QLabel(gridLayoutWidget_5);
        label_125->setObjectName(QStringLiteral("label_125"));
        label_125->setFont(font1);

        gridLayout_30->addWidget(label_125, 2, 0, 1, 1);

        label_126 = new QLabel(gridLayoutWidget_5);
        label_126->setObjectName(QStringLiteral("label_126"));
        label_126->setFont(font1);

        gridLayout_30->addWidget(label_126, 1, 0, 1, 1);

        label_127 = new QLabel(gridLayoutWidget_5);
        label_127->setObjectName(QStringLiteral("label_127"));
        label_127->setFont(font1);

        gridLayout_30->addWidget(label_127, 2, 3, 1, 1);

        label_128 = new QLabel(gridLayoutWidget_5);
        label_128->setObjectName(QStringLiteral("label_128"));
        label_128->setFont(font1);

        gridLayout_30->addWidget(label_128, 2, 6, 1, 1);

        line_39 = new QFrame(gridLayoutWidget_5);
        line_39->setObjectName(QStringLiteral("line_39"));
        line_39->setFrameShape(QFrame::VLine);
        line_39->setFrameShadow(QFrame::Sunken);

        gridLayout_30->addWidget(line_39, 1, 2, 2, 1);

        calib_raw_r_1 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_1->setObjectName(QStringLiteral("calib_raw_r_1"));

        gridLayout_30->addWidget(calib_raw_r_1, 1, 1, 1, 1);

        calib_raw_c_2 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_2->setObjectName(QStringLiteral("calib_raw_c_2"));

        gridLayout_30->addWidget(calib_raw_c_2, 2, 4, 1, 1);

        line_40 = new QFrame(gridLayoutWidget_5);
        line_40->setObjectName(QStringLiteral("line_40"));
        line_40->setFrameShape(QFrame::VLine);
        line_40->setFrameShadow(QFrame::Sunken);

        gridLayout_30->addWidget(line_40, 1, 5, 2, 1);

        calib_raw_r_2 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_2->setObjectName(QStringLiteral("calib_raw_r_2"));
        calib_raw_r_2->setFont(font2);
        calib_raw_r_2->setProperty("intValue", QVariant(0));

        gridLayout_30->addWidget(calib_raw_r_2, 1, 4, 1, 1);

        label_129 = new QLabel(gridLayoutWidget_5);
        label_129->setObjectName(QStringLiteral("label_129"));
        label_129->setFont(font1);

        gridLayout_30->addWidget(label_129, 1, 3, 1, 1);


        gridLayout_18->addLayout(gridLayout_30, 1, 0, 1, 1);

        label_100 = new QLabel(gridLayoutWidget_5);
        label_100->setObjectName(QStringLiteral("label_100"));
        label_100->setAlignment(Qt::AlignCenter);

        gridLayout_18->addWidget(label_100, 0, 0, 1, 1);

        gridLayout_28 = new QGridLayout();
        gridLayout_28->setSpacing(6);
        gridLayout_28->setObjectName(QStringLiteral("gridLayout_28"));
        label_137 = new QLabel(gridLayoutWidget_5);
        label_137->setObjectName(QStringLiteral("label_137"));
        label_137->setFont(font1);

        gridLayout_28->addWidget(label_137, 2, 6, 1, 1);

        calib_raw_r_6 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_6->setObjectName(QStringLiteral("calib_raw_r_6"));
        calib_raw_r_6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_28->addWidget(calib_raw_r_6, 1, 7, 1, 1);

        label_148 = new QLabel(gridLayoutWidget_5);
        label_148->setObjectName(QStringLiteral("label_148"));
        label_148->setFont(font1);

        gridLayout_28->addWidget(label_148, 1, 3, 1, 1);

        label_161 = new QLabel(gridLayoutWidget_5);
        label_161->setObjectName(QStringLiteral("label_161"));
        label_161->setFont(font1);

        gridLayout_28->addWidget(label_161, 1, 6, 1, 1);

        line_51 = new QFrame(gridLayoutWidget_5);
        line_51->setObjectName(QStringLiteral("line_51"));
        line_51->setFrameShape(QFrame::VLine);
        line_51->setFrameShadow(QFrame::Sunken);

        gridLayout_28->addWidget(line_51, 1, 2, 2, 1);

        line_52 = new QFrame(gridLayoutWidget_5);
        line_52->setObjectName(QStringLiteral("line_52"));
        line_52->setFrameShape(QFrame::VLine);
        line_52->setFrameShadow(QFrame::Sunken);

        gridLayout_28->addWidget(line_52, 1, 5, 2, 1);

        calib_raw_c_6 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_6->setObjectName(QStringLiteral("calib_raw_c_6"));

        gridLayout_28->addWidget(calib_raw_c_6, 2, 7, 1, 1);

        label_163 = new QLabel(gridLayoutWidget_5);
        label_163->setObjectName(QStringLiteral("label_163"));
        label_163->setFont(font1);

        gridLayout_28->addWidget(label_163, 1, 0, 1, 1);

        label_164 = new QLabel(gridLayoutWidget_5);
        label_164->setObjectName(QStringLiteral("label_164"));
        label_164->setFont(font1);

        gridLayout_28->addWidget(label_164, 2, 3, 1, 1);

        calib_raw_c_4 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_4->setObjectName(QStringLiteral("calib_raw_c_4"));

        gridLayout_28->addWidget(calib_raw_c_4, 2, 1, 1, 1);

        calib_raw_r_5 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_5->setObjectName(QStringLiteral("calib_raw_r_5"));
        calib_raw_r_5->setFont(font2);

        gridLayout_28->addWidget(calib_raw_r_5, 1, 4, 1, 1);

        calib_raw_c_5 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_c_5->setObjectName(QStringLiteral("calib_raw_c_5"));

        gridLayout_28->addWidget(calib_raw_c_5, 2, 4, 1, 1);

        label_165 = new QLabel(gridLayoutWidget_5);
        label_165->setObjectName(QStringLiteral("label_165"));
        label_165->setFont(font1);

        gridLayout_28->addWidget(label_165, 2, 0, 1, 1);

        calib_raw_r_4 = new QLCDNumber(gridLayoutWidget_5);
        calib_raw_r_4->setObjectName(QStringLiteral("calib_raw_r_4"));

        gridLayout_28->addWidget(calib_raw_r_4, 1, 1, 1, 1);


        gridLayout_18->addLayout(gridLayout_28, 2, 0, 1, 1);

        gridLayoutWidget_7 = new QWidget(tab_8);
        gridLayoutWidget_7->setObjectName(QStringLiteral("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(10, 150, 691, 142));
        gridLayout_25 = new QGridLayout(gridLayoutWidget_7);
        gridLayout_25->setSpacing(6);
        gridLayout_25->setContentsMargins(11, 11, 11, 11);
        gridLayout_25->setObjectName(QStringLiteral("gridLayout_25"));
        gridLayout_25->setContentsMargins(0, 0, 0, 0);
        captura_data_2 = new QPushButton(gridLayoutWidget_7);
        captura_data_2->setObjectName(QStringLiteral("captura_data_2"));

        gridLayout_25->addWidget(captura_data_2, 0, 0, 1, 1);

        gridLayout_36 = new QGridLayout();
        gridLayout_36->setSpacing(6);
        gridLayout_36->setObjectName(QStringLiteral("gridLayout_36"));
        calib_cap_r_1 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_1->setObjectName(QStringLiteral("calib_cap_r_1"));
        calib_cap_r_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_r_1, 1, 1, 1, 1);

        label_172 = new QLabel(gridLayoutWidget_7);
        label_172->setObjectName(QStringLiteral("label_172"));
        label_172->setFont(font1);

        gridLayout_36->addWidget(label_172, 1, 3, 1, 1);

        label_173 = new QLabel(gridLayoutWidget_7);
        label_173->setObjectName(QStringLiteral("label_173"));
        label_173->setFont(font1);

        gridLayout_36->addWidget(label_173, 2, 6, 1, 1);

        calib_cap_r_2 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_2->setObjectName(QStringLiteral("calib_cap_r_2"));
        calib_cap_r_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_r_2, 1, 4, 1, 1);

        line_55 = new QFrame(gridLayoutWidget_7);
        line_55->setObjectName(QStringLiteral("line_55"));
        line_55->setFrameShape(QFrame::VLine);
        line_55->setFrameShadow(QFrame::Sunken);

        gridLayout_36->addWidget(line_55, 1, 2, 2, 1);

        calib_cap_c_2 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_2->setObjectName(QStringLiteral("calib_cap_c_2"));
        calib_cap_c_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_c_2, 2, 4, 1, 1);

        label_174 = new QLabel(gridLayoutWidget_7);
        label_174->setObjectName(QStringLiteral("label_174"));
        label_174->setFont(font1);

        gridLayout_36->addWidget(label_174, 1, 0, 1, 1);

        label_175 = new QLabel(gridLayoutWidget_7);
        label_175->setObjectName(QStringLiteral("label_175"));
        label_175->setFont(font1);

        gridLayout_36->addWidget(label_175, 1, 6, 1, 1);

        line_56 = new QFrame(gridLayoutWidget_7);
        line_56->setObjectName(QStringLiteral("line_56"));
        line_56->setFrameShape(QFrame::VLine);
        line_56->setFrameShadow(QFrame::Sunken);

        gridLayout_36->addWidget(line_56, 1, 5, 2, 1);

        calib_cap_c_3 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_3->setObjectName(QStringLiteral("calib_cap_c_3"));
        calib_cap_c_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_c_3, 2, 7, 1, 1);

        calib_cap_r_3 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_3->setObjectName(QStringLiteral("calib_cap_r_3"));
        calib_cap_r_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_r_3, 1, 7, 1, 1);

        label_176 = new QLabel(gridLayoutWidget_7);
        label_176->setObjectName(QStringLiteral("label_176"));
        label_176->setFont(font1);

        gridLayout_36->addWidget(label_176, 2, 0, 1, 1);

        calib_cap_c_1 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_1->setObjectName(QStringLiteral("calib_cap_c_1"));
        calib_cap_c_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_36->addWidget(calib_cap_c_1, 2, 1, 1, 1);

        label_177 = new QLabel(gridLayoutWidget_7);
        label_177->setObjectName(QStringLiteral("label_177"));
        label_177->setFont(font1);

        gridLayout_36->addWidget(label_177, 2, 3, 1, 1);


        gridLayout_25->addLayout(gridLayout_36, 1, 0, 1, 1);

        gridLayout_35 = new QGridLayout();
        gridLayout_35->setSpacing(6);
        gridLayout_35->setObjectName(QStringLiteral("gridLayout_35"));
        calib_cap_r_4 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_4->setObjectName(QStringLiteral("calib_cap_r_4"));
        calib_cap_r_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_r_4, 1, 1, 1, 1);

        label_166 = new QLabel(gridLayoutWidget_7);
        label_166->setObjectName(QStringLiteral("label_166"));
        label_166->setFont(font1);

        gridLayout_35->addWidget(label_166, 1, 3, 1, 1);

        label_167 = new QLabel(gridLayoutWidget_7);
        label_167->setObjectName(QStringLiteral("label_167"));
        label_167->setFont(font1);

        gridLayout_35->addWidget(label_167, 2, 6, 1, 1);

        calib_cap_r_5 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_5->setObjectName(QStringLiteral("calib_cap_r_5"));
        calib_cap_r_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_r_5, 1, 4, 1, 1);

        line_53 = new QFrame(gridLayoutWidget_7);
        line_53->setObjectName(QStringLiteral("line_53"));
        line_53->setFrameShape(QFrame::VLine);
        line_53->setFrameShadow(QFrame::Sunken);

        gridLayout_35->addWidget(line_53, 1, 2, 2, 1);

        calib_cap_c_5 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_5->setObjectName(QStringLiteral("calib_cap_c_5"));
        calib_cap_c_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_c_5, 2, 4, 1, 1);

        label_168 = new QLabel(gridLayoutWidget_7);
        label_168->setObjectName(QStringLiteral("label_168"));
        label_168->setFont(font1);

        gridLayout_35->addWidget(label_168, 1, 0, 1, 1);

        label_169 = new QLabel(gridLayoutWidget_7);
        label_169->setObjectName(QStringLiteral("label_169"));
        label_169->setFont(font1);

        gridLayout_35->addWidget(label_169, 1, 6, 1, 1);

        line_54 = new QFrame(gridLayoutWidget_7);
        line_54->setObjectName(QStringLiteral("line_54"));
        line_54->setFrameShape(QFrame::VLine);
        line_54->setFrameShadow(QFrame::Sunken);

        gridLayout_35->addWidget(line_54, 1, 5, 2, 1);

        calib_cap_c_6 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_6->setObjectName(QStringLiteral("calib_cap_c_6"));
        calib_cap_c_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_c_6, 2, 7, 1, 1);

        calib_cap_r_6 = new QLabel(gridLayoutWidget_7);
        calib_cap_r_6->setObjectName(QStringLiteral("calib_cap_r_6"));
        calib_cap_r_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_r_6, 1, 7, 1, 1);

        label_170 = new QLabel(gridLayoutWidget_7);
        label_170->setObjectName(QStringLiteral("label_170"));
        label_170->setFont(font1);

        gridLayout_35->addWidget(label_170, 2, 0, 1, 1);

        calib_cap_c_4 = new QLabel(gridLayoutWidget_7);
        calib_cap_c_4->setObjectName(QStringLiteral("calib_cap_c_4"));
        calib_cap_c_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_35->addWidget(calib_cap_c_4, 2, 1, 1, 1);

        label_171 = new QLabel(gridLayoutWidget_7);
        label_171->setObjectName(QStringLiteral("label_171"));
        label_171->setFont(font1);

        gridLayout_35->addWidget(label_171, 2, 3, 1, 1);


        gridLayout_25->addLayout(gridLayout_35, 2, 0, 1, 1);

        groupBox = new QGroupBox(tab_8);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(409, 9, 301, 131));
        groupBox->setAlignment(Qt::AlignCenter);
        doubleSpinBox = new QDoubleSpinBox(groupBox);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(170, 30, 101, 26));
        doubleSpinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        doubleSpinBox->setValue(38);
        doubleSpinBox_3 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setGeometry(QRect(170, 90, 101, 26));
        doubleSpinBox_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        checkBox = new QCheckBox(groupBox);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(40, 30, 121, 23));
        checkBox_2 = new QCheckBox(groupBox);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(40, 90, 121, 23));
        checkBox_3 = new QCheckBox(groupBox);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(40, 60, 121, 23));
        spinBox_4 = new QSpinBox(groupBox);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setGeometry(QRect(170, 60, 101, 26));
        spinBox_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBox_4->setMaximum(400);
        spinBox_4->setSingleStep(0);
        spinBox_4->setValue(100);
        line_30 = new QFrame(tab_8);
        line_30->setObjectName(QStringLiteral("line_30"));
        line_30->setGeometry(QRect(390, 10, 20, 131));
        line_30->setFrameShape(QFrame::VLine);
        line_30->setFrameShadow(QFrame::Sunken);
        line_29 = new QFrame(tab_8);
        line_29->setObjectName(QStringLiteral("line_29"));
        line_29->setGeometry(QRect(10, 130, 381, 20));
        line_29->setFrameShape(QFrame::HLine);
        line_29->setFrameShadow(QFrame::Sunken);
        calib_message = new QLabel(tab_8);
        calib_message->setObjectName(QStringLiteral("calib_message"));
        calib_message->setGeometry(QRect(10, 290, 691, 31));
        tabWidget_4 = new QTabWidget(tab_8);
        tabWidget_4->setObjectName(QStringLiteral("tabWidget_4"));
        tabWidget_4->setGeometry(QRect(6, 319, 701, 261));
        tab_16 = new QWidget();
        tab_16->setObjectName(QStringLiteral("tab_16"));
        result_obj = new QGroupBox(tab_16);
        result_obj->setObjectName(QStringLiteral("result_obj"));
        result_obj->setGeometry(QRect(0, 0, 701, 241));
        QFont font4;
        font4.setPointSize(14);
        result_obj->setFont(font4);
        result_obj->setAlignment(Qt::AlignCenter);
        label_95 = new QLabel(result_obj);
        label_95->setObjectName(QStringLiteral("label_95"));
        label_95->setGeometry(QRect(10, 60, 221, 21));
        label_96 = new QLabel(result_obj);
        label_96->setObjectName(QStringLiteral("label_96"));
        label_96->setGeometry(QRect(10, 90, 221, 21));
        label_97 = new QLabel(result_obj);
        label_97->setObjectName(QStringLiteral("label_97"));
        label_97->setGeometry(QRect(10, 150, 191, 31));
        label_101 = new QLabel(result_obj);
        label_101->setObjectName(QStringLiteral("label_101"));
        label_101->setGeometry(QRect(10, 120, 221, 21));
        line_31 = new QFrame(result_obj);
        line_31->setObjectName(QStringLiteral("line_31"));
        line_31->setGeometry(QRect(210, 70, 20, 131));
        line_31->setFrameShape(QFrame::VLine);
        line_31->setFrameShadow(QFrame::Sunken);
        label_98 = new QLabel(result_obj);
        label_98->setObjectName(QStringLiteral("label_98"));
        label_98->setGeometry(QRect(240, 0, 151, 20));
        label_98->setAlignment(Qt::AlignCenter);
        label_102 = new QLabel(result_obj);
        label_102->setObjectName(QStringLiteral("label_102"));
        label_102->setGeometry(QRect(390, 0, 171, 20));
        label_102->setAlignment(Qt::AlignCenter);
        avg_oil = new QPushButton(result_obj);
        avg_oil->setObjectName(QStringLiteral("avg_oil"));
        avg_oil->setGeometry(QRect(580, 60, 91, 25));
        avg_oil->setAutoRepeat(false);
        avg_air = new QPushButton(result_obj);
        avg_air->setObjectName(QStringLiteral("avg_air"));
        avg_air->setGeometry(QRect(580, 90, 91, 25));
        avg_air->setLayoutDirection(Qt::LeftToRight);
        avg_w = new QPushButton(result_obj);
        avg_w->setObjectName(QStringLiteral("avg_w"));
        avg_w->setGeometry(QRect(580, 120, 91, 25));
        avg_fw = new QPushButton(result_obj);
        avg_fw->setObjectName(QStringLiteral("avg_fw"));
        avg_fw->setGeometry(QRect(580, 150, 91, 25));
        res_avg_oil = new QSpinBox(result_obj);
        res_avg_oil->setObjectName(QStringLiteral("res_avg_oil"));
        res_avg_oil->setGeometry(QRect(230, 60, 81, 26));
        res_avg_oil->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        res_avg_oil->setMinimum(10000);
        res_avg_oil->setMaximum(16000);
        res_avg_f_water = new QSpinBox(result_obj);
        res_avg_f_water->setObjectName(QStringLiteral("res_avg_f_water"));
        res_avg_f_water->setGeometry(QRect(230, 150, 81, 26));
        res_avg_f_water->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        res_avg_f_water->setMinimum(200);
        res_avg_f_water->setMaximum(8000);
        res_avg_water = new QSpinBox(result_obj);
        res_avg_water->setObjectName(QStringLiteral("res_avg_water"));
        res_avg_water->setGeometry(QRect(230, 120, 81, 26));
        res_avg_water->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        res_avg_water->setMinimum(200);
        res_avg_water->setMaximum(8000);
        res_avg_water->setValue(300);
        cap_avg_oil = new QSpinBox(result_obj);
        cap_avg_oil->setObjectName(QStringLiteral("cap_avg_oil"));
        cap_avg_oil->setGeometry(QRect(400, 60, 81, 26));
        cap_avg_oil->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cap_avg_oil->setMinimum(12000);
        cap_avg_oil->setMaximum(17000);
        res_avg_air = new QSpinBox(result_obj);
        res_avg_air->setObjectName(QStringLiteral("res_avg_air"));
        res_avg_air->setGeometry(QRect(230, 90, 81, 26));
        res_avg_air->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        res_avg_air->setMinimum(10000);
        res_avg_air->setMaximum(16000);
        cap_avg_air = new QSpinBox(result_obj);
        cap_avg_air->setObjectName(QStringLiteral("cap_avg_air"));
        cap_avg_air->setGeometry(QRect(400, 90, 81, 26));
        cap_avg_air->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cap_avg_air->setMinimum(10000);
        cap_avg_air->setMaximum(17000);
        cap_avg_water = new QSpinBox(result_obj);
        cap_avg_water->setObjectName(QStringLiteral("cap_avg_water"));
        cap_avg_water->setGeometry(QRect(400, 120, 81, 26));
        cap_avg_water->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cap_avg_water->setMinimum(5500);
        cap_avg_water->setMaximum(7500);
        cap_avg_f_water = new QSpinBox(result_obj);
        cap_avg_f_water->setObjectName(QStringLiteral("cap_avg_f_water"));
        cap_avg_f_water->setGeometry(QRect(400, 150, 81, 26));
        cap_avg_f_water->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        cap_avg_f_water->setMinimum(100);
        cap_avg_f_water->setMaximum(8000);
        label_216 = new QLabel(result_obj);
        label_216->setObjectName(QStringLiteral("label_216"));
        label_216->setGeometry(QRect(20, 30, 191, 17));
        label_217 = new QLabel(result_obj);
        label_217->setObjectName(QStringLiteral("label_217"));
        label_217->setGeometry(QRect(230, 40, 71, 20));
        label_217->setAlignment(Qt::AlignCenter);
        label_218 = new QLabel(result_obj);
        label_218->setObjectName(QStringLiteral("label_218"));
        label_218->setGeometry(QRect(320, 40, 67, 17));
        label_218->setAlignment(Qt::AlignCenter);
        label_219 = new QLabel(result_obj);
        label_219->setObjectName(QStringLiteral("label_219"));
        label_219->setGeometry(QRect(320, 60, 61, 17));
        label_219->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_220 = new QLabel(result_obj);
        label_220->setObjectName(QStringLiteral("label_220"));
        label_220->setGeometry(QRect(320, 90, 61, 17));
        label_220->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_221 = new QLabel(result_obj);
        label_221->setObjectName(QStringLiteral("label_221"));
        label_221->setGeometry(QRect(320, 150, 61, 17));
        label_221->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_222 = new QLabel(result_obj);
        label_222->setObjectName(QStringLiteral("label_222"));
        label_222->setGeometry(QRect(320, 120, 61, 17));
        label_222->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        line_32 = new QFrame(result_obj);
        line_32->setObjectName(QStringLiteral("line_32"));
        line_32->setGeometry(QRect(380, 100, 20, 81));
        line_32->setFrameShape(QFrame::VLine);
        line_32->setFrameShadow(QFrame::Sunken);
        label_223 = new QLabel(result_obj);
        label_223->setObjectName(QStringLiteral("label_223"));
        label_223->setGeometry(QRect(410, 30, 71, 20));
        label_223->setAlignment(Qt::AlignCenter);
        label_224 = new QLabel(result_obj);
        label_224->setObjectName(QStringLiteral("label_224"));
        label_224->setGeometry(QRect(490, 30, 67, 17));
        label_224->setAlignment(Qt::AlignCenter);
        label_240 = new QLabel(result_obj);
        label_240->setObjectName(QStringLiteral("label_240"));
        label_240->setGeometry(QRect(490, 150, 61, 17));
        label_240->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_241 = new QLabel(result_obj);
        label_241->setObjectName(QStringLiteral("label_241"));
        label_241->setGeometry(QRect(490, 60, 61, 17));
        label_241->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_242 = new QLabel(result_obj);
        label_242->setObjectName(QStringLiteral("label_242"));
        label_242->setGeometry(QRect(490, 120, 61, 17));
        label_242->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_243 = new QLabel(result_obj);
        label_243->setObjectName(QStringLiteral("label_243"));
        label_243->setGeometry(QRect(490, 90, 61, 17));
        label_243->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        calcule_curves = new QPushButton(result_obj);
        calcule_curves->setObjectName(QStringLiteral("calcule_curves"));
        calcule_curves->setGeometry(QRect(0, 190, 691, 31));
        label_109 = new QLabel(result_obj);
        label_109->setObjectName(QStringLiteral("label_109"));
        label_109->setGeometry(QRect(566, 30, 131, 20));
        label_109->setAlignment(Qt::AlignCenter);
        tabWidget_4->addTab(tab_16, QString());
        tab_17 = new QWidget();
        tab_17->setObjectName(QStringLiteral("tab_17"));
        widget = new QWidget(tab_17);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 30, 671, 31));
        label_108 = new QLabel(widget);
        label_108->setObjectName(QStringLiteral("label_108"));
        label_108->setGeometry(QRect(0, 0, 121, 41));
        label_108->setWordWrap(true);
        value_hwc_up = new QSpinBox(widget);
        value_hwc_up->setObjectName(QStringLiteral("value_hwc_up"));
        value_hwc_up->setGeometry(QRect(270, 0, 81, 31));
        value_hwc_up->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_hwc_up->setMinimum(0);
        value_hwc_up->setMaximum(6000);
        value_hwc_up->setValue(5000);
        value_hwc_dw = new QSpinBox(widget);
        value_hwc_dw->setObjectName(QStringLiteral("value_hwc_dw"));
        value_hwc_dw->setGeometry(QRect(510, 0, 81, 31));
        value_hwc_dw->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_hwc_dw->setMinimum(1000);
        value_hwc_dw->setMaximum(8000);
        value_hwc_dw->setValue(7500);
        percent_hwc_up = new QSpinBox(widget);
        percent_hwc_up->setObjectName(QStringLiteral("percent_hwc_up"));
        percent_hwc_up->setGeometry(QRect(140, 0, 81, 31));
        percent_hwc_up->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_hwc_up->setMinimum(80);
        percent_hwc_up->setMaximum(100);
        percent_hwc_up->setValue(100);
        label_187 = new QLabel(widget);
        label_187->setObjectName(QStringLiteral("label_187"));
        label_187->setGeometry(QRect(230, 0, 31, 31));
        label_193 = new QLabel(widget);
        label_193->setObjectName(QStringLiteral("label_193"));
        label_193->setGeometry(QRect(480, 0, 31, 31));
        percent_hwc_dw = new QSpinBox(widget);
        percent_hwc_dw->setObjectName(QStringLiteral("percent_hwc_dw"));
        percent_hwc_dw->setGeometry(QRect(390, 0, 81, 31));
        percent_hwc_dw->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_hwc_dw->setMinimum(60);
        percent_hwc_dw->setMaximum(98);
        percent_hwc_dw->setValue(90);
        line_58 = new QFrame(widget);
        line_58->setObjectName(QStringLiteral("line_58"));
        line_58->setGeometry(QRect(360, 0, 16, 31));
        line_58->setFrameShape(QFrame::VLine);
        line_58->setFrameShadow(QFrame::Sunken);
        widget_2 = new QWidget(tab_17);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setGeometry(QRect(10, 140, 671, 31));
        label_192 = new QLabel(widget_2);
        label_192->setObjectName(QStringLiteral("label_192"));
        label_192->setGeometry(QRect(0, 0, 201, 31));
        value_lwc_up = new QSpinBox(widget_2);
        value_lwc_up->setObjectName(QStringLiteral("value_lwc_up"));
        value_lwc_up->setGeometry(QRect(270, 0, 81, 31));
        value_lwc_up->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_lwc_up->setMinimum(10000);
        value_lwc_up->setMaximum(15000);
        value_lwc_up->setValue(13500);
        value_lwc_dw = new QSpinBox(widget_2);
        value_lwc_dw->setObjectName(QStringLiteral("value_lwc_dw"));
        value_lwc_dw->setGeometry(QRect(510, 0, 81, 31));
        value_lwc_dw->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_lwc_dw->setMinimum(11000);
        value_lwc_dw->setMaximum(17000);
        value_lwc_dw->setValue(15000);
        percent_lwc_up = new QSpinBox(widget_2);
        percent_lwc_up->setObjectName(QStringLiteral("percent_lwc_up"));
        percent_lwc_up->setGeometry(QRect(140, 0, 81, 31));
        percent_lwc_up->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_lwc_up->setMinimum(5);
        percent_lwc_up->setMaximum(30);
        percent_lwc_up->setValue(10);
        label_189 = new QLabel(widget_2);
        label_189->setObjectName(QStringLiteral("label_189"));
        label_189->setGeometry(QRect(230, 0, 31, 31));
        label_206 = new QLabel(widget_2);
        label_206->setObjectName(QStringLiteral("label_206"));
        label_206->setGeometry(QRect(480, 0, 31, 31));
        percent_lwc_dw = new QSpinBox(widget_2);
        percent_lwc_dw->setObjectName(QStringLiteral("percent_lwc_dw"));
        percent_lwc_dw->setGeometry(QRect(390, 0, 81, 31));
        percent_lwc_dw->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_lwc_dw->setMinimum(0);
        percent_lwc_dw->setMaximum(25);
        percent_lwc_dw->setValue(0);
        line_59 = new QFrame(widget_2);
        line_59->setObjectName(QStringLiteral("line_59"));
        line_59->setGeometry(QRect(360, 0, 16, 31));
        line_59->setFrameShape(QFrame::VLine);
        line_59->setFrameShadow(QFrame::Sunken);
        calcule_curves_hl = new QPushButton(tab_17);
        calcule_curves_hl->setObjectName(QStringLiteral("calcule_curves_hl"));
        calcule_curves_hl->setGeometry(QRect(0, 190, 691, 31));
        label_207 = new QLabel(tab_17);
        label_207->setObjectName(QStringLiteral("label_207"));
        label_207->setGeometry(QRect(150, 0, 81, 31));
        label_208 = new QLabel(tab_17);
        label_208->setObjectName(QStringLiteral("label_208"));
        label_208->setGeometry(QRect(400, 0, 81, 31));
        label_209 = new QLabel(tab_17);
        label_209->setObjectName(QStringLiteral("label_209"));
        label_209->setGeometry(QRect(280, 0, 81, 31));
        label_209->setAlignment(Qt::AlignCenter);
        label_210 = new QLabel(tab_17);
        label_210->setObjectName(QStringLiteral("label_210"));
        label_210->setGeometry(QRect(520, 0, 81, 31));
        label_210->setAlignment(Qt::AlignCenter);
        label_211 = new QLabel(tab_17);
        label_211->setObjectName(QStringLiteral("label_211"));
        label_211->setGeometry(QRect(280, 110, 81, 31));
        label_211->setAlignment(Qt::AlignCenter);
        label_212 = new QLabel(tab_17);
        label_212->setObjectName(QStringLiteral("label_212"));
        label_212->setGeometry(QRect(400, 110, 81, 31));
        label_213 = new QLabel(tab_17);
        label_213->setObjectName(QStringLiteral("label_213"));
        label_213->setGeometry(QRect(520, 110, 81, 31));
        label_213->setAlignment(Qt::AlignCenter);
        label_214 = new QLabel(tab_17);
        label_214->setObjectName(QStringLiteral("label_214"));
        label_214->setGeometry(QRect(150, 110, 81, 31));
        label_215 = new QLabel(tab_17);
        label_215->setObjectName(QStringLiteral("label_215"));
        label_215->setGeometry(QRect(0, 170, 671, 21));
        label_226 = new QLabel(tab_17);
        label_226->setObjectName(QStringLiteral("label_226"));
        label_226->setGeometry(QRect(10, 0, 121, 31));
        widget_5 = new QWidget(tab_17);
        widget_5->setObjectName(QStringLiteral("widget_5"));
        widget_5->setGeometry(QRect(10, 70, 671, 31));
        label_228 = new QLabel(widget_5);
        label_228->setObjectName(QStringLiteral("label_228"));
        label_228->setGeometry(QRect(0, 0, 121, 41));
        label_228->setWordWrap(true);
        value_hwc_up_2 = new QSpinBox(widget_5);
        value_hwc_up_2->setObjectName(QStringLiteral("value_hwc_up_2"));
        value_hwc_up_2->setGeometry(QRect(270, 0, 81, 31));
        value_hwc_up_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_hwc_up_2->setMinimum(0);
        value_hwc_up_2->setMaximum(6000);
        value_hwc_up_2->setValue(5000);
        value_hwc_dw_2 = new QSpinBox(widget_5);
        value_hwc_dw_2->setObjectName(QStringLiteral("value_hwc_dw_2"));
        value_hwc_dw_2->setGeometry(QRect(510, 0, 81, 31));
        value_hwc_dw_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_hwc_dw_2->setMinimum(1000);
        value_hwc_dw_2->setMaximum(8000);
        value_hwc_dw_2->setValue(7500);
        percent_hwc_up_2 = new QSpinBox(widget_5);
        percent_hwc_up_2->setObjectName(QStringLiteral("percent_hwc_up_2"));
        percent_hwc_up_2->setGeometry(QRect(140, 0, 81, 31));
        percent_hwc_up_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_hwc_up_2->setMinimum(80);
        percent_hwc_up_2->setMaximum(100);
        percent_hwc_up_2->setValue(100);
        label_230 = new QLabel(widget_5);
        label_230->setObjectName(QStringLiteral("label_230"));
        label_230->setGeometry(QRect(230, 0, 31, 31));
        label_271 = new QLabel(widget_5);
        label_271->setObjectName(QStringLiteral("label_271"));
        label_271->setGeometry(QRect(480, 0, 31, 31));
        percent_hwc_dw_2 = new QSpinBox(widget_5);
        percent_hwc_dw_2->setObjectName(QStringLiteral("percent_hwc_dw_2"));
        percent_hwc_dw_2->setGeometry(QRect(390, 0, 81, 31));
        percent_hwc_dw_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        percent_hwc_dw_2->setMinimum(60);
        percent_hwc_dw_2->setMaximum(98);
        percent_hwc_dw_2->setValue(90);
        line_62 = new QFrame(widget_5);
        line_62->setObjectName(QStringLiteral("line_62"));
        line_62->setGeometry(QRect(360, 0, 16, 31));
        line_62->setFrameShape(QFrame::VLine);
        line_62->setFrameShadow(QFrame::Sunken);
        tabWidget_4->addTab(tab_17, QString());
        tab_18 = new QWidget();
        tab_18->setObjectName(QStringLiteral("tab_18"));
        widget_3 = new QWidget(tab_18);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        widget_3->setGeometry(QRect(10, 30, 671, 31));
        label_200 = new QLabel(widget_3);
        label_200->setObjectName(QStringLiteral("label_200"));
        label_200->setGeometry(QRect(0, 0, 201, 31));
        value_t_phases_up = new QSpinBox(widget_3);
        value_t_phases_up->setObjectName(QStringLiteral("value_t_phases_up"));
        value_t_phases_up->setGeometry(QRect(350, 0, 81, 31));
        value_t_phases_up->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_t_phases_up->setMinimum(12000);
        value_t_phases_up->setMaximum(16000);
        value_t_phases_up->setValue(13500);
        line_60 = new QFrame(widget_3);
        line_60->setObjectName(QStringLiteral("line_60"));
        line_60->setGeometry(QRect(110, 0, 16, 31));
        line_60->setFrameShape(QFrame::VLine);
        line_60->setFrameShadow(QFrame::Sunken);
        label_227 = new QLabel(widget_3);
        label_227->setObjectName(QStringLiteral("label_227"));
        label_227->setGeometry(QRect(136, -1, 191, 31));
        widget_4 = new QWidget(tab_18);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        widget_4->setGeometry(QRect(10, 70, 671, 31));
        value_t_phases_dw = new QSpinBox(widget_4);
        value_t_phases_dw->setObjectName(QStringLiteral("value_t_phases_dw"));
        value_t_phases_dw->setGeometry(QRect(350, 0, 81, 31));
        value_t_phases_dw->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        value_t_phases_dw->setMinimum(15000);
        value_t_phases_dw->setMaximum(17000);
        value_t_phases_dw->setValue(16500);
        line_61 = new QFrame(widget_4);
        line_61->setObjectName(QStringLiteral("line_61"));
        line_61->setGeometry(QRect(110, 0, 16, 31));
        line_61->setFrameShape(QFrame::VLine);
        line_61->setFrameShadow(QFrame::Sunken);
        label_229 = new QLabel(widget_4);
        label_229->setObjectName(QStringLiteral("label_229"));
        label_229->setGeometry(QRect(136, -1, 191, 31));
        calcule_curves_TFM = new QPushButton(tab_18);
        calcule_curves_TFM->setObjectName(QStringLiteral("calcule_curves_TFM"));
        calcule_curves_TFM->setGeometry(QRect(0, 170, 691, 31));
        tabWidget_4->addTab(tab_18, QString());
        tabWidget_2->addTab(tab_8, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QStringLiteral("tab_7"));
        capacitance_curves = new QTabWidget(tab_7);
        capacitance_curves->setObjectName(QStringLiteral("capacitance_curves"));
        capacitance_curves->setGeometry(QRect(20, 20, 641, 541));
        tab_10 = new QWidget();
        tab_10->setObjectName(QStringLiteral("tab_10"));
        customplot_r = new QCustomPlot(tab_10);
        customplot_r->setObjectName(QStringLiteral("customplot_r"));
        customplot_r->setGeometry(QRect(20, 30, 591, 451));
        label_111 = new QLabel(tab_10);
        label_111->setObjectName(QStringLiteral("label_111"));
        label_111->setGeometry(QRect(20, 10, 571, 20));
        label_111->setAlignment(Qt::AlignCenter);
        capacitance_curves->addTab(tab_10, QString());
        tab_11 = new QWidget();
        tab_11->setObjectName(QStringLiteral("tab_11"));
        customplot_c = new QCustomPlot(tab_11);
        customplot_c->setObjectName(QStringLiteral("customplot_c"));
        customplot_c->setGeometry(QRect(39, 20, 571, 451));
        label_110 = new QLabel(tab_11);
        label_110->setObjectName(QStringLiteral("label_110"));
        label_110->setGeometry(QRect(36, 0, 571, 20));
        label_110->setAlignment(Qt::AlignCenter);
        capacitance_curves->addTab(tab_11, QString());
        tab_19 = new QWidget();
        tab_19->setObjectName(QStringLiteral("tab_19"));
        customplot_TFM = new QCustomPlot(tab_19);
        customplot_TFM->setObjectName(QStringLiteral("customplot_TFM"));
        customplot_TFM->setGeometry(QRect(20, 40, 591, 451));
        label_225 = new QLabel(tab_19);
        label_225->setObjectName(QStringLiteral("label_225"));
        label_225->setGeometry(QRect(20, 20, 571, 20));
        label_225->setAlignment(Qt::AlignCenter);
        capacitance_curves->addTab(tab_19, QString());
        tabWidget_2->addTab(tab_7, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QStringLiteral("tab_9"));
        groupBox_2 = new QGroupBox(tab_9);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 0, 711, 61));
        groupBox_2->setAlignment(Qt::AlignCenter);
        checkBox_4 = new QCheckBox(groupBox_2);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(300, 30, 121, 23));
        RB_ST_CO = new QCheckBox(groupBox_2);
        RB_ST_CO->setObjectName(QStringLiteral("RB_ST_CO"));
        RB_ST_CO->setGeometry(QRect(150, 30, 81, 23));
        RB_ST_CO->setCheckable(true);
        RB_ST_CO->setChecked(false);
        checkBox_5 = new QCheckBox(groupBox_2);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(440, 30, 121, 23));
        RB_TP_CO = new QCheckBox(groupBox_2);
        RB_TP_CO->setObjectName(QStringLiteral("RB_TP_CO"));
        RB_TP_CO->setGeometry(QRect(0, 30, 121, 25));
        RB_TP_CO->setChecked(false);
        groupBox_3 = new QGroupBox(tab_9);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(9, 320, 701, 261));
        label_194 = new QLabel(groupBox_3);
        label_194->setObjectName(QStringLiteral("label_194"));
        label_194->setGeometry(QRect(40, 30, 121, 31));
        LD_RE_LAM = new QLCDNumber(groupBox_3);
        LD_RE_LAM->setObjectName(QStringLiteral("LD_RE_LAM"));
        LD_RE_LAM->setGeometry(QRect(40, 60, 113, 30));
        label_195 = new QLabel(groupBox_3);
        label_195->setObjectName(QStringLiteral("label_195"));
        label_195->setGeometry(QRect(190, 30, 121, 31));
        LD_RE_TUR = new QLCDNumber(groupBox_3);
        LD_RE_TUR->setObjectName(QStringLiteral("LD_RE_TUR"));
        LD_RE_TUR->setGeometry(QRect(190, 60, 113, 30));
        label_196 = new QLabel(groupBox_3);
        label_196->setObjectName(QStringLiteral("label_196"));
        label_196->setGeometry(QRect(320, 30, 181, 31));
        LD_TL_FINAL = new QLCDNumber(groupBox_3);
        LD_TL_FINAL->setObjectName(QStringLiteral("LD_TL_FINAL"));
        LD_TL_FINAL->setGeometry(QRect(340, 60, 113, 30));
        label_197 = new QLabel(groupBox_3);
        label_197->setObjectName(QStringLiteral("label_197"));
        label_197->setGeometry(QRect(530, 30, 121, 31));
        LD_RE_16 = new QLCDNumber(groupBox_3);
        LD_RE_16->setObjectName(QStringLiteral("LD_RE_16"));
        LD_RE_16->setGeometry(QRect(530, 60, 113, 30));
        label_198 = new QLabel(groupBox_3);
        label_198->setObjectName(QStringLiteral("label_198"));
        label_198->setGeometry(QRect(40, 100, 121, 61));
        label_198->setWordWrap(true);
        LD_RE_17 = new QLCDNumber(groupBox_3);
        LD_RE_17->setObjectName(QStringLiteral("LD_RE_17"));
        LD_RE_17->setGeometry(QRect(40, 160, 113, 30));
        label_199 = new QLabel(groupBox_3);
        label_199->setObjectName(QStringLiteral("label_199"));
        label_199->setGeometry(QRect(190, 100, 111, 51));
        LD_RE_18 = new QLCDNumber(groupBox_3);
        LD_RE_18->setObjectName(QStringLiteral("LD_RE_18"));
        LD_RE_18->setGeometry(QRect(190, 160, 113, 30));
        LD_RE_20 = new QLCDNumber(groupBox_3);
        LD_RE_20->setObjectName(QStringLiteral("LD_RE_20"));
        LD_RE_20->setGeometry(QRect(530, 160, 113, 30));
        label_201 = new QLabel(groupBox_3);
        label_201->setObjectName(QStringLiteral("label_201"));
        label_201->setGeometry(QRect(530, 100, 131, 51));
        label_202 = new QLabel(groupBox_3);
        label_202->setObjectName(QStringLiteral("label_202"));
        label_202->setGeometry(QRect(10, 220, 131, 31));
        label_203 = new QLabel(groupBox_3);
        label_203->setObjectName(QStringLiteral("label_203"));
        label_203->setGeometry(QRect(120, 220, 71, 31));
        label_204 = new QLabel(groupBox_3);
        label_204->setObjectName(QStringLiteral("label_204"));
        label_204->setGeometry(QRect(460, 220, 51, 31));
        label_205 = new QLabel(groupBox_3);
        label_205->setObjectName(QStringLiteral("label_205"));
        label_205->setGeometry(QRect(300, 220, 41, 31));
        LD_RE_21 = new QLCDNumber(groupBox_3);
        LD_RE_21->setObjectName(QStringLiteral("LD_RE_21"));
        LD_RE_21->setGeometry(QRect(190, 220, 101, 30));
        LD_RE_22 = new QLCDNumber(groupBox_3);
        LD_RE_22->setObjectName(QStringLiteral("LD_RE_22"));
        LD_RE_22->setGeometry(QRect(340, 220, 113, 30));
        LD_RE_23 = new QLCDNumber(groupBox_3);
        LD_RE_23->setObjectName(QStringLiteral("LD_RE_23"));
        LD_RE_23->setGeometry(QRect(510, 220, 113, 30));
        line_63 = new QFrame(groupBox_3);
        line_63->setObjectName(QStringLiteral("line_63"));
        line_63->setGeometry(QRect(160, 60, 20, 131));
        line_63->setFrameShape(QFrame::VLine);
        line_63->setFrameShadow(QFrame::Sunken);
        line_64 = new QFrame(groupBox_3);
        line_64->setObjectName(QStringLiteral("line_64"));
        line_64->setGeometry(QRect(310, 60, 20, 131));
        line_64->setFrameShape(QFrame::VLine);
        line_64->setFrameShadow(QFrame::Sunken);
        line_65 = new QFrame(groupBox_3);
        line_65->setObjectName(QStringLiteral("line_65"));
        line_65->setGeometry(QRect(500, 60, 20, 131));
        line_65->setFrameShape(QFrame::VLine);
        line_65->setFrameShadow(QFrame::Sunken);
        line_66 = new QFrame(groupBox_3);
        line_66->setObjectName(QStringLiteral("line_66"));
        line_66->setGeometry(QRect(10, 200, 671, 20));
        line_66->setFrameShape(QFrame::HLine);
        line_66->setFrameShadow(QFrame::Sunken);
        label_190 = new QLabel(groupBox_3);
        label_190->setObjectName(QStringLiteral("label_190"));
        label_190->setGeometry(QRect(320, 111, 113, 23));
        label_190->setFont(font1);
        LD_TL_RES = new QLCDNumber(groupBox_3);
        LD_TL_RES->setObjectName(QStringLiteral("LD_TL_RES"));
        LD_TL_RES->setGeometry(QRect(390, 110, 106, 27));
        label_191 = new QLabel(groupBox_3);
        label_191->setObjectName(QStringLiteral("label_191"));
        label_191->setGeometry(QRect(320, 140, 113, 23));
        label_191->setFont(font1);
        LD_TL_CAP = new QLCDNumber(groupBox_3);
        LD_TL_CAP->setObjectName(QStringLiteral("LD_TL_CAP"));
        LD_TL_CAP->setGeometry(QRect(390, 140, 106, 27));
        functionescorrect = new QTabWidget(tab_9);
        functionescorrect->setObjectName(QStringLiteral("functionescorrect"));
        functionescorrect->setGeometry(QRect(0, 59, 711, 261));
        functionescorrect->setTabsClosable(false);
        functionescorrect->setMovable(false);
        functionescorrect->setTabBarAutoHide(false);
        tab_correct_temp = new QWidget();
        tab_correct_temp->setObjectName(QStringLiteral("tab_correct_temp"));
        tab_correct_temp->setEnabled(true);
        gridLayoutWidget_10 = new QWidget(tab_correct_temp);
        gridLayoutWidget_10->setObjectName(QStringLiteral("gridLayoutWidget_10"));
        gridLayoutWidget_10->setGeometry(QRect(10, 80, 681, 131));
        gridLayout_23 = new QGridLayout(gridLayoutWidget_10);
        gridLayout_23->setSpacing(6);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        gridLayout_23->setContentsMargins(0, 0, 0, 0);
        gridLayout_45 = new QGridLayout();
        gridLayout_45->setSpacing(6);
        gridLayout_45->setObjectName(QStringLiteral("gridLayout_45"));
        label_259 = new QLabel(gridLayoutWidget_10);
        label_259->setObjectName(QStringLiteral("label_259"));
        label_259->setFont(font1);

        gridLayout_45->addWidget(label_259, 2, 6, 1, 1);

        LD_RE_T6 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T6->setObjectName(QStringLiteral("LD_RE_T6"));
        LD_RE_T6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_45->addWidget(LD_RE_T6, 1, 7, 1, 1);

        label_260 = new QLabel(gridLayoutWidget_10);
        label_260->setObjectName(QStringLiteral("label_260"));
        label_260->setFont(font1);

        gridLayout_45->addWidget(label_260, 1, 3, 1, 1);

        label_261 = new QLabel(gridLayoutWidget_10);
        label_261->setObjectName(QStringLiteral("label_261"));
        label_261->setFont(font1);

        gridLayout_45->addWidget(label_261, 1, 6, 1, 1);

        line_79 = new QFrame(gridLayoutWidget_10);
        line_79->setObjectName(QStringLiteral("line_79"));
        line_79->setFrameShape(QFrame::VLine);
        line_79->setFrameShadow(QFrame::Sunken);

        gridLayout_45->addWidget(line_79, 1, 2, 2, 1);

        line_80 = new QFrame(gridLayoutWidget_10);
        line_80->setObjectName(QStringLiteral("line_80"));
        line_80->setFrameShape(QFrame::VLine);
        line_80->setFrameShadow(QFrame::Sunken);

        gridLayout_45->addWidget(line_80, 1, 5, 2, 1);

        LD_CA_T6 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T6->setObjectName(QStringLiteral("LD_CA_T6"));

        gridLayout_45->addWidget(LD_CA_T6, 2, 7, 1, 1);

        label_262 = new QLabel(gridLayoutWidget_10);
        label_262->setObjectName(QStringLiteral("label_262"));
        label_262->setFont(font1);

        gridLayout_45->addWidget(label_262, 1, 0, 1, 1);

        label_263 = new QLabel(gridLayoutWidget_10);
        label_263->setObjectName(QStringLiteral("label_263"));
        label_263->setFont(font1);

        gridLayout_45->addWidget(label_263, 2, 3, 1, 1);

        LD_CA_T4 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T4->setObjectName(QStringLiteral("LD_CA_T4"));

        gridLayout_45->addWidget(LD_CA_T4, 2, 1, 1, 1);

        LD_RE_T5 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T5->setObjectName(QStringLiteral("LD_RE_T5"));
        LD_RE_T5->setFont(font2);

        gridLayout_45->addWidget(LD_RE_T5, 1, 4, 1, 1);

        LD_CA_T5 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T5->setObjectName(QStringLiteral("LD_CA_T5"));

        gridLayout_45->addWidget(LD_CA_T5, 2, 4, 1, 1);

        label_264 = new QLabel(gridLayoutWidget_10);
        label_264->setObjectName(QStringLiteral("label_264"));
        label_264->setFont(font1);

        gridLayout_45->addWidget(label_264, 2, 0, 1, 1);

        LD_RE_T4 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T4->setObjectName(QStringLiteral("LD_RE_T4"));

        gridLayout_45->addWidget(LD_RE_T4, 1, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_45, 1, 0, 1, 1);

        gridLayout_46 = new QGridLayout();
        gridLayout_46->setSpacing(6);
        gridLayout_46->setObjectName(QStringLiteral("gridLayout_46"));
        LD_RE_T3 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T3->setObjectName(QStringLiteral("LD_RE_T3"));
        LD_RE_T3->setSegmentStyle(QLCDNumber::Filled);
        LD_RE_T3->setProperty("intValue", QVariant(0));

        gridLayout_46->addWidget(LD_RE_T3, 1, 7, 1, 1);

        label_265 = new QLabel(gridLayoutWidget_10);
        label_265->setObjectName(QStringLiteral("label_265"));
        label_265->setFont(font1);

        gridLayout_46->addWidget(label_265, 1, 6, 1, 1);

        LD_CA_T3 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T3->setObjectName(QStringLiteral("LD_CA_T3"));

        gridLayout_46->addWidget(LD_CA_T3, 2, 7, 1, 1);

        LD_CA_T1 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T1->setObjectName(QStringLiteral("LD_CA_T1"));

        gridLayout_46->addWidget(LD_CA_T1, 2, 1, 1, 1);

        label_266 = new QLabel(gridLayoutWidget_10);
        label_266->setObjectName(QStringLiteral("label_266"));
        label_266->setFont(font1);

        gridLayout_46->addWidget(label_266, 2, 0, 1, 1);

        label_267 = new QLabel(gridLayoutWidget_10);
        label_267->setObjectName(QStringLiteral("label_267"));
        label_267->setFont(font1);

        gridLayout_46->addWidget(label_267, 1, 0, 1, 1);

        label_268 = new QLabel(gridLayoutWidget_10);
        label_268->setObjectName(QStringLiteral("label_268"));
        label_268->setFont(font1);

        gridLayout_46->addWidget(label_268, 2, 3, 1, 1);

        label_269 = new QLabel(gridLayoutWidget_10);
        label_269->setObjectName(QStringLiteral("label_269"));
        label_269->setFont(font1);

        gridLayout_46->addWidget(label_269, 2, 6, 1, 1);

        line_81 = new QFrame(gridLayoutWidget_10);
        line_81->setObjectName(QStringLiteral("line_81"));
        line_81->setFrameShape(QFrame::VLine);
        line_81->setFrameShadow(QFrame::Sunken);

        gridLayout_46->addWidget(line_81, 1, 2, 2, 1);

        LD_RE_T1 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T1->setObjectName(QStringLiteral("LD_RE_T1"));

        gridLayout_46->addWidget(LD_RE_T1, 1, 1, 1, 1);

        LD_CA_T2 = new QLCDNumber(gridLayoutWidget_10);
        LD_CA_T2->setObjectName(QStringLiteral("LD_CA_T2"));

        gridLayout_46->addWidget(LD_CA_T2, 2, 4, 1, 1);

        line_82 = new QFrame(gridLayoutWidget_10);
        line_82->setObjectName(QStringLiteral("line_82"));
        line_82->setFrameShape(QFrame::VLine);
        line_82->setFrameShadow(QFrame::Sunken);

        gridLayout_46->addWidget(line_82, 1, 5, 2, 1);

        LD_RE_T2 = new QLCDNumber(gridLayoutWidget_10);
        LD_RE_T2->setObjectName(QStringLiteral("LD_RE_T2"));
        LD_RE_T2->setFont(font2);

        gridLayout_46->addWidget(LD_RE_T2, 1, 4, 1, 1);

        label_270 = new QLabel(gridLayoutWidget_10);
        label_270->setObjectName(QStringLiteral("label_270"));
        label_270->setFont(font1);

        gridLayout_46->addWidget(label_270, 1, 3, 1, 1);


        gridLayout_23->addLayout(gridLayout_46, 0, 0, 1, 1);

        LD_TP = new QLCDNumber(tab_correct_temp);
        LD_TP->setObjectName(QStringLiteral("LD_TP"));
        LD_TP->setGeometry(QRect(260, 30, 91, 31));
        SD_TP_F_CH = new QDoubleSpinBox(tab_correct_temp);
        SD_TP_F_CH->setObjectName(QStringLiteral("SD_TP_F_CH"));
        SD_TP_F_CH->setEnabled(false);
        SD_TP_F_CH->setGeometry(QRect(130, 30, 111, 31));
        SD_TP_F_CH->setAlignment(Qt::AlignCenter);
        SD_TP_F_CH->setDecimals(1);
        SD_TP_F_CH->setValue(24.3);
        CK_TP_F_HD = new QCheckBox(tab_correct_temp);
        CK_TP_F_HD->setObjectName(QStringLiteral("CK_TP_F_HD"));
        CK_TP_F_HD->setGeometry(QRect(0, 30, 121, 25));
        label_104 = new QLabel(tab_correct_temp);
        label_104->setObjectName(QStringLiteral("label_104"));
        label_104->setGeometry(QRect(130, 10, 111, 20));
        label_104->setAlignment(Qt::AlignCenter);
        label_105 = new QLabel(tab_correct_temp);
        label_105->setObjectName(QStringLiteral("label_105"));
        label_105->setGeometry(QRect(260, 10, 91, 20));
        label_105->setAlignment(Qt::AlignCenter);
        functionescorrect->addTab(tab_correct_temp, QString());
        tab_correct_sali = new QWidget();
        tab_correct_sali->setObjectName(QStringLiteral("tab_correct_sali"));
        gridLayoutWidget_9 = new QWidget(tab_correct_sali);
        gridLayoutWidget_9->setObjectName(QStringLiteral("gridLayoutWidget_9"));
        gridLayoutWidget_9->setGeometry(QRect(10, 90, 691, 131));
        gridLayout_22 = new QGridLayout(gridLayoutWidget_9);
        gridLayout_22->setSpacing(6);
        gridLayout_22->setContentsMargins(11, 11, 11, 11);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        gridLayout_22->setContentsMargins(0, 0, 0, 0);
        gridLayout_43 = new QGridLayout();
        gridLayout_43->setSpacing(6);
        gridLayout_43->setObjectName(QStringLiteral("gridLayout_43"));
        label_247 = new QLabel(gridLayoutWidget_9);
        label_247->setObjectName(QStringLiteral("label_247"));
        label_247->setFont(font1);

        gridLayout_43->addWidget(label_247, 2, 6, 1, 1);

        LD_RE_S6 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S6->setObjectName(QStringLiteral("LD_RE_S6"));
        LD_RE_S6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_43->addWidget(LD_RE_S6, 1, 7, 1, 1);

        label_248 = new QLabel(gridLayoutWidget_9);
        label_248->setObjectName(QStringLiteral("label_248"));
        label_248->setFont(font1);

        gridLayout_43->addWidget(label_248, 1, 3, 1, 1);

        label_249 = new QLabel(gridLayoutWidget_9);
        label_249->setObjectName(QStringLiteral("label_249"));
        label_249->setFont(font1);

        gridLayout_43->addWidget(label_249, 1, 6, 1, 1);

        line_75 = new QFrame(gridLayoutWidget_9);
        line_75->setObjectName(QStringLiteral("line_75"));
        line_75->setFrameShape(QFrame::VLine);
        line_75->setFrameShadow(QFrame::Sunken);

        gridLayout_43->addWidget(line_75, 1, 2, 2, 1);

        line_76 = new QFrame(gridLayoutWidget_9);
        line_76->setObjectName(QStringLiteral("line_76"));
        line_76->setFrameShape(QFrame::VLine);
        line_76->setFrameShadow(QFrame::Sunken);

        gridLayout_43->addWidget(line_76, 1, 5, 2, 1);

        LD_CA_S6 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S6->setObjectName(QStringLiteral("LD_CA_S6"));

        gridLayout_43->addWidget(LD_CA_S6, 2, 7, 1, 1);

        label_250 = new QLabel(gridLayoutWidget_9);
        label_250->setObjectName(QStringLiteral("label_250"));
        label_250->setFont(font1);

        gridLayout_43->addWidget(label_250, 1, 0, 1, 1);

        label_251 = new QLabel(gridLayoutWidget_9);
        label_251->setObjectName(QStringLiteral("label_251"));
        label_251->setFont(font1);

        gridLayout_43->addWidget(label_251, 2, 3, 1, 1);

        LD_CA_S4 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S4->setObjectName(QStringLiteral("LD_CA_S4"));

        gridLayout_43->addWidget(LD_CA_S4, 2, 1, 1, 1);

        LD_RE_S5 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S5->setObjectName(QStringLiteral("LD_RE_S5"));
        LD_RE_S5->setFont(font2);

        gridLayout_43->addWidget(LD_RE_S5, 1, 4, 1, 1);

        LD_CA_S5 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S5->setObjectName(QStringLiteral("LD_CA_S5"));

        gridLayout_43->addWidget(LD_CA_S5, 2, 4, 1, 1);

        label_252 = new QLabel(gridLayoutWidget_9);
        label_252->setObjectName(QStringLiteral("label_252"));
        label_252->setFont(font1);

        gridLayout_43->addWidget(label_252, 2, 0, 1, 1);

        LD_RE_S4 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S4->setObjectName(QStringLiteral("LD_RE_S4"));

        gridLayout_43->addWidget(LD_RE_S4, 1, 1, 1, 1);


        gridLayout_22->addLayout(gridLayout_43, 1, 0, 1, 1);

        gridLayout_44 = new QGridLayout();
        gridLayout_44->setSpacing(6);
        gridLayout_44->setObjectName(QStringLiteral("gridLayout_44"));
        LD_RE_S3 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S3->setObjectName(QStringLiteral("LD_RE_S3"));
        LD_RE_S3->setSegmentStyle(QLCDNumber::Filled);
        LD_RE_S3->setProperty("intValue", QVariant(0));

        gridLayout_44->addWidget(LD_RE_S3, 1, 7, 1, 1);

        label_253 = new QLabel(gridLayoutWidget_9);
        label_253->setObjectName(QStringLiteral("label_253"));
        label_253->setFont(font1);

        gridLayout_44->addWidget(label_253, 1, 6, 1, 1);

        LD_CA_S3 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S3->setObjectName(QStringLiteral("LD_CA_S3"));

        gridLayout_44->addWidget(LD_CA_S3, 2, 7, 1, 1);

        LD_CA_S1 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S1->setObjectName(QStringLiteral("LD_CA_S1"));

        gridLayout_44->addWidget(LD_CA_S1, 2, 1, 1, 1);

        label_254 = new QLabel(gridLayoutWidget_9);
        label_254->setObjectName(QStringLiteral("label_254"));
        label_254->setFont(font1);

        gridLayout_44->addWidget(label_254, 2, 0, 1, 1);

        label_255 = new QLabel(gridLayoutWidget_9);
        label_255->setObjectName(QStringLiteral("label_255"));
        label_255->setFont(font1);

        gridLayout_44->addWidget(label_255, 1, 0, 1, 1);

        label_256 = new QLabel(gridLayoutWidget_9);
        label_256->setObjectName(QStringLiteral("label_256"));
        label_256->setFont(font1);

        gridLayout_44->addWidget(label_256, 2, 3, 1, 1);

        label_257 = new QLabel(gridLayoutWidget_9);
        label_257->setObjectName(QStringLiteral("label_257"));
        label_257->setFont(font1);

        gridLayout_44->addWidget(label_257, 2, 6, 1, 1);

        line_77 = new QFrame(gridLayoutWidget_9);
        line_77->setObjectName(QStringLiteral("line_77"));
        line_77->setFrameShape(QFrame::VLine);
        line_77->setFrameShadow(QFrame::Sunken);

        gridLayout_44->addWidget(line_77, 1, 2, 2, 1);

        LD_RE_S1 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S1->setObjectName(QStringLiteral("LD_RE_S1"));

        gridLayout_44->addWidget(LD_RE_S1, 1, 1, 1, 1);

        LD_CA_S2 = new QLCDNumber(gridLayoutWidget_9);
        LD_CA_S2->setObjectName(QStringLiteral("LD_CA_S2"));

        gridLayout_44->addWidget(LD_CA_S2, 2, 4, 1, 1);

        line_78 = new QFrame(gridLayoutWidget_9);
        line_78->setObjectName(QStringLiteral("line_78"));
        line_78->setFrameShape(QFrame::VLine);
        line_78->setFrameShadow(QFrame::Sunken);

        gridLayout_44->addWidget(line_78, 1, 5, 2, 1);

        LD_RE_S2 = new QLCDNumber(gridLayoutWidget_9);
        LD_RE_S2->setObjectName(QStringLiteral("LD_RE_S2"));
        LD_RE_S2->setFont(font2);

        gridLayout_44->addWidget(LD_RE_S2, 1, 4, 1, 1);

        label_258 = new QLabel(gridLayoutWidget_9);
        label_258->setObjectName(QStringLiteral("label_258"));
        label_258->setFont(font1);

        gridLayout_44->addWidget(label_258, 1, 3, 1, 1);


        gridLayout_22->addLayout(gridLayout_44, 0, 0, 1, 1);

        line_33 = new QFrame(tab_correct_sali);
        line_33->setObjectName(QStringLiteral("line_33"));
        line_33->setGeometry(QRect(10, 30, 351, 20));
        line_33->setFrameShape(QFrame::HLine);
        line_33->setFrameShadow(QFrame::Sunken);
        SI_ST_F_CH = new QSpinBox(tab_correct_sali);
        SI_ST_F_CH->setObjectName(QStringLiteral("SI_ST_F_CH"));
        SI_ST_F_CH->setEnabled(false);
        SI_ST_F_CH->setGeometry(QRect(190, 50, 161, 28));
        SI_ST_F_CH->setMinimum(0);
        SI_ST_F_CH->setMaximum(5000);
        SI_ST_F_CH->setSingleStep(100);
        SI_ST_F_CH->setValue(250);
        CK_ST_F_HD = new QCheckBox(tab_correct_sali);
        CK_ST_F_HD->setObjectName(QStringLiteral("CK_ST_F_HD"));
        CK_ST_F_HD->setGeometry(QRect(10, 50, 171, 25));
        CK_ST_HD_1 = new QCheckBox(tab_correct_sali);
        CK_ST_HD_1->setObjectName(QStringLiteral("CK_ST_HD_1"));
        CK_ST_HD_1->setGeometry(QRect(10, 10, 171, 25));
        CK_ST_HD_1->setChecked(true);
        CK_ST_HD_1->setAutoExclusive(true);
        CK_ST_HD_6 = new QCheckBox(tab_correct_sali);
        CK_ST_HD_6->setObjectName(QStringLiteral("CK_ST_HD_6"));
        CK_ST_HD_6->setGeometry(QRect(190, 10, 161, 25));
        CK_ST_HD_6->setChecked(false);
        line_34 = new QFrame(tab_correct_sali);
        line_34->setObjectName(QStringLiteral("line_34"));
        line_34->setGeometry(QRect(380, 10, 20, 71));
        line_34->setFrameShape(QFrame::VLine);
        line_34->setFrameShadow(QFrame::Sunken);
        functionescorrect->addTab(tab_correct_sali, QString());
        tab_13 = new QWidget();
        tab_13->setObjectName(QStringLiteral("tab_13"));
        groupBox_4 = new QGroupBox(tab_13);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(0, 0, 301, 111));
        groupBox_4->setAlignment(Qt::AlignCenter);
        spin_regime = new QSpinBox(groupBox_4);
        spin_regime->setObjectName(QStringLiteral("spin_regime"));
        spin_regime->setGeometry(QRect(210, 30, 51, 26));
        spin_regime->setMinimum(10);
        spin_regime->setMaximum(30);
        spin_regime->setValue(30);
        REGIME_LABEL = new QLabel(groupBox_4);
        REGIME_LABEL->setObjectName(QStringLiteral("REGIME_LABEL"));
        REGIME_LABEL->setGeometry(QRect(170, 80, 131, 20));
        REGIME_LABEL->setAlignment(Qt::AlignCenter);
        label_106 = new QLabel(groupBox_4);
        label_106->setObjectName(QStringLiteral("label_106"));
        label_106->setGeometry(QRect(20, 30, 151, 31));
        label_107 = new QLabel(groupBox_4);
        label_107->setObjectName(QStringLiteral("label_107"));
        label_107->setGeometry(QRect(20, 80, 141, 17));
        functionescorrect->addTab(tab_13, QString());
        tab_12 = new QWidget();
        tab_12->setObjectName(QStringLiteral("tab_12"));
        gridLayoutWidget_8 = new QWidget(tab_12);
        gridLayoutWidget_8->setObjectName(QStringLiteral("gridLayoutWidget_8"));
        gridLayoutWidget_8->setGeometry(QRect(10, 10, 691, 131));
        gridLayout_21 = new QGridLayout(gridLayoutWidget_8);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        gridLayout_21->setContentsMargins(0, 0, 0, 0);
        gridLayout_42 = new QGridLayout();
        gridLayout_42->setSpacing(6);
        gridLayout_42->setObjectName(QStringLiteral("gridLayout_42"));
        label_237 = new QLabel(gridLayoutWidget_8);
        label_237->setObjectName(QStringLiteral("label_237"));
        label_237->setFont(font1);

        gridLayout_42->addWidget(label_237, 2, 6, 1, 1);

        wc_res_6 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_6->setObjectName(QStringLiteral("wc_res_6"));
        wc_res_6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_42->addWidget(wc_res_6, 1, 7, 1, 1);

        label_238 = new QLabel(gridLayoutWidget_8);
        label_238->setObjectName(QStringLiteral("label_238"));
        label_238->setFont(font1);

        gridLayout_42->addWidget(label_238, 1, 3, 1, 1);

        label_239 = new QLabel(gridLayoutWidget_8);
        label_239->setObjectName(QStringLiteral("label_239"));
        label_239->setFont(font1);

        gridLayout_42->addWidget(label_239, 1, 6, 1, 1);

        line_73 = new QFrame(gridLayoutWidget_8);
        line_73->setObjectName(QStringLiteral("line_73"));
        line_73->setFrameShape(QFrame::VLine);
        line_73->setFrameShadow(QFrame::Sunken);

        gridLayout_42->addWidget(line_73, 1, 2, 2, 1);

        line_74 = new QFrame(gridLayoutWidget_8);
        line_74->setObjectName(QStringLiteral("line_74"));
        line_74->setFrameShape(QFrame::VLine);
        line_74->setFrameShadow(QFrame::Sunken);

        gridLayout_42->addWidget(line_74, 1, 5, 2, 1);

        wc_cap_6 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_6->setObjectName(QStringLiteral("wc_cap_6"));

        gridLayout_42->addWidget(wc_cap_6, 2, 7, 1, 1);

        label_244 = new QLabel(gridLayoutWidget_8);
        label_244->setObjectName(QStringLiteral("label_244"));
        label_244->setFont(font1);

        gridLayout_42->addWidget(label_244, 1, 0, 1, 1);

        label_245 = new QLabel(gridLayoutWidget_8);
        label_245->setObjectName(QStringLiteral("label_245"));
        label_245->setFont(font1);

        gridLayout_42->addWidget(label_245, 2, 3, 1, 1);

        wc_cap_4 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_4->setObjectName(QStringLiteral("wc_cap_4"));

        gridLayout_42->addWidget(wc_cap_4, 2, 1, 1, 1);

        wc_res_5 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_5->setObjectName(QStringLiteral("wc_res_5"));
        wc_res_5->setFont(font2);

        gridLayout_42->addWidget(wc_res_5, 1, 4, 1, 1);

        wc_cap_5 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_5->setObjectName(QStringLiteral("wc_cap_5"));

        gridLayout_42->addWidget(wc_cap_5, 2, 4, 1, 1);

        label_246 = new QLabel(gridLayoutWidget_8);
        label_246->setObjectName(QStringLiteral("label_246"));
        label_246->setFont(font1);

        gridLayout_42->addWidget(label_246, 2, 0, 1, 1);

        wc_res_4 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_4->setObjectName(QStringLiteral("wc_res_4"));

        gridLayout_42->addWidget(wc_res_4, 1, 1, 1, 1);


        gridLayout_21->addLayout(gridLayout_42, 1, 0, 1, 1);

        gridLayout_41 = new QGridLayout();
        gridLayout_41->setSpacing(6);
        gridLayout_41->setObjectName(QStringLiteral("gridLayout_41"));
        wc_res_3 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_3->setObjectName(QStringLiteral("wc_res_3"));
        wc_res_3->setSegmentStyle(QLCDNumber::Filled);
        wc_res_3->setProperty("intValue", QVariant(0));

        gridLayout_41->addWidget(wc_res_3, 1, 7, 1, 1);

        label_231 = new QLabel(gridLayoutWidget_8);
        label_231->setObjectName(QStringLiteral("label_231"));
        label_231->setFont(font1);

        gridLayout_41->addWidget(label_231, 1, 6, 1, 1);

        wc_cap_3 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_3->setObjectName(QStringLiteral("wc_cap_3"));

        gridLayout_41->addWidget(wc_cap_3, 2, 7, 1, 1);

        wc_cap_1 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_1->setObjectName(QStringLiteral("wc_cap_1"));

        gridLayout_41->addWidget(wc_cap_1, 2, 1, 1, 1);

        label_232 = new QLabel(gridLayoutWidget_8);
        label_232->setObjectName(QStringLiteral("label_232"));
        label_232->setFont(font1);

        gridLayout_41->addWidget(label_232, 2, 0, 1, 1);

        label_233 = new QLabel(gridLayoutWidget_8);
        label_233->setObjectName(QStringLiteral("label_233"));
        label_233->setFont(font1);

        gridLayout_41->addWidget(label_233, 1, 0, 1, 1);

        label_234 = new QLabel(gridLayoutWidget_8);
        label_234->setObjectName(QStringLiteral("label_234"));
        label_234->setFont(font1);

        gridLayout_41->addWidget(label_234, 2, 3, 1, 1);

        label_235 = new QLabel(gridLayoutWidget_8);
        label_235->setObjectName(QStringLiteral("label_235"));
        label_235->setFont(font1);

        gridLayout_41->addWidget(label_235, 2, 6, 1, 1);

        line_71 = new QFrame(gridLayoutWidget_8);
        line_71->setObjectName(QStringLiteral("line_71"));
        line_71->setFrameShape(QFrame::VLine);
        line_71->setFrameShadow(QFrame::Sunken);

        gridLayout_41->addWidget(line_71, 1, 2, 2, 1);

        wc_res_1 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_1->setObjectName(QStringLiteral("wc_res_1"));

        gridLayout_41->addWidget(wc_res_1, 1, 1, 1, 1);

        wc_cap_2 = new QLCDNumber(gridLayoutWidget_8);
        wc_cap_2->setObjectName(QStringLiteral("wc_cap_2"));

        gridLayout_41->addWidget(wc_cap_2, 2, 4, 1, 1);

        line_72 = new QFrame(gridLayoutWidget_8);
        line_72->setObjectName(QStringLiteral("line_72"));
        line_72->setFrameShape(QFrame::VLine);
        line_72->setFrameShadow(QFrame::Sunken);

        gridLayout_41->addWidget(line_72, 1, 5, 2, 1);

        wc_res_2 = new QLCDNumber(gridLayoutWidget_8);
        wc_res_2->setObjectName(QStringLiteral("wc_res_2"));
        wc_res_2->setFont(font2);

        gridLayout_41->addWidget(wc_res_2, 1, 4, 1, 1);

        label_236 = new QLabel(gridLayoutWidget_8);
        label_236->setObjectName(QStringLiteral("label_236"));
        label_236->setFont(font1);

        gridLayout_41->addWidget(label_236, 1, 3, 1, 1);


        gridLayout_21->addLayout(gridLayout_41, 0, 0, 1, 1);

        label_75 = new QLabel(tab_12);
        label_75->setObjectName(QStringLiteral("label_75"));
        label_75->setGeometry(QRect(13, 150, 113, 23));
        label_75->setFont(font1);
        label_72 = new QLabel(tab_12);
        label_72->setObjectName(QStringLiteral("label_72"));
        label_72->setGeometry(QRect(240, 180, 113, 23));
        label_72->setFont(font1);
        label_74 = new QLabel(tab_12);
        label_74->setObjectName(QStringLiteral("label_74"));
        label_74->setGeometry(QRect(13, 179, 113, 23));
        label_74->setFont(font1);
        label_71 = new QLabel(tab_12);
        label_71->setObjectName(QStringLiteral("label_71"));
        label_71->setGeometry(QRect(240, 151, 113, 23));
        label_71->setFont(font1);
        LD_LM_CA = new QLCDNumber(tab_12);
        LD_LM_CA->setObjectName(QStringLiteral("LD_LM_CA"));
        LD_LM_CA->setGeometry(QRect(120, 150, 106, 27));
        LD_LM_RE = new QLCDNumber(tab_12);
        LD_LM_RE->setObjectName(QStringLiteral("LD_LM_RE"));
        LD_LM_RE->setGeometry(QRect(120, 180, 106, 27));
        LD_TU_CA = new QLCDNumber(tab_12);
        LD_TU_CA->setObjectName(QStringLiteral("LD_TU_CA"));
        LD_TU_CA->setGeometry(QRect(360, 150, 106, 27));
        LD_TU_RE = new QLCDNumber(tab_12);
        LD_TU_RE->setObjectName(QStringLiteral("LD_TU_RE"));
        LD_TU_RE->setGeometry(QRect(360, 180, 106, 27));
        label_188 = new QLabel(tab_12);
        label_188->setObjectName(QStringLiteral("label_188"));
        label_188->setGeometry(QRect(486, 150, 211, 20));
        LD_SEND_TYPE = new QLCDNumber(tab_12);
        LD_SEND_TYPE->setObjectName(QStringLiteral("LD_SEND_TYPE"));
        LD_SEND_TYPE->setGeometry(QRect(590, 150, 113, 30));
        LD_SEND_VALUE = new QLCDNumber(tab_12);
        LD_SEND_VALUE->setObjectName(QStringLiteral("LD_SEND_VALUE"));
        LD_SEND_VALUE->setGeometry(QRect(590, 180, 113, 30));
        functionescorrect->addTab(tab_12, QString());
        tabWidget_2->addTab(tab_9, QString());
        tabWidget->addTab(tab_5, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        horizontalLayoutWidget = new QWidget(tab_3);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 10, 711, 617));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        label_29 = new QLabel(horizontalLayoutWidget);
        label_29->setObjectName(QStringLiteral("label_29"));

        gridLayout_11->addWidget(label_29, 1, 0, 1, 1);

        CK_TP_F_HDx = new QCheckBox(horizontalLayoutWidget);
        CK_TP_F_HDx->setObjectName(QStringLiteral("CK_TP_F_HDx"));

        gridLayout_11->addWidget(CK_TP_F_HDx, 2, 0, 1, 1);

        label_45 = new QLabel(horizontalLayoutWidget);
        label_45->setObjectName(QStringLiteral("label_45"));

        gridLayout_11->addWidget(label_45, 2, 2, 1, 1);

        label_30 = new QLabel(horizontalLayoutWidget);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_30, 0, 0, 1, 3);

        CB_TP_SL = new QComboBox(horizontalLayoutWidget);
        CB_TP_SL->setObjectName(QStringLiteral("CB_TP_SL"));
        CB_TP_SL->setEnabled(false);

        gridLayout_11->addWidget(CB_TP_SL, 1, 1, 1, 2);

        SD_TP_F_CHx = new QDoubleSpinBox(horizontalLayoutWidget);
        SD_TP_F_CHx->setObjectName(QStringLiteral("SD_TP_F_CHx"));
        SD_TP_F_CHx->setEnabled(false);
        SD_TP_F_CHx->setAlignment(Qt::AlignCenter);
        SD_TP_F_CHx->setDecimals(1);
        SD_TP_F_CHx->setValue(24.3);

        gridLayout_11->addWidget(SD_TP_F_CHx, 2, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_11);

        line_5 = new QFrame(horizontalLayoutWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_14 = new QLabel(horizontalLayoutWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setMaximumSize(QSize(153, 16777215));

        gridLayout_2->addWidget(label_14, 1, 0, 1, 1);

        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label, 0, 0, 1, 3);

        SD_FL_F_CH = new QDoubleSpinBox(horizontalLayoutWidget);
        SD_FL_F_CH->setObjectName(QStringLiteral("SD_FL_F_CH"));
        SD_FL_F_CH->setValue(10);

        gridLayout_2->addWidget(SD_FL_F_CH, 1, 1, 1, 1);

        label_35 = new QLabel(horizontalLayoutWidget);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_35, 1, 2, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        line_6 = new QFrame(horizontalLayoutWidget);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_6);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        label_82 = new QLabel(horizontalLayoutWidget);
        label_82->setObjectName(QStringLiteral("label_82"));
        label_82->setMaximumSize(QSize(250, 50));

        gridLayout_17->addWidget(label_82, 1, 0, 1, 1);

        label_85 = new QLabel(horizontalLayoutWidget);
        label_85->setObjectName(QStringLiteral("label_85"));
        label_85->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(label_85, 0, 0, 1, 3);

        label_86 = new QLabel(horizontalLayoutWidget);
        label_86->setObjectName(QStringLiteral("label_86"));
        label_86->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_17->addWidget(label_86, 1, 2, 1, 1);

        SI_PO_CH = new QSpinBox(horizontalLayoutWidget);
        SI_PO_CH->setObjectName(QStringLiteral("SI_PO_CH"));
        SI_PO_CH->setMaximum(100);
        SI_PO_CH->setValue(10);

        gridLayout_17->addWidget(SI_PO_CH, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_17);

        line = new QFrame(horizontalLayoutWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        gridLayout_14 = new QGridLayout();
        gridLayout_14->setSpacing(6);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        label_15 = new QLabel(horizontalLayoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_15, 0, 0, 1, 2);

        CK_ST_F_HDx = new QCheckBox(horizontalLayoutWidget);
        CK_ST_F_HDx->setObjectName(QStringLiteral("CK_ST_F_HDx"));

        gridLayout_14->addWidget(CK_ST_F_HDx, 3, 0, 1, 1);

        SI_ST_F_CHx = new QSpinBox(horizontalLayoutWidget);
        SI_ST_F_CHx->setObjectName(QStringLiteral("SI_ST_F_CHx"));
        SI_ST_F_CHx->setEnabled(false);
        SI_ST_F_CHx->setMinimum(0);
        SI_ST_F_CHx->setMaximum(5000);
        SI_ST_F_CHx->setSingleStep(100);
        SI_ST_F_CHx->setValue(250);

        gridLayout_14->addWidget(SI_ST_F_CHx, 3, 1, 1, 1);

        CK_ST_HD_1x = new QCheckBox(horizontalLayoutWidget);
        CK_ST_HD_1x->setObjectName(QStringLiteral("CK_ST_HD_1x"));
        CK_ST_HD_1x->setChecked(true);
        CK_ST_HD_1x->setAutoExclusive(true);

        gridLayout_14->addWidget(CK_ST_HD_1x, 1, 0, 1, 1);

        CK_ST_HD_6x = new QCheckBox(horizontalLayoutWidget);
        CK_ST_HD_6x->setObjectName(QStringLiteral("CK_ST_HD_6x"));
        CK_ST_HD_6x->setChecked(false);

        gridLayout_14->addWidget(CK_ST_HD_6x, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_14);

        line_2 = new QFrame(horizontalLayoutWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        PB_RE_VW = new QProgressBar(horizontalLayoutWidget);
        PB_RE_VW->setObjectName(QStringLiteral("PB_RE_VW"));
        PB_RE_VW->setValue(0);

        gridLayout_7->addWidget(PB_RE_VW, 1, 1, 1, 1);

        label_27 = new QLabel(horizontalLayoutWidget);
        label_27->setObjectName(QStringLiteral("label_27"));

        gridLayout_7->addWidget(label_27, 2, 0, 1, 1);

        PB_CA_VW = new QProgressBar(horizontalLayoutWidget);
        PB_CA_VW->setObjectName(QStringLiteral("PB_CA_VW"));
        PB_CA_VW->setValue(0);

        gridLayout_7->addWidget(PB_CA_VW, 2, 1, 1, 1);

        label_25 = new QLabel(horizontalLayoutWidget);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_25, 0, 0, 1, 2);

        label_23 = new QLabel(horizontalLayoutWidget);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_7->addWidget(label_23, 1, 0, 1, 1);


        verticalLayout->addLayout(gridLayout_7);

        line_3 = new QFrame(horizontalLayoutWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        label_34 = new QLabel(horizontalLayoutWidget);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_34, 1, 2, 1, 1);

        SI_REST_L_CH = new QSpinBox(horizontalLayoutWidget);
        SI_REST_L_CH->setObjectName(QStringLiteral("SI_REST_L_CH"));
        SI_REST_L_CH->setMaximum(16000);
        SI_REST_L_CH->setSingleStep(100);
        SI_REST_L_CH->setValue(8000);

        gridLayout_10->addWidget(SI_REST_L_CH, 2, 2, 1, 1);

        SI_CATP_L_CH = new QSpinBox(horizontalLayoutWidget);
        SI_CATP_L_CH->setObjectName(QStringLiteral("SI_CATP_L_CH"));
        SI_CATP_L_CH->setMaximum(16000);
        SI_CATP_L_CH->setSingleStep(100);
        SI_CATP_L_CH->setValue(6500);

        gridLayout_10->addWidget(SI_CATP_L_CH, 3, 1, 1, 1);

        label_26 = new QLabel(horizontalLayoutWidget);
        label_26->setObjectName(QStringLiteral("label_26"));

        gridLayout_10->addWidget(label_26, 2, 0, 1, 1);

        label_28 = new QLabel(horizontalLayoutWidget);
        label_28->setObjectName(QStringLiteral("label_28"));

        gridLayout_10->addWidget(label_28, 3, 0, 1, 1);

        SI_RETP_L_CH = new QSpinBox(horizontalLayoutWidget);
        SI_RETP_L_CH->setObjectName(QStringLiteral("SI_RETP_L_CH"));
        SI_RETP_L_CH->setMaximum(16000);
        SI_RETP_L_CH->setSingleStep(100);
        SI_RETP_L_CH->setValue(8000);

        gridLayout_10->addWidget(SI_RETP_L_CH, 2, 1, 1, 1);

        label_31 = new QLabel(horizontalLayoutWidget);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_31, 1, 1, 1, 1);

        SI_CAST_L_CH = new QSpinBox(horizontalLayoutWidget);
        SI_CAST_L_CH->setObjectName(QStringLiteral("SI_CAST_L_CH"));
        SI_CAST_L_CH->setMaximum(16000);
        SI_CAST_L_CH->setSingleStep(100);
        SI_CAST_L_CH->setValue(6500);

        gridLayout_10->addWidget(SI_CAST_L_CH, 3, 2, 1, 1);

        label_24 = new QLabel(horizontalLayoutWidget);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setFont(font);
        label_24->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_24, 0, 0, 1, 3);


        verticalLayout->addLayout(gridLayout_10);


        horizontalLayout->addLayout(verticalLayout);

        line_4 = new QFrame(horizontalLayoutWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line_4);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_17 = new QLabel(horizontalLayoutWidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_8->addWidget(label_17, 1, 0, 1, 1);

        line_11 = new QFrame(horizontalLayoutWidget);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);

        gridLayout_8->addWidget(line_11, 7, 0, 1, 4);

        label_18 = new QLabel(horizontalLayoutWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout_8->addWidget(label_18, 3, 0, 1, 1);

        label_37 = new QLabel(horizontalLayoutWidget);
        label_37->setObjectName(QStringLiteral("label_37"));

        gridLayout_8->addWidget(label_37, 2, 3, 1, 1);

        LD_TPx = new QLCDNumber(horizontalLayoutWidget);
        LD_TPx->setObjectName(QStringLiteral("LD_TPx"));
        LD_TPx->setMaximumSize(QSize(100, 100));
        LD_TPx->setDigitCount(7);

        gridLayout_8->addWidget(LD_TPx, 2, 1, 1, 1);

        LD_ST = new QLCDNumber(horizontalLayoutWidget);
        LD_ST->setObjectName(QStringLiteral("LD_ST"));
        LD_ST->setMaximumSize(QSize(100, 100));
        LD_ST->setDigitCount(7);

        gridLayout_8->addWidget(LD_ST, 3, 1, 1, 1);

        LD_PS = new QLCDNumber(horizontalLayoutWidget);
        LD_PS->setObjectName(QStringLiteral("LD_PS"));
        LD_PS->setMinimumSize(QSize(0, 0));
        LD_PS->setMaximumSize(QSize(100, 100));
        LD_PS->setFont(font1);
        LD_PS->setDigitCount(7);

        gridLayout_8->addWidget(LD_PS, 1, 1, 1, 1);

        label_36 = new QLabel(horizontalLayoutWidget);
        label_36->setObjectName(QStringLiteral("label_36"));

        gridLayout_8->addWidget(label_36, 1, 3, 1, 1);

        label_42 = new QLabel(horizontalLayoutWidget);
        label_42->setObjectName(QStringLiteral("label_42"));

        gridLayout_8->addWidget(label_42, 3, 3, 1, 1);

        label_16 = new QLabel(horizontalLayoutWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_8->addWidget(label_16, 2, 0, 1, 1);

        RB_TP_COx = new QRadioButton(horizontalLayoutWidget);
        RB_TP_COx->setObjectName(QStringLiteral("RB_TP_COx"));
        RB_TP_COx->setChecked(true);
        RB_TP_COx->setAutoExclusive(false);

        gridLayout_8->addWidget(RB_TP_COx, 8, 0, 1, 4);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_8->addItem(verticalSpacer_4, 5, 0, 1, 1);

        label_19 = new QLabel(horizontalLayoutWidget);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_8->addWidget(label_19, 6, 0, 1, 4);

        RB_ST_COx = new QRadioButton(horizontalLayoutWidget);
        RB_ST_COx->setObjectName(QStringLiteral("RB_ST_COx"));
        RB_ST_COx->setChecked(true);
        RB_ST_COx->setAutoExclusive(false);

        gridLayout_8->addWidget(RB_ST_COx, 9, 0, 1, 4);

        RB_FL_CO = new QRadioButton(horizontalLayoutWidget);
        RB_FL_CO->setObjectName(QStringLiteral("RB_FL_CO"));
        RB_FL_CO->setChecked(true);
        RB_FL_CO->setAutoExclusive(false);

        gridLayout_8->addWidget(RB_FL_CO, 10, 0, 1, 4);

        SINF = new QPushButton(horizontalLayoutWidget);
        SINF->setObjectName(QStringLiteral("SINF"));

        gridLayout_8->addWidget(SINF, 0, 0, 1, 4);


        horizontalLayout->addLayout(gridLayout_8);

        tabWidget->addTab(tab_3, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayoutWidget = new QWidget(tab);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 731, 621));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        gridLayout_12 = new QGridLayout();
        gridLayout_12->setSpacing(6);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        LD_TU_CAX = new QLCDNumber(verticalLayoutWidget);
        LD_TU_CAX->setObjectName(QStringLiteral("LD_TU_CAX"));

        gridLayout_12->addWidget(LD_TU_CAX, 1, 4, 1, 1);

        LD_LM_REX = new QLCDNumber(verticalLayoutWidget);
        LD_LM_REX->setObjectName(QStringLiteral("LD_LM_REX"));

        gridLayout_12->addWidget(LD_LM_REX, 2, 1, 1, 1);

        L_LM_CA = new QLCDNumber(verticalLayoutWidget);
        L_LM_CA->setObjectName(QStringLiteral("L_LM_CA"));

        gridLayout_12->addWidget(L_LM_CA, 1, 1, 1, 1);

        LD_TU_REX = new QLCDNumber(verticalLayoutWidget);
        LD_TU_REX->setObjectName(QStringLiteral("LD_TU_REX"));

        gridLayout_12->addWidget(LD_TU_REX, 2, 4, 1, 1);

        line_24 = new QFrame(verticalLayoutWidget);
        line_24->setObjectName(QStringLiteral("line_24"));
        line_24->setFrameShape(QFrame::VLine);
        line_24->setFrameShadow(QFrame::Sunken);

        gridLayout_12->addWidget(line_24, 1, 2, 3, 1);

        line_26 = new QFrame(verticalLayoutWidget);
        line_26->setObjectName(QStringLiteral("line_26"));
        line_26->setFrameShape(QFrame::VLine);
        line_26->setFrameShadow(QFrame::Sunken);

        gridLayout_12->addWidget(line_26, 1, 5, 3, 1);

        label_83 = new QLabel(verticalLayoutWidget);
        label_83->setObjectName(QStringLiteral("label_83"));
        label_83->setFont(font1);

        gridLayout_12->addWidget(label_83, 3, 3, 1, 1);

        LD_TU_ME = new QLCDNumber(verticalLayoutWidget);
        LD_TU_ME->setObjectName(QStringLiteral("LD_TU_ME"));

        gridLayout_12->addWidget(LD_TU_ME, 3, 4, 1, 1);

        label_84 = new QLabel(verticalLayoutWidget);
        label_84->setObjectName(QStringLiteral("label_84"));
        label_84->setFont(font1);

        gridLayout_12->addWidget(label_84, 3, 0, 1, 1);

        label_88 = new QLabel(verticalLayoutWidget);
        label_88->setObjectName(QStringLiteral("label_88"));
        label_88->setFont(font1);

        gridLayout_12->addWidget(label_88, 2, 6, 1, 1);

        LD_VA_RE = new QLCDNumber(verticalLayoutWidget);
        LD_VA_RE->setObjectName(QStringLiteral("LD_VA_RE"));

        gridLayout_12->addWidget(LD_VA_RE, 2, 7, 1, 1);

        LD_LM_ME = new QLCDNumber(verticalLayoutWidget);
        LD_LM_ME->setObjectName(QStringLiteral("LD_LM_ME"));

        gridLayout_12->addWidget(LD_LM_ME, 3, 1, 1, 1);

        label_90 = new QLabel(verticalLayoutWidget);
        label_90->setObjectName(QStringLiteral("label_90"));
        label_90->setFont(font1);

        gridLayout_12->addWidget(label_90, 3, 6, 1, 1);

        LD_VA_CA = new QLCDNumber(verticalLayoutWidget);
        LD_VA_CA->setObjectName(QStringLiteral("LD_VA_CA"));

        gridLayout_12->addWidget(LD_VA_CA, 3, 7, 1, 1);


        verticalLayout_2->addLayout(gridLayout_12);

        line_22 = new QFrame(verticalLayoutWidget);
        line_22->setObjectName(QStringLiteral("line_22"));
        line_22->setFrameShape(QFrame::HLine);
        line_22->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_22);

        label_80 = new QLabel(verticalLayoutWidget);
        label_80->setObjectName(QStringLiteral("label_80"));
        label_80->setMaximumSize(QSize(16777215, 15));

        verticalLayout_2->addWidget(label_80);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        LD_RE_3 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_3->setObjectName(QStringLiteral("LD_RE_3"));
        LD_RE_3->setSegmentStyle(QLCDNumber::Filled);

        gridLayout->addWidget(LD_RE_3, 1, 7, 1, 1);

        label_10 = new QLabel(verticalLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font1);

        gridLayout->addWidget(label_10, 1, 6, 1, 1);

        LD_CA_3 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_3->setObjectName(QStringLiteral("LD_CA_3"));

        gridLayout->addWidget(LD_CA_3, 2, 7, 1, 1);

        LD_CA_1 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_1->setObjectName(QStringLiteral("LD_CA_1"));

        gridLayout->addWidget(LD_CA_1, 2, 1, 1, 1);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_7 = new QLabel(verticalLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font1);

        gridLayout->addWidget(label_7, 2, 3, 1, 1);

        label_11 = new QLabel(verticalLayoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font1);

        gridLayout->addWidget(label_11, 2, 6, 1, 1);

        line_7 = new QFrame(verticalLayoutWidget);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setFrameShape(QFrame::VLine);
        line_7->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_7, 1, 2, 2, 1);

        LD_RE_1 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_1->setObjectName(QStringLiteral("LD_RE_1"));

        gridLayout->addWidget(LD_RE_1, 1, 1, 1, 1);

        LD_CA_2 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_2->setObjectName(QStringLiteral("LD_CA_2"));

        gridLayout->addWidget(LD_CA_2, 2, 4, 1, 1);

        line_8 = new QFrame(verticalLayoutWidget);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setFrameShape(QFrame::VLine);
        line_8->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_8, 1, 5, 2, 1);

        LD_RE_2 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_2->setObjectName(QStringLiteral("LD_RE_2"));
        LD_RE_2->setFont(font2);

        gridLayout->addWidget(LD_RE_2, 1, 4, 1, 1);

        label_6 = new QLabel(verticalLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 1, 3, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_8 = new QLabel(verticalLayoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font1);

        gridLayout_3->addWidget(label_8, 0, 0, 1, 1);

        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);

        gridLayout_3->addWidget(label_5, 1, 3, 1, 1);

        LD_CA_6 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_6->setObjectName(QStringLiteral("LD_CA_6"));

        gridLayout_3->addWidget(LD_CA_6, 1, 7, 1, 1);

        line_10 = new QFrame(verticalLayoutWidget);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setFrameShape(QFrame::VLine);
        line_10->setFrameShadow(QFrame::Sunken);

        gridLayout_3->addWidget(line_10, 0, 5, 2, 1);

        LD_RE_5 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_5->setObjectName(QStringLiteral("LD_RE_5"));

        gridLayout_3->addWidget(LD_RE_5, 0, 4, 1, 1);

        label_9 = new QLabel(verticalLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font1);

        gridLayout_3->addWidget(label_9, 1, 0, 1, 1);

        LD_CA_5 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_5->setObjectName(QStringLiteral("LD_CA_5"));

        gridLayout_3->addWidget(LD_CA_5, 1, 4, 1, 1);

        LD_RE_6 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_6->setObjectName(QStringLiteral("LD_RE_6"));

        gridLayout_3->addWidget(LD_RE_6, 0, 7, 1, 1);

        label_13 = new QLabel(verticalLayoutWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setFont(font1);

        gridLayout_3->addWidget(label_13, 1, 6, 1, 1);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font1);

        gridLayout_3->addWidget(label_4, 0, 3, 1, 1);

        label_12 = new QLabel(verticalLayoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font1);

        gridLayout_3->addWidget(label_12, 0, 6, 1, 1);

        LD_CA_4 = new QLCDNumber(verticalLayoutWidget);
        LD_CA_4->setObjectName(QStringLiteral("LD_CA_4"));

        gridLayout_3->addWidget(LD_CA_4, 1, 1, 1, 1);

        LD_RE_4 = new QLCDNumber(verticalLayoutWidget);
        LD_RE_4->setObjectName(QStringLiteral("LD_RE_4"));

        gridLayout_3->addWidget(LD_RE_4, 0, 1, 1, 1);

        line_9 = new QFrame(verticalLayoutWidget);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setFrameShape(QFrame::VLine);
        line_9->setFrameShadow(QFrame::Sunken);

        gridLayout_3->addWidget(line_9, 0, 2, 2, 1);


        verticalLayout_2->addLayout(gridLayout_3);

        line_13 = new QFrame(verticalLayoutWidget);
        line_13->setObjectName(QStringLiteral("line_13"));
        line_13->setFrameShape(QFrame::HLine);
        line_13->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_13);

        label_79 = new QLabel(verticalLayoutWidget);
        label_79->setObjectName(QStringLiteral("label_79"));
        label_79->setMaximumSize(QSize(16777215, 15));

        verticalLayout_2->addWidget(label_79);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_47 = new QLabel(verticalLayoutWidget);
        label_47->setObjectName(QStringLiteral("label_47"));
        label_47->setFont(font1);

        gridLayout_4->addWidget(label_47, 1, 3, 1, 1);

        label_48 = new QLabel(verticalLayoutWidget);
        label_48->setObjectName(QStringLiteral("label_48"));
        label_48->setFont(font1);

        gridLayout_4->addWidget(label_48, 1, 6, 1, 1);

        line_14 = new QFrame(verticalLayoutWidget);
        line_14->setObjectName(QStringLiteral("line_14"));
        line_14->setFrameShape(QFrame::VLine);
        line_14->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line_14, 0, 2, 2, 1);

        LD_RE_T1x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T1x->setObjectName(QStringLiteral("LD_RE_T1x"));

        gridLayout_4->addWidget(LD_RE_T1x, 0, 1, 1, 1);

        LD_CA_T1x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T1x->setObjectName(QStringLiteral("LD_CA_T1x"));

        gridLayout_4->addWidget(LD_CA_T1x, 1, 1, 1, 1);

        label_49 = new QLabel(verticalLayoutWidget);
        label_49->setObjectName(QStringLiteral("label_49"));
        label_49->setFont(font1);

        gridLayout_4->addWidget(label_49, 1, 0, 1, 1);

        label_51 = new QLabel(verticalLayoutWidget);
        label_51->setObjectName(QStringLiteral("label_51"));
        label_51->setFont(font1);

        gridLayout_4->addWidget(label_51, 0, 0, 1, 1);

        LD_CA_T2x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T2x->setObjectName(QStringLiteral("LD_CA_T2x"));

        gridLayout_4->addWidget(LD_CA_T2x, 1, 4, 1, 1);

        line_15 = new QFrame(verticalLayoutWidget);
        line_15->setObjectName(QStringLiteral("line_15"));
        line_15->setFrameShape(QFrame::VLine);
        line_15->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line_15, 0, 5, 2, 1);

        LD_RE_T2x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T2x->setObjectName(QStringLiteral("LD_RE_T2x"));
        LD_RE_T2x->setFont(font2);

        gridLayout_4->addWidget(LD_RE_T2x, 0, 4, 1, 1);

        label_52 = new QLabel(verticalLayoutWidget);
        label_52->setObjectName(QStringLiteral("label_52"));
        label_52->setFont(font1);

        gridLayout_4->addWidget(label_52, 0, 3, 1, 1);

        LD_RE_T3x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T3x->setObjectName(QStringLiteral("LD_RE_T3x"));
        LD_RE_T3x->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_4->addWidget(LD_RE_T3x, 0, 7, 1, 1);

        label_53 = new QLabel(verticalLayoutWidget);
        label_53->setObjectName(QStringLiteral("label_53"));
        label_53->setFont(font1);

        gridLayout_4->addWidget(label_53, 0, 6, 1, 1);

        LD_CA_T3x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T3x->setObjectName(QStringLiteral("LD_CA_T3x"));

        gridLayout_4->addWidget(LD_CA_T3x, 1, 7, 1, 1);


        verticalLayout_2->addLayout(gridLayout_4);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        LD_CA_T4x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T4x->setObjectName(QStringLiteral("LD_CA_T4x"));

        gridLayout_5->addWidget(LD_CA_T4x, 1, 1, 1, 1);

        line_17 = new QFrame(verticalLayoutWidget);
        line_17->setObjectName(QStringLiteral("line_17"));
        line_17->setFrameShape(QFrame::VLine);
        line_17->setFrameShadow(QFrame::Sunken);

        gridLayout_5->addWidget(line_17, 0, 5, 2, 1);

        LD_CA_T5x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T5x->setObjectName(QStringLiteral("LD_CA_T5x"));

        gridLayout_5->addWidget(LD_CA_T5x, 1, 4, 1, 1);

        label_57 = new QLabel(verticalLayoutWidget);
        label_57->setObjectName(QStringLiteral("label_57"));
        label_57->setFont(font1);

        gridLayout_5->addWidget(label_57, 0, 6, 1, 1);

        label_55 = new QLabel(verticalLayoutWidget);
        label_55->setObjectName(QStringLiteral("label_55"));
        label_55->setFont(font1);

        gridLayout_5->addWidget(label_55, 0, 3, 1, 1);

        LD_CA_T6x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_T6x->setObjectName(QStringLiteral("LD_CA_T6x"));

        gridLayout_5->addWidget(LD_CA_T6x, 1, 7, 1, 1);

        label_58 = new QLabel(verticalLayoutWidget);
        label_58->setObjectName(QStringLiteral("label_58"));
        label_58->setFont(font1);

        gridLayout_5->addWidget(label_58, 1, 3, 1, 1);

        LD_RE_T6x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T6x->setObjectName(QStringLiteral("LD_RE_T6x"));

        gridLayout_5->addWidget(LD_RE_T6x, 0, 7, 1, 1);

        label_61 = new QLabel(verticalLayoutWidget);
        label_61->setObjectName(QStringLiteral("label_61"));
        label_61->setFont(font1);

        gridLayout_5->addWidget(label_61, 1, 6, 1, 1);

        LD_RE_T5x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T5x->setObjectName(QStringLiteral("LD_RE_T5x"));

        gridLayout_5->addWidget(LD_RE_T5x, 0, 4, 1, 1);

        label_46 = new QLabel(verticalLayoutWidget);
        label_46->setObjectName(QStringLiteral("label_46"));
        label_46->setFont(font1);

        gridLayout_5->addWidget(label_46, 0, 0, 1, 1);

        LD_RE_T4x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_T4x->setObjectName(QStringLiteral("LD_RE_T4x"));

        gridLayout_5->addWidget(LD_RE_T4x, 0, 1, 1, 1);

        label_50 = new QLabel(verticalLayoutWidget);
        label_50->setObjectName(QStringLiteral("label_50"));
        label_50->setFont(font1);

        gridLayout_5->addWidget(label_50, 1, 0, 1, 1);

        line_16 = new QFrame(verticalLayoutWidget);
        line_16->setObjectName(QStringLiteral("line_16"));
        line_16->setFrameShape(QFrame::VLine);
        line_16->setFrameShadow(QFrame::Sunken);

        gridLayout_5->addWidget(line_16, 0, 2, 2, 1);


        verticalLayout_2->addLayout(gridLayout_5);

        line_23 = new QFrame(verticalLayoutWidget);
        line_23->setObjectName(QStringLiteral("line_23"));
        line_23->setFrameShape(QFrame::HLine);
        line_23->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_23);

        label_78 = new QLabel(verticalLayoutWidget);
        label_78->setObjectName(QStringLiteral("label_78"));
        label_78->setMaximumSize(QSize(1000, 15));

        verticalLayout_2->addWidget(label_78);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_64 = new QLabel(verticalLayoutWidget);
        label_64->setObjectName(QStringLiteral("label_64"));
        label_64->setFont(font1);

        gridLayout_9->addWidget(label_64, 1, 3, 1, 1);

        label_65 = new QLabel(verticalLayoutWidget);
        label_65->setObjectName(QStringLiteral("label_65"));
        label_65->setFont(font1);

        gridLayout_9->addWidget(label_65, 1, 6, 1, 1);

        line_20 = new QFrame(verticalLayoutWidget);
        line_20->setObjectName(QStringLiteral("line_20"));
        line_20->setFrameShape(QFrame::VLine);
        line_20->setFrameShadow(QFrame::Sunken);

        gridLayout_9->addWidget(line_20, 0, 2, 2, 1);

        LD_RE_S1x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S1x->setObjectName(QStringLiteral("LD_RE_S1x"));

        gridLayout_9->addWidget(LD_RE_S1x, 0, 1, 1, 1);

        LD_CA_S1x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S1x->setObjectName(QStringLiteral("LD_CA_S1x"));

        gridLayout_9->addWidget(LD_CA_S1x, 1, 1, 1, 1);

        label_66 = new QLabel(verticalLayoutWidget);
        label_66->setObjectName(QStringLiteral("label_66"));
        label_66->setFont(font1);

        gridLayout_9->addWidget(label_66, 1, 0, 1, 1);

        label_67 = new QLabel(verticalLayoutWidget);
        label_67->setObjectName(QStringLiteral("label_67"));
        label_67->setFont(font1);

        gridLayout_9->addWidget(label_67, 0, 0, 1, 1);

        LD_CA_S2x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S2x->setObjectName(QStringLiteral("LD_CA_S2x"));

        gridLayout_9->addWidget(LD_CA_S2x, 1, 4, 1, 1);

        line_21 = new QFrame(verticalLayoutWidget);
        line_21->setObjectName(QStringLiteral("line_21"));
        line_21->setFrameShape(QFrame::VLine);
        line_21->setFrameShadow(QFrame::Sunken);

        gridLayout_9->addWidget(line_21, 0, 5, 2, 1);

        LD_RE_S2x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S2x->setObjectName(QStringLiteral("LD_RE_S2x"));
        LD_RE_S2x->setFont(font2);

        gridLayout_9->addWidget(LD_RE_S2x, 0, 4, 1, 1);

        label_68 = new QLabel(verticalLayoutWidget);
        label_68->setObjectName(QStringLiteral("label_68"));
        label_68->setFont(font1);

        gridLayout_9->addWidget(label_68, 0, 3, 1, 1);

        LD_RE_S3x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S3x->setObjectName(QStringLiteral("LD_RE_S3x"));
        LD_RE_S3x->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_9->addWidget(LD_RE_S3x, 0, 7, 1, 1);

        label_69 = new QLabel(verticalLayoutWidget);
        label_69->setObjectName(QStringLiteral("label_69"));
        label_69->setFont(font1);

        gridLayout_9->addWidget(label_69, 0, 6, 1, 1);

        LD_CA_S3x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S3x->setObjectName(QStringLiteral("LD_CA_S3x"));

        gridLayout_9->addWidget(LD_CA_S3x, 1, 7, 1, 1);


        verticalLayout_2->addLayout(gridLayout_9);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        line_18 = new QFrame(verticalLayoutWidget);
        line_18->setObjectName(QStringLiteral("line_18"));
        line_18->setFrameShape(QFrame::VLine);
        line_18->setFrameShadow(QFrame::Sunken);

        gridLayout_6->addWidget(line_18, 0, 5, 2, 1);

        label_62 = new QLabel(verticalLayoutWidget);
        label_62->setObjectName(QStringLiteral("label_62"));
        label_62->setFont(font1);

        gridLayout_6->addWidget(label_62, 1, 6, 1, 1);

        LD_RE_S4x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S4x->setObjectName(QStringLiteral("LD_RE_S4x"));

        gridLayout_6->addWidget(LD_RE_S4x, 0, 1, 1, 1);

        label_60 = new QLabel(verticalLayoutWidget);
        label_60->setObjectName(QStringLiteral("label_60"));
        label_60->setFont(font1);

        gridLayout_6->addWidget(label_60, 1, 3, 1, 1);

        line_19 = new QFrame(verticalLayoutWidget);
        line_19->setObjectName(QStringLiteral("line_19"));
        line_19->setFrameShape(QFrame::VLine);
        line_19->setFrameShadow(QFrame::Sunken);

        gridLayout_6->addWidget(line_19, 0, 2, 2, 1);

        label_54 = new QLabel(verticalLayoutWidget);
        label_54->setObjectName(QStringLiteral("label_54"));
        label_54->setFont(font1);

        gridLayout_6->addWidget(label_54, 0, 0, 1, 1);

        label_56 = new QLabel(verticalLayoutWidget);
        label_56->setObjectName(QStringLiteral("label_56"));
        label_56->setFont(font1);

        gridLayout_6->addWidget(label_56, 0, 3, 1, 1);

        LD_RE_S6x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S6x->setObjectName(QStringLiteral("LD_RE_S6x"));

        gridLayout_6->addWidget(LD_RE_S6x, 0, 7, 1, 1);

        LD_CA_S4x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S4x->setObjectName(QStringLiteral("LD_CA_S4x"));

        gridLayout_6->addWidget(LD_CA_S4x, 1, 1, 1, 1);

        label_63 = new QLabel(verticalLayoutWidget);
        label_63->setObjectName(QStringLiteral("label_63"));
        label_63->setFont(font1);

        gridLayout_6->addWidget(label_63, 1, 0, 1, 1);

        LD_RE_S5x = new QLCDNumber(verticalLayoutWidget);
        LD_RE_S5x->setObjectName(QStringLiteral("LD_RE_S5x"));

        gridLayout_6->addWidget(LD_RE_S5x, 0, 4, 1, 1);

        LD_CA_S5x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S5x->setObjectName(QStringLiteral("LD_CA_S5x"));

        gridLayout_6->addWidget(LD_CA_S5x, 1, 4, 1, 1);

        LD_CA_S6x = new QLCDNumber(verticalLayoutWidget);
        LD_CA_S6x->setObjectName(QStringLiteral("LD_CA_S6x"));

        gridLayout_6->addWidget(LD_CA_S6x, 1, 7, 1, 1);

        label_59 = new QLabel(verticalLayoutWidget);
        label_59->setObjectName(QStringLiteral("label_59"));
        label_59->setFont(font1);

        gridLayout_6->addWidget(label_59, 0, 6, 1, 1);


        verticalLayout_2->addLayout(gridLayout_6);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayoutWidget = new QWidget(tab_2);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 150, 425, 251));
        gridLayout_13 = new QGridLayout(gridLayoutWidget);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        gridLayout_13->setContentsMargins(0, 0, 0, 0);
        label_21 = new QLabel(gridLayoutWidget);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_13->addWidget(label_21, 2, 1, 1, 1);

        label_20 = new QLabel(gridLayoutWidget);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_13->addWidget(label_20, 3, 3, 1, 1);

        label_33 = new QLabel(gridLayoutWidget);
        label_33->setObjectName(QStringLiteral("label_33"));

        gridLayout_13->addWidget(label_33, 3, 1, 1, 1);

        SB_Y2 = new QSpinBox(gridLayoutWidget);
        SB_Y2->setObjectName(QStringLiteral("SB_Y2"));
        SB_Y2->setMaximum(99);
        SB_Y2->setValue(70);

        gridLayout_13->addWidget(SB_Y2, 3, 2, 1, 1);

        SB_X1 = new QSpinBox(gridLayoutWidget);
        SB_X1->setObjectName(QStringLiteral("SB_X1"));
        SB_X1->setMaximum(16000);
        SB_X1->setValue(5400);

        gridLayout_13->addWidget(SB_X1, 3, 0, 1, 1);

        SB_Y1 = new QSpinBox(gridLayoutWidget);
        SB_Y1->setObjectName(QStringLiteral("SB_Y1"));
        SB_Y1->setEnabled(false);
        SB_Y1->setMaximum(100);
        SB_Y1->setValue(100);

        gridLayout_13->addWidget(SB_Y1, 2, 2, 1, 1);

        SB_X2 = new QSpinBox(gridLayoutWidget);
        SB_X2->setObjectName(QStringLiteral("SB_X2"));
        SB_X2->setMaximum(16000);
        SB_X2->setValue(6000);

        gridLayout_13->addWidget(SB_X2, 2, 0, 1, 1);

        label_32 = new QLabel(gridLayoutWidget);
        label_32->setObjectName(QStringLiteral("label_32"));

        gridLayout_13->addWidget(label_32, 2, 3, 1, 1);

        BT_GE_GR = new QPushButton(gridLayoutWidget);
        BT_GE_GR->setObjectName(QStringLiteral("BT_GE_GR"));

        gridLayout_13->addWidget(BT_GE_GR, 4, 0, 1, 4);

        label_40 = new QLabel(gridLayoutWidget);
        label_40->setObjectName(QStringLiteral("label_40"));

        gridLayout_13->addWidget(label_40, 0, 0, 1, 1);

        RB_EQ_HC = new QRadioButton(gridLayoutWidget);
        RB_EQ_HC->setObjectName(QStringLiteral("RB_EQ_HC"));
        RB_EQ_HC->setAutoExclusive(false);

        gridLayout_13->addWidget(RB_EQ_HC, 0, 1, 1, 1);

        line_12 = new QFrame(gridLayoutWidget);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(line_12, 1, 0, 1, 4);

        LE_HC = new QLineEdit(gridLayoutWidget);
        LE_HC->setObjectName(QStringLiteral("LE_HC"));

        gridLayout_13->addWidget(LE_HC, 0, 2, 1, 2);

        gridLayoutWidget_2 = new QWidget(tab_2);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 10, 421, 91));
        gridLayout_15 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        gridLayout_15->setContentsMargins(0, 0, 0, 0);
        label_22 = new QLabel(gridLayoutWidget_2);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout_15->addWidget(label_22, 0, 0, 1, 1);

        label_39 = new QLabel(gridLayoutWidget_2);
        label_39->setObjectName(QStringLiteral("label_39"));

        gridLayout_15->addWidget(label_39, 2, 0, 1, 1);

        LD_HC_CA = new QLCDNumber(gridLayoutWidget_2);
        LD_HC_CA->setObjectName(QStringLiteral("LD_HC_CA"));

        gridLayout_15->addWidget(LD_HC_CA, 1, 1, 1, 1);

        LD_HC_ME = new QLCDNumber(gridLayoutWidget_2);
        LD_HC_ME->setObjectName(QStringLiteral("LD_HC_ME"));

        gridLayout_15->addWidget(LD_HC_ME, 2, 1, 1, 1);

        label_38 = new QLabel(gridLayoutWidget_2);
        label_38->setObjectName(QStringLiteral("label_38"));

        gridLayout_15->addWidget(label_38, 1, 0, 1, 1);

        LD_HC_RE = new QLCDNumber(gridLayoutWidget_2);
        LD_HC_RE->setObjectName(QStringLiteral("LD_HC_RE"));

        gridLayout_15->addWidget(LD_HC_RE, 0, 1, 1, 1);

        LD_HC_CA_CO = new QLCDNumber(gridLayoutWidget_2);
        LD_HC_CA_CO->setObjectName(QStringLiteral("LD_HC_CA_CO"));

        gridLayout_15->addWidget(LD_HC_CA_CO, 1, 2, 1, 1);

        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        label_41 = new QLabel(tab_4);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setGeometry(QRect(20, 220, 341, 51));
        label_41->setFrameShadow(QFrame::Plain);
        label_41->setLineWidth(2);
        label_41->setTextFormat(Qt::PlainText);
        label_41->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_41->setWordWrap(true);
        charge_data = new QPushButton(tab_4);
        charge_data->setObjectName(QStringLiteral("charge_data"));
        charge_data->setGeometry(QRect(20, 280, 241, 25));
        line_25 = new QFrame(tab_4);
        line_25->setObjectName(QStringLiteral("line_25"));
        line_25->setGeometry(QRect(400, 10, 20, 611));
        line_25->setFrameShape(QFrame::VLine);
        line_25->setFrameShadow(QFrame::Sunken);
        label_43 = new QLabel(tab_4);
        label_43->setObjectName(QStringLiteral("label_43"));
        label_43->setGeometry(QRect(20, 310, 321, 61));
        label_43->setWordWrap(true);
        refactor = new QPushButton(tab_4);
        refactor->setObjectName(QStringLiteral("refactor"));
        refactor->setGeometry(QRect(20, 380, 231, 25));
        label_44 = new QLabel(tab_4);
        label_44->setObjectName(QStringLiteral("label_44"));
        label_44->setGeometry(QRect(436, 30, 281, 20));
        label_44->setAlignment(Qt::AlignCenter);
        connect_txt = new QLabel(tab_4);
        connect_txt->setObjectName(QStringLiteral("connect_txt"));
        connect_txt->setGeometry(QRect(436, 70, 281, 541));
        connect_txt->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        connect_txt->setWordWrap(true);
        label_70 = new QLabel(tab_4);
        label_70->setObjectName(QStringLiteral("label_70"));
        label_70->setGeometry(QRect(20, 20, 341, 17));
        select_folder = new QPushButton(tab_4);
        select_folder->setObjectName(QStringLiteral("select_folder"));
        select_folder->setGeometry(QRect(20, 60, 231, 25));
        folder_txt = new QLabel(tab_4);
        folder_txt->setObjectName(QStringLiteral("folder_txt"));
        folder_txt->setGeometry(QRect(20, 120, 371, 41));
        folder_txt->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        folder_txt->setWordWrap(true);
        dest_db_label = new QLabel(tab_4);
        dest_db_label->setObjectName(QStringLiteral("dest_db_label"));
        dest_db_label->setGeometry(QRect(20, 180, 361, 41));
        dest_db_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        dest_db_label->setWordWrap(true);
        label_73 = new QLabel(tab_4);
        label_73->setObjectName(QStringLiteral("label_73"));
        label_73->setGeometry(QRect(20, 160, 361, 17));
        label_76 = new QLabel(tab_4);
        label_76->setObjectName(QStringLiteral("label_76"));
        label_76->setGeometry(QRect(20, 100, 361, 17));
        start_refactor = new QPushButton(tab_4);
        start_refactor->setObjectName(QStringLiteral("start_refactor"));
        start_refactor->setGeometry(QRect(20, 500, 231, 25));
        label_103 = new QLabel(tab_4);
        label_103->setObjectName(QStringLiteral("label_103"));
        label_103->setGeometry(QRect(20, 430, 321, 61));
        label_103->setWordWrap(true);
        check_refactor = new QCheckBox(tab_4);
        check_refactor->setObjectName(QStringLiteral("check_refactor"));
        check_refactor->setGeometry(QRect(270, 500, 91, 23));
        persistant = new QPushButton(tab_4);
        persistant->setObjectName(QStringLiteral("persistant"));
        persistant->setGeometry(QRect(20, 560, 171, 25));
        per_init = new QSpinBox(tab_4);
        per_init->setObjectName(QStringLiteral("per_init"));
        per_init->setGeometry(QRect(210, 560, 191, 26));
        per_init->setMaximum(313000);
        per_fin = new QSpinBox(tab_4);
        per_fin->setObjectName(QStringLiteral("per_fin"));
        per_fin->setGeometry(QRect(210, 600, 191, 26));
        per_fin->setMaximum(313000);
        tabWidget->addTab(tab_4, QString());
        jptwatercut->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(jptwatercut);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        jptwatercut->setStatusBar(statusBar);

        retranslateUi(jptwatercut);

        tabWidget->setCurrentIndex(0);
        tabWidget_2->setCurrentIndex(3);
        tabWidget_3->setCurrentIndex(0);
        tabWidget_4->setCurrentIndex(1);
        capacitance_curves->setCurrentIndex(2);
        functionescorrect->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(jptwatercut);
    } // setupUi

    void retranslateUi(QMainWindow *jptwatercut)
    {
        jptwatercut->setWindowTitle(QApplication::translate("jptwatercut", "Water Cut", Q_NULLPTR));
        label_112->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        label_113->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        label_114->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_115->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        label_116->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        label_117->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_122->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_123->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_118->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        label_120->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_121->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_119->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_99->setText(QApplication::translate("jptwatercut", "Entry Data Raw", Q_NULLPTR));
        cap_r_4->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_153->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        cap_c_6->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        cap_r_6->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_159->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        cap_c_4->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_154->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        cap_r_5->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        cap_c_5->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_157->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_149->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_150->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        cap_r_1->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_135->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_134->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        cap_r_2->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        cap_c_2->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_132->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_130->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        cap_c_3->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        cap_r_3->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_131->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        cap_c_1->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_133->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        captura_data->setText(QApplication::translate("jptwatercut", "Capture Data", Q_NULLPTR));
        dateEdit->setDisplayFormat(QString());
        label_92->setText(QApplication::translate("jptwatercut", "Date ", Q_NULLPTR));
        label_93->setText(QApplication::translate("jptwatercut", "Water Salinity ", Q_NULLPTR));
        label_87->setText(QApplication::translate("jptwatercut", "Temperature [C\302\260]", Q_NULLPTR));
        label_94->setText(QApplication::translate("jptwatercut", "Presure", Q_NULLPTR));
        Group_set_parameters_2->setTitle(QApplication::translate("jptwatercut", "Set Parameters  Resistivity ", Q_NULLPTR));
        calcule_def_dlt_2->setText(QApplication::translate("jptwatercut", "Calculate Delta R.", Q_NULLPTR));
        label_178->setText(QApplication::translate("jptwatercut", "Default Value                      ", Q_NULLPTR));
        default_var_2->setText(QApplication::translate("jptwatercut", "16300", Q_NULLPTR));
        label_179->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        label_180->setText(QApplication::translate("jptwatercut", "Manual Value", Q_NULLPTR));
        label_181->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        calcule_man_dlt_2->setText(QApplication::translate("jptwatercut", "Calculate Delta R.", Q_NULLPTR));
        label_182->setText(QApplication::translate("jptwatercut", "Auto Value", Q_NULLPTR));
        calcule_auto_dlt_2->setText(QApplication::translate("jptwatercut", "Calculate Delta R.", Q_NULLPTR));
        update_dlt_val_2->setText(QApplication::translate("jptwatercut", "Charge Delta Values R.", Q_NULLPTR));
        label_183->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        atomatic_var_2->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        update_dlt_average_2->setText(QApplication::translate("jptwatercut", "Charge Delta Average R.", Q_NULLPTR));
        update_labal_2->setText(QApplication::translate("jptwatercut", ". . .", Q_NULLPTR));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_14), QApplication::translate("jptwatercut", "Normalize Resistivity", Q_NULLPTR));
        Group_set_parameters->setTitle(QApplication::translate("jptwatercut", "Set Parameters Capacitance", Q_NULLPTR));
        calcule_def_dlt->setText(QApplication::translate("jptwatercut", "Calculate Delta C.", Q_NULLPTR));
        label_136->setText(QApplication::translate("jptwatercut", "Default Value                      ", Q_NULLPTR));
        default_var->setText(QApplication::translate("jptwatercut", "16300", Q_NULLPTR));
        label_138->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        label_139->setText(QApplication::translate("jptwatercut", "Manual Value", Q_NULLPTR));
        label_140->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        calcule_man_dlt->setText(QApplication::translate("jptwatercut", "Calculate Delta C.", Q_NULLPTR));
        label_141->setText(QApplication::translate("jptwatercut", "Auto Value", Q_NULLPTR));
        calcule_auto_dlt->setText(QApplication::translate("jptwatercut", "Calculate Delta C.", Q_NULLPTR));
        update_dlt_val->setText(QApplication::translate("jptwatercut", "Charge Delta Values C.", Q_NULLPTR));
        label_160->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        atomatic_var->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        update_dlt_average->setText(QApplication::translate("jptwatercut", "Charge Delta Average C.", Q_NULLPTR));
        update_labal->setText(QApplication::translate("jptwatercut", ". . .", Q_NULLPTR));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_15), QApplication::translate("jptwatercut", "Normalize Capacitance", Q_NULLPTR));
        delta_c_6->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        delta_c_4->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_162->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_158->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_156->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        delta_r_4->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_152->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_155->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        delta_c_5->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        delta_r_5->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_151->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        delta_r_6->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_146->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        delta_r_2->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_143->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        delta_c_3->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_144->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        delta_c_1->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        delta_r_1->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_142->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_145->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        delta_r_3->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        delta_c_2->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_147->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        delta_average->setText(QApplication::translate("jptwatercut", ". . .", Q_NULLPTR));
        label_81->setText(QApplication::translate("jptwatercut", "Average Resistivity", Q_NULLPTR));
        label_89->setText(QApplication::translate("jptwatercut", "[", Q_NULLPTR));
        label_91->setText(QApplication::translate("jptwatercut", "]", Q_NULLPTR));
        label_77->setText(QApplication::translate("jptwatercut", "DELTA VALUES [ R / C]", Q_NULLPTR));
        label_184->setText(QApplication::translate("jptwatercut", "[", Q_NULLPTR));
        label_185->setText(QApplication::translate("jptwatercut", "Average Capacitance", Q_NULLPTR));
        delta_average_2->setText(QApplication::translate("jptwatercut", ". . .", Q_NULLPTR));
        label_186->setText(QApplication::translate("jptwatercut", "]", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QApplication::translate("jptwatercut", "Water Normalization", Q_NULLPTR));
        label_124->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        label_125->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        label_126->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_127->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        label_128->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        label_129->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_100->setText(QApplication::translate("jptwatercut", "Raw Data Normalize", Q_NULLPTR));
        label_137->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_148->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_161->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        label_163->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_164->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_165->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        captura_data_2->setText(QApplication::translate("jptwatercut", "Capture Data", Q_NULLPTR));
        calib_cap_r_1->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_172->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_173->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        calib_cap_r_2->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        calib_cap_c_2->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_174->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_175->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        calib_cap_c_3->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        calib_cap_r_3->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_176->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        calib_cap_c_1->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_177->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        calib_cap_r_4->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_166->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_167->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        calib_cap_r_5->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        calib_cap_c_5->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_168->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_169->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        calib_cap_c_6->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        calib_cap_r_6->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_170->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        calib_cap_c_4->setText(QApplication::translate("jptwatercut", "Data1  /  Data2  /Data 3", Q_NULLPTR));
        label_171->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("jptwatercut", "Conditions", Q_NULLPTR));
        checkBox->setText(QApplication::translate("jptwatercut", "Oil API", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("jptwatercut", "Temperature", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("jptwatercut", "Salinity", Q_NULLPTR));
        calib_message->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        result_obj->setTitle(QString());
        label_95->setText(QApplication::translate("jptwatercut", "Avg Oil", Q_NULLPTR));
        label_96->setText(QApplication::translate("jptwatercut", "Avg Air", Q_NULLPTR));
        label_97->setText(QApplication::translate("jptwatercut", "Avg. Formation Water", Q_NULLPTR));
        label_101->setText(QApplication::translate("jptwatercut", "Avg. Water", Q_NULLPTR));
        label_98->setText(QApplication::translate("jptwatercut", "Resistivity  ", Q_NULLPTR));
        label_102->setText(QApplication::translate("jptwatercut", "Capacitance", Q_NULLPTR));
        avg_oil->setText(QApplication::translate("jptwatercut", " Avg Oil", Q_NULLPTR));
        avg_air->setText(QApplication::translate("jptwatercut", "Avg AIr", Q_NULLPTR));
        avg_w->setText(QApplication::translate("jptwatercut", "Avg Water", Q_NULLPTR));
        avg_fw->setText(QApplication::translate("jptwatercut", "Avg F-W", Q_NULLPTR));
        label_216->setText(QApplication::translate("jptwatercut", "Average Values", Q_NULLPTR));
        label_217->setText(QApplication::translate("jptwatercut", "R", Q_NULLPTR));
        label_218->setText(QApplication::translate("jptwatercut", "Rs", Q_NULLPTR));
        label_219->setText(QApplication::translate("jptwatercut", "14500", Q_NULLPTR));
        label_220->setText(QApplication::translate("jptwatercut", "16000", Q_NULLPTR));
        label_221->setText(QApplication::translate("jptwatercut", "500", Q_NULLPTR));
        label_222->setText(QApplication::translate("jptwatercut", "5000", Q_NULLPTR));
        label_223->setText(QApplication::translate("jptwatercut", "C", Q_NULLPTR));
        label_224->setText(QApplication::translate("jptwatercut", "Cs", Q_NULLPTR));
        label_240->setText(QApplication::translate("jptwatercut", "500", Q_NULLPTR));
        label_241->setText(QApplication::translate("jptwatercut", "13500", Q_NULLPTR));
        label_242->setText(QApplication::translate("jptwatercut", "5500", Q_NULLPTR));
        label_243->setText(QApplication::translate("jptwatercut", "15500", Q_NULLPTR));
        calcule_curves->setText(QApplication::translate("jptwatercut", "GENERATE STF", Q_NULLPTR));
        label_109->setText(QApplication::translate("jptwatercut", "Calcule Values", Q_NULLPTR));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_16), QApplication::translate("jptwatercut", "Calibration STF", Q_NULLPTR));
        label_108->setText(QApplication::translate("jptwatercut", "Capacitance", Q_NULLPTR));
        label_187->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        label_193->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        label_192->setText(QApplication::translate("jptwatercut", "Low Water Cut", Q_NULLPTR));
        label_189->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        label_206->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        calcule_curves_hl->setText(QApplication::translate("jptwatercut", "GENERATE High/Low TF", Q_NULLPTR));
        label_207->setText(QApplication::translate("jptwatercut", "HWC UP%", Q_NULLPTR));
        label_208->setText(QApplication::translate("jptwatercut", "HWC DW%", Q_NULLPTR));
        label_209->setText(QApplication::translate("jptwatercut", "VALUE", Q_NULLPTR));
        label_210->setText(QApplication::translate("jptwatercut", "VALUE", Q_NULLPTR));
        label_211->setText(QApplication::translate("jptwatercut", "VALUE", Q_NULLPTR));
        label_212->setText(QApplication::translate("jptwatercut", "LWC DW%", Q_NULLPTR));
        label_213->setText(QApplication::translate("jptwatercut", "VALUE", Q_NULLPTR));
        label_214->setText(QApplication::translate("jptwatercut", "LWC UP%", Q_NULLPTR));
        label_215->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        label_226->setText(QApplication::translate("jptwatercut", "High Water Cut", Q_NULLPTR));
        label_228->setText(QApplication::translate("jptwatercut", "Resistivity", Q_NULLPTR));
        label_230->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        label_271->setText(QApplication::translate("jptwatercut", ">> ", Q_NULLPTR));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_17), QApplication::translate("jptwatercut", "Calibration High/Low WC", Q_NULLPTR));
        label_200->setText(QApplication::translate("jptwatercut", "Three Phases ", Q_NULLPTR));
        label_227->setText(QApplication::translate("jptwatercut", "100 % Oil ---0 % Gas    >>", Q_NULLPTR));
        label_229->setText(QApplication::translate("jptwatercut", "0 % Oil --- 100% Gas    >>", Q_NULLPTR));
        calcule_curves_TFM->setText(QApplication::translate("jptwatercut", "GENERATE Three Phases Model TF", Q_NULLPTR));
        tabWidget_4->setTabText(tabWidget_4->indexOf(tab_18), QApplication::translate("jptwatercut", "Calibration Three Phases ", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_8), QApplication::translate("jptwatercut", "Calibration", Q_NULLPTR));
        label_111->setText(QApplication::translate("jptwatercut", "SPECIFIC TRANSFER FUNCTION", Q_NULLPTR));
        capacitance_curves->setTabText(capacitance_curves->indexOf(tab_10), QApplication::translate("jptwatercut", "STF Resistivity", Q_NULLPTR));
        label_110->setText(QApplication::translate("jptwatercut", "SPECIFIC TRANSFER FUNCTION", Q_NULLPTR));
        capacitance_curves->setTabText(capacitance_curves->indexOf(tab_11), QApplication::translate("jptwatercut", "STF Capacitance", Q_NULLPTR));
        label_225->setText(QApplication::translate("jptwatercut", "THREE PHASES OIL AND GAS MODEL ", Q_NULLPTR));
        capacitance_curves->setTabText(capacitance_curves->indexOf(tab_19), QApplication::translate("jptwatercut", "STF Thress Phases Model ", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_7), QApplication::translate("jptwatercut", "STF", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("jptwatercut", "Conditions", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("jptwatercut", "FLow", Q_NULLPTR));
        RB_ST_CO->setText(QApplication::translate("jptwatercut", "Salinity", Q_NULLPTR));
        checkBox_5->setText(QApplication::translate("jptwatercut", "Oil API", Q_NULLPTR));
        RB_TP_CO->setText(QApplication::translate("jptwatercut", "Temp. Value. ", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("jptwatercut", "GroupBox", Q_NULLPTR));
        label_194->setText(QApplication::translate("jptwatercut", "Laminar WC", Q_NULLPTR));
        label_195->setText(QApplication::translate("jptwatercut", "Turbulent WC", Q_NULLPTR));
        label_196->setText(QApplication::translate("jptwatercut", "Turbulent Laminar  WC", Q_NULLPTR));
        label_197->setText(QApplication::translate("jptwatercut", "Single Phase", Q_NULLPTR));
        label_198->setText(QApplication::translate("jptwatercut", "Emulsion Heavy Oil WC", Q_NULLPTR));
        label_199->setText(QApplication::translate("jptwatercut", "High Water Cut", Q_NULLPTR));
        label_201->setText(QApplication::translate("jptwatercut", "Very Low Water", Q_NULLPTR));
        label_202->setText(QApplication::translate("jptwatercut", "Three Phase  :", Q_NULLPTR));
        label_203->setText(QApplication::translate("jptwatercut", "Water->", Q_NULLPTR));
        label_204->setText(QApplication::translate("jptwatercut", "Gas->", Q_NULLPTR));
        label_205->setText(QApplication::translate("jptwatercut", "Oil->", Q_NULLPTR));
        label_190->setText(QApplication::translate("jptwatercut", "T/L Res", Q_NULLPTR));
        label_191->setText(QApplication::translate("jptwatercut", "T/L Cap", Q_NULLPTR));
        label_259->setText(QApplication::translate("jptwatercut", "WC-C6", Q_NULLPTR));
        label_260->setText(QApplication::translate("jptwatercut", "WC-R5", Q_NULLPTR));
        label_261->setText(QApplication::translate("jptwatercut", "WC-R6", Q_NULLPTR));
        label_262->setText(QApplication::translate("jptwatercut", "WC-R4", Q_NULLPTR));
        label_263->setText(QApplication::translate("jptwatercut", "WC-C5", Q_NULLPTR));
        label_264->setText(QApplication::translate("jptwatercut", "WC-C4", Q_NULLPTR));
        label_265->setText(QApplication::translate("jptwatercut", "WC-R3", Q_NULLPTR));
        label_266->setText(QApplication::translate("jptwatercut", "WC-C1", Q_NULLPTR));
        label_267->setText(QApplication::translate("jptwatercut", "WC-R1", Q_NULLPTR));
        label_268->setText(QApplication::translate("jptwatercut", "WC-C2", Q_NULLPTR));
        label_269->setText(QApplication::translate("jptwatercut", "WC-C3", Q_NULLPTR));
        label_270->setText(QApplication::translate("jptwatercut", "WC_R2", Q_NULLPTR));
        CK_TP_F_HD->setText(QApplication::translate("jptwatercut", "Temp. Value. ", Q_NULLPTR));
        label_104->setText(QApplication::translate("jptwatercut", "Default", Q_NULLPTR));
        label_105->setText(QApplication::translate("jptwatercut", "Sensor", Q_NULLPTR));
        functionescorrect->setTabText(functionescorrect->indexOf(tab_correct_temp), QApplication::translate("jptwatercut", "Temp Correction", Q_NULLPTR));
        label_247->setText(QApplication::translate("jptwatercut", "WC-C6", Q_NULLPTR));
        label_248->setText(QApplication::translate("jptwatercut", "WC-R5", Q_NULLPTR));
        label_249->setText(QApplication::translate("jptwatercut", "WC-R6", Q_NULLPTR));
        label_250->setText(QApplication::translate("jptwatercut", "WC-R4", Q_NULLPTR));
        label_251->setText(QApplication::translate("jptwatercut", "WC-C5", Q_NULLPTR));
        label_252->setText(QApplication::translate("jptwatercut", "WC-C4", Q_NULLPTR));
        label_253->setText(QApplication::translate("jptwatercut", "WC-R3", Q_NULLPTR));
        label_254->setText(QApplication::translate("jptwatercut", "WC-C1", Q_NULLPTR));
        label_255->setText(QApplication::translate("jptwatercut", "WC-R1", Q_NULLPTR));
        label_256->setText(QApplication::translate("jptwatercut", "WC-C2", Q_NULLPTR));
        label_257->setText(QApplication::translate("jptwatercut", "WC-C3", Q_NULLPTR));
        label_258->setText(QApplication::translate("jptwatercut", "WC_R2", Q_NULLPTR));
        CK_ST_F_HD->setText(QApplication::translate("jptwatercut", "Fixed PPM. Value.  ", Q_NULLPTR));
        CK_ST_HD_1->setText(QApplication::translate("jptwatercut", "PPM Sensor Res[1]", Q_NULLPTR));
        CK_ST_HD_6->setText(QApplication::translate("jptwatercut", "PPM Sensor Res[6]", Q_NULLPTR));
        functionescorrect->setTabText(functionescorrect->indexOf(tab_correct_sali), QApplication::translate("jptwatercut", "Salinity Correction", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("jptwatercut", "FLOW REGIME", Q_NULLPTR));
        REGIME_LABEL->setText(QApplication::translate("jptwatercut", ". . .", Q_NULLPTR));
        label_106->setText(QApplication::translate("jptwatercut", "Standard Deviation", Q_NULLPTR));
        label_107->setText(QApplication::translate("jptwatercut", "FLow Regime       >>", Q_NULLPTR));
        functionescorrect->setTabText(functionescorrect->indexOf(tab_13), QApplication::translate("jptwatercut", "Flow Correction", Q_NULLPTR));
        label_237->setText(QApplication::translate("jptwatercut", "WC-C6", Q_NULLPTR));
        label_238->setText(QApplication::translate("jptwatercut", "WC-R5", Q_NULLPTR));
        label_239->setText(QApplication::translate("jptwatercut", "WC-R6", Q_NULLPTR));
        label_244->setText(QApplication::translate("jptwatercut", "WC-R4", Q_NULLPTR));
        label_245->setText(QApplication::translate("jptwatercut", "WC-C5", Q_NULLPTR));
        label_246->setText(QApplication::translate("jptwatercut", "WC-C4", Q_NULLPTR));
        label_231->setText(QApplication::translate("jptwatercut", "WC-R3", Q_NULLPTR));
        label_232->setText(QApplication::translate("jptwatercut", "WC-C1", Q_NULLPTR));
        label_233->setText(QApplication::translate("jptwatercut", "WC-R1", Q_NULLPTR));
        label_234->setText(QApplication::translate("jptwatercut", "WC-C2", Q_NULLPTR));
        label_235->setText(QApplication::translate("jptwatercut", "WC-C3", Q_NULLPTR));
        label_236->setText(QApplication::translate("jptwatercut", "WC_R2", Q_NULLPTR));
        label_75->setText(QApplication::translate("jptwatercut", "Lam_cap", Q_NULLPTR));
        label_72->setText(QApplication::translate("jptwatercut", "Tur_res", Q_NULLPTR));
        label_74->setText(QApplication::translate("jptwatercut", "Lam_res", Q_NULLPTR));
        label_71->setText(QApplication::translate("jptwatercut", "Tur_cap", Q_NULLPTR));
        label_188->setText(QApplication::translate("jptwatercut", "Data To Send", Q_NULLPTR));
        functionescorrect->setTabText(functionescorrect->indexOf(tab_12), QApplication::translate("jptwatercut", "Water Cut", Q_NULLPTR));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_9), QApplication::translate("jptwatercut", "Measurament Parameters", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("jptwatercut", "Normalization ", Q_NULLPTR));
        label_29->setText(QApplication::translate("jptwatercut", "Temp.Transducer", Q_NULLPTR));
        CK_TP_F_HDx->setText(QApplication::translate("jptwatercut", "Fixed Temp. Value. ", Q_NULLPTR));
        label_45->setText(QApplication::translate("jptwatercut", "C", Q_NULLPTR));
        label_30->setText(QApplication::translate("jptwatercut", "Temperature Correction Selection.", Q_NULLPTR));
        CB_TP_SL->clear();
        CB_TP_SL->insertItems(0, QStringList()
         << QApplication::translate("jptwatercut", "NT-1000", Q_NULLPTR)
         << QApplication::translate("jptwatercut", "PT-1000", Q_NULLPTR)
        );
        label_14->setText(QApplication::translate("jptwatercut", "Flow                                 ", Q_NULLPTR));
        label->setText(QApplication::translate("jptwatercut", "Flow Correction Selection", Q_NULLPTR));
        label_35->setText(QApplication::translate("jptwatercut", "Glt/m", Q_NULLPTR));
        label_82->setText(QApplication::translate("jptwatercut", "Limit", Q_NULLPTR));
        label_85->setText(QApplication::translate("jptwatercut", "Flow Correction Selection", Q_NULLPTR));
        label_86->setText(QApplication::translate("jptwatercut", "%", Q_NULLPTR));
        label_15->setText(QApplication::translate("jptwatercut", "PPM Selection", Q_NULLPTR));
        CK_ST_F_HDx->setText(QApplication::translate("jptwatercut", "Fixed PPM. Value.  ", Q_NULLPTR));
        CK_ST_HD_1x->setText(QApplication::translate("jptwatercut", "PPM Sensor Res[1]", Q_NULLPTR));
        CK_ST_HD_6x->setText(QApplication::translate("jptwatercut", "PPM Sensor Res[6]", Q_NULLPTR));
        label_27->setText(QApplication::translate("jptwatercut", "C - Water Cut", Q_NULLPTR));
        label_25->setText(QApplication::translate("jptwatercut", "RC - Transducer Water Cut", Q_NULLPTR));
        label_23->setText(QApplication::translate("jptwatercut", "R - Water Cut", Q_NULLPTR));
        label_34->setText(QApplication::translate("jptwatercut", "Sali.", Q_NULLPTR));
        label_26->setText(QApplication::translate("jptwatercut", "Resistive                         ", Q_NULLPTR));
        label_28->setText(QApplication::translate("jptwatercut", "Capacitance", Q_NULLPTR));
        label_31->setText(QApplication::translate("jptwatercut", "Temp.", Q_NULLPTR));
        label_24->setText(QApplication::translate("jptwatercut", "Limits Correction Transducer.", Q_NULLPTR));
        label_17->setText(QApplication::translate("jptwatercut", "Pressure", Q_NULLPTR));
        label_18->setText(QApplication::translate("jptwatercut", "Salinity", Q_NULLPTR));
        label_37->setText(QApplication::translate("jptwatercut", "C", Q_NULLPTR));
        label_36->setText(QApplication::translate("jptwatercut", "PSI", Q_NULLPTR));
        label_42->setText(QApplication::translate("jptwatercut", "PPM", Q_NULLPTR));
        label_16->setText(QApplication::translate("jptwatercut", "Temperature", Q_NULLPTR));
        RB_TP_COx->setText(QApplication::translate("jptwatercut", "Temperature", Q_NULLPTR));
        label_19->setText(QApplication::translate("jptwatercut", "Corrections", Q_NULLPTR));
        RB_ST_COx->setText(QApplication::translate("jptwatercut", "Salinity", Q_NULLPTR));
        RB_FL_CO->setText(QApplication::translate("jptwatercut", "Flujo", Q_NULLPTR));
        SINF->setText(QApplication::translate("jptwatercut", "Save info", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("jptwatercut", "SWC", Q_NULLPTR));
        label_83->setText(QApplication::translate("jptwatercut", "Tur_med", Q_NULLPTR));
        label_84->setText(QApplication::translate("jptwatercut", "Lam_med", Q_NULLPTR));
        label_88->setText(QApplication::translate("jptwatercut", "Var_res", Q_NULLPTR));
        label_90->setText(QApplication::translate("jptwatercut", "Var_cap", Q_NULLPTR));
        label_80->setText(QApplication::translate("jptwatercut", "Data Raw + Normalize ", Q_NULLPTR));
        label_10->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        label_3->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        label_2->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_7->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        label_11->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        label_6->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_8->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_5->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_9->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_13->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_4->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_12->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        label_79->setText(QApplication::translate("jptwatercut", "Correccion por temperatura", Q_NULLPTR));
        label_47->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        label_48->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        label_49->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        label_51->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_52->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_53->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        label_57->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        label_55->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_58->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_61->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_46->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_50->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_78->setText(QApplication::translate("jptwatercut", "Correccion por salinidad", Q_NULLPTR));
        label_64->setText(QApplication::translate("jptwatercut", "SC2", Q_NULLPTR));
        label_65->setText(QApplication::translate("jptwatercut", "SC3", Q_NULLPTR));
        label_66->setText(QApplication::translate("jptwatercut", "SC1", Q_NULLPTR));
        label_67->setText(QApplication::translate("jptwatercut", "SR1", Q_NULLPTR));
        label_68->setText(QApplication::translate("jptwatercut", "SR2", Q_NULLPTR));
        label_69->setText(QApplication::translate("jptwatercut", "SR3", Q_NULLPTR));
        label_62->setText(QApplication::translate("jptwatercut", "SC6", Q_NULLPTR));
        label_60->setText(QApplication::translate("jptwatercut", "SC5", Q_NULLPTR));
        label_54->setText(QApplication::translate("jptwatercut", "SR4", Q_NULLPTR));
        label_56->setText(QApplication::translate("jptwatercut", "SR5", Q_NULLPTR));
        label_63->setText(QApplication::translate("jptwatercut", "SC4", Q_NULLPTR));
        label_59->setText(QApplication::translate("jptwatercut", "SR6", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("jptwatercut", "Values", Q_NULLPTR));
        label_21->setText(QApplication::translate("jptwatercut", "raw_sup", Q_NULLPTR));
        label_20->setText(QApplication::translate("jptwatercut", "min_cut", Q_NULLPTR));
        label_33->setText(QApplication::translate("jptwatercut", "raw_inf", Q_NULLPTR));
        label_32->setText(QApplication::translate("jptwatercut", "max_cut", Q_NULLPTR));
        BT_GE_GR->setText(QApplication::translate("jptwatercut", "Calcule equation", Q_NULLPTR));
        label_40->setText(QApplication::translate("jptwatercut", "Equ. correction:", Q_NULLPTR));
        RB_EQ_HC->setText(QApplication::translate("jptwatercut", "Aplicate", Q_NULLPTR));
        LE_HC->setText(QApplication::translate("jptwatercut", "(1 * w) + 0", Q_NULLPTR));
        label_22->setText(QApplication::translate("jptwatercut", "High Cut Res", Q_NULLPTR));
        label_39->setText(QApplication::translate("jptwatercut", "High Cut", Q_NULLPTR));
        label_38->setText(QApplication::translate("jptwatercut", "High Cut Cap", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("jptwatercut", "High cut", Q_NULLPTR));
        label_41->setText(QApplication::translate("jptwatercut", "2- Charge the Database to make the Data Refactor.", Q_NULLPTR));
        charge_data->setText(QApplication::translate("jptwatercut", "Charge Data Base ", Q_NULLPTR));
        label_43->setText(QApplication::translate("jptwatercut", "3- If the database loaded correctly, Press Refactor to continue.", Q_NULLPTR));
        refactor->setText(QApplication::translate("jptwatercut", "Refactor  Data", Q_NULLPTR));
        label_44->setText(QApplication::translate("jptwatercut", "System Messages", Q_NULLPTR));
        connect_txt->setText(QApplication::translate("jptwatercut", ">>", Q_NULLPTR));
        label_70->setText(QApplication::translate("jptwatercut", "1- Select the folder to save the refactor database.", Q_NULLPTR));
        select_folder->setText(QApplication::translate("jptwatercut", "Select Folder ", Q_NULLPTR));
        folder_txt->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        dest_db_label->setText(QApplication::translate("jptwatercut", "...", Q_NULLPTR));
        label_73->setText(QApplication::translate("jptwatercut", "Destination DB:", Q_NULLPTR));
        label_76->setText(QApplication::translate("jptwatercut", "Folder:", Q_NULLPTR));
        start_refactor->setText(QApplication::translate("jptwatercut", "Start Refactor", Q_NULLPTR));
        label_103->setText(QApplication::translate("jptwatercut", "4- Start or close the process. ", Q_NULLPTR));
        check_refactor->setText(QApplication::translate("jptwatercut", "ON/OFF", Q_NULLPTR));
        persistant->setText(QApplication::translate("jptwatercut", "Persistant", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("jptwatercut", "Data Refactor ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class jptwatercut: public Ui_jptwatercut {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JPTWATERCUT_H
