#JPT WATERCUT FOR ARMv7

El siguiente documento explica de forma simple como lograr que el Widget denominado JPT-Watercut funcione dentro de un elemento de nucleo ARM , en el caso particular en el cual fue provado el sistema el nucleo es ARMv7, **Beable Bone Black** y **Raspberry Pi 3 B+**
Antes de iniciar con el proceso se hacer funcionar el sistema para su desarrollo en un equipo de nuclero ARM , se recomienda correr el sitema dentro de un computador o laptop. 
Este archivo dice de forma muy general como correr el sistema y su funcionamiento, para obtener informacion mas completa sobre los procesos y funcionamiento del software se debe leer los comentarios dentro del codigo del sistema, los cuales explican las rutinas y elementos de cada funcion en el. 

En primera instancia es necesario tener en cuenta que el sistema presenta ciertas modificaciones respecto a la organizacion y arquitectura del sistema dise?ado para computador. 


Existe una ficha de despliegue la cual se encuentra dentro de la carpeta fichas del sistema y esta a su vez se encuentra dentro de la documentacion del software. 
La ficha en cuestion presenta los requerimientos basicos para la instalacion del sistema dentro de la Raspberry Pi 3.

Para el funcionamiento correcto del sistema sobre un nucleo ARM se debe tener: 

**1. QT Creator superior a 5.7.1:**
	Si ya se ha realizado el proceso de funcionamiento para laptop, se diferencia facilmente que para laptop se requeria 	una version de QT Creator superior a 5.9
	los requerimientos en la version depende de la instalacion o no del sistema para ModBus, dado que en la version de 	ARM se encuentra deshabilitada esta opcion, no es necesario que el sistema posea un QT creator superior a 5.7.1
	Para instalar el sistema de Qt Creator en su version 5.7.1 bastaria con realizar dos sencillos comandos en la consola: 
	**sudo apt-get install qt5-default** y **sudo apt-get install qt-creator**.
	**Nota:** La Beagle Bone posee caracteristicas suficientes para realizar procesos de compilacion pero por cuestios de procesamiento y tiempo de Debug, se ha dise?ado el sistema para compilarse en nucleo ARM de Raspberry y solamente implementarse en Bleagle Bone. 


**2. QT 5 Tools:**
	Las caracteristica de las diferentes tarjetas de nucleo ARM hacen que estan poseean una preinstalacion con algunas 	de las librerias de Qt Tools, por este motivo no es necesario instalar Qt Tools para ARM.

**3. QT Serial Port:**
	La libreria de serial port de Qt no se instala por defecto con los aplicativos, ni con el instalador base de QT CREATOR. esta libreria incluye el Qt Serial port 	V5 y el Qt Serial Bus 5.
	Nota: Para el caso de instalacion en ARM solamente nos interesa que se instale el Qt Serial Port, 


Luego de contar con las librerias de Qt Creator y sus aplicaivos es necesario tener en cuenta que el sistema Jpt Watercut depende de unos elementos externos del software especificos.  Los elementos adicionales son :

		
**1. La base de datos:**
	Para el correcto funcionamiento del sistema es necesario tener una base de datos de nombre **"jptwatercut"**,  la cual debe estar ubicada en  la  carpeta External del area de compilacion del sistema. Esta base de datos debe de encontrarse preconfigurada y debe de existir una copia de ella en una memoria SD insertada en el dispositivo esta memoria tendra de nombre **JPTSD**.
Para ARM el area de compilacion debe estar ubicada en una parpeta en el directorio raiz llamada **JPT** y dentro de ella la carpeta de **jptwatercut** y dentro de la misma la carpeta **External.**
Existe una carpeta de operaciones la cual tiene una copia de todos los elementos para Beagle Bone Black y que ayudara a entender plenamente la orgnizacion de los archvos. 

**NOTA:** Para fines de desarrollo es pereciso tener en cuenta que la direccion de almacenamiento de la base de datos se encuentra en el archivo **jptdatabase.cpp**. la cual por defecto busca la direccion External; Esta direccion se pude cambiar segun los requeriemientos de implementacion o del sistema. 


**2. Archivos de configuracion:**
	El sistema depende de seis  archivos de texto plano: El archivo de configuracin de la normalizacion, **delta_n.txt**; El archivo de la configuracion de la calibracion, **Calibration_values.text**; El archivo de configuracion de alto y bajo corte, **hlwc.txt** y tres archivos de configuracion general de temperatura y calibracion **sali.text**, **temp.text** y **tempval.txt**

**NOTA:**Para fines de desarrollo es preciso tener en cuenta que la direccin de almacenamiento de los archivos se puede cambiar dentro del proceso de implementacion de cada uno de ellos. Para realizar este proceso revisar el codigo del archibo jptwatercut.cpp


----------------------

Sabiendo lo anterior ahora sera posible compilar o correr de manera correcta el software JPTWAERCUT en un nucleo ARM.
Se ha dejado una copia del software dentro del repositorio de **BTBUCKET** , en la carpeta **JPT Watercut For ARMv7**

Los pasos para el despliegue como se dijo anteriormente se encuentran detallados  dentro de la ficha de despliegue, en la carpeta de docmentacion del software. 

Por otro lado se da una breve explicacion del proceso para hacer funcionar el software en Raspberry .Pi 3 B+
El software el cual ya se encuentra compilado tiene por nomenclatura **build-jptwatercut-Desktop_Pi-Debug**, esta nomenclatura corresponde a un directorio dentro del JPT Watercut for ARMv7.

Para lograr que el elemento funcione sin inconveniete se debe tener una Raspberry pi 3 B+ con un sistema operativo Rasbian Stretch, con fecha de despliegue superior a abril de 2018, el cual debe contar con el usuariopor defecto denominado **pi**, este usuario a su vez debe contar con un directrio llamado **Documentos*, de tal manera que dentro de este directorio se situa el actual software quedando asi una ruta de la siguiente manera: 

**/home/pi/Docuementos/build-jptwatercut-Desktop_Pi-Debug/**

se recuerda que dentro de la carpeta principal se debe encontrar la carpeta con los elementos externos, esta carpeta se denormina **External**.

Teniendo esto se puede correr el programa abriendo una consola en el directorio de **Debug** y escribiendo:

**sudo ./jptwatercut**

**NOTA:**Se recuerda que el software Jpt Watercut depende de un sistema denominado Jpt Monitoring el cual dbe estar corriendo con anterioridad dentro del equipo. Existe un proceso similar al realizado para lograr correr el sistema Jpt Watercut para el sistema Jpt Monitoring 


**NOTA:** Existe una carpeta 	llamada **build-jptwatercut-Desktop_Pi-Release** la cual contiene la version de Release del software, si se desea correr dicha version se debe de situar el directorio External dentro de la misma y correr el sistema. El sistema se corre con el mismo comando que para el **Debug**
